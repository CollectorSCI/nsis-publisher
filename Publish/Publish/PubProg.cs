﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Reflection;
using System.IO;
using System.Windows.Forms;

namespace NSISPublisher
{
    /// <summary>
    /// Publish
    /// </summary>
    
    class Publish
    {
        private static string pubEXEPath = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath.Replace("%20", " ");
        private static string pubPath = Path.GetDirectoryName(pubEXEPath).Replace("%20", " ");
        private static string pubName = Path.GetFileName(pubEXEPath);
        
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                MessageBox.Show("\"" + pubName + "\" is intended to be called from SCI Companion", "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int i = 0;
            foreach (string arg in args)
            {
                if (String.IsNullOrEmpty(arg))
                {
                    MessageBox.Show("No game has been opened. Please open a game in SCI Companion and try again", "No Game Opened",
                        MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }

                i = i + 1;
                if (i == 1)
                {
                    Process.Start(pubPath + "\\NSISPublisher.exe", "\"" + arg + "\"" + "*");
                }
            }
        }
    }
}