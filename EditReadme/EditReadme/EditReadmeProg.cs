﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;

namespace NSISPublisher
{
    /// <summary>
    /// Edit Readme
    /// </summary>
    
    class EditReadmeProg
    {
        private static string editEXE = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath.Replace("%20", " ");
        private static string editEXEName = Path.GetFileName(editEXE);
        private static string nsisPubPath = Path.GetDirectoryName(editEXE);
        private static string templateReadme = nsisPubPath + @"\Include\README.TXT";

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                MessageBox.Show("\"" + editEXEName + "\" is intended to be called from SCI Companion", "Error",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            int i = 0;
            foreach (string arg in args)
            {
                if (String.IsNullOrEmpty(arg))
                {
                    MessageBox.Show("No game has been opened. Please open a game in SCI Companion and try again", "No Game Opened",
                        MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    return;
                }

                string projPath = arg;
                i = i + 1;
                if (i == 1)
                {
                    if (Directory.Exists(projPath))
                    {
                        // Check Publish_Include folder
                        string includePath = projPath + @"\Publish_Include";
                        if (!Directory.Exists(includePath))
                        {
                            try
                            {
                                Directory.CreateDirectory(includePath);
                            }
                            catch { }
                        }

                        // Check Readme
                        string projReadme = includePath + "\\README.TXT";
                        if (!File.Exists(projReadme))
                        {
                            // If missing, check project base folder
                            if (File.Exists(projPath + @"\README.TXT"))
                            {
                                try
                                {
                                    File.Move(projPath + @"\README.TXT", projReadme);
                                }
                                catch { }
                            }
                            else
                            {
                                // Check Template Readme
                                if (File.Exists(templateReadme))
                                {
                                    try
                                    {
                                        File.Copy(templateReadme, projReadme);
                                    }
                                    catch { }
                                }
                                else
                                {
                                    // If Template Readme missing, write new one
                                    try
                                    {
                                        string readmeContent = "README Place Holder - Type a description of your game here.";
                                        StreamWriter writer = new StreamWriter(projReadme, false, Encoding.GetEncoding("UTF-8")); //Unicode"))
                                        writer.Write(readmeContent);
                                        writer.Dispose();
                                        writer.Close();
                                    }
                                    catch { }
                                }
                            }
                        }

                        // Open Readme
                        if (File.Exists(projReadme))
                        {
                            try
                            {
                                Process.Start(projReadme);
                            }
                            catch { }
                        }
                    }
                }
            }
        }
    }
}
