NSIS Publisher
Version 1.0
By Andrew Branscom
http://sierrahelp.com/


NSIS Publisher is tool to be used as a plugin for Phil Fortier's SCI Companion v3 (http://scicompanion.com/) for generating NSIS install scripts for fan made SCI0 SCI1.0 and SCI1.1 games. It can automatically generate an installer for your SCI Companion created game. The generated script can be tweaked/customized and recompiled manually in the included script editor. The resulting script can be found in the game's base folder for later corrections or revisions. You can reopen a script through the open script dialog of the tool or by double clicking the NSI file. Game installers created by NSIS Publisher will include DOSBox and the games will be automatically configured to run in it.

You can add items to your installer by copying them to the "Publish_Include" folder inside your game's folder. It will preserve any directory structure you setup in that folder. It will automatically include a game launcher, config tool and game icon. The standard icon in that folder can be replaced with your own icon.


To setup NSIS Publisher for use in SCI Companion just unpack it into a subdirectory under Companion's Plugins folder. Three new items will appear in the Plugins menu of SCI Companion; Edit Readme, Publish and NSIS Publisher. First time running it will open the options dialog to choose defaults. Thereafter it will generate an NSIS script for your current project and compile it it with an included DOSBox by selecting "Publish" in SCI Companion's Plugins menu.

See the help file for more information.

