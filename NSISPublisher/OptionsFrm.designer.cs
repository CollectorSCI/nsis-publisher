﻿namespace NSISPublisher
{
    partial class OptionsFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionsFrm));
            this.toolTips = new System.Windows.Forms.ToolTip(this.components);
            this.browseDefaultProjPathBtn = new System.Windows.Forms.Button();
            this.browsePubOutPathBtn = new System.Windows.Forms.Button();
            this.runNSISChkBox = new System.Windows.Forms.CheckBox();
            this.deleteNSISSrptChkBox = new System.Windows.Forms.CheckBox();
            this.loggingChkBox = new System.Windows.Forms.CheckBox();
            this.openInstFolderChkBox = new System.Windows.Forms.CheckBox();
            this.seInlineSpellCheckerChkBox = new System.Windows.Forms.CheckBox();
            this.getNSISLnk = new System.Windows.Forms.LinkLabel();
            this.pubURLTxtBox = new System.Windows.Forms.TextBox();
            this.defaultPubNameTxtBox = new System.Windows.Forms.TextBox();
            this.useLangFileChkBox = new System.Windows.Forms.CheckBox();
            this.getLanguageFileLnk = new System.Windows.Forms.LinkLabel();
            this.aspectChkBox = new System.Windows.Forms.CheckBox();
            this.getLogNSISLnk = new System.Windows.Forms.LinkLabel();
            this.unicodeChkBox = new System.Windows.Forms.CheckBox();
            this.pubOutChkBox = new System.Windows.Forms.CheckBox();
            this.pubOutPathTxtBox = new System.Windows.Forms.TextBox();
            this.chooseLicenseHelpLnk = new System.Windows.Forms.LinkLabel();
            this.licenseCmboBox = new System.Windows.Forms.ComboBox();
            this.saveScriptChkBox = new System.Windows.Forms.CheckBox();
            this.noIncludeWarning = new System.Windows.Forms.CheckBox();
            this.missngSGReadmeChkBox = new System.Windows.Forms.CheckBox();
            this.regDevBtn = new System.Windows.Forms.Button();
            this.okBtn = new System.Windows.Forms.Button();
            this.applyBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.defaultProjFolderGrpBox = new System.Windows.Forms.GroupBox();
            this.defaultProjFoldInfoTxtBox = new System.Windows.Forms.TextBox();
            this.defaultProjPathTxtBox = new System.Windows.Forms.TextBox();
            this.optionsInfoTextBox = new System.Windows.Forms.TextBox();
            this.projURLGrpBox = new System.Windows.Forms.GroupBox();
            this.projURLInfoTxtBox = new System.Windows.Forms.TextBox();
            this.projPubGrpBox = new System.Windows.Forms.GroupBox();
            this.pubInfoTxtBox = new System.Windows.Forms.TextBox();
            this.dosboxOptionsGrpBox = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.nsisOptionsGrpBox = new System.Windows.Forms.GroupBox();
            this.pubOutGrpBox = new System.Windows.Forms.GroupBox();
            this.pubOutFoldInfoTxtBox = new System.Windows.Forms.TextBox();
            this.editOptionsGrpBox = new System.Windows.Forms.GroupBox();
            this.includeSRCChkBox = new System.Windows.Forms.CheckBox();
            this.editReadmeBtn = new System.Windows.Forms.Button();
            this.editLicenseBtn = new System.Windows.Forms.Button();
            this.licenceLbl = new System.Windows.Forms.Label();
            this.helpProvider = new System.Windows.Forms.HelpProvider();
            this.headerPnl = new System.Windows.Forms.Panel();
            this.nsisDocsLinkLbl = new System.Windows.Forms.LinkLabel();
            this.helpLinkLbl = new System.Windows.Forms.LinkLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabCtrl = new System.Windows.Forms.TabControl();
            this.genTabPg = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.scrOverwritePnl = new System.Windows.Forms.Panel();
            this.scrOverwriteLbl = new System.Windows.Forms.Label();
            this.askRadioBtn = new System.Windows.Forms.RadioButton();
            this.noOverwriteRadioBtn = new System.Windows.Forms.RadioButton();
            this.overWriteRadioBtn = new System.Windows.Forms.RadioButton();
            this.allProjTabPg = new System.Windows.Forms.TabPage();
            this.pathsTabPg = new System.Windows.Forms.TabPage();
            this.editTabPg = new System.Windows.Forms.TabPage();
            this.nsisTabPg = new System.Windows.Forms.TabPage();
            this.dbTabPg = new System.Windows.Forms.TabPage();
            this.footerPnl = new System.Windows.Forms.Panel();
            this.defaultProjFolderGrpBox.SuspendLayout();
            this.projURLGrpBox.SuspendLayout();
            this.projPubGrpBox.SuspendLayout();
            this.dosboxOptionsGrpBox.SuspendLayout();
            this.nsisOptionsGrpBox.SuspendLayout();
            this.pubOutGrpBox.SuspendLayout();
            this.editOptionsGrpBox.SuspendLayout();
            this.headerPnl.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabCtrl.SuspendLayout();
            this.genTabPg.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.scrOverwritePnl.SuspendLayout();
            this.allProjTabPg.SuspendLayout();
            this.pathsTabPg.SuspendLayout();
            this.editTabPg.SuspendLayout();
            this.nsisTabPg.SuspendLayout();
            this.dbTabPg.SuspendLayout();
            this.footerPnl.SuspendLayout();
            this.SuspendLayout();
            // 
            // browseDefaultProjPathBtn
            // 
            this.browseDefaultProjPathBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browseDefaultProjPathBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browseDefaultProjPathBtn.Location = new System.Drawing.Point(367, 78);
            this.browseDefaultProjPathBtn.Name = "browseDefaultProjPathBtn";
            this.browseDefaultProjPathBtn.Size = new System.Drawing.Size(27, 23);
            this.browseDefaultProjPathBtn.TabIndex = 1;
            this.browseDefaultProjPathBtn.Text = "...";
            this.toolTips.SetToolTip(this.browseDefaultProjPathBtn, "Browse to selected folder for your Projects\' DOSBox Captures folder");
            this.browseDefaultProjPathBtn.UseVisualStyleBackColor = true;
            this.browseDefaultProjPathBtn.Click += new System.EventHandler(this.browseDefaultProjPathBtn_Click);
            // 
            // browsePubOutPathBtn
            // 
            this.browsePubOutPathBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.browsePubOutPathBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.browsePubOutPathBtn.Location = new System.Drawing.Point(367, 82);
            this.browsePubOutPathBtn.Name = "browsePubOutPathBtn";
            this.browsePubOutPathBtn.Size = new System.Drawing.Size(27, 23);
            this.browsePubOutPathBtn.TabIndex = 1001;
            this.browsePubOutPathBtn.Text = "...";
            this.toolTips.SetToolTip(this.browsePubOutPathBtn, "Browse to select target folder for compiled installers");
            this.browsePubOutPathBtn.UseVisualStyleBackColor = true;
            this.browsePubOutPathBtn.Click += new System.EventHandler(this.browsePubOutPathBtn_Click);
            // 
            // runNSISChkBox
            // 
            this.runNSISChkBox.AutoSize = true;
            this.runNSISChkBox.Checked = true;
            this.runNSISChkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.runNSISChkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.runNSISChkBox.Location = new System.Drawing.Point(6, 42);
            this.runNSISChkBox.Name = "runNSISChkBox";
            this.runNSISChkBox.Size = new System.Drawing.Size(128, 17);
            this.runNSISChkBox.TabIndex = 0;
            this.runNSISChkBox.Text = "Run NSIS On Publish";
            this.toolTips.SetToolTip(this.runNSISChkBox, resources.GetString("runNSISChkBox.ToolTip"));
            this.runNSISChkBox.CheckedChanged += new System.EventHandler(this.RunNSISCheckBox_CheckedChanged);
            this.runNSISChkBox.Click += new System.EventHandler(this.RunNSISCheckBox_Click);
            // 
            // deleteNSISSrptChkBox
            // 
            this.deleteNSISSrptChkBox.AutoSize = true;
            this.deleteNSISSrptChkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.deleteNSISSrptChkBox.Location = new System.Drawing.Point(6, 19);
            this.deleteNSISSrptChkBox.Name = "deleteNSISSrptChkBox";
            this.deleteNSISSrptChkBox.Size = new System.Drawing.Size(177, 17);
            this.deleteNSISSrptChkBox.TabIndex = 1;
            this.deleteNSISSrptChkBox.Text = "Delete NSIS Script After Publish";
            this.toolTips.SetToolTip(this.deleteNSISSrptChkBox, resources.GetString("deleteNSISSrptChkBox.ToolTip"));
            this.deleteNSISSrptChkBox.CheckedChanged += new System.EventHandler(this.DeleteNSISSrptCheckBox_CheckedChanged);
            this.deleteNSISSrptChkBox.Click += new System.EventHandler(this.DeleteNSISSrptCheckBox_Click);
            // 
            // loggingChkBox
            // 
            this.loggingChkBox.AutoSize = true;
            this.loggingChkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loggingChkBox.Location = new System.Drawing.Point(6, 88);
            this.loggingChkBox.Name = "loggingChkBox";
            this.loggingChkBox.Size = new System.Drawing.Size(142, 17);
            this.loggingChkBox.TabIndex = 1047;
            this.loggingChkBox.Text = "Create Logging Installers";
            this.toolTips.SetToolTip(this.loggingChkBox, "Create Logging Installer (requires logging build o NSIS");
            this.loggingChkBox.CheckedChanged += new System.EventHandler(this.loggingChkBox_CheckedChanged);
            // 
            // openInstFolderChkBox
            // 
            this.openInstFolderChkBox.AutoSize = true;
            this.openInstFolderChkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.openInstFolderChkBox.Location = new System.Drawing.Point(6, 88);
            this.openInstFolderChkBox.Name = "openInstFolderChkBox";
            this.openInstFolderChkBox.Size = new System.Drawing.Size(157, 17);
            this.openInstFolderChkBox.TabIndex = 1048;
            this.openInstFolderChkBox.Text = "Open Folder After Compiling";
            this.toolTips.SetToolTip(this.openInstFolderChkBox, "Opens folder containg compiled installer after build");
            // 
            // seInlineSpellCheckerChkBox
            // 
            this.seInlineSpellCheckerChkBox.AutoSize = true;
            this.seInlineSpellCheckerChkBox.Enabled = false;
            this.seInlineSpellCheckerChkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.seInlineSpellCheckerChkBox.Location = new System.Drawing.Point(6, 19);
            this.seInlineSpellCheckerChkBox.Name = "seInlineSpellCheckerChkBox";
            this.seInlineSpellCheckerChkBox.Size = new System.Drawing.Size(142, 17);
            this.seInlineSpellCheckerChkBox.TabIndex = 1027;
            this.seInlineSpellCheckerChkBox.Text = "Use Inline Spell Checker";
            this.toolTips.SetToolTip(this.seInlineSpellCheckerChkBox, "Use Inline Spell Checker");
            this.seInlineSpellCheckerChkBox.CheckedChanged += new System.EventHandler(this.seInlineSpellCheckerChkBox_CheckedChanged);
            // 
            // getNSISLnk
            // 
            this.getNSISLnk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.getNSISLnk.AutoSize = true;
            this.getNSISLnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getNSISLnk.Location = new System.Drawing.Point(311, 238);
            this.getNSISLnk.Name = "getNSISLnk";
            this.getNSISLnk.Size = new System.Drawing.Size(83, 13);
            this.getNSISLnk.TabIndex = 2;
            this.getNSISLnk.TabStop = true;
            this.getNSISLnk.Text = "Download NSIS";
            this.toolTips.SetToolTip(this.getNSISLnk, "Go to NSIS Download Page");
            this.getNSISLnk.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.getNSISLink_LnkClicked);
            // 
            // pubURLTxtBox
            // 
            this.pubURLTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pubURLTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.pubURLTxtBox.Location = new System.Drawing.Point(6, 80);
            this.pubURLTxtBox.Name = "pubURLTxtBox";
            this.pubURLTxtBox.Size = new System.Drawing.Size(388, 20);
            this.pubURLTxtBox.TabIndex = 6;
            this.toolTips.SetToolTip(this.pubURLTxtBox, "Sets the default support web site for your games");
            this.pubURLTxtBox.TextChanged += new System.EventHandler(this.pubURLTxtBox_TextChanged);
            this.pubURLTxtBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.projURLTxtBox_KeyDown);
            this.pubURLTxtBox.Leave += new System.EventHandler(this.projURLTxtBox_Leave);
            // 
            // defaultPubNameTxtBox
            // 
            this.defaultPubNameTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.defaultPubNameTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defaultPubNameTxtBox.Location = new System.Drawing.Point(6, 80);
            this.defaultPubNameTxtBox.Name = "defaultPubNameTxtBox";
            this.defaultPubNameTxtBox.Size = new System.Drawing.Size(388, 20);
            this.defaultPubNameTxtBox.TabIndex = 1002;
            this.toolTips.SetToolTip(this.defaultPubNameTxtBox, "Sets default developer or publisher name");
            this.defaultPubNameTxtBox.TextChanged += new System.EventHandler(this.defaultPubNameTxtBox_TextChanged);
            // 
            // useLangFileChkBox
            // 
            this.useLangFileChkBox.AutoSize = true;
            this.useLangFileChkBox.Enabled = false;
            this.useLangFileChkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.useLangFileChkBox.Location = new System.Drawing.Point(6, 65);
            this.useLangFileChkBox.Name = "useLangFileChkBox";
            this.useLangFileChkBox.Size = new System.Drawing.Size(115, 17);
            this.useLangFileChkBox.TabIndex = 1046;
            this.useLangFileChkBox.Text = "Use Language File";
            this.toolTips.SetToolTip(this.useLangFileChkBox, "Set installers to include DOSBox language file");
            // 
            // getLanguageFileLnk
            // 
            this.getLanguageFileLnk.AutoSize = true;
            this.getLanguageFileLnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getLanguageFileLnk.Location = new System.Drawing.Point(264, 238);
            this.getLanguageFileLnk.Name = "getLanguageFileLnk";
            this.getLanguageFileLnk.Size = new System.Drawing.Size(130, 13);
            this.getLanguageFileLnk.TabIndex = 1045;
            this.getLanguageFileLnk.TabStop = true;
            this.getLanguageFileLnk.Text = "Download Language Files";
            this.toolTips.SetToolTip(this.getLanguageFileLnk, "Go to DOSBox Downloads Page");
            this.getLanguageFileLnk.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.getLanguageFileLnk_LinkClicked);
            // 
            // aspectChkBox
            // 
            this.aspectChkBox.AutoSize = true;
            this.aspectChkBox.Checked = true;
            this.aspectChkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.aspectChkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aspectChkBox.Location = new System.Drawing.Point(6, 42);
            this.aspectChkBox.Name = "aspectChkBox";
            this.aspectChkBox.Size = new System.Drawing.Size(110, 17);
            this.aspectChkBox.TabIndex = 1043;
            this.aspectChkBox.Text = "Aspect Correction";
            this.toolTips.SetToolTip(this.aspectChkBox, "Set ratio aspect correction default");
            this.aspectChkBox.CheckedChanged += new System.EventHandler(this.aspectChkBox_CheckedChanged);
            // 
            // getLogNSISLnk
            // 
            this.getLogNSISLnk.AutoSize = true;
            this.getLogNSISLnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.getLogNSISLnk.Location = new System.Drawing.Point(2, 238);
            this.getLogNSISLnk.Name = "getLogNSISLnk";
            this.getLogNSISLnk.Size = new System.Drawing.Size(162, 13);
            this.getLogNSISLnk.TabIndex = 1040;
            this.getLogNSISLnk.TabStop = true;
            this.getLogNSISLnk.Text = "Download Logging Build of NSIS";
            this.toolTips.SetToolTip(this.getLogNSISLnk, "Go to NSIS Special Build Download Page");
            this.getLogNSISLnk.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.getLogNSISLnk_LinkClicked);
            // 
            // unicodeChkBox
            // 
            this.unicodeChkBox.AutoSize = true;
            this.unicodeChkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unicodeChkBox.Location = new System.Drawing.Point(6, 65);
            this.unicodeChkBox.Name = "unicodeChkBox";
            this.unicodeChkBox.Size = new System.Drawing.Size(144, 17);
            this.unicodeChkBox.TabIndex = 1046;
            this.unicodeChkBox.Text = "Create Unicode Installers";
            this.toolTips.SetToolTip(this.unicodeChkBox, " Create Unicode Installer (requires NSIS 3.x)");
            this.unicodeChkBox.CheckedChanged += new System.EventHandler(this.unicodeChkBox_CheckedChanged);
            // 
            // pubOutChkBox
            // 
            this.pubOutChkBox.AutoSize = true;
            this.pubOutChkBox.Checked = true;
            this.pubOutChkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.pubOutChkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pubOutChkBox.Location = new System.Drawing.Point(6, 53);
            this.pubOutChkBox.Name = "pubOutChkBox";
            this.pubOutChkBox.Size = new System.Drawing.Size(108, 17);
            this.pubOutChkBox.TabIndex = 1003;
            this.pubOutChkBox.Text = "Use Game Folder";
            this.toolTips.SetToolTip(this.pubOutChkBox, "Select current project\'s target folder for compiled installer");
            this.pubOutChkBox.UseVisualStyleBackColor = true;
            this.pubOutChkBox.CheckedChanged += new System.EventHandler(this.pubOutChkBox_CheckedChanged);
            // 
            // pubOutPathTxtBox
            // 
            this.pubOutPathTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pubOutPathTxtBox.Enabled = false;
            this.pubOutPathTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pubOutPathTxtBox.Location = new System.Drawing.Point(6, 80);
            this.pubOutPathTxtBox.Name = "pubOutPathTxtBox";
            this.pubOutPathTxtBox.Size = new System.Drawing.Size(355, 20);
            this.pubOutPathTxtBox.TabIndex = 1002;
            this.toolTips.SetToolTip(this.pubOutPathTxtBox, "Specify target folder for compiled installers");
            this.pubOutPathTxtBox.TextChanged += new System.EventHandler(this.pubOutPathTextBox_TextChanged);
            // 
            // chooseLicenseHelpLnk
            // 
            this.chooseLicenseHelpLnk.AutoSize = true;
            this.chooseLicenseHelpLnk.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chooseLicenseHelpLnk.Location = new System.Drawing.Point(3, 56);
            this.chooseLicenseHelpLnk.Name = "chooseLicenseHelpLnk";
            this.chooseLicenseHelpLnk.Size = new System.Drawing.Size(116, 13);
            this.chooseLicenseHelpLnk.TabIndex = 1053;
            this.chooseLicenseHelpLnk.TabStop = true;
            this.chooseLicenseHelpLnk.Text = "Choosing License Help";
            this.toolTips.SetToolTip(this.chooseLicenseHelpLnk, "Information about the different licenses");
            this.chooseLicenseHelpLnk.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.chooseLicenseHelpLnk_LinkClicked);
            // 
            // licenseCmboBox
            // 
            this.licenseCmboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.licenseCmboBox.FormattingEnabled = true;
            this.licenseCmboBox.Items.AddRange(new object[] {
            "BSD 2-clause License",
            "BSD 3-clause License",
            "GNU GPL-2 License",
            "GNU GPL-2.1 License",
            "GNU GPL-3 License",
            "GNU Lesser GPL-3 License",
            "ISC License",
            "LGPL License",
            "MIT License",
            "WTFPL License",
            "Unlicense",
            "No License",
            "Custom"});
            this.licenseCmboBox.Location = new System.Drawing.Point(6, 32);
            this.licenseCmboBox.Name = "licenseCmboBox";
            this.licenseCmboBox.Size = new System.Drawing.Size(139, 21);
            this.licenseCmboBox.TabIndex = 1049;
            this.licenseCmboBox.Text = "GNU GPL-2 License";
            this.toolTips.SetToolTip(this.licenseCmboBox, "Select license to use");
            this.licenseCmboBox.SelectedIndexChanged += new System.EventHandler(this.licenseCmboBox_SelectedIndexChanged);
            // 
            // saveScriptChkBox
            // 
            this.saveScriptChkBox.AutoSize = true;
            this.saveScriptChkBox.Checked = true;
            this.saveScriptChkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.saveScriptChkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.saveScriptChkBox.Location = new System.Drawing.Point(6, 42);
            this.saveScriptChkBox.Name = "saveScriptChkBox";
            this.saveScriptChkBox.Size = new System.Drawing.Size(163, 17);
            this.saveScriptChkBox.TabIndex = 1047;
            this.saveScriptChkBox.Text = "Save Script Before Compiling";
            this.toolTips.SetToolTip(this.saveScriptChkBox, "Leaves NSI script in project\'s folder for later editing.");
            // 
            // noIncludeWarning
            // 
            this.noIncludeWarning.AutoSize = true;
            this.noIncludeWarning.Checked = true;
            this.noIncludeWarning.CheckState = System.Windows.Forms.CheckState.Checked;
            this.noIncludeWarning.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noIncludeWarning.Location = new System.Drawing.Point(6, 95);
            this.noIncludeWarning.Name = "noIncludeWarning";
            this.noIncludeWarning.Size = new System.Drawing.Size(171, 17);
            this.noIncludeWarning.TabIndex = 1044;
            this.noIncludeWarning.Text = "Missing Include folder Warning";
            this.toolTips.SetToolTip(this.noIncludeWarning, "Uncheck to disable this warning");
            // 
            // missngSGReadmeChkBox
            // 
            this.missngSGReadmeChkBox.AutoSize = true;
            this.missngSGReadmeChkBox.Checked = true;
            this.missngSGReadmeChkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.missngSGReadmeChkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.missngSGReadmeChkBox.Location = new System.Drawing.Point(6, 72);
            this.missngSGReadmeChkBox.Name = "missngSGReadmeChkBox";
            this.missngSGReadmeChkBox.Size = new System.Drawing.Size(206, 17);
            this.missngSGReadmeChkBox.TabIndex = 1041;
            this.missngSGReadmeChkBox.Text = "Missing Save Game Readme Warning";
            this.toolTips.SetToolTip(this.missngSGReadmeChkBox, "Uncheck to disable this warning");
            // 
            // regDevBtn
            // 
            this.regDevBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.regDevBtn.Location = new System.Drawing.Point(6, 124);
            this.regDevBtn.Name = "regDevBtn";
            this.regDevBtn.Size = new System.Drawing.Size(295, 23);
            this.regDevBtn.TabIndex = 1041;
            this.regDevBtn.Text = "Register NSIS Publisher as default Editor for  NSIS Scripts";
            this.toolTips.SetToolTip(this.regDevBtn, "Registers NSIS Publisher as default Editor for  NSIS Scripts");
            this.regDevBtn.UseVisualStyleBackColor = true;
            this.regDevBtn.Click += new System.EventHandler(this.regDevBtn_Click);
            // 
            // okBtn
            // 
            this.okBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okBtn.Location = new System.Drawing.Point(171, 7);
            this.okBtn.Name = "okBtn";
            this.okBtn.Size = new System.Drawing.Size(75, 23);
            this.okBtn.TabIndex = 0;
            this.okBtn.Text = "OK";
            this.okBtn.UseVisualStyleBackColor = true;
            this.okBtn.Click += new System.EventHandler(this.BtnOptionsOK_Click);
            // 
            // applyBtn
            // 
            this.applyBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.applyBtn.Location = new System.Drawing.Point(333, 7);
            this.applyBtn.Name = "applyBtn";
            this.applyBtn.Size = new System.Drawing.Size(75, 23);
            this.applyBtn.TabIndex = 2;
            this.applyBtn.Text = "Apply";
            this.applyBtn.UseVisualStyleBackColor = true;
            this.applyBtn.Click += new System.EventHandler(this.BtnOptionsApply_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelBtn.Location = new System.Drawing.Point(252, 7);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 1;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.BtnOptionsCancel_Click);
            // 
            // defaultProjFolderGrpBox
            // 
            this.defaultProjFolderGrpBox.Controls.Add(this.defaultProjFoldInfoTxtBox);
            this.defaultProjFolderGrpBox.Controls.Add(this.browseDefaultProjPathBtn);
            this.defaultProjFolderGrpBox.Controls.Add(this.defaultProjPathTxtBox);
            this.defaultProjFolderGrpBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defaultProjFolderGrpBox.Location = new System.Drawing.Point(6, 15);
            this.defaultProjFolderGrpBox.Name = "defaultProjFolderGrpBox";
            this.defaultProjFolderGrpBox.Size = new System.Drawing.Size(400, 124);
            this.defaultProjFolderGrpBox.TabIndex = 1029;
            this.defaultProjFolderGrpBox.TabStop = false;
            this.defaultProjFolderGrpBox.Text = "Default Projects Folder";
            // 
            // defaultProjFoldInfoTxtBox
            // 
            this.defaultProjFoldInfoTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.defaultProjFoldInfoTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.defaultProjFoldInfoTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defaultProjFoldInfoTxtBox.HideSelection = false;
            this.defaultProjFoldInfoTxtBox.Location = new System.Drawing.Point(6, 19);
            this.defaultProjFoldInfoTxtBox.Multiline = true;
            this.defaultProjFoldInfoTxtBox.Name = "defaultProjFoldInfoTxtBox";
            this.defaultProjFoldInfoTxtBox.ReadOnly = true;
            this.defaultProjFoldInfoTxtBox.ShortcutsEnabled = false;
            this.defaultProjFoldInfoTxtBox.Size = new System.Drawing.Size(378, 55);
            this.defaultProjFoldInfoTxtBox.TabIndex = 1000;
            this.defaultProjFoldInfoTxtBox.TabStop = false;
            this.defaultProjFoldInfoTxtBox.Text = "Set the default location of your projects working folder. Current default:\r\n\r\n   " +
    " \"%MyDocuments%\"";
            // 
            // defaultProjPathTxtBox
            // 
            this.defaultProjPathTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.defaultProjPathTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.defaultProjPathTxtBox.Location = new System.Drawing.Point(6, 80);
            this.defaultProjPathTxtBox.Name = "defaultProjPathTxtBox";
            this.defaultProjPathTxtBox.Size = new System.Drawing.Size(355, 20);
            this.defaultProjPathTxtBox.TabIndex = 0;
            this.defaultProjPathTxtBox.TextChanged += new System.EventHandler(this.defaultProjPathTxtBox_TextChanged);
            // 
            // optionsInfoTextBox
            // 
            this.optionsInfoTextBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.optionsInfoTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optionsInfoTextBox.Location = new System.Drawing.Point(15, 15);
            this.optionsInfoTextBox.Multiline = true;
            this.optionsInfoTextBox.Name = "optionsInfoTextBox";
            this.optionsInfoTextBox.ReadOnly = true;
            this.optionsInfoTextBox.Size = new System.Drawing.Size(390, 57);
            this.optionsInfoTextBox.TabIndex = 1039;
            this.optionsInfoTextBox.Text = resources.GetString("optionsInfoTextBox.Text");
            // 
            // projURLGrpBox
            // 
            this.projURLGrpBox.Controls.Add(this.projURLInfoTxtBox);
            this.projURLGrpBox.Controls.Add(this.pubURLTxtBox);
            this.projURLGrpBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.projURLGrpBox.Location = new System.Drawing.Point(6, 145);
            this.projURLGrpBox.Name = "projURLGrpBox";
            this.projURLGrpBox.Size = new System.Drawing.Size(400, 124);
            this.projURLGrpBox.TabIndex = 1037;
            this.projURLGrpBox.TabStop = false;
            this.projURLGrpBox.Text = "Default Developer / Publisher Website";
            // 
            // projURLInfoTxtBox
            // 
            this.projURLInfoTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projURLInfoTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.projURLInfoTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.projURLInfoTxtBox.Location = new System.Drawing.Point(9, 18);
            this.projURLInfoTxtBox.Multiline = true;
            this.projURLInfoTxtBox.Name = "projURLInfoTxtBox";
            this.projURLInfoTxtBox.ReadOnly = true;
            this.projURLInfoTxtBox.Size = new System.Drawing.Size(383, 55);
            this.projURLInfoTxtBox.TabIndex = 7;
            this.projURLInfoTxtBox.Text = "Set address to default Devloper\'s or Publisher\'s website. Default:\r\n\r\n   http://s" +
    "ciprogramming.com/";
            // 
            // projPubGrpBox
            // 
            this.projPubGrpBox.Controls.Add(this.pubInfoTxtBox);
            this.projPubGrpBox.Controls.Add(this.defaultPubNameTxtBox);
            this.projPubGrpBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.projPubGrpBox.Location = new System.Drawing.Point(6, 15);
            this.projPubGrpBox.Name = "projPubGrpBox";
            this.projPubGrpBox.Size = new System.Drawing.Size(400, 124);
            this.projPubGrpBox.TabIndex = 1032;
            this.projPubGrpBox.TabStop = false;
            this.projPubGrpBox.Text = "Default Developer / Publisher Name:";
            // 
            // pubInfoTxtBox
            // 
            this.pubInfoTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pubInfoTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.pubInfoTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pubInfoTxtBox.HideSelection = false;
            this.pubInfoTxtBox.Location = new System.Drawing.Point(6, 19);
            this.pubInfoTxtBox.Multiline = true;
            this.pubInfoTxtBox.Name = "pubInfoTxtBox";
            this.pubInfoTxtBox.ReadOnly = true;
            this.pubInfoTxtBox.ShortcutsEnabled = false;
            this.pubInfoTxtBox.Size = new System.Drawing.Size(386, 44);
            this.pubInfoTxtBox.TabIndex = 1001;
            this.pubInfoTxtBox.TabStop = false;
            this.pubInfoTxtBox.Text = resources.GetString("pubInfoTxtBox.Text");
            // 
            // dosboxOptionsGrpBox
            // 
            this.dosboxOptionsGrpBox.Controls.Add(this.useLangFileChkBox);
            this.dosboxOptionsGrpBox.Controls.Add(this.getLanguageFileLnk);
            this.dosboxOptionsGrpBox.Controls.Add(this.textBox1);
            this.dosboxOptionsGrpBox.Controls.Add(this.aspectChkBox);
            this.dosboxOptionsGrpBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dosboxOptionsGrpBox.Location = new System.Drawing.Point(6, 15);
            this.dosboxOptionsGrpBox.Name = "dosboxOptionsGrpBox";
            this.dosboxOptionsGrpBox.Size = new System.Drawing.Size(400, 254);
            this.dosboxOptionsGrpBox.TabIndex = 1031;
            this.dosboxOptionsGrpBox.TabStop = false;
            this.dosboxOptionsGrpBox.Text = "DOSBox Options:";
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(6, 19);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(388, 13);
            this.textBox1.TabIndex = 1044;
            this.textBox1.Text = "Selects config tool to be used";
            // 
            // nsisOptionsGrpBox
            // 
            this.nsisOptionsGrpBox.Controls.Add(this.getNSISLnk);
            this.nsisOptionsGrpBox.Controls.Add(this.loggingChkBox);
            this.nsisOptionsGrpBox.Controls.Add(this.deleteNSISSrptChkBox);
            this.nsisOptionsGrpBox.Controls.Add(this.getLogNSISLnk);
            this.nsisOptionsGrpBox.Controls.Add(this.unicodeChkBox);
            this.nsisOptionsGrpBox.Controls.Add(this.runNSISChkBox);
            this.nsisOptionsGrpBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nsisOptionsGrpBox.Location = new System.Drawing.Point(6, 15);
            this.nsisOptionsGrpBox.Name = "nsisOptionsGrpBox";
            this.nsisOptionsGrpBox.Size = new System.Drawing.Size(400, 254);
            this.nsisOptionsGrpBox.TabIndex = 1036;
            this.nsisOptionsGrpBox.TabStop = false;
            this.nsisOptionsGrpBox.Text = "NSIS Options";
            // 
            // pubOutGrpBox
            // 
            this.pubOutGrpBox.Controls.Add(this.pubOutChkBox);
            this.pubOutGrpBox.Controls.Add(this.browsePubOutPathBtn);
            this.pubOutGrpBox.Controls.Add(this.pubOutPathTxtBox);
            this.pubOutGrpBox.Controls.Add(this.pubOutFoldInfoTxtBox);
            this.pubOutGrpBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pubOutGrpBox.Location = new System.Drawing.Point(6, 145);
            this.pubOutGrpBox.Name = "pubOutGrpBox";
            this.pubOutGrpBox.Size = new System.Drawing.Size(400, 124);
            this.pubOutGrpBox.TabIndex = 1030;
            this.pubOutGrpBox.TabStop = false;
            this.pubOutGrpBox.Text = "Publish Out Folder";
            // 
            // pubOutFoldInfoTxtBox
            // 
            this.pubOutFoldInfoTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pubOutFoldInfoTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.pubOutFoldInfoTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pubOutFoldInfoTxtBox.HideSelection = false;
            this.pubOutFoldInfoTxtBox.Location = new System.Drawing.Point(6, 18);
            this.pubOutFoldInfoTxtBox.Multiline = true;
            this.pubOutFoldInfoTxtBox.Name = "pubOutFoldInfoTxtBox";
            this.pubOutFoldInfoTxtBox.ReadOnly = true;
            this.pubOutFoldInfoTxtBox.ShortcutsEnabled = false;
            this.pubOutFoldInfoTxtBox.Size = new System.Drawing.Size(384, 29);
            this.pubOutFoldInfoTxtBox.TabIndex = 1001;
            this.pubOutFoldInfoTxtBox.TabStop = false;
            this.pubOutFoldInfoTxtBox.Text = "Set the location of your project\'s Publish Out folder. This is where the compiled" +
    " intallers will be found.";
            // 
            // editOptionsGrpBox
            // 
            this.editOptionsGrpBox.Controls.Add(this.includeSRCChkBox);
            this.editOptionsGrpBox.Controls.Add(this.openInstFolderChkBox);
            this.editOptionsGrpBox.Controls.Add(this.saveScriptChkBox);
            this.editOptionsGrpBox.Controls.Add(this.seInlineSpellCheckerChkBox);
            this.editOptionsGrpBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editOptionsGrpBox.Location = new System.Drawing.Point(6, 15);
            this.editOptionsGrpBox.Name = "editOptionsGrpBox";
            this.editOptionsGrpBox.Size = new System.Drawing.Size(400, 254);
            this.editOptionsGrpBox.TabIndex = 1037;
            this.editOptionsGrpBox.TabStop = false;
            this.editOptionsGrpBox.Text = "Editor Options:";
            // 
            // includeSRCChkBox
            // 
            this.includeSRCChkBox.AutoSize = true;
            this.includeSRCChkBox.Checked = true;
            this.includeSRCChkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.includeSRCChkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.includeSRCChkBox.Location = new System.Drawing.Point(6, 65);
            this.includeSRCChkBox.Name = "includeSRCChkBox";
            this.includeSRCChkBox.Size = new System.Drawing.Size(129, 17);
            this.includeSRCChkBox.TabIndex = 1042;
            this.includeSRCChkBox.Text = "Include Game Source";
            this.includeSRCChkBox.UseVisualStyleBackColor = true;
            // 
            // editReadmeBtn
            // 
            this.editReadmeBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editReadmeBtn.Location = new System.Drawing.Point(279, 19);
            this.editReadmeBtn.Name = "editReadmeBtn";
            this.editReadmeBtn.Size = new System.Drawing.Size(115, 23);
            this.editReadmeBtn.TabIndex = 1052;
            this.editReadmeBtn.Text = "Edit Default Readme";
            this.editReadmeBtn.UseVisualStyleBackColor = true;
            this.editReadmeBtn.Click += new System.EventHandler(this.editReadmeBtn_Click);
            // 
            // editLicenseBtn
            // 
            this.editLicenseBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.editLicenseBtn.Location = new System.Drawing.Point(279, 48);
            this.editLicenseBtn.Name = "editLicenseBtn";
            this.editLicenseBtn.Size = new System.Drawing.Size(115, 23);
            this.editLicenseBtn.TabIndex = 1051;
            this.editLicenseBtn.Text = "Edit Custom License";
            this.editLicenseBtn.UseVisualStyleBackColor = true;
            this.editLicenseBtn.Click += new System.EventHandler(this.editLicenseBtn_Click);
            // 
            // licenceLbl
            // 
            this.licenceLbl.AutoSize = true;
            this.licenceLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.licenceLbl.Location = new System.Drawing.Point(3, 16);
            this.licenceLbl.Name = "licenceLbl";
            this.licenceLbl.Size = new System.Drawing.Size(81, 13);
            this.licenceLbl.TabIndex = 1050;
            this.licenceLbl.Text = "License to Use:";
            // 
            // helpProvider
            // 
            this.helpProvider.HelpNamespace = "Documents\\NSIS Publisher Help.chm";
            // 
            // headerPnl
            // 
            this.headerPnl.BackColor = System.Drawing.SystemColors.Control;
            this.headerPnl.Controls.Add(this.nsisDocsLinkLbl);
            this.headerPnl.Controls.Add(this.helpLinkLbl);
            this.headerPnl.Controls.Add(this.optionsInfoTextBox);
            this.headerPnl.Dock = System.Windows.Forms.DockStyle.Top;
            this.headerPnl.Location = new System.Drawing.Point(0, 0);
            this.headerPnl.Name = "headerPnl";
            this.headerPnl.Padding = new System.Windows.Forms.Padding(15, 15, 15, 10);
            this.headerPnl.Size = new System.Drawing.Size(420, 82);
            this.headerPnl.TabIndex = 2;
            // 
            // nsisDocsLinkLbl
            // 
            this.nsisDocsLinkLbl.AutoSize = true;
            this.nsisDocsLinkLbl.Location = new System.Drawing.Point(91, 54);
            this.nsisDocsLinkLbl.Margin = new System.Windows.Forms.Padding(0);
            this.nsisDocsLinkLbl.Name = "nsisDocsLinkLbl";
            this.nsisDocsLinkLbl.Size = new System.Drawing.Size(105, 13);
            this.nsisDocsLinkLbl.TabIndex = 1056;
            this.nsisDocsLinkLbl.TabStop = true;
            this.nsisDocsLinkLbl.Text = "NSIS documentation";
            this.nsisDocsLinkLbl.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.nsisDocsLinkLbl_LinkClicked);
            // 
            // helpLinkLbl
            // 
            this.helpLinkLbl.AutoSize = true;
            this.helpLinkLbl.Location = new System.Drawing.Point(11, 54);
            this.helpLinkLbl.Margin = new System.Windows.Forms.Padding(0);
            this.helpLinkLbl.MaximumSize = new System.Drawing.Size(45, 13);
            this.helpLinkLbl.Name = "helpLinkLbl";
            this.helpLinkLbl.Size = new System.Drawing.Size(45, 13);
            this.helpLinkLbl.TabIndex = 1055;
            this.helpLinkLbl.TabStop = true;
            this.helpLinkLbl.Text = "Help file";
            this.helpLinkLbl.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.helpLinkLbl_LinkClicked);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.licenseCmboBox);
            this.groupBox1.Controls.Add(this.licenceLbl);
            this.groupBox1.Controls.Add(this.chooseLicenseHelpLnk);
            this.groupBox1.Controls.Add(this.editReadmeBtn);
            this.groupBox1.Controls.Add(this.editLicenseBtn);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(6, 182);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(400, 87);
            this.groupBox1.TabIndex = 1038;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Project Documents";
            // 
            // tabCtrl
            // 
            this.tabCtrl.Controls.Add(this.genTabPg);
            this.tabCtrl.Controls.Add(this.allProjTabPg);
            this.tabCtrl.Controls.Add(this.pathsTabPg);
            this.tabCtrl.Controls.Add(this.editTabPg);
            this.tabCtrl.Controls.Add(this.nsisTabPg);
            this.tabCtrl.Controls.Add(this.dbTabPg);
            this.tabCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCtrl.Location = new System.Drawing.Point(0, 82);
            this.tabCtrl.Name = "tabCtrl";
            this.tabCtrl.SelectedIndex = 0;
            this.tabCtrl.Size = new System.Drawing.Size(420, 340);
            this.tabCtrl.TabIndex = 1;
            // 
            // genTabPg
            // 
            this.genTabPg.BackColor = System.Drawing.SystemColors.Control;
            this.genTabPg.Controls.Add(this.groupBox2);
            this.genTabPg.Controls.Add(this.groupBox1);
            this.genTabPg.Location = new System.Drawing.Point(4, 22);
            this.genTabPg.Name = "genTabPg";
            this.genTabPg.Padding = new System.Windows.Forms.Padding(3);
            this.genTabPg.Size = new System.Drawing.Size(412, 314);
            this.genTabPg.TabIndex = 0;
            this.genTabPg.Text = "General";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.scrOverwritePnl);
            this.groupBox2.Controls.Add(this.missngSGReadmeChkBox);
            this.groupBox2.Controls.Add(this.noIncludeWarning);
            this.groupBox2.Controls.Add(this.regDevBtn);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(6, 15);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(400, 161);
            this.groupBox2.TabIndex = 1054;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Application Settings";
            // 
            // scrOverwritePnl
            // 
            this.scrOverwritePnl.Controls.Add(this.scrOverwriteLbl);
            this.scrOverwritePnl.Controls.Add(this.askRadioBtn);
            this.scrOverwritePnl.Controls.Add(this.noOverwriteRadioBtn);
            this.scrOverwritePnl.Controls.Add(this.overWriteRadioBtn);
            this.scrOverwritePnl.Dock = System.Windows.Forms.DockStyle.Top;
            this.scrOverwritePnl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scrOverwritePnl.Location = new System.Drawing.Point(3, 16);
            this.scrOverwritePnl.Name = "scrOverwritePnl";
            this.scrOverwritePnl.Size = new System.Drawing.Size(394, 50);
            this.scrOverwritePnl.TabIndex = 1045;
            // 
            // scrOverwriteLbl
            // 
            this.scrOverwriteLbl.AutoSize = true;
            this.scrOverwriteLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.scrOverwriteLbl.Location = new System.Drawing.Point(0, 14);
            this.scrOverwriteLbl.Margin = new System.Windows.Forms.Padding(0);
            this.scrOverwriteLbl.Name = "scrOverwriteLbl";
            this.scrOverwriteLbl.Size = new System.Drawing.Size(128, 13);
            this.scrOverwriteLbl.TabIndex = 1046;
            this.scrOverwriteLbl.Text = "Script Overwrite Warning:";
            // 
            // askRadioBtn
            // 
            this.askRadioBtn.AutoSize = true;
            this.askRadioBtn.Checked = true;
            this.askRadioBtn.Location = new System.Drawing.Point(223, 30);
            this.askRadioBtn.Name = "askRadioBtn";
            this.askRadioBtn.Size = new System.Drawing.Size(79, 17);
            this.askRadioBtn.TabIndex = 2;
            this.askRadioBtn.TabStop = true;
            this.askRadioBtn.Text = "Always Ask";
            this.askRadioBtn.UseVisualStyleBackColor = true;
            this.askRadioBtn.CheckedChanged += new System.EventHandler(this.askRadioBtn_CheckedChanged);
            // 
            // noOverwriteRadioBtn
            // 
            this.noOverwriteRadioBtn.AutoSize = true;
            this.noOverwriteRadioBtn.Location = new System.Drawing.Point(115, 30);
            this.noOverwriteRadioBtn.Name = "noOverwriteRadioBtn";
            this.noOverwriteRadioBtn.Size = new System.Drawing.Size(102, 17);
            this.noOverwriteRadioBtn.TabIndex = 1;
            this.noOverwriteRadioBtn.TabStop = true;
            this.noOverwriteRadioBtn.Text = "Never Overwrite";
            this.noOverwriteRadioBtn.UseVisualStyleBackColor = true;
            this.noOverwriteRadioBtn.CheckedChanged += new System.EventHandler(this.noOverwriteRadioBtn_CheckedChanged);
            // 
            // overWriteRadioBtn
            // 
            this.overWriteRadioBtn.AutoSize = true;
            this.overWriteRadioBtn.Location = new System.Drawing.Point(3, 30);
            this.overWriteRadioBtn.Name = "overWriteRadioBtn";
            this.overWriteRadioBtn.Size = new System.Drawing.Size(106, 17);
            this.overWriteRadioBtn.TabIndex = 0;
            this.overWriteRadioBtn.TabStop = true;
            this.overWriteRadioBtn.Text = "Always Overwrite";
            this.overWriteRadioBtn.UseVisualStyleBackColor = true;
            this.overWriteRadioBtn.CheckedChanged += new System.EventHandler(this.overWriteRadioBtn_CheckedChanged);
            // 
            // allProjTabPg
            // 
            this.allProjTabPg.BackColor = System.Drawing.SystemColors.Control;
            this.allProjTabPg.Controls.Add(this.projPubGrpBox);
            this.allProjTabPg.Controls.Add(this.projURLGrpBox);
            this.allProjTabPg.Location = new System.Drawing.Point(4, 22);
            this.allProjTabPg.Name = "allProjTabPg";
            this.allProjTabPg.Size = new System.Drawing.Size(412, 314);
            this.allProjTabPg.TabIndex = 5;
            this.allProjTabPg.Text = "All Projects";
            // 
            // pathsTabPg
            // 
            this.pathsTabPg.BackColor = System.Drawing.SystemColors.Control;
            this.pathsTabPg.Controls.Add(this.pubOutGrpBox);
            this.pathsTabPg.Controls.Add(this.defaultProjFolderGrpBox);
            this.pathsTabPg.Location = new System.Drawing.Point(4, 22);
            this.pathsTabPg.Name = "pathsTabPg";
            this.pathsTabPg.Size = new System.Drawing.Size(412, 314);
            this.pathsTabPg.TabIndex = 4;
            this.pathsTabPg.Text = "Paths";
            // 
            // editTabPg
            // 
            this.editTabPg.BackColor = System.Drawing.SystemColors.Control;
            this.editTabPg.Controls.Add(this.editOptionsGrpBox);
            this.editTabPg.Location = new System.Drawing.Point(4, 22);
            this.editTabPg.Name = "editTabPg";
            this.editTabPg.Padding = new System.Windows.Forms.Padding(3);
            this.editTabPg.Size = new System.Drawing.Size(412, 314);
            this.editTabPg.TabIndex = 1;
            this.editTabPg.Text = "Script Editor";
            // 
            // nsisTabPg
            // 
            this.nsisTabPg.BackColor = System.Drawing.SystemColors.Control;
            this.nsisTabPg.Controls.Add(this.nsisOptionsGrpBox);
            this.nsisTabPg.Location = new System.Drawing.Point(4, 22);
            this.nsisTabPg.Name = "nsisTabPg";
            this.nsisTabPg.Padding = new System.Windows.Forms.Padding(3);
            this.nsisTabPg.Size = new System.Drawing.Size(412, 314);
            this.nsisTabPg.TabIndex = 2;
            this.nsisTabPg.Text = "NSIS Options";
            // 
            // dbTabPg
            // 
            this.dbTabPg.BackColor = System.Drawing.SystemColors.Control;
            this.dbTabPg.Controls.Add(this.dosboxOptionsGrpBox);
            this.dbTabPg.Location = new System.Drawing.Point(4, 22);
            this.dbTabPg.Name = "dbTabPg";
            this.dbTabPg.Padding = new System.Windows.Forms.Padding(3);
            this.dbTabPg.Size = new System.Drawing.Size(412, 314);
            this.dbTabPg.TabIndex = 3;
            this.dbTabPg.Text = "DOSBox Options";
            // 
            // footerPnl
            // 
            this.footerPnl.Controls.Add(this.okBtn);
            this.footerPnl.Controls.Add(this.cancelBtn);
            this.footerPnl.Controls.Add(this.applyBtn);
            this.footerPnl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.footerPnl.Location = new System.Drawing.Point(0, 380);
            this.footerPnl.Name = "footerPnl";
            this.footerPnl.Size = new System.Drawing.Size(420, 42);
            this.footerPnl.TabIndex = 0;
            // 
            // OptionsFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(420, 422);
            this.Controls.Add(this.footerPnl);
            this.Controls.Add(this.tabCtrl);
            this.Controls.Add(this.headerPnl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(436, 460);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(436, 460);
            this.Name = "OptionsFrm";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NSIS Publisher Options";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Closing);
            this.Load += new System.EventHandler(this.Form_Load);
            this.defaultProjFolderGrpBox.ResumeLayout(false);
            this.defaultProjFolderGrpBox.PerformLayout();
            this.projURLGrpBox.ResumeLayout(false);
            this.projURLGrpBox.PerformLayout();
            this.projPubGrpBox.ResumeLayout(false);
            this.projPubGrpBox.PerformLayout();
            this.dosboxOptionsGrpBox.ResumeLayout(false);
            this.dosboxOptionsGrpBox.PerformLayout();
            this.nsisOptionsGrpBox.ResumeLayout(false);
            this.nsisOptionsGrpBox.PerformLayout();
            this.pubOutGrpBox.ResumeLayout(false);
            this.pubOutGrpBox.PerformLayout();
            this.editOptionsGrpBox.ResumeLayout(false);
            this.editOptionsGrpBox.PerformLayout();
            this.headerPnl.ResumeLayout(false);
            this.headerPnl.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabCtrl.ResumeLayout(false);
            this.genTabPg.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.scrOverwritePnl.ResumeLayout(false);
            this.scrOverwritePnl.PerformLayout();
            this.allProjTabPg.ResumeLayout(false);
            this.pathsTabPg.ResumeLayout(false);
            this.editTabPg.ResumeLayout(false);
            this.nsisTabPg.ResumeLayout(false);
            this.dbTabPg.ResumeLayout(false);
            this.footerPnl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ToolTip toolTips;
        internal System.Windows.Forms.Button okBtn;
        internal System.Windows.Forms.Button applyBtn;
        internal System.Windows.Forms.Button cancelBtn;
        private System.Windows.Forms.GroupBox pubOutGrpBox;
        internal System.Windows.Forms.Button browsePubOutPathBtn;
        private System.Windows.Forms.TextBox pubOutPathTxtBox;
        internal System.Windows.Forms.TextBox pubOutFoldInfoTxtBox;
        private System.Windows.Forms.GroupBox defaultProjFolderGrpBox;
        internal System.Windows.Forms.TextBox defaultProjFoldInfoTxtBox;
        internal System.Windows.Forms.Button browseDefaultProjPathBtn;
        private System.Windows.Forms.TextBox defaultProjPathTxtBox;
        private System.Windows.Forms.GroupBox dosboxOptionsGrpBox;
        private System.Windows.Forms.TextBox defaultPubNameTxtBox;
        internal System.Windows.Forms.TextBox pubInfoTxtBox;
        private System.Windows.Forms.LinkLabel getNSISLnk;
        private System.Windows.Forms.CheckBox deleteNSISSrptChkBox;
        private System.Windows.Forms.CheckBox runNSISChkBox;
        private System.Windows.Forms.GroupBox projPubGrpBox;
        private System.Windows.Forms.GroupBox nsisOptionsGrpBox;
        private System.Windows.Forms.TextBox optionsInfoTextBox;
        private System.Windows.Forms.GroupBox editOptionsGrpBox;
        private System.Windows.Forms.CheckBox missngSGReadmeChkBox;
        internal System.Windows.Forms.HelpProvider helpProvider;
        private System.Windows.Forms.CheckBox seInlineSpellCheckerChkBox;
        private System.Windows.Forms.Panel headerPnl;
        private System.Windows.Forms.CheckBox aspectChkBox;
        internal System.Windows.Forms.GroupBox projURLGrpBox;
        private System.Windows.Forms.TextBox projURLInfoTxtBox;
        internal System.Windows.Forms.TextBox pubURLTxtBox;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.LinkLabel getLogNSISLnk;
        private System.Windows.Forms.LinkLabel getLanguageFileLnk;
        private System.Windows.Forms.CheckBox saveScriptChkBox;
        private System.Windows.Forms.CheckBox unicodeChkBox;
        private System.Windows.Forms.CheckBox noIncludeWarning;
        private System.Windows.Forms.CheckBox useLangFileChkBox;
        private System.Windows.Forms.CheckBox loggingChkBox;
        private System.Windows.Forms.CheckBox openInstFolderChkBox;
        private System.Windows.Forms.Label licenceLbl;
        private System.Windows.Forms.ComboBox licenseCmboBox;
        private System.Windows.Forms.CheckBox pubOutChkBox;
        private System.Windows.Forms.Button editLicenseBtn;
        private System.Windows.Forms.Button editReadmeBtn;
        private System.Windows.Forms.LinkLabel chooseLicenseHelpLnk;
        private System.Windows.Forms.Button regDevBtn;
        private System.Windows.Forms.CheckBox includeSRCChkBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabControl tabCtrl;
        private System.Windows.Forms.TabPage genTabPg;
        private System.Windows.Forms.TabPage editTabPg;
        private System.Windows.Forms.TabPage pathsTabPg;
        private System.Windows.Forms.TabPage nsisTabPg;
        private System.Windows.Forms.TabPage dbTabPg;
        private System.Windows.Forms.Panel footerPnl;
        private System.Windows.Forms.TabPage allProjTabPg;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.LinkLabel helpLinkLbl;
        private System.Windows.Forms.LinkLabel nsisDocsLinkLbl;
        private System.Windows.Forms.Label scrOverwriteLbl;
        private System.Windows.Forms.Panel scrOverwritePnl;
        private System.Windows.Forms.RadioButton askRadioBtn;
        private System.Windows.Forms.RadioButton noOverwriteRadioBtn;
        private System.Windows.Forms.RadioButton overWriteRadioBtn;
    }
}