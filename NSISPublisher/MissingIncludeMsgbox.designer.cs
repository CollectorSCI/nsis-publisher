﻿namespace NSISPublisher
{
    partial class MissingIncludeMsgbox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MissingIncludeMsgbox));
            this.checkBox = new System.Windows.Forms.CheckBox();
            this.yesBtn = new System.Windows.Forms.Button();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.includeMsgTxtBox = new System.Windows.Forms.TextBox();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.Title = new System.Windows.Forms.Label();
            this.noBtn = new System.Windows.Forms.Button();
            this.cancelBtn = new System.Windows.Forms.Button();
            this.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBox
            // 
            this.checkBox.AutoSize = true;
            this.checkBox.Location = new System.Drawing.Point(8, 90);
            this.checkBox.Name = "checkBox";
            this.checkBox.Size = new System.Drawing.Size(179, 17);
            this.checkBox.TabIndex = 12;
            this.checkBox.Text = "Do not show this message again";
            this.checkBox.UseVisualStyleBackColor = true;
            this.checkBox.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // yesBtn
            // 
            this.yesBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.yesBtn.Location = new System.Drawing.Point(173, 117);
            this.yesBtn.Name = "yesBtn";
            this.yesBtn.Size = new System.Drawing.Size(75, 23);
            this.yesBtn.TabIndex = 10;
            this.yesBtn.Text = "Yes";
            this.yesBtn.UseVisualStyleBackColor = true;
            this.yesBtn.Click += new System.EventHandler(this.yesBtn_Click);
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.Color.White;
            this.Panel1.Controls.Add(this.checkBox);
            this.Panel1.Controls.Add(this.includeMsgTxtBox);
            this.Panel1.Controls.Add(this.PictureBox1);
            this.Panel1.Controls.Add(this.Title);
            this.Panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel1.Location = new System.Drawing.Point(0, 0);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(422, 111);
            this.Panel1.TabIndex = 11;
            // 
            // includeMsgTxtBox
            // 
            this.includeMsgTxtBox.BackColor = System.Drawing.SystemColors.Window;
            this.includeMsgTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.includeMsgTxtBox.Location = new System.Drawing.Point(8, 51);
            this.includeMsgTxtBox.Multiline = true;
            this.includeMsgTxtBox.Name = "includeMsgTxtBox";
            this.includeMsgTxtBox.ReadOnly = true;
            this.includeMsgTxtBox.Size = new System.Drawing.Size(411, 33);
            this.includeMsgTxtBox.TabIndex = 3;
            this.includeMsgTxtBox.Text = "The Publish Include folder is missing. Would you like to create it now?\r\n";
            // 
            // PictureBox1
            // 
            this.PictureBox1.Image = global::NSISPublisher.Properties.Resources.Warning;
            this.PictureBox1.Location = new System.Drawing.Point(16, 14);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(31, 28);
            this.PictureBox1.TabIndex = 2;
            this.PictureBox1.TabStop = false;
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(53, 18);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(94, 24);
            this.Title.TabIndex = 0;
            this.Title.Text = "Warning:";
            // 
            // noBtn
            // 
            this.noBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.noBtn.Location = new System.Drawing.Point(254, 117);
            this.noBtn.Name = "noBtn";
            this.noBtn.Size = new System.Drawing.Size(75, 23);
            this.noBtn.TabIndex = 13;
            this.noBtn.Text = "No";
            this.noBtn.UseVisualStyleBackColor = true;
            this.noBtn.Click += new System.EventHandler(this.noBtn_Click);
            // 
            // cancelBtn
            // 
            this.cancelBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cancelBtn.Location = new System.Drawing.Point(335, 117);
            this.cancelBtn.Name = "cancelBtn";
            this.cancelBtn.Size = new System.Drawing.Size(75, 23);
            this.cancelBtn.TabIndex = 14;
            this.cancelBtn.Text = "Cancel";
            this.cancelBtn.UseVisualStyleBackColor = true;
            this.cancelBtn.Click += new System.EventHandler(this.cancelBtn_Click);
            // 
            // MissingIncludeMsgbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 149);
            this.Controls.Add(this.cancelBtn);
            this.Controls.Add(this.noBtn);
            this.Controls.Add(this.yesBtn);
            this.Controls.Add(this.Panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(438, 187);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(438, 187);
            this.Name = "MissingIncludeMsgbox";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Missing Folder";
            this.Load += new System.EventHandler(this.Form_Load);
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.CheckBox checkBox;
        internal System.Windows.Forms.Button yesBtn;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.Label Title;
        public System.Windows.Forms.TextBox includeMsgTxtBox;
        internal System.Windows.Forms.Button noBtn;
        internal System.Windows.Forms.Button cancelBtn;
    }
}