﻿namespace NSISPublisher
{
    partial class MainFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainFrm));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.cntxtMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.undoCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.redoCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.cntxMenuSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.cutCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.copyCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.delCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.cntxMenuSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.selectAllCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.clearCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.statusbar = new System.Windows.Forms.StatusStrip();
            this.statusbarMainMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.splitContainer = new System.Windows.Forms.SplitContainer();
            this.splashPanel = new System.Windows.Forms.Panel();
            this.notifyLbl = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.scriptbox = new FastColoredTextBoxNS.FastColoredTextBox();
            this.outputBox = new System.Windows.Forms.RichTextBox();
            this.outputBoxCntxtMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.selectAllOutputBoxCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.copyOutputBoxCntxtMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPages = new System.Windows.Forms.TabControl();
            this.propsTabPg = new System.Windows.Forms.TabPage();
            this.unicodeChkBox = new System.Windows.Forms.CheckBox();
            this.includeSRCChkBox = new System.Windows.Forms.CheckBox();
            this.loggingChkBox = new System.Windows.Forms.CheckBox();
            this.infoLbl = new System.Windows.Forms.Label();
            this.aspectChkBox = new System.Windows.Forms.CheckBox();
            this.projNotesGrpBox = new System.Windows.Forms.GroupBox();
            this.projNotesTxtBox = new System.Windows.Forms.TextBox();
            this.propsSplitCntnr = new System.Windows.Forms.SplitContainer();
            this.projShortNameGrpBox = new System.Windows.Forms.GroupBox();
            this.projShortNameInfoTxtBox = new System.Windows.Forms.TextBox();
            this.projShortNameTxtBox = new System.Windows.Forms.TextBox();
            this.projPathGrpBox = new System.Windows.Forms.GroupBox();
            this.projPathInfoTxtBox = new System.Windows.Forms.TextBox();
            this.projPathBrowseBtn = new System.Windows.Forms.Button();
            this.projPathTxtBox = new System.Windows.Forms.TextBox();
            this.projPubGrpBox = new System.Windows.Forms.GroupBox();
            this.projPubInfoTxtBox = new System.Windows.Forms.TextBox();
            this.projPubTxtBox = new System.Windows.Forms.TextBox();
            this.projURLGrpBox = new System.Windows.Forms.GroupBox();
            this.projURLInfoTxtBox = new System.Windows.Forms.TextBox();
            this.projURLTxtBox = new System.Windows.Forms.TextBox();
            this.projNameGrpBox = new System.Windows.Forms.GroupBox();
            this.projNameInfoTxtBox = new System.Windows.Forms.TextBox();
            this.projNameTxtBox = new System.Windows.Forms.TextBox();
            this.projMainEXEGrpBox = new System.Windows.Forms.GroupBox();
            this.projMainEXEInfoTxtBox = new System.Windows.Forms.TextBox();
            this.projMainEXETxtBox = new System.Windows.Forms.TextBox();
            this.propsToolbar = new System.Windows.Forms.ToolStrip();
            this.clearBtn = new System.Windows.Forms.ToolStripButton();
            this.toolbarSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.projEngCmboBox = new System.Windows.Forms.ToolStripComboBox();
            this.toolbarSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.publishBtn = new System.Windows.Forms.ToolStripButton();
            this.toolbarSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.scriptTabPg = new System.Windows.Forms.TabPage();
            this.toolbarContainer = new System.Windows.Forms.ToolStripContainer();
            this.findToolbar = new System.Windows.Forms.ToolStrip();
            this.findBtn = new System.Windows.Forms.ToolStripButton();
            this.findNextBtn = new System.Windows.Forms.ToolStripButton();
            this.replaceBtn = new System.Windows.Forms.ToolStripButton();
            this.goToBtn = new System.Windows.Forms.ToolStripButton();
            this.buildToolbar = new System.Windows.Forms.ToolStrip();
            this.compileBtn = new System.Windows.Forms.ToolStripButton();
            this.compileDebugBtn = new System.Windows.Forms.ToolStripButton();
            this.stopcompileBtn = new System.Windows.Forms.ToolStripButton();
            this.debugBtn = new System.Windows.Forms.ToolStripButton();
            this.scriptEditToolbar = new System.Windows.Forms.ToolStrip();
            this.lockBtn = new System.Windows.Forms.ToolStripButton();
            this.scriptPropsBtn = new System.Windows.Forms.ToolStripButton();
            this.toolbarSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.cutBtn = new System.Windows.Forms.ToolStripButton();
            this.copyBtn = new System.Windows.Forms.ToolStripButton();
            this.pasteBtn = new System.Windows.Forms.ToolStripButton();
            this.toolbarSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.undoBtn = new System.Windows.Forms.ToolStripButton();
            this.redoBtn = new System.Windows.Forms.ToolStripButton();
            this.selectAllBtn = new System.Windows.Forms.ToolStripButton();
            this.deleteBtn = new System.Windows.Forms.ToolStripButton();
            this.clearScriptBtn = new System.Windows.Forms.ToolStripButton();
            this.helpProvider = new System.Windows.Forms.HelpProvider();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.mainToolbar = new System.Windows.Forms.ToolStrip();
            this.newBtn = new System.Windows.Forms.ToolStripButton();
            this.openBtn = new System.Windows.Forms.ToolStripButton();
            this.saveBtn = new System.Windows.Forms.ToolStripButton();
            this.saveAsBtn = new System.Windows.Forms.ToolStripButton();
            this.toolbarSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.openIncludeBtn = new System.Windows.Forms.ToolStripButton();
            this.licenseBtn = new System.Windows.Forms.ToolStripButton();
            this.readmeBtn = new System.Windows.Forms.ToolStripButton();
            this.toolbarSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.helpBtn = new System.Windows.Forms.ToolStripButton();
            this.menubar = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuNew = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuOpenScript = new System.Windows.Forms.ToolStripMenuItem();
            this.menubarSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.fileMenuSave = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuSaveAs = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuSaveAll = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuOpenConf = new System.Windows.Forms.ToolStripMenuItem();
            this.menubarSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.fileMenuPageSetup = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuPrintPreview = new System.Windows.Forms.ToolStripMenuItem();
            this.fileMenuPrint = new System.Windows.Forms.ToolStripMenuItem();
            this.menubarSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.fileMenuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenuUndo = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenuRedo = new System.Windows.Forms.ToolStripMenuItem();
            this.menubarSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.editMenuCut = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenuCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenuPaste = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenuDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.menubarSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.editMenuFind = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenuFindNext = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenuReplace = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenuGoTo = new System.Windows.Forms.ToolStripMenuItem();
            this.menubarSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.editMenuSelectAll = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenuClear = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenuTimeDate = new System.Windows.Forms.ToolStripMenuItem();
            this.buildMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.buildMenuCompile = new System.Windows.Forms.ToolStripMenuItem();
            this.buildMenuCompileDebug = new System.Windows.Forms.ToolStripMenuItem();
            this.buildMenuStopcompile = new System.Windows.Forms.ToolStripMenuItem();
            this.buildMenuDebug = new System.Windows.Forms.ToolStripMenuItem();
            this.formatMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.formatMenuWordWrap = new System.Windows.Forms.ToolStripMenuItem();
            this.formatMenuFont = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenuInlineSpellChecker = new System.Windows.Forms.ToolStripMenuItem();
            this.menubarSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.viewMenuToolbars = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenuHideToolbars = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenuMainToolbar = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenuPropsToolbar = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenuEditToolbar = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenuBuildToolbar = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenuFindToolbar = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenuStatusbar = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenuLineNumbers = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenuFoldingLines = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenuGetSEC1FolderSize = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenuGetSEC2FolderSize = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenuByteConverter = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenuClipboardTool = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenuSpellCheck = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsMenuEditDictionary = new System.Windows.Forms.ToolStripMenuItem();
            this.menubarSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolsMenuOptions = new System.Windows.Forms.ToolStripMenuItem();
            this.menubarSeparator9 = new System.Windows.Forms.ToolStripSeparator();
            this.toolsMenuScriptProperties = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuContents = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuIndex = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuSearch = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuNSISManual = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuNSISWiki = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuNSISForum = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenuSCICommunity = new System.Windows.Forms.ToolStripMenuItem();
            this.menubarSeparator10 = new System.Windows.Forms.ToolStripSeparator();
            this.helpMenuAbout = new System.Windows.Forms.ToolStripMenuItem();
            this.cntxtMenu.SuspendLayout();
            this.statusbar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).BeginInit();
            this.splitContainer.Panel1.SuspendLayout();
            this.splitContainer.Panel2.SuspendLayout();
            this.splitContainer.SuspendLayout();
            this.splashPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scriptbox)).BeginInit();
            this.outputBoxCntxtMenu.SuspendLayout();
            this.tabPages.SuspendLayout();
            this.propsTabPg.SuspendLayout();
            this.projNotesGrpBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.propsSplitCntnr)).BeginInit();
            this.propsSplitCntnr.Panel1.SuspendLayout();
            this.propsSplitCntnr.Panel2.SuspendLayout();
            this.propsSplitCntnr.SuspendLayout();
            this.projShortNameGrpBox.SuspendLayout();
            this.projPathGrpBox.SuspendLayout();
            this.projPubGrpBox.SuspendLayout();
            this.projURLGrpBox.SuspendLayout();
            this.projNameGrpBox.SuspendLayout();
            this.projMainEXEGrpBox.SuspendLayout();
            this.propsToolbar.SuspendLayout();
            this.scriptTabPg.SuspendLayout();
            this.toolbarContainer.ContentPanel.SuspendLayout();
            this.toolbarContainer.TopToolStripPanel.SuspendLayout();
            this.toolbarContainer.SuspendLayout();
            this.findToolbar.SuspendLayout();
            this.buildToolbar.SuspendLayout();
            this.scriptEditToolbar.SuspendLayout();
            this.mainToolbar.SuspendLayout();
            this.menubar.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "scriptx16.png");
            this.imageList.Images.SetKeyName(1, "appx16.png");
            this.imageList.Images.SetKeyName(2, "1302166543_virtualbox.png");
            // 
            // cntxtMenu
            // 
            this.cntxtMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.undoCntxtMenu,
            this.redoCntxtMenu,
            this.cntxMenuSeparator1,
            this.cutCntxtMenu,
            this.copyCntxtMenu,
            this.pasteCntxtMenu,
            this.delCntxtMenu,
            this.cntxMenuSeparator2,
            this.selectAllCntxtMenu,
            this.clearCntxtMenu});
            this.cntxtMenu.Name = "ContextMenu";
            this.cntxtMenu.Size = new System.Drawing.Size(123, 192);
            this.cntxtMenu.Opening += new System.ComponentModel.CancelEventHandler(this.ScriptContextMenu_Opening);
            // 
            // undoCntxtMenu
            // 
            this.undoCntxtMenu.Enabled = false;
            this.undoCntxtMenu.Image = global::NSISPublisher.Properties.Resources.Undo;
            this.undoCntxtMenu.Name = "undoCntxtMenu";
            this.undoCntxtMenu.Size = new System.Drawing.Size(122, 22);
            this.undoCntxtMenu.Text = "Undo";
            this.undoCntxtMenu.Click += new System.EventHandler(this.undoCntxtMenu_Click);
            // 
            // redoCntxtMenu
            // 
            this.redoCntxtMenu.Enabled = false;
            this.redoCntxtMenu.Image = global::NSISPublisher.Properties.Resources.Redo;
            this.redoCntxtMenu.Name = "redoCntxtMenu";
            this.redoCntxtMenu.Size = new System.Drawing.Size(122, 22);
            this.redoCntxtMenu.Text = "Redo";
            this.redoCntxtMenu.Click += new System.EventHandler(this.redoCntxtMenu_Click);
            // 
            // cntxMenuSeparator1
            // 
            this.cntxMenuSeparator1.Name = "cntxMenuSeparator1";
            this.cntxMenuSeparator1.Size = new System.Drawing.Size(119, 6);
            // 
            // cutCntxtMenu
            // 
            this.cutCntxtMenu.Image = global::NSISPublisher.Properties.Resources.Cut;
            this.cutCntxtMenu.Name = "cutCntxtMenu";
            this.cutCntxtMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.cutCntxtMenu.ShowShortcutKeys = false;
            this.cutCntxtMenu.Size = new System.Drawing.Size(122, 22);
            this.cutCntxtMenu.Text = "Cut";
            this.cutCntxtMenu.Click += new System.EventHandler(this.cutCntxtMenu_Click);
            // 
            // copyCntxtMenu
            // 
            this.copyCntxtMenu.Image = global::NSISPublisher.Properties.Resources.Copy;
            this.copyCntxtMenu.Name = "copyCntxtMenu";
            this.copyCntxtMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.copyCntxtMenu.ShowShortcutKeys = false;
            this.copyCntxtMenu.Size = new System.Drawing.Size(122, 22);
            this.copyCntxtMenu.Text = "Copy";
            this.copyCntxtMenu.Click += new System.EventHandler(this.copyCntxtMenu_Click);
            // 
            // pasteCntxtMenu
            // 
            this.pasteCntxtMenu.Image = global::NSISPublisher.Properties.Resources.Paste;
            this.pasteCntxtMenu.Name = "pasteCntxtMenu";
            this.pasteCntxtMenu.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.pasteCntxtMenu.ShowShortcutKeys = false;
            this.pasteCntxtMenu.Size = new System.Drawing.Size(122, 22);
            this.pasteCntxtMenu.Text = "Paste";
            this.pasteCntxtMenu.Click += new System.EventHandler(this.pasteCntxtMenu_Click);
            // 
            // delCntxtMenu
            // 
            this.delCntxtMenu.Image = global::NSISPublisher.Properties.Resources.Delete;
            this.delCntxtMenu.Name = "delCntxtMenu";
            this.delCntxtMenu.Size = new System.Drawing.Size(122, 22);
            this.delCntxtMenu.Text = "Delete";
            this.delCntxtMenu.Click += new System.EventHandler(this.delCntxtMenu_Click);
            // 
            // cntxMenuSeparator2
            // 
            this.cntxMenuSeparator2.Name = "cntxMenuSeparator2";
            this.cntxMenuSeparator2.Size = new System.Drawing.Size(119, 6);
            // 
            // selectAllCntxtMenu
            // 
            this.selectAllCntxtMenu.Enabled = false;
            this.selectAllCntxtMenu.Image = global::NSISPublisher.Properties.Resources.SelectAll;
            this.selectAllCntxtMenu.Name = "selectAllCntxtMenu";
            this.selectAllCntxtMenu.Size = new System.Drawing.Size(122, 22);
            this.selectAllCntxtMenu.Text = "Select All";
            this.selectAllCntxtMenu.Click += new System.EventHandler(this.selectAllCntxtMenu_Click);
            // 
            // clearCntxtMenu
            // 
            this.clearCntxtMenu.Enabled = false;
            this.clearCntxtMenu.Image = global::NSISPublisher.Properties.Resources.Clear;
            this.clearCntxtMenu.Name = "clearCntxtMenu";
            this.clearCntxtMenu.Size = new System.Drawing.Size(122, 22);
            this.clearCntxtMenu.Text = "Clear All";
            this.clearCntxtMenu.Click += new System.EventHandler(this.clearCntxtMenu_Click);
            // 
            // statusbar
            // 
            this.statusbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusbarMainMessage});
            this.statusbar.Location = new System.Drawing.Point(0, 640);
            this.statusbar.Name = "statusbar";
            this.statusbar.Size = new System.Drawing.Size(674, 22);
            this.statusbar.TabIndex = 8;
            this.statusbar.Text = "statusbar";
            // 
            // statusbarMainMessage
            // 
            this.statusbarMainMessage.Name = "statusbarMainMessage";
            this.statusbarMainMessage.Size = new System.Drawing.Size(39, 17);
            this.statusbarMainMessage.Text = "Ready";
            // 
            // splitContainer
            // 
            this.splitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer.Location = new System.Drawing.Point(0, 0);
            this.splitContainer.Name = "splitContainer";
            this.splitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer.Panel1
            // 
            this.splitContainer.Panel1.Controls.Add(this.splashPanel);
            this.splitContainer.Panel1.Controls.Add(this.scriptbox);
            // 
            // splitContainer.Panel2
            // 
            this.splitContainer.Panel2.Controls.Add(this.outputBox);
            this.splitContainer.Panel2MinSize = 0;
            this.splitContainer.Size = new System.Drawing.Size(660, 534);
            this.splitContainer.SplitterDistance = 465;
            this.splitContainer.TabIndex = 9;
            // 
            // splashPanel
            // 
            this.splashPanel.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.splashPanel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splashPanel.Controls.Add(this.notifyLbl);
            this.splashPanel.Controls.Add(this.label1);
            this.splashPanel.Location = new System.Drawing.Point(230, 191);
            this.splashPanel.Name = "splashPanel";
            this.splashPanel.Size = new System.Drawing.Size(200, 69);
            this.splashPanel.TabIndex = 4;
            this.splashPanel.Visible = false;
            // 
            // notifyLbl
            // 
            this.notifyLbl.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.notifyLbl.AutoSize = true;
            this.notifyLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.notifyLbl.Location = new System.Drawing.Point(28, 14);
            this.notifyLbl.Name = "notifyLbl";
            this.notifyLbl.Size = new System.Drawing.Size(140, 20);
            this.notifyLbl.TabIndex = 11;
            this.notifyLbl.Text = "Compiling installer,";
            this.notifyLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(51, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 20);
            this.label1.TabIndex = 12;
            this.label1.Text = "please wait...";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // scriptbox
            // 
            this.scriptbox.AutoCompleteBracketsList = new char[] {
        '(',
        ')',
        '{',
        '}',
        '[',
        ']',
        '\"',
        '\"',
        '\'',
        '\''};
            this.scriptbox.AutoScrollMinSize = new System.Drawing.Size(25, 15);
            this.scriptbox.BackBrush = null;
            this.scriptbox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.scriptbox.CharHeight = 15;
            this.scriptbox.CharWidth = 7;
            this.scriptbox.ContextMenuStrip = this.cntxtMenu;
            this.scriptbox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.scriptbox.DelayedEventsInterval = 500;
            this.scriptbox.DelayedTextChangedInterval = 500;
            this.scriptbox.DisabledColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.scriptbox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scriptbox.Font = new System.Drawing.Font("Consolas", 9.75F);
            this.scriptbox.IsReplaceMode = false;
            this.scriptbox.LeftBracket = '(';
            this.scriptbox.Location = new System.Drawing.Point(0, 0);
            this.scriptbox.Name = "scriptbox";
            this.scriptbox.Paddings = new System.Windows.Forms.Padding(0);
            this.scriptbox.RightBracket = ')';
            this.scriptbox.SelectionColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.scriptbox.ServiceColors = ((FastColoredTextBoxNS.ServiceColors)(resources.GetObject("scriptbox.ServiceColors")));
            this.scriptbox.ShowFoldingLines = true;
            this.scriptbox.Size = new System.Drawing.Size(660, 465);
            this.scriptbox.TabIndex = 3;
            this.scriptbox.Zoom = 100;
            this.scriptbox.TextChanged += new System.EventHandler<FastColoredTextBoxNS.TextChangedEventArgs>(this.scriptbox_TextChanged);
            this.scriptbox.Load += new System.EventHandler(this.scriptbox_Load);
            this.scriptbox.DragDrop += new System.Windows.Forms.DragEventHandler(this.scriptbox_DragDrop);
            this.scriptbox.DragEnter += new System.Windows.Forms.DragEventHandler(this.scriptbox_DragEnter);
            // 
            // outputBox
            // 
            this.outputBox.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.outputBox.ContextMenuStrip = this.outputBoxCntxtMenu;
            this.outputBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.outputBox.Font = new System.Drawing.Font("Courier New", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.outputBox.Location = new System.Drawing.Point(0, 0);
            this.outputBox.Name = "outputBox";
            this.outputBox.Size = new System.Drawing.Size(660, 65);
            this.outputBox.TabIndex = 10;
            this.outputBox.Text = "";
            this.outputBox.MouseClick += new System.Windows.Forms.MouseEventHandler(this.outputBox_MouseClick);
            // 
            // outputBoxCntxtMenu
            // 
            this.outputBoxCntxtMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectAllOutputBoxCntxtMenu,
            this.copyOutputBoxCntxtMenu});
            this.outputBoxCntxtMenu.Name = "contextMenuStrip1";
            this.outputBoxCntxtMenu.Size = new System.Drawing.Size(123, 48);
            // 
            // selectAllOutputBoxCntxtMenu
            // 
            this.selectAllOutputBoxCntxtMenu.Name = "selectAllOutputBoxCntxtMenu";
            this.selectAllOutputBoxCntxtMenu.Size = new System.Drawing.Size(122, 22);
            this.selectAllOutputBoxCntxtMenu.Text = "Select &All";
            this.selectAllOutputBoxCntxtMenu.Click += new System.EventHandler(this.selectAllOutputBoxCntxtMenu_Click);
            // 
            // copyOutputBoxCntxtMenu
            // 
            this.copyOutputBoxCntxtMenu.Name = "copyOutputBoxCntxtMenu";
            this.copyOutputBoxCntxtMenu.Size = new System.Drawing.Size(122, 22);
            this.copyOutputBoxCntxtMenu.Text = "&Copy";
            this.copyOutputBoxCntxtMenu.Click += new System.EventHandler(this.copyOutputBoxCntxtMenu_Click);
            // 
            // tabPages
            // 
            this.tabPages.Controls.Add(this.propsTabPg);
            this.tabPages.Controls.Add(this.scriptTabPg);
            this.tabPages.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPages.Location = new System.Drawing.Point(0, 49);
            this.tabPages.Name = "tabPages";
            this.tabPages.SelectedIndex = 0;
            this.tabPages.Size = new System.Drawing.Size(674, 591);
            this.tabPages.TabIndex = 10;
            this.tabPages.SelectedIndexChanged += new System.EventHandler(this.tabCntrl_SelectedIndexChanged);
            // 
            // propsTabPg
            // 
            this.propsTabPg.BackColor = System.Drawing.SystemColors.Control;
            this.propsTabPg.Controls.Add(this.unicodeChkBox);
            this.propsTabPg.Controls.Add(this.includeSRCChkBox);
            this.propsTabPg.Controls.Add(this.loggingChkBox);
            this.propsTabPg.Controls.Add(this.infoLbl);
            this.propsTabPg.Controls.Add(this.aspectChkBox);
            this.propsTabPg.Controls.Add(this.projNotesGrpBox);
            this.propsTabPg.Controls.Add(this.propsSplitCntnr);
            this.propsTabPg.Controls.Add(this.propsToolbar);
            this.propsTabPg.Location = new System.Drawing.Point(4, 22);
            this.propsTabPg.Name = "propsTabPg";
            this.propsTabPg.Padding = new System.Windows.Forms.Padding(3);
            this.propsTabPg.Size = new System.Drawing.Size(666, 565);
            this.propsTabPg.TabIndex = 0;
            this.propsTabPg.Text = "Project Properties";
            // 
            // unicodeChkBox
            // 
            this.unicodeChkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.unicodeChkBox.AutoSize = true;
            this.unicodeChkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.unicodeChkBox.Location = new System.Drawing.Point(260, 256);
            this.unicodeChkBox.Name = "unicodeChkBox";
            this.unicodeChkBox.Size = new System.Drawing.Size(139, 17);
            this.unicodeChkBox.TabIndex = 1048;
            this.unicodeChkBox.Text = "Create Unicode Installer";
            this.unicodeChkBox.CheckedChanged += new System.EventHandler(this.unicodeChkBox_CheckedChanged);
            // 
            // includeSRCChkBox
            // 
            this.includeSRCChkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.includeSRCChkBox.AutoSize = true;
            this.includeSRCChkBox.BackColor = System.Drawing.Color.Transparent;
            this.includeSRCChkBox.Checked = true;
            this.includeSRCChkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.includeSRCChkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.includeSRCChkBox.Location = new System.Drawing.Point(405, 256);
            this.includeSRCChkBox.Name = "includeSRCChkBox";
            this.includeSRCChkBox.Size = new System.Drawing.Size(129, 17);
            this.includeSRCChkBox.TabIndex = 1041;
            this.includeSRCChkBox.Text = "Include Game Source";
            this.toolTip.SetToolTip(this.includeSRCChkBox, "Select to Include Source in Installer");
            this.includeSRCChkBox.UseVisualStyleBackColor = false;
            this.includeSRCChkBox.CheckedChanged += new System.EventHandler(this.includeSRCChkBox_CheckedChanged);
            // 
            // loggingChkBox
            // 
            this.loggingChkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.loggingChkBox.AutoSize = true;
            this.loggingChkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loggingChkBox.Location = new System.Drawing.Point(117, 256);
            this.loggingChkBox.Name = "loggingChkBox";
            this.loggingChkBox.Size = new System.Drawing.Size(137, 17);
            this.loggingChkBox.TabIndex = 1049;
            this.loggingChkBox.Text = "Create Logging Installer";
            this.loggingChkBox.CheckedChanged += new System.EventHandler(this.loggingChkBox_CheckedChanged);
            // 
            // infoLbl
            // 
            this.infoLbl.AutoSize = true;
            this.infoLbl.Location = new System.Drawing.Point(6, 257);
            this.infoLbl.Name = "infoLbl";
            this.infoLbl.Size = new System.Drawing.Size(87, 13);
            this.infoLbl.TabIndex = 1043;
            this.infoLbl.Text = "* Required Fields";
            // 
            // aspectChkBox
            // 
            this.aspectChkBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.aspectChkBox.AutoSize = true;
            this.aspectChkBox.BackColor = System.Drawing.Color.Transparent;
            this.aspectChkBox.Checked = true;
            this.aspectChkBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.aspectChkBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aspectChkBox.Location = new System.Drawing.Point(548, 256);
            this.aspectChkBox.Name = "aspectChkBox";
            this.aspectChkBox.Size = new System.Drawing.Size(110, 17);
            this.aspectChkBox.TabIndex = 1042;
            this.aspectChkBox.Text = "Aspect Correction";
            this.toolTip.SetToolTip(this.aspectChkBox, "Sets Which Config Tool to be Included");
            this.aspectChkBox.UseVisualStyleBackColor = false;
            this.aspectChkBox.CheckedChanged += new System.EventHandler(this.aspectChkBox_CheckedChanged);
            // 
            // projNotesGrpBox
            // 
            this.projNotesGrpBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projNotesGrpBox.Controls.Add(this.projNotesTxtBox);
            this.projNotesGrpBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.projNotesGrpBox.Location = new System.Drawing.Point(6, 285);
            this.projNotesGrpBox.Name = "projNotesGrpBox";
            this.projNotesGrpBox.Size = new System.Drawing.Size(652, 271);
            this.projNotesGrpBox.TabIndex = 1;
            this.projNotesGrpBox.TabStop = false;
            this.projNotesGrpBox.Text = "Notes";
            // 
            // projNotesTxtBox
            // 
            this.projNotesTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projNotesTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.projNotesTxtBox.Location = new System.Drawing.Point(6, 19);
            this.projNotesTxtBox.Multiline = true;
            this.projNotesTxtBox.Name = "projNotesTxtBox";
            this.projNotesTxtBox.Size = new System.Drawing.Size(640, 246);
            this.projNotesTxtBox.TabIndex = 0;
            this.toolTip.SetToolTip(this.projNotesTxtBox, "Installer Comments to be Included at the top of the Script");
            this.projNotesTxtBox.TextChanged += new System.EventHandler(this.projNotesTxtBox_TextChanged);
            this.projNotesTxtBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.projNotesTxtBox_KeyUp);
            // 
            // propsSplitCntnr
            // 
            this.propsSplitCntnr.BackColor = System.Drawing.Color.Transparent;
            this.propsSplitCntnr.Dock = System.Windows.Forms.DockStyle.Top;
            this.propsSplitCntnr.IsSplitterFixed = true;
            this.propsSplitCntnr.Location = new System.Drawing.Point(3, 28);
            this.propsSplitCntnr.Name = "propsSplitCntnr";
            // 
            // propsSplitCntnr.Panel1
            // 
            this.propsSplitCntnr.Panel1.Controls.Add(this.projShortNameGrpBox);
            this.propsSplitCntnr.Panel1.Controls.Add(this.projPathGrpBox);
            this.propsSplitCntnr.Panel1.Controls.Add(this.projPubGrpBox);
            // 
            // propsSplitCntnr.Panel2
            // 
            this.propsSplitCntnr.Panel2.Controls.Add(this.projURLGrpBox);
            this.propsSplitCntnr.Panel2.Controls.Add(this.projNameGrpBox);
            this.propsSplitCntnr.Panel2.Controls.Add(this.projMainEXEGrpBox);
            this.propsSplitCntnr.Size = new System.Drawing.Size(660, 228);
            this.propsSplitCntnr.SplitterDistance = 327;
            this.propsSplitCntnr.TabIndex = 0;
            // 
            // projShortNameGrpBox
            // 
            this.projShortNameGrpBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projShortNameGrpBox.Controls.Add(this.projShortNameInfoTxtBox);
            this.projShortNameGrpBox.Controls.Add(this.projShortNameTxtBox);
            this.projShortNameGrpBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.projShortNameGrpBox.Location = new System.Drawing.Point(6, 80);
            this.projShortNameGrpBox.Name = "projShortNameGrpBox";
            this.projShortNameGrpBox.Size = new System.Drawing.Size(318, 68);
            this.projShortNameGrpBox.TabIndex = 10;
            this.projShortNameGrpBox.TabStop = false;
            this.projShortNameGrpBox.Text = "Game Abbreviated Name *";
            // 
            // projShortNameInfoTxtBox
            // 
            this.projShortNameInfoTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projShortNameInfoTxtBox.BackColor = System.Drawing.SystemColors.Control;
            this.projShortNameInfoTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.projShortNameInfoTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.projShortNameInfoTxtBox.Location = new System.Drawing.Point(9, 19);
            this.projShortNameInfoTxtBox.Name = "projShortNameInfoTxtBox";
            this.projShortNameInfoTxtBox.Size = new System.Drawing.Size(308, 13);
            this.projShortNameInfoTxtBox.TabIndex = 6;
            this.projShortNameInfoTxtBox.Text = "Shortened Project Name:";
            // 
            // projShortNameTxtBox
            // 
            this.projShortNameTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projShortNameTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.projShortNameTxtBox.Location = new System.Drawing.Point(6, 38);
            this.projShortNameTxtBox.Name = "projShortNameTxtBox";
            this.projShortNameTxtBox.Size = new System.Drawing.Size(306, 20);
            this.projShortNameTxtBox.TabIndex = 4;
            this.toolTip.SetToolTip(this.projShortNameTxtBox, "Shortened Form of Name (Should be File System Safe)");
            this.projShortNameTxtBox.TextChanged += new System.EventHandler(this.projShortNameTxtBox_TextChanged);
            // 
            // projPathGrpBox
            // 
            this.projPathGrpBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projPathGrpBox.Controls.Add(this.projPathInfoTxtBox);
            this.projPathGrpBox.Controls.Add(this.projPathBrowseBtn);
            this.projPathGrpBox.Controls.Add(this.projPathTxtBox);
            this.projPathGrpBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.projPathGrpBox.Location = new System.Drawing.Point(6, 6);
            this.projPathGrpBox.Name = "projPathGrpBox";
            this.projPathGrpBox.Size = new System.Drawing.Size(318, 68);
            this.projPathGrpBox.TabIndex = 9;
            this.projPathGrpBox.TabStop = false;
            this.projPathGrpBox.Text = "Project Path *";
            // 
            // projPathInfoTxtBox
            // 
            this.projPathInfoTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projPathInfoTxtBox.BackColor = System.Drawing.SystemColors.Control;
            this.projPathInfoTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.projPathInfoTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.projPathInfoTxtBox.Location = new System.Drawing.Point(9, 16);
            this.projPathInfoTxtBox.Name = "projPathInfoTxtBox";
            this.projPathInfoTxtBox.Size = new System.Drawing.Size(307, 13);
            this.projPathInfoTxtBox.TabIndex = 3;
            this.projPathInfoTxtBox.Text = "Set Your Project\'s Working Folder:";
            // 
            // projPathBrowseBtn
            // 
            this.projPathBrowseBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.projPathBrowseBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.projPathBrowseBtn.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.projPathBrowseBtn.Location = new System.Drawing.Point(285, 36);
            this.projPathBrowseBtn.Name = "projPathBrowseBtn";
            this.projPathBrowseBtn.Size = new System.Drawing.Size(27, 23);
            this.projPathBrowseBtn.TabIndex = 2;
            this.projPathBrowseBtn.Text = "...";
            this.toolTip.SetToolTip(this.projPathBrowseBtn, "Browse to Project Path");
            this.projPathBrowseBtn.UseVisualStyleBackColor = true;
            this.projPathBrowseBtn.Click += new System.EventHandler(this.projPathBrowseBtn_Click);
            // 
            // projPathTxtBox
            // 
            this.projPathTxtBox.AllowDrop = true;
            this.projPathTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projPathTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.projPathTxtBox.Location = new System.Drawing.Point(6, 38);
            this.projPathTxtBox.Name = "projPathTxtBox";
            this.projPathTxtBox.Size = new System.Drawing.Size(273, 20);
            this.projPathTxtBox.TabIndex = 1;
            this.toolTip.SetToolTip(this.projPathTxtBox, "Enter or Browse to Project Path");
            this.projPathTxtBox.TextChanged += new System.EventHandler(this.projPathTxtBox_TextChanged);
            this.projPathTxtBox.DragDrop += new System.Windows.Forms.DragEventHandler(this.projPathTxtBox_DragDrop);
            this.projPathTxtBox.DragEnter += new System.Windows.Forms.DragEventHandler(this.projPathTxtBox_DragEnter);
            // 
            // projPubGrpBox
            // 
            this.projPubGrpBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projPubGrpBox.Controls.Add(this.projPubInfoTxtBox);
            this.projPubGrpBox.Controls.Add(this.projPubTxtBox);
            this.projPubGrpBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.projPubGrpBox.Location = new System.Drawing.Point(6, 154);
            this.projPubGrpBox.Name = "projPubGrpBox";
            this.projPubGrpBox.Size = new System.Drawing.Size(318, 70);
            this.projPubGrpBox.TabIndex = 16;
            this.projPubGrpBox.TabStop = false;
            this.projPubGrpBox.Text = "Project Publisher";
            // 
            // projPubInfoTxtBox
            // 
            this.projPubInfoTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projPubInfoTxtBox.BackColor = System.Drawing.SystemColors.Control;
            this.projPubInfoTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.projPubInfoTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.projPubInfoTxtBox.Location = new System.Drawing.Point(9, 19);
            this.projPubInfoTxtBox.Name = "projPubInfoTxtBox";
            this.projPubInfoTxtBox.Size = new System.Drawing.Size(307, 13);
            this.projPubInfoTxtBox.TabIndex = 7;
            this.projPubInfoTxtBox.Text = "Set the Game\'s Devloper or Publisher Name:";
            // 
            // projPubTxtBox
            // 
            this.projPubTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projPubTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.projPubTxtBox.Location = new System.Drawing.Point(9, 40);
            this.projPubTxtBox.Name = "projPubTxtBox";
            this.projPubTxtBox.Size = new System.Drawing.Size(303, 20);
            this.projPubTxtBox.TabIndex = 6;
            this.toolTip.SetToolTip(this.projPubTxtBox, "Sets the Devloper or Publisher Name of Project");
            this.projPubTxtBox.TextChanged += new System.EventHandler(this.projPubTxtBox_TextChanged);
            // 
            // projURLGrpBox
            // 
            this.projURLGrpBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projURLGrpBox.Controls.Add(this.projURLInfoTxtBox);
            this.projURLGrpBox.Controls.Add(this.projURLTxtBox);
            this.projURLGrpBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.projURLGrpBox.Location = new System.Drawing.Point(5, 154);
            this.projURLGrpBox.Name = "projURLGrpBox";
            this.projURLGrpBox.Size = new System.Drawing.Size(319, 70);
            this.projURLGrpBox.TabIndex = 50;
            this.projURLGrpBox.TabStop = false;
            this.projURLGrpBox.Text = "Publisher Website";
            // 
            // projURLInfoTxtBox
            // 
            this.projURLInfoTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projURLInfoTxtBox.BackColor = System.Drawing.SystemColors.Control;
            this.projURLInfoTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.projURLInfoTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.projURLInfoTxtBox.Location = new System.Drawing.Point(9, 19);
            this.projURLInfoTxtBox.Name = "projURLInfoTxtBox";
            this.projURLInfoTxtBox.Size = new System.Drawing.Size(309, 13);
            this.projURLInfoTxtBox.TabIndex = 7;
            this.projURLInfoTxtBox.Text = "Set address to Devloper\'s or Publisher\'s website";
            // 
            // projURLTxtBox
            // 
            this.projURLTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projURLTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.projURLTxtBox.Location = new System.Drawing.Point(7, 40);
            this.projURLTxtBox.Name = "projURLTxtBox";
            this.projURLTxtBox.Size = new System.Drawing.Size(306, 20);
            this.projURLTxtBox.TabIndex = 6;
            this.toolTip.SetToolTip(this.projURLTxtBox, "Sets the URL to Support Website");
            this.projURLTxtBox.TextChanged += new System.EventHandler(this.projURLTxtBox_TextChanged);
            this.projURLTxtBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.projURLTxtBox_KeyDown);
            this.projURLTxtBox.Leave += new System.EventHandler(this.projURLTxtBox_Leave);
            // 
            // projNameGrpBox
            // 
            this.projNameGrpBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projNameGrpBox.Controls.Add(this.projNameInfoTxtBox);
            this.projNameGrpBox.Controls.Add(this.projNameTxtBox);
            this.projNameGrpBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold);
            this.projNameGrpBox.Location = new System.Drawing.Point(5, 6);
            this.projNameGrpBox.Name = "projNameGrpBox";
            this.projNameGrpBox.Size = new System.Drawing.Size(319, 68);
            this.projNameGrpBox.TabIndex = 13;
            this.projNameGrpBox.TabStop = false;
            this.projNameGrpBox.Text = "Game Name *";
            // 
            // projNameInfoTxtBox
            // 
            this.projNameInfoTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projNameInfoTxtBox.BackColor = System.Drawing.SystemColors.Control;
            this.projNameInfoTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.projNameInfoTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.projNameInfoTxtBox.Location = new System.Drawing.Point(9, 20);
            this.projNameInfoTxtBox.Name = "projNameInfoTxtBox";
            this.projNameInfoTxtBox.Size = new System.Drawing.Size(308, 13);
            this.projNameInfoTxtBox.TabIndex = 5;
            this.projNameInfoTxtBox.Text = "Project game name:";
            // 
            // projNameTxtBox
            // 
            this.projNameTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projNameTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.projNameTxtBox.Location = new System.Drawing.Point(6, 38);
            this.projNameTxtBox.Name = "projNameTxtBox";
            this.projNameTxtBox.Size = new System.Drawing.Size(307, 20);
            this.projNameTxtBox.TabIndex = 0;
            this.toolTip.SetToolTip(this.projNameTxtBox, "Sets the Name of Game to be Displayed");
            this.projNameTxtBox.TextChanged += new System.EventHandler(this.projNameTxtBox_TextChanged);
            // 
            // projMainEXEGrpBox
            // 
            this.projMainEXEGrpBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projMainEXEGrpBox.Controls.Add(this.projMainEXEInfoTxtBox);
            this.projMainEXEGrpBox.Controls.Add(this.projMainEXETxtBox);
            this.projMainEXEGrpBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.projMainEXEGrpBox.Location = new System.Drawing.Point(4, 80);
            this.projMainEXEGrpBox.Name = "projMainEXEGrpBox";
            this.projMainEXEGrpBox.Size = new System.Drawing.Size(320, 68);
            this.projMainEXEGrpBox.TabIndex = 49;
            this.projMainEXEGrpBox.TabStop = false;
            this.projMainEXEGrpBox.Text = "Game Launcher *";
            // 
            // projMainEXEInfoTxtBox
            // 
            this.projMainEXEInfoTxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projMainEXEInfoTxtBox.BackColor = System.Drawing.SystemColors.Control;
            this.projMainEXEInfoTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.projMainEXEInfoTxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.projMainEXEInfoTxtBox.Location = new System.Drawing.Point(6, 19);
            this.projMainEXEInfoTxtBox.Name = "projMainEXEInfoTxtBox";
            this.projMainEXEInfoTxtBox.Size = new System.Drawing.Size(276, 13);
            this.projMainEXEInfoTxtBox.TabIndex = 48;
            this.projMainEXEInfoTxtBox.Text = "Name of the Windows Launcher:";
            // 
            // projMainEXETxtBox
            // 
            this.projMainEXETxtBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.projMainEXETxtBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.projMainEXETxtBox.Location = new System.Drawing.Point(6, 38);
            this.projMainEXETxtBox.Name = "projMainEXETxtBox";
            this.projMainEXETxtBox.Size = new System.Drawing.Size(308, 20);
            this.projMainEXETxtBox.TabIndex = 47;
            this.toolTip.SetToolTip(this.projMainEXETxtBox, "Sets the Name of the File that Will Launch the Game in DOSBox.");
            this.projMainEXETxtBox.TextChanged += new System.EventHandler(this.projMainEXETxtBox_TextChanged);
            this.projMainEXETxtBox.Leave += new System.EventHandler(this.projMainEXETxtBox_Leave);
            // 
            // propsToolbar
            // 
            this.propsToolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.clearBtn,
            this.toolbarSeparator3,
            this.projEngCmboBox,
            this.toolbarSeparator4,
            this.publishBtn,
            this.toolbarSeparator5});
            this.propsToolbar.Location = new System.Drawing.Point(3, 3);
            this.propsToolbar.Name = "propsToolbar";
            this.propsToolbar.Size = new System.Drawing.Size(660, 25);
            this.propsToolbar.TabIndex = 2;
            this.propsToolbar.Text = "propsToolbar";
            // 
            // clearBtn
            // 
            this.clearBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.clearBtn.Image = global::NSISPublisher.Properties.Resources.Clear;
            this.clearBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clearBtn.Name = "clearBtn";
            this.clearBtn.Size = new System.Drawing.Size(23, 22);
            this.clearBtn.Text = "Clear All";
            this.clearBtn.ToolTipText = "Clear All Fields";
            this.clearBtn.Visible = false;
            this.clearBtn.Click += new System.EventHandler(this.clearBtn_Click);
            // 
            // toolbarSeparator3
            // 
            this.toolbarSeparator3.Name = "toolbarSeparator3";
            this.toolbarSeparator3.Size = new System.Drawing.Size(6, 25);
            this.toolbarSeparator3.Visible = false;
            // 
            // projEngCmboBox
            // 
            this.projEngCmboBox.Items.AddRange(new object[] {
            "AGI",
            "SCI0 Parser Interface",
            "SCI0 Parser Interface with sciAudio",
            "SCI0 Point and Click interface",
            "SCI0 Point and Click interface with sciAudio",
            "SCI1",
            "SCI1.1"});
            this.projEngCmboBox.Name = "projEngCmboBox";
            this.projEngCmboBox.Size = new System.Drawing.Size(255, 25);
            this.projEngCmboBox.Text = "Game Template *";
            this.projEngCmboBox.ToolTipText = "Select Project\'s Template Used";
            // 
            // toolbarSeparator4
            // 
            this.toolbarSeparator4.Name = "toolbarSeparator4";
            this.toolbarSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // publishBtn
            // 
            this.publishBtn.Image = global::NSISPublisher.Properties.Resources.NSIS;
            this.publishBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.publishBtn.Name = "publishBtn";
            this.publishBtn.Size = new System.Drawing.Size(66, 22);
            this.publishBtn.Text = "Publish";
            this.publishBtn.ToolTipText = "Create NSI Script";
            this.publishBtn.Click += new System.EventHandler(this.publishBtn_Click);
            // 
            // toolbarSeparator5
            // 
            this.toolbarSeparator5.Name = "toolbarSeparator5";
            this.toolbarSeparator5.Size = new System.Drawing.Size(6, 25);
            this.toolbarSeparator5.Visible = false;
            // 
            // scriptTabPg
            // 
            this.scriptTabPg.Controls.Add(this.toolbarContainer);
            this.scriptTabPg.Location = new System.Drawing.Point(4, 22);
            this.scriptTabPg.Name = "scriptTabPg";
            this.scriptTabPg.Padding = new System.Windows.Forms.Padding(3);
            this.scriptTabPg.Size = new System.Drawing.Size(666, 565);
            this.scriptTabPg.TabIndex = 1;
            this.scriptTabPg.Text = "NSI Script Editor";
            this.scriptTabPg.UseVisualStyleBackColor = true;
            // 
            // toolbarContainer
            // 
            // 
            // toolbarContainer.ContentPanel
            // 
            this.toolbarContainer.ContentPanel.Controls.Add(this.splitContainer);
            this.toolbarContainer.ContentPanel.Size = new System.Drawing.Size(660, 534);
            this.toolbarContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolbarContainer.Location = new System.Drawing.Point(3, 3);
            this.toolbarContainer.Name = "toolbarContainer";
            this.toolbarContainer.Size = new System.Drawing.Size(660, 559);
            this.toolbarContainer.TabIndex = 10;
            this.toolbarContainer.Text = "toolStripContainer1";
            // 
            // toolbarContainer.TopToolStripPanel
            // 
            this.toolbarContainer.TopToolStripPanel.Controls.Add(this.scriptEditToolbar);
            this.toolbarContainer.TopToolStripPanel.Controls.Add(this.buildToolbar);
            this.toolbarContainer.TopToolStripPanel.Controls.Add(this.findToolbar);
            // 
            // findToolbar
            // 
            this.findToolbar.BackColor = System.Drawing.Color.Transparent;
            this.findToolbar.Dock = System.Windows.Forms.DockStyle.None;
            this.findToolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.findBtn,
            this.findNextBtn,
            this.replaceBtn,
            this.goToBtn});
            this.findToolbar.Location = new System.Drawing.Point(361, 0);
            this.findToolbar.Name = "findToolbar";
            this.findToolbar.Size = new System.Drawing.Size(104, 25);
            this.findToolbar.TabIndex = 7;
            this.findToolbar.Text = "Find Toolbar";
            // 
            // findBtn
            // 
            this.findBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.findBtn.Enabled = false;
            this.findBtn.Image = global::NSISPublisher.Properties.Resources.Find;
            this.findBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.findBtn.Name = "findBtn";
            this.findBtn.Size = new System.Drawing.Size(23, 22);
            this.findBtn.Text = "Find";
            this.findBtn.ToolTipText = "Find in Script";
            this.findBtn.Click += new System.EventHandler(this.findBtn_Click);
            // 
            // findNextBtn
            // 
            this.findNextBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.findNextBtn.Enabled = false;
            this.findNextBtn.Image = global::NSISPublisher.Properties.Resources.FindNext;
            this.findNextBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.findNextBtn.Name = "findNextBtn";
            this.findNextBtn.Size = new System.Drawing.Size(23, 22);
            this.findNextBtn.Text = "Find Next";
            this.findNextBtn.Click += new System.EventHandler(this.findNextBtn_Click);
            // 
            // replaceBtn
            // 
            this.replaceBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.replaceBtn.Enabled = false;
            this.replaceBtn.Image = global::NSISPublisher.Properties.Resources.Replace;
            this.replaceBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.replaceBtn.Name = "replaceBtn";
            this.replaceBtn.Size = new System.Drawing.Size(23, 22);
            this.replaceBtn.Text = "Replace";
            this.replaceBtn.ToolTipText = "Replace in Script";
            this.replaceBtn.Click += new System.EventHandler(this.replaceBtn_Click);
            // 
            // goToBtn
            // 
            this.goToBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.goToBtn.Enabled = false;
            this.goToBtn.Image = global::NSISPublisher.Properties.Resources.GoTo;
            this.goToBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.goToBtn.Name = "goToBtn";
            this.goToBtn.Size = new System.Drawing.Size(23, 22);
            this.goToBtn.Text = "Go To...";
            this.goToBtn.Click += new System.EventHandler(this.goToBtn_Click);
            // 
            // buildToolbar
            // 
            this.buildToolbar.BackColor = System.Drawing.Color.Transparent;
            this.buildToolbar.Dock = System.Windows.Forms.DockStyle.None;
            this.buildToolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.compileBtn,
            this.compileDebugBtn,
            this.stopcompileBtn,
            this.debugBtn});
            this.buildToolbar.Location = new System.Drawing.Point(257, 0);
            this.buildToolbar.Name = "buildToolbar";
            this.buildToolbar.Size = new System.Drawing.Size(104, 25);
            this.buildToolbar.TabIndex = 9;
            this.buildToolbar.Text = "Compile Toolbar";
            // 
            // compileBtn
            // 
            this.compileBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.compileBtn.Enabled = false;
            this.compileBtn.Image = ((System.Drawing.Image)(resources.GetObject("compileBtn.Image")));
            this.compileBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.compileBtn.Name = "compileBtn";
            this.compileBtn.Size = new System.Drawing.Size(23, 22);
            this.compileBtn.Text = "Compile";
            this.compileBtn.ToolTipText = "Compile Script";
            this.compileBtn.Click += new System.EventHandler(this.compileBtn_Click);
            // 
            // compileDebugBtn
            // 
            this.compileDebugBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.compileDebugBtn.Enabled = false;
            this.compileDebugBtn.Image = global::NSISPublisher.Properties.Resources.BuildDebug;
            this.compileDebugBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.compileDebugBtn.Name = "compileDebugBtn";
            this.compileDebugBtn.Size = new System.Drawing.Size(23, 22);
            this.compileDebugBtn.Text = "Compile and Debug";
            this.compileDebugBtn.ToolTipText = "Compile and Debug Script";
            this.compileDebugBtn.Click += new System.EventHandler(this.compileDebugBtn_Click);
            // 
            // stopcompileBtn
            // 
            this.stopcompileBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stopcompileBtn.Enabled = false;
            this.stopcompileBtn.Image = global::NSISPublisher.Properties.Resources.BuildStop;
            this.stopcompileBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stopcompileBtn.Name = "stopcompileBtn";
            this.stopcompileBtn.Size = new System.Drawing.Size(23, 22);
            this.stopcompileBtn.Text = "Stop Compile";
            this.stopcompileBtn.Click += new System.EventHandler(this.CompileStopBtn_Click);
            // 
            // debugBtn
            // 
            this.debugBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.debugBtn.Enabled = false;
            this.debugBtn.Image = global::NSISPublisher.Properties.Resources.Debug;
            this.debugBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.debugBtn.Name = "debugBtn";
            this.debugBtn.Size = new System.Drawing.Size(23, 22);
            this.debugBtn.Text = "Debug Installer";
            this.debugBtn.ToolTipText = "Debug installer";
            this.debugBtn.Click += new System.EventHandler(this.debugBtn_Click);
            // 
            // scriptEditToolbar
            // 
            this.scriptEditToolbar.BackColor = System.Drawing.Color.Transparent;
            this.scriptEditToolbar.Dock = System.Windows.Forms.DockStyle.None;
            this.scriptEditToolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lockBtn,
            this.scriptPropsBtn,
            this.toolbarSeparator6,
            this.cutBtn,
            this.copyBtn,
            this.pasteBtn,
            this.toolbarSeparator7,
            this.undoBtn,
            this.redoBtn,
            this.selectAllBtn,
            this.deleteBtn,
            this.clearScriptBtn});
            this.scriptEditToolbar.Location = new System.Drawing.Point(3, 0);
            this.scriptEditToolbar.Name = "scriptEditToolbar";
            this.scriptEditToolbar.Size = new System.Drawing.Size(254, 25);
            this.scriptEditToolbar.TabIndex = 6;
            this.scriptEditToolbar.Text = "Edit Toolbar";
            // 
            // lockBtn
            // 
            this.lockBtn.CheckOnClick = true;
            this.lockBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.lockBtn.Image = global::NSISPublisher.Properties.Resources.UnlockScript;
            this.lockBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.lockBtn.Name = "lockBtn";
            this.lockBtn.Size = new System.Drawing.Size(23, 22);
            this.lockBtn.Text = "Lock";
            this.lockBtn.ToolTipText = "Make Script ReadOnly";
            this.lockBtn.CheckStateChanged += new System.EventHandler(this.lockBtn_CheckStateChanged);
            // 
            // scriptPropsBtn
            // 
            this.scriptPropsBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.scriptPropsBtn.Enabled = false;
            this.scriptPropsBtn.Image = global::NSISPublisher.Properties.Resources.Properties;
            this.scriptPropsBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.scriptPropsBtn.Name = "scriptPropsBtn";
            this.scriptPropsBtn.Size = new System.Drawing.Size(23, 22);
            this.scriptPropsBtn.Text = "Properties";
            this.scriptPropsBtn.ToolTipText = "Script Properties";
            this.scriptPropsBtn.Click += new System.EventHandler(this.scriptPropsBtn_Click);
            // 
            // toolbarSeparator6
            // 
            this.toolbarSeparator6.Name = "toolbarSeparator6";
            this.toolbarSeparator6.Size = new System.Drawing.Size(6, 25);
            // 
            // cutBtn
            // 
            this.cutBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.cutBtn.Enabled = false;
            this.cutBtn.Image = ((System.Drawing.Image)(resources.GetObject("cutBtn.Image")));
            this.cutBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.cutBtn.Name = "cutBtn";
            this.cutBtn.Size = new System.Drawing.Size(23, 22);
            this.cutBtn.Text = "Cut";
            this.cutBtn.Click += new System.EventHandler(this.cutBtn_Click);
            // 
            // copyBtn
            // 
            this.copyBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.copyBtn.Enabled = false;
            this.copyBtn.Image = ((System.Drawing.Image)(resources.GetObject("copyBtn.Image")));
            this.copyBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.copyBtn.Name = "copyBtn";
            this.copyBtn.Size = new System.Drawing.Size(23, 22);
            this.copyBtn.Text = "Copy";
            this.copyBtn.Click += new System.EventHandler(this.copyBtn_Click);
            // 
            // pasteBtn
            // 
            this.pasteBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.pasteBtn.Enabled = false;
            this.pasteBtn.Image = ((System.Drawing.Image)(resources.GetObject("pasteBtn.Image")));
            this.pasteBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.pasteBtn.Name = "pasteBtn";
            this.pasteBtn.Size = new System.Drawing.Size(23, 22);
            this.pasteBtn.Text = "Paste";
            this.pasteBtn.Click += new System.EventHandler(this.pasteBtn_Click);
            // 
            // toolbarSeparator7
            // 
            this.toolbarSeparator7.Name = "toolbarSeparator7";
            this.toolbarSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // undoBtn
            // 
            this.undoBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.undoBtn.Enabled = false;
            this.undoBtn.Image = ((System.Drawing.Image)(resources.GetObject("undoBtn.Image")));
            this.undoBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.undoBtn.Name = "undoBtn";
            this.undoBtn.Size = new System.Drawing.Size(23, 22);
            this.undoBtn.Text = "Undo";
            this.undoBtn.ToolTipText = "Undo";
            this.undoBtn.Click += new System.EventHandler(this.undoBtnBtn_Click);
            // 
            // redoBtn
            // 
            this.redoBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.redoBtn.Enabled = false;
            this.redoBtn.Image = ((System.Drawing.Image)(resources.GetObject("redoBtn.Image")));
            this.redoBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.redoBtn.Name = "redoBtn";
            this.redoBtn.Size = new System.Drawing.Size(23, 22);
            this.redoBtn.Text = "Redo";
            this.redoBtn.ToolTipText = "Redo";
            this.redoBtn.Click += new System.EventHandler(this.redoBtn_Click);
            // 
            // selectAllBtn
            // 
            this.selectAllBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.selectAllBtn.Enabled = false;
            this.selectAllBtn.Image = ((System.Drawing.Image)(resources.GetObject("selectAllBtn.Image")));
            this.selectAllBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.selectAllBtn.Name = "selectAllBtn";
            this.selectAllBtn.Size = new System.Drawing.Size(23, 22);
            this.selectAllBtn.Text = "Select All";
            this.selectAllBtn.ToolTipText = "Select All";
            this.selectAllBtn.Click += new System.EventHandler(this.selectAllBtn_Click);
            // 
            // deleteBtn
            // 
            this.deleteBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.deleteBtn.Enabled = false;
            this.deleteBtn.Image = ((System.Drawing.Image)(resources.GetObject("deleteBtn.Image")));
            this.deleteBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteBtn.Name = "deleteBtn";
            this.deleteBtn.Size = new System.Drawing.Size(23, 22);
            this.deleteBtn.Text = "Delete";
            this.deleteBtn.ToolTipText = "Delete";
            this.deleteBtn.Click += new System.EventHandler(this.deleteBtn_Click);
            // 
            // clearScriptBtn
            // 
            this.clearScriptBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.clearScriptBtn.Enabled = false;
            this.clearScriptBtn.Image = global::NSISPublisher.Properties.Resources.Clear;
            this.clearScriptBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.clearScriptBtn.Name = "clearScriptBtn";
            this.clearScriptBtn.Size = new System.Drawing.Size(23, 22);
            this.clearScriptBtn.Text = "Clear All";
            this.clearScriptBtn.Click += new System.EventHandler(this.clearScriptBtn_Click);
            // 
            // helpProvider
            // 
            this.helpProvider.HelpNamespace = "Documents\\NSIS Publisher Help.chm";
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // mainToolbar
            // 
            this.mainToolbar.BackColor = System.Drawing.Color.Transparent;
            this.mainToolbar.BackgroundImage = global::NSISPublisher.Properties.Resources.TB_BG;
            this.mainToolbar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newBtn,
            this.openBtn,
            this.saveBtn,
            this.saveAsBtn,
            this.toolbarSeparator1,
            this.openIncludeBtn,
            this.licenseBtn,
            this.readmeBtn,
            this.toolbarSeparator2,
            this.helpBtn});
            this.mainToolbar.Location = new System.Drawing.Point(0, 24);
            this.mainToolbar.Name = "mainToolbar";
            this.mainToolbar.Size = new System.Drawing.Size(674, 25);
            this.mainToolbar.TabIndex = 5;
            this.mainToolbar.Text = "Main Toolbar";
            // 
            // newBtn
            // 
            this.newBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.newBtn.Image = ((System.Drawing.Image)(resources.GetObject("newBtn.Image")));
            this.newBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newBtn.Name = "newBtn";
            this.newBtn.Size = new System.Drawing.Size(23, 22);
            this.newBtn.Text = "New";
            this.newBtn.Click += new System.EventHandler(this.newBtn_Click);
            // 
            // openBtn
            // 
            this.openBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openBtn.Image = ((System.Drawing.Image)(resources.GetObject("openBtn.Image")));
            this.openBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openBtn.Name = "openBtn";
            this.openBtn.Size = new System.Drawing.Size(23, 22);
            this.openBtn.Text = "Open";
            this.openBtn.ToolTipText = "Open Script";
            this.openBtn.Click += new System.EventHandler(this.openBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveBtn.Enabled = false;
            this.saveBtn.Image = ((System.Drawing.Image)(resources.GetObject("saveBtn.Image")));
            this.saveBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(23, 22);
            this.saveBtn.Text = "Save";
            this.saveBtn.ToolTipText = "Save Script";
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // saveAsBtn
            // 
            this.saveAsBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveAsBtn.Image = global::NSISPublisher.Properties.Resources.SaveAs;
            this.saveAsBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveAsBtn.Name = "saveAsBtn";
            this.saveAsBtn.Size = new System.Drawing.Size(23, 22);
            this.saveAsBtn.Text = "Save As...";
            this.saveAsBtn.ToolTipText = "Save Script As...";
            this.saveAsBtn.Click += new System.EventHandler(this.saveAsBtn_Click);
            // 
            // toolbarSeparator1
            // 
            this.toolbarSeparator1.Name = "toolbarSeparator1";
            this.toolbarSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // openIncludeBtn
            // 
            this.openIncludeBtn.Image = global::NSISPublisher.Properties.Resources.Folder;
            this.openIncludeBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openIncludeBtn.Name = "openIncludeBtn";
            this.openIncludeBtn.Size = new System.Drawing.Size(71, 22);
            this.openIncludeBtn.Text = "Includes";
            this.openIncludeBtn.ToolTipText = "Open Installer Includes Folder";
            this.openIncludeBtn.Click += new System.EventHandler(this.openIncludeBtn_Click);
            // 
            // licenseBtn
            // 
            this.licenseBtn.Image = global::NSISPublisher.Properties.Resources.License;
            this.licenseBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.licenseBtn.Name = "licenseBtn";
            this.licenseBtn.Size = new System.Drawing.Size(66, 22);
            this.licenseBtn.Text = "License";
            this.licenseBtn.ToolTipText = "Open License";
            this.licenseBtn.Click += new System.EventHandler(this.openLicenseBtn_Click);
            // 
            // readmeBtn
            // 
            this.readmeBtn.Image = global::NSISPublisher.Properties.Resources.Readme;
            this.readmeBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.readmeBtn.Name = "readmeBtn";
            this.readmeBtn.Size = new System.Drawing.Size(70, 22);
            this.readmeBtn.Text = "Readme";
            this.readmeBtn.ToolTipText = "Edit your game\'s Readme";
            this.readmeBtn.Click += new System.EventHandler(this.editReadmeBtn_Click);
            // 
            // toolbarSeparator2
            // 
            this.toolbarSeparator2.Name = "toolbarSeparator2";
            this.toolbarSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // helpBtn
            // 
            this.helpBtn.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.helpBtn.Image = global::NSISPublisher.Properties.Resources.Help;
            this.helpBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.helpBtn.Name = "helpBtn";
            this.helpBtn.Size = new System.Drawing.Size(23, 22);
            this.helpBtn.Text = "Help";
            this.helpBtn.Click += new System.EventHandler(this.helpBtn_Click);
            // 
            // menubar
            // 
            this.menubar.BackgroundImage = global::NSISPublisher.Properties.Resources.MenuBG;
            this.menubar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.editMenu,
            this.buildMenu,
            this.formatMenu,
            this.viewMenu,
            this.toolsMenu,
            this.helpMenu});
            this.menubar.Location = new System.Drawing.Point(0, 0);
            this.menubar.Name = "menubar";
            this.menubar.Size = new System.Drawing.Size(674, 24);
            this.menubar.TabIndex = 4;
            this.menubar.Text = "menubar";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenuNew,
            this.fileMenuOpenScript,
            this.menubarSeparator1,
            this.fileMenuSave,
            this.fileMenuSaveAs,
            this.fileMenuSaveAll,
            this.fileMenuOpenConf,
            this.menubarSeparator2,
            this.fileMenuPageSetup,
            this.fileMenuPrintPreview,
            this.fileMenuPrint,
            this.menubarSeparator3,
            this.fileMenuExit});
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "&File";
            // 
            // fileMenuNew
            // 
            this.fileMenuNew.Image = global::NSISPublisher.Properties.Resources.New;
            this.fileMenuNew.Name = "fileMenuNew";
            this.fileMenuNew.Size = new System.Drawing.Size(187, 22);
            this.fileMenuNew.Text = "New";
            this.fileMenuNew.Click += new System.EventHandler(this.fileMenuNew_Click);
            // 
            // fileMenuOpenScript
            // 
            this.fileMenuOpenScript.Image = ((System.Drawing.Image)(resources.GetObject("fileMenuOpenScript.Image")));
            this.fileMenuOpenScript.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fileMenuOpenScript.Name = "fileMenuOpenScript";
            this.fileMenuOpenScript.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.fileMenuOpenScript.Size = new System.Drawing.Size(187, 22);
            this.fileMenuOpenScript.Text = "&Open";
            this.fileMenuOpenScript.Click += new System.EventHandler(this.fileMenuOpen_Click);
            // 
            // menubarSeparator1
            // 
            this.menubarSeparator1.Name = "menubarSeparator1";
            this.menubarSeparator1.Size = new System.Drawing.Size(184, 6);
            // 
            // fileMenuSave
            // 
            this.fileMenuSave.Enabled = false;
            this.fileMenuSave.Image = ((System.Drawing.Image)(resources.GetObject("fileMenuSave.Image")));
            this.fileMenuSave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fileMenuSave.Name = "fileMenuSave";
            this.fileMenuSave.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.fileMenuSave.Size = new System.Drawing.Size(187, 22);
            this.fileMenuSave.Text = "&Save";
            this.fileMenuSave.Click += new System.EventHandler(this.fileMenuSave_Click);
            // 
            // fileMenuSaveAs
            // 
            this.fileMenuSaveAs.Image = global::NSISPublisher.Properties.Resources.SaveAs;
            this.fileMenuSaveAs.Name = "fileMenuSaveAs";
            this.fileMenuSaveAs.Size = new System.Drawing.Size(187, 22);
            this.fileMenuSaveAs.Text = "Save &As";
            this.fileMenuSaveAs.Click += new System.EventHandler(this.fileMenuSaveAs_Click);
            // 
            // fileMenuSaveAll
            // 
            this.fileMenuSaveAll.Enabled = false;
            this.fileMenuSaveAll.Image = global::NSISPublisher.Properties.Resources.SaveAll;
            this.fileMenuSaveAll.Name = "fileMenuSaveAll";
            this.fileMenuSaveAll.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.fileMenuSaveAll.Size = new System.Drawing.Size(187, 22);
            this.fileMenuSaveAll.Text = "Save A&ll";
            // 
            // fileMenuOpenConf
            // 
            this.fileMenuOpenConf.Name = "fileMenuOpenConf";
            this.fileMenuOpenConf.Size = new System.Drawing.Size(187, 22);
            this.fileMenuOpenConf.Text = "Open Conf";
            this.fileMenuOpenConf.Click += new System.EventHandler(this.fileMenuOpenConf_Click);
            // 
            // menubarSeparator2
            // 
            this.menubarSeparator2.Name = "menubarSeparator2";
            this.menubarSeparator2.Size = new System.Drawing.Size(184, 6);
            // 
            // fileMenuPageSetup
            // 
            this.fileMenuPageSetup.Enabled = false;
            this.fileMenuPageSetup.Image = global::NSISPublisher.Properties.Resources.PageSetup;
            this.fileMenuPageSetup.Name = "fileMenuPageSetup";
            this.fileMenuPageSetup.Size = new System.Drawing.Size(187, 22);
            this.fileMenuPageSetup.Text = "Page Set&up...";
            this.fileMenuPageSetup.Click += new System.EventHandler(this.fileMenuPageSetup_Click);
            // 
            // fileMenuPrintPreview
            // 
            this.fileMenuPrintPreview.Enabled = false;
            this.fileMenuPrintPreview.Image = ((System.Drawing.Image)(resources.GetObject("fileMenuPrintPreview.Image")));
            this.fileMenuPrintPreview.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fileMenuPrintPreview.Name = "fileMenuPrintPreview";
            this.fileMenuPrintPreview.Size = new System.Drawing.Size(187, 22);
            this.fileMenuPrintPreview.Text = "Print Pre&view";
            this.fileMenuPrintPreview.Click += new System.EventHandler(this.fileMenuPrintPreview_Click);
            // 
            // fileMenuPrint
            // 
            this.fileMenuPrint.Enabled = false;
            this.fileMenuPrint.Image = ((System.Drawing.Image)(resources.GetObject("fileMenuPrint.Image")));
            this.fileMenuPrint.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.fileMenuPrint.Name = "fileMenuPrint";
            this.fileMenuPrint.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.P)));
            this.fileMenuPrint.Size = new System.Drawing.Size(187, 22);
            this.fileMenuPrint.Text = "&Print";
            this.fileMenuPrint.Click += new System.EventHandler(this.fileMenuPrint_Click);
            // 
            // menubarSeparator3
            // 
            this.menubarSeparator3.Name = "menubarSeparator3";
            this.menubarSeparator3.Size = new System.Drawing.Size(184, 6);
            // 
            // fileMenuExit
            // 
            this.fileMenuExit.Image = global::NSISPublisher.Properties.Resources.Exit;
            this.fileMenuExit.Name = "fileMenuExit";
            this.fileMenuExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F4)));
            this.fileMenuExit.Size = new System.Drawing.Size(187, 22);
            this.fileMenuExit.Text = "E&xit";
            this.fileMenuExit.Click += new System.EventHandler(this.fileMenuExit_Click);
            // 
            // editMenu
            // 
            this.editMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editMenuUndo,
            this.editMenuRedo,
            this.menubarSeparator4,
            this.editMenuCut,
            this.editMenuCopy,
            this.editMenuPaste,
            this.editMenuDelete,
            this.menubarSeparator5,
            this.editMenuFind,
            this.editMenuFindNext,
            this.editMenuReplace,
            this.editMenuGoTo,
            this.menubarSeparator6,
            this.editMenuSelectAll,
            this.editMenuClear,
            this.editMenuTimeDate});
            this.editMenu.Name = "editMenu";
            this.editMenu.Size = new System.Drawing.Size(39, 20);
            this.editMenu.Text = "&Edit";
            // 
            // editMenuUndo
            // 
            this.editMenuUndo.Enabled = false;
            this.editMenuUndo.Image = global::NSISPublisher.Properties.Resources.Undo;
            this.editMenuUndo.Name = "editMenuUndo";
            this.editMenuUndo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Z)));
            this.editMenuUndo.Size = new System.Drawing.Size(149, 22);
            this.editMenuUndo.Text = "&Undo";
            this.editMenuUndo.Click += new System.EventHandler(this.editMenuUndo_Click);
            // 
            // editMenuRedo
            // 
            this.editMenuRedo.Enabled = false;
            this.editMenuRedo.Image = global::NSISPublisher.Properties.Resources.Redo;
            this.editMenuRedo.Name = "editMenuRedo";
            this.editMenuRedo.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Y)));
            this.editMenuRedo.Size = new System.Drawing.Size(149, 22);
            this.editMenuRedo.Text = "&Redo";
            this.editMenuRedo.Click += new System.EventHandler(this.editMenuRedo_Click);
            // 
            // menubarSeparator4
            // 
            this.menubarSeparator4.Name = "menubarSeparator4";
            this.menubarSeparator4.Size = new System.Drawing.Size(146, 6);
            // 
            // editMenuCut
            // 
            this.editMenuCut.Enabled = false;
            this.editMenuCut.Image = ((System.Drawing.Image)(resources.GetObject("editMenuCut.Image")));
            this.editMenuCut.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editMenuCut.Name = "editMenuCut";
            this.editMenuCut.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.X)));
            this.editMenuCut.Size = new System.Drawing.Size(149, 22);
            this.editMenuCut.Text = "Cu&t";
            this.editMenuCut.Click += new System.EventHandler(this.editMenuCut_Click);
            // 
            // editMenuCopy
            // 
            this.editMenuCopy.Enabled = false;
            this.editMenuCopy.Image = ((System.Drawing.Image)(resources.GetObject("editMenuCopy.Image")));
            this.editMenuCopy.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editMenuCopy.Name = "editMenuCopy";
            this.editMenuCopy.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.C)));
            this.editMenuCopy.Size = new System.Drawing.Size(149, 22);
            this.editMenuCopy.Text = "&Copy";
            this.editMenuCopy.Click += new System.EventHandler(this.editMenuCopy_Click);
            // 
            // editMenuPaste
            // 
            this.editMenuPaste.Enabled = false;
            this.editMenuPaste.Image = ((System.Drawing.Image)(resources.GetObject("editMenuPaste.Image")));
            this.editMenuPaste.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editMenuPaste.Name = "editMenuPaste";
            this.editMenuPaste.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.V)));
            this.editMenuPaste.Size = new System.Drawing.Size(149, 22);
            this.editMenuPaste.Text = "&Paste";
            this.editMenuPaste.Click += new System.EventHandler(this.editMenuPaste_Click);
            // 
            // editMenuDelete
            // 
            this.editMenuDelete.Enabled = false;
            this.editMenuDelete.Image = global::NSISPublisher.Properties.Resources.Delete;
            this.editMenuDelete.Name = "editMenuDelete";
            this.editMenuDelete.Size = new System.Drawing.Size(149, 22);
            this.editMenuDelete.Text = "&Delete";
            this.editMenuDelete.Click += new System.EventHandler(this.editMenuDelete_Click);
            // 
            // menubarSeparator5
            // 
            this.menubarSeparator5.Name = "menubarSeparator5";
            this.menubarSeparator5.Size = new System.Drawing.Size(146, 6);
            // 
            // editMenuFind
            // 
            this.editMenuFind.Enabled = false;
            this.editMenuFind.Image = global::NSISPublisher.Properties.Resources.Find;
            this.editMenuFind.Name = "editMenuFind";
            this.editMenuFind.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.editMenuFind.Size = new System.Drawing.Size(149, 22);
            this.editMenuFind.Text = "&Find";
            this.editMenuFind.Click += new System.EventHandler(this.editMenuFind_Click);
            // 
            // editMenuFindNext
            // 
            this.editMenuFindNext.Enabled = false;
            this.editMenuFindNext.Image = global::NSISPublisher.Properties.Resources.FindNext;
            this.editMenuFindNext.Name = "editMenuFindNext";
            this.editMenuFindNext.ShortcutKeys = System.Windows.Forms.Keys.F3;
            this.editMenuFindNext.Size = new System.Drawing.Size(149, 22);
            this.editMenuFindNext.Text = "Find &Next";
            this.editMenuFindNext.Click += new System.EventHandler(this.editMenuFindNext_Click);
            // 
            // editMenuReplace
            // 
            this.editMenuReplace.Enabled = false;
            this.editMenuReplace.Image = global::NSISPublisher.Properties.Resources.Replace;
            this.editMenuReplace.Name = "editMenuReplace";
            this.editMenuReplace.Size = new System.Drawing.Size(149, 22);
            this.editMenuReplace.Text = "&R&eplace";
            this.editMenuReplace.Click += new System.EventHandler(this.editMenuReplace_Click);
            // 
            // editMenuGoTo
            // 
            this.editMenuGoTo.Enabled = false;
            this.editMenuGoTo.Image = global::NSISPublisher.Properties.Resources.GoTo;
            this.editMenuGoTo.Name = "editMenuGoTo";
            this.editMenuGoTo.Size = new System.Drawing.Size(149, 22);
            this.editMenuGoTo.Text = "&GoTo";
            this.editMenuGoTo.Click += new System.EventHandler(this.editMenuGoTo_Click);
            // 
            // menubarSeparator6
            // 
            this.menubarSeparator6.Name = "menubarSeparator6";
            this.menubarSeparator6.Size = new System.Drawing.Size(146, 6);
            // 
            // editMenuSelectAll
            // 
            this.editMenuSelectAll.Enabled = false;
            this.editMenuSelectAll.Image = global::NSISPublisher.Properties.Resources.SelectAll;
            this.editMenuSelectAll.Name = "editMenuSelectAll";
            this.editMenuSelectAll.Size = new System.Drawing.Size(149, 22);
            this.editMenuSelectAll.Text = "Select &All";
            this.editMenuSelectAll.Click += new System.EventHandler(this.editMenuSelectAll_Click);
            // 
            // editMenuClear
            // 
            this.editMenuClear.Enabled = false;
            this.editMenuClear.Image = global::NSISPublisher.Properties.Resources.Clear;
            this.editMenuClear.Name = "editMenuClear";
            this.editMenuClear.Size = new System.Drawing.Size(149, 22);
            this.editMenuClear.Text = "C&lear All";
            this.editMenuClear.Click += new System.EventHandler(this.editMenuClear_Click);
            // 
            // editMenuTimeDate
            // 
            this.editMenuTimeDate.Image = global::NSISPublisher.Properties.Resources.DateTime;
            this.editMenuTimeDate.Name = "editMenuTimeDate";
            this.editMenuTimeDate.ShortcutKeys = System.Windows.Forms.Keys.F5;
            this.editMenuTimeDate.Size = new System.Drawing.Size(149, 22);
            this.editMenuTimeDate.Text = "Time/&Date";
            this.editMenuTimeDate.Click += new System.EventHandler(this.editMenuTimeDate_Click);
            // 
            // buildMenu
            // 
            this.buildMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buildMenuCompile,
            this.buildMenuCompileDebug,
            this.buildMenuStopcompile,
            this.buildMenuDebug});
            this.buildMenu.Name = "buildMenu";
            this.buildMenu.Size = new System.Drawing.Size(46, 20);
            this.buildMenu.Text = "&Build";
            // 
            // buildMenuCompile
            // 
            this.buildMenuCompile.Enabled = false;
            this.buildMenuCompile.Image = global::NSISPublisher.Properties.Resources.Build;
            this.buildMenuCompile.Name = "buildMenuCompile";
            this.buildMenuCompile.Size = new System.Drawing.Size(180, 22);
            this.buildMenuCompile.Text = "&Compile";
            this.buildMenuCompile.Click += new System.EventHandler(this.buildMenuCompile_Click);
            // 
            // buildMenuCompileDebug
            // 
            this.buildMenuCompileDebug.Enabled = false;
            this.buildMenuCompileDebug.Image = global::NSISPublisher.Properties.Resources.BuildDebug;
            this.buildMenuCompileDebug.Name = "buildMenuCompileDebug";
            this.buildMenuCompileDebug.Size = new System.Drawing.Size(180, 22);
            this.buildMenuCompileDebug.Text = "Compile and &Debug";
            this.buildMenuCompileDebug.Click += new System.EventHandler(this.buildMenuCompileDebug_Click);
            // 
            // buildMenuStopcompile
            // 
            this.buildMenuStopcompile.Enabled = false;
            this.buildMenuStopcompile.Image = global::NSISPublisher.Properties.Resources.BuildStop;
            this.buildMenuStopcompile.Name = "buildMenuStopcompile";
            this.buildMenuStopcompile.Size = new System.Drawing.Size(180, 22);
            this.buildMenuStopcompile.Text = "&Stop Compile";
            this.buildMenuStopcompile.Click += new System.EventHandler(this.buildMenuStopcompile_Click);
            // 
            // buildMenuDebug
            // 
            this.buildMenuDebug.Enabled = false;
            this.buildMenuDebug.Image = global::NSISPublisher.Properties.Resources.Debug;
            this.buildMenuDebug.Name = "buildMenuDebug";
            this.buildMenuDebug.Size = new System.Drawing.Size(180, 22);
            this.buildMenuDebug.Text = "De&bug";
            this.buildMenuDebug.Click += new System.EventHandler(this.scriptMenuDebug_Click);
            // 
            // formatMenu
            // 
            this.formatMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.formatMenuWordWrap,
            this.formatMenuFont});
            this.formatMenu.Name = "formatMenu";
            this.formatMenu.Size = new System.Drawing.Size(57, 20);
            this.formatMenu.Text = "F&ormat";
            // 
            // formatMenuWordWrap
            // 
            this.formatMenuWordWrap.Checked = true;
            this.formatMenuWordWrap.CheckOnClick = true;
            this.formatMenuWordWrap.CheckState = System.Windows.Forms.CheckState.Checked;
            this.formatMenuWordWrap.Name = "formatMenuWordWrap";
            this.formatMenuWordWrap.Size = new System.Drawing.Size(140, 22);
            this.formatMenuWordWrap.Text = "&Word Wrap";
            this.formatMenuWordWrap.Click += new System.EventHandler(this.formatMenuWordWrap_Click);
            // 
            // formatMenuFont
            // 
            this.formatMenuFont.Image = global::NSISPublisher.Properties.Resources.FontIcon;
            this.formatMenuFont.Name = "formatMenuFont";
            this.formatMenuFont.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
            this.formatMenuFont.Size = new System.Drawing.Size(140, 22);
            this.formatMenuFont.Text = "&Font";
            this.formatMenuFont.Click += new System.EventHandler(this.formatMenuFont_Click);
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewMenuInlineSpellChecker,
            this.menubarSeparator7,
            this.viewMenuToolbars,
            this.viewMenuStatusbar,
            this.viewMenuLineNumbers,
            this.viewMenuFoldingLines});
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(44, 20);
            this.viewMenu.Text = "&View";
            // 
            // viewMenuInlineSpellChecker
            // 
            this.viewMenuInlineSpellChecker.CheckOnClick = true;
            this.viewMenuInlineSpellChecker.Enabled = false;
            this.viewMenuInlineSpellChecker.Name = "viewMenuInlineSpellChecker";
            this.viewMenuInlineSpellChecker.Size = new System.Drawing.Size(177, 22);
            this.viewMenuInlineSpellChecker.Text = "Inline Spell Checker";
            this.viewMenuInlineSpellChecker.CheckStateChanged += new System.EventHandler(this.viewMenuInlineSpellChecker_CheckStateChanged);
            this.viewMenuInlineSpellChecker.Click += new System.EventHandler(this.viewMenuInlineSpellChecker_Click);
            // 
            // menubarSeparator7
            // 
            this.menubarSeparator7.Name = "menubarSeparator7";
            this.menubarSeparator7.Size = new System.Drawing.Size(174, 6);
            // 
            // viewMenuToolbars
            // 
            this.viewMenuToolbars.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.viewMenuHideToolbars,
            this.viewMenuMainToolbar,
            this.viewMenuPropsToolbar,
            this.viewMenuEditToolbar,
            this.viewMenuBuildToolbar,
            this.viewMenuFindToolbar});
            this.viewMenuToolbars.Name = "viewMenuToolbars";
            this.viewMenuToolbars.Size = new System.Drawing.Size(177, 22);
            this.viewMenuToolbars.Text = "&Toolbars";
            // 
            // viewMenuHideToolbars
            // 
            this.viewMenuHideToolbars.CheckOnClick = true;
            this.viewMenuHideToolbars.Name = "viewMenuHideToolbars";
            this.viewMenuHideToolbars.Size = new System.Drawing.Size(168, 22);
            this.viewMenuHideToolbars.Text = "&Hide All Toolbars";
            this.viewMenuHideToolbars.CheckStateChanged += new System.EventHandler(this.viewMenuHideToolbars_CheckStateChanged);
            // 
            // viewMenuMainToolbar
            // 
            this.viewMenuMainToolbar.Checked = true;
            this.viewMenuMainToolbar.CheckOnClick = true;
            this.viewMenuMainToolbar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.viewMenuMainToolbar.Name = "viewMenuMainToolbar";
            this.viewMenuMainToolbar.Size = new System.Drawing.Size(168, 22);
            this.viewMenuMainToolbar.Text = "&Main Toolbar";
            this.viewMenuMainToolbar.CheckStateChanged += new System.EventHandler(this.viewMenuMainToolbar_CheckStateChanged);
            // 
            // viewMenuPropsToolbar
            // 
            this.viewMenuPropsToolbar.Checked = true;
            this.viewMenuPropsToolbar.CheckOnClick = true;
            this.viewMenuPropsToolbar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.viewMenuPropsToolbar.Name = "viewMenuPropsToolbar";
            this.viewMenuPropsToolbar.Size = new System.Drawing.Size(168, 22);
            this.viewMenuPropsToolbar.Text = "&PropertiesToolbar";
            this.viewMenuPropsToolbar.CheckStateChanged += new System.EventHandler(this.viewMenuPropsToolbar_CheckStateChanged);
            // 
            // viewMenuEditToolbar
            // 
            this.viewMenuEditToolbar.Checked = true;
            this.viewMenuEditToolbar.CheckOnClick = true;
            this.viewMenuEditToolbar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.viewMenuEditToolbar.Name = "viewMenuEditToolbar";
            this.viewMenuEditToolbar.Size = new System.Drawing.Size(168, 22);
            this.viewMenuEditToolbar.Text = "&Edit Toolbar";
            this.viewMenuEditToolbar.CheckStateChanged += new System.EventHandler(this.viewMenuEditToolbar_CheckStateChanged);
            // 
            // viewMenuBuildToolbar
            // 
            this.viewMenuBuildToolbar.Checked = true;
            this.viewMenuBuildToolbar.CheckOnClick = true;
            this.viewMenuBuildToolbar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.viewMenuBuildToolbar.Name = "viewMenuBuildToolbar";
            this.viewMenuBuildToolbar.Size = new System.Drawing.Size(168, 22);
            this.viewMenuBuildToolbar.Text = "&Build Toolbar";
            this.viewMenuBuildToolbar.CheckStateChanged += new System.EventHandler(this.viewMenuBuildToolbar_CheckStateChanged);
            // 
            // viewMenuFindToolbar
            // 
            this.viewMenuFindToolbar.Checked = true;
            this.viewMenuFindToolbar.CheckOnClick = true;
            this.viewMenuFindToolbar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.viewMenuFindToolbar.Name = "viewMenuFindToolbar";
            this.viewMenuFindToolbar.Size = new System.Drawing.Size(168, 22);
            this.viewMenuFindToolbar.Text = "&Find Toolbar";
            this.viewMenuFindToolbar.CheckStateChanged += new System.EventHandler(this.viewMenuFindToolbar_CheckStateChanged);
            // 
            // viewMenuStatusbar
            // 
            this.viewMenuStatusbar.Checked = true;
            this.viewMenuStatusbar.CheckOnClick = true;
            this.viewMenuStatusbar.CheckState = System.Windows.Forms.CheckState.Checked;
            this.viewMenuStatusbar.Name = "viewMenuStatusbar";
            this.viewMenuStatusbar.Size = new System.Drawing.Size(177, 22);
            this.viewMenuStatusbar.Text = "&Status Bar";
            this.viewMenuStatusbar.CheckStateChanged += new System.EventHandler(this.viewMenuStatusbar_CheckStateChanged);
            // 
            // viewMenuLineNumbers
            // 
            this.viewMenuLineNumbers.Checked = true;
            this.viewMenuLineNumbers.CheckOnClick = true;
            this.viewMenuLineNumbers.CheckState = System.Windows.Forms.CheckState.Checked;
            this.viewMenuLineNumbers.Name = "viewMenuLineNumbers";
            this.viewMenuLineNumbers.Size = new System.Drawing.Size(177, 22);
            this.viewMenuLineNumbers.Text = "&Line Numbers";
            this.viewMenuLineNumbers.CheckStateChanged += new System.EventHandler(this.viewMenuLineNumbers_CheckStateChanged);
            // 
            // viewMenuFoldingLines
            // 
            this.viewMenuFoldingLines.Checked = true;
            this.viewMenuFoldingLines.CheckOnClick = true;
            this.viewMenuFoldingLines.CheckState = System.Windows.Forms.CheckState.Checked;
            this.viewMenuFoldingLines.Name = "viewMenuFoldingLines";
            this.viewMenuFoldingLines.Size = new System.Drawing.Size(177, 22);
            this.viewMenuFoldingLines.Text = "&Folding Lines";
            this.viewMenuFoldingLines.CheckStateChanged += new System.EventHandler(this.viewMenuFoldingLines_CheckStateChanged);
            // 
            // toolsMenu
            // 
            this.toolsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolsMenuGetSEC1FolderSize,
            this.toolsMenuGetSEC2FolderSize,
            this.toolsMenuByteConverter,
            this.toolsMenuClipboardTool,
            this.toolsMenuSpellCheck,
            this.toolsMenuEditDictionary,
            this.menubarSeparator8,
            this.toolsMenuOptions,
            this.menubarSeparator9,
            this.toolsMenuScriptProperties});
            this.toolsMenu.Name = "toolsMenu";
            this.toolsMenu.Size = new System.Drawing.Size(48, 20);
            this.toolsMenu.Text = "&Tools";
            // 
            // toolsMenuGetSEC1FolderSize
            // 
            this.toolsMenuGetSEC1FolderSize.Image = global::NSISPublisher.Properties.Resources.FolderSize;
            this.toolsMenuGetSEC1FolderSize.Name = "toolsMenuGetSEC1FolderSize";
            this.toolsMenuGetSEC1FolderSize.Size = new System.Drawing.Size(217, 22);
            this.toolsMenuGetSEC1FolderSize.Text = "Get SEC1 Folder Size";
            this.toolsMenuGetSEC1FolderSize.Visible = false;
            this.toolsMenuGetSEC1FolderSize.Click += new System.EventHandler(this.toolsMenuGetFolderSize_Click);
            // 
            // toolsMenuGetSEC2FolderSize
            // 
            this.toolsMenuGetSEC2FolderSize.Image = global::NSISPublisher.Properties.Resources.FolderSize;
            this.toolsMenuGetSEC2FolderSize.Name = "toolsMenuGetSEC2FolderSize";
            this.toolsMenuGetSEC2FolderSize.Size = new System.Drawing.Size(217, 22);
            this.toolsMenuGetSEC2FolderSize.Text = "Get SEC2 Folder Size";
            this.toolsMenuGetSEC2FolderSize.Visible = false;
            this.toolsMenuGetSEC2FolderSize.Click += new System.EventHandler(this.toolsMenuGetSEC2FolderSize_Click);
            // 
            // toolsMenuByteConverter
            // 
            this.toolsMenuByteConverter.Image = global::NSISPublisher.Properties.Resources.Converter;
            this.toolsMenuByteConverter.Name = "toolsMenuByteConverter";
            this.toolsMenuByteConverter.Size = new System.Drawing.Size(217, 22);
            this.toolsMenuByteConverter.Text = "&Byte Converter";
            this.toolsMenuByteConverter.Click += new System.EventHandler(this.toolsMenuByteConverter_Click);
            // 
            // toolsMenuClipboardTool
            // 
            this.toolsMenuClipboardTool.Image = global::NSISPublisher.Properties.Resources.Clipboard;
            this.toolsMenuClipboardTool.Name = "toolsMenuClipboardTool";
            this.toolsMenuClipboardTool.Size = new System.Drawing.Size(217, 22);
            this.toolsMenuClipboardTool.Text = "Clipboard Utility";
            this.toolsMenuClipboardTool.Click += new System.EventHandler(this.toolsMenuClipboardTool_Click);
            // 
            // toolsMenuSpellCheck
            // 
            this.toolsMenuSpellCheck.Enabled = false;
            this.toolsMenuSpellCheck.Image = global::NSISPublisher.Properties.Resources.SpellCheck;
            this.toolsMenuSpellCheck.Name = "toolsMenuSpellCheck";
            this.toolsMenuSpellCheck.Size = new System.Drawing.Size(217, 22);
            this.toolsMenuSpellCheck.Text = "Spell Check";
            this.toolsMenuSpellCheck.Click += new System.EventHandler(this.toolsMenuSpellCheck_Click);
            // 
            // toolsMenuEditDictionary
            // 
            this.toolsMenuEditDictionary.Enabled = false;
            this.toolsMenuEditDictionary.Image = global::NSISPublisher.Properties.Resources.Dictionary;
            this.toolsMenuEditDictionary.Name = "toolsMenuEditDictionary";
            this.toolsMenuEditDictionary.Size = new System.Drawing.Size(217, 22);
            this.toolsMenuEditDictionary.Text = "Edit Dictionary";
            this.toolsMenuEditDictionary.Click += new System.EventHandler(this.toolsMenuEditDictionary_Click);
            // 
            // menubarSeparator8
            // 
            this.menubarSeparator8.Name = "menubarSeparator8";
            this.menubarSeparator8.Size = new System.Drawing.Size(214, 6);
            // 
            // toolsMenuOptions
            // 
            this.toolsMenuOptions.Image = global::NSISPublisher.Properties.Resources.Options;
            this.toolsMenuOptions.Name = "toolsMenuOptions";
            this.toolsMenuOptions.Size = new System.Drawing.Size(217, 22);
            this.toolsMenuOptions.Text = "&Options";
            this.toolsMenuOptions.Click += new System.EventHandler(this.toolsMenuOptions_Click);
            // 
            // menubarSeparator9
            // 
            this.menubarSeparator9.Name = "menubarSeparator9";
            this.menubarSeparator9.Size = new System.Drawing.Size(214, 6);
            // 
            // toolsMenuScriptProperties
            // 
            this.toolsMenuScriptProperties.Enabled = false;
            this.toolsMenuScriptProperties.Image = global::NSISPublisher.Properties.Resources.Properties;
            this.toolsMenuScriptProperties.Name = "toolsMenuScriptProperties";
            this.toolsMenuScriptProperties.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.Return)));
            this.toolsMenuScriptProperties.Size = new System.Drawing.Size(217, 22);
            this.toolsMenuScriptProperties.Text = "Script &Properties";
            this.toolsMenuScriptProperties.Click += new System.EventHandler(this.toolsMenuScriptProperties_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpMenuContents,
            this.helpMenuIndex,
            this.helpMenuSearch,
            this.helpMenuNSISManual,
            this.helpMenuNSISWiki,
            this.helpMenuNSISForum,
            this.helpMenuSCICommunity,
            this.menubarSeparator10,
            this.helpMenuAbout});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(44, 20);
            this.helpMenu.Text = "&Help";
            // 
            // helpMenuContents
            // 
            this.helpMenuContents.Image = global::NSISPublisher.Properties.Resources.Help;
            this.helpMenuContents.Name = "helpMenuContents";
            this.helpMenuContents.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.helpMenuContents.Size = new System.Drawing.Size(214, 22);
            this.helpMenuContents.Text = "&Contents";
            this.helpMenuContents.Click += new System.EventHandler(this.helpMenuContents_Click);
            // 
            // helpMenuIndex
            // 
            this.helpMenuIndex.Image = global::NSISPublisher.Properties.Resources.HelpIndex;
            this.helpMenuIndex.Name = "helpMenuIndex";
            this.helpMenuIndex.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.I)));
            this.helpMenuIndex.Size = new System.Drawing.Size(214, 22);
            this.helpMenuIndex.Text = "&Index";
            this.helpMenuIndex.Click += new System.EventHandler(this.helpMenuIndex_Click);
            // 
            // helpMenuSearch
            // 
            this.helpMenuSearch.Image = global::NSISPublisher.Properties.Resources.Search;
            this.helpMenuSearch.Name = "helpMenuSearch";
            this.helpMenuSearch.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.S)));
            this.helpMenuSearch.Size = new System.Drawing.Size(214, 22);
            this.helpMenuSearch.Text = "&Search";
            this.helpMenuSearch.Click += new System.EventHandler(this.helpMenuSearch_Click);
            // 
            // helpMenuNSISManual
            // 
            this.helpMenuNSISManual.Image = global::NSISPublisher.Properties.Resources.CMH;
            this.helpMenuNSISManual.Name = "helpMenuNSISManual";
            this.helpMenuNSISManual.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.F1)));
            this.helpMenuNSISManual.Size = new System.Drawing.Size(214, 22);
            this.helpMenuNSISManual.Text = "NSIS Users Manual";
            this.helpMenuNSISManual.Click += new System.EventHandler(this.helpMenuNSISManual_Click);
            // 
            // helpMenuNSISWiki
            // 
            this.helpMenuNSISWiki.Image = global::NSISPublisher.Properties.Resources.NSISFaviIco;
            this.helpMenuNSISWiki.Name = "helpMenuNSISWiki";
            this.helpMenuNSISWiki.Size = new System.Drawing.Size(214, 22);
            this.helpMenuNSISWiki.Text = "NSIS Wiki";
            this.helpMenuNSISWiki.Click += new System.EventHandler(this.helpMenuNSISWiki_Click);
            // 
            // helpMenuNSISForum
            // 
            this.helpMenuNSISForum.Image = global::NSISPublisher.Properties.Resources.WinampFaviIco;
            this.helpMenuNSISForum.Name = "helpMenuNSISForum";
            this.helpMenuNSISForum.Size = new System.Drawing.Size(214, 22);
            this.helpMenuNSISForum.Text = "NSIS Forum";
            this.helpMenuNSISForum.Click += new System.EventHandler(this.helpMenuNSISForum_Click);
            // 
            // helpMenuSCICommunity
            // 
            this.helpMenuSCICommunity.Image = global::NSISPublisher.Properties.Resources.SCICommunity;
            this.helpMenuSCICommunity.Name = "helpMenuSCICommunity";
            this.helpMenuSCICommunity.Size = new System.Drawing.Size(214, 22);
            this.helpMenuSCICommunity.Text = "SCI Community";
            this.helpMenuSCICommunity.Click += new System.EventHandler(this.helpMenuSCICommunity_Click);
            // 
            // menubarSeparator10
            // 
            this.menubarSeparator10.Name = "menubarSeparator10";
            this.menubarSeparator10.Size = new System.Drawing.Size(211, 6);
            // 
            // helpMenuAbout
            // 
            this.helpMenuAbout.Image = global::NSISPublisher.Properties.Resources.NSIS;
            this.helpMenuAbout.Name = "helpMenuAbout";
            this.helpMenuAbout.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Alt | System.Windows.Forms.Keys.A)));
            this.helpMenuAbout.Size = new System.Drawing.Size(214, 22);
            this.helpMenuAbout.Text = "&About...";
            this.helpMenuAbout.Click += new System.EventHandler(this.helpMenuAbout_Click);
            // 
            // MainFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(674, 662);
            this.Controls.Add(this.tabPages);
            this.Controls.Add(this.mainToolbar);
            this.Controls.Add(this.statusbar);
            this.Controls.Add(this.menubar);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menubar;
            this.MinimumSize = new System.Drawing.Size(690, 700);
            this.Name = "MainFrm";
            this.Text = "NSIS Publisher";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form_Closing);
            this.Load += new System.EventHandler(this.Form_Load);
            this.Shown += new System.EventHandler(this.MainFrm_Shown);
            this.cntxtMenu.ResumeLayout(false);
            this.statusbar.ResumeLayout(false);
            this.statusbar.PerformLayout();
            this.splitContainer.Panel1.ResumeLayout(false);
            this.splitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer)).EndInit();
            this.splitContainer.ResumeLayout(false);
            this.splashPanel.ResumeLayout(false);
            this.splashPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.scriptbox)).EndInit();
            this.outputBoxCntxtMenu.ResumeLayout(false);
            this.tabPages.ResumeLayout(false);
            this.propsTabPg.ResumeLayout(false);
            this.propsTabPg.PerformLayout();
            this.projNotesGrpBox.ResumeLayout(false);
            this.projNotesGrpBox.PerformLayout();
            this.propsSplitCntnr.Panel1.ResumeLayout(false);
            this.propsSplitCntnr.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.propsSplitCntnr)).EndInit();
            this.propsSplitCntnr.ResumeLayout(false);
            this.projShortNameGrpBox.ResumeLayout(false);
            this.projShortNameGrpBox.PerformLayout();
            this.projPathGrpBox.ResumeLayout(false);
            this.projPathGrpBox.PerformLayout();
            this.projPubGrpBox.ResumeLayout(false);
            this.projPubGrpBox.PerformLayout();
            this.projURLGrpBox.ResumeLayout(false);
            this.projURLGrpBox.PerformLayout();
            this.projNameGrpBox.ResumeLayout(false);
            this.projNameGrpBox.PerformLayout();
            this.projMainEXEGrpBox.ResumeLayout(false);
            this.projMainEXEGrpBox.PerformLayout();
            this.propsToolbar.ResumeLayout(false);
            this.propsToolbar.PerformLayout();
            this.scriptTabPg.ResumeLayout(false);
            this.toolbarContainer.ContentPanel.ResumeLayout(false);
            this.toolbarContainer.TopToolStripPanel.ResumeLayout(false);
            this.toolbarContainer.TopToolStripPanel.PerformLayout();
            this.toolbarContainer.ResumeLayout(false);
            this.toolbarContainer.PerformLayout();
            this.findToolbar.ResumeLayout(false);
            this.findToolbar.PerformLayout();
            this.buildToolbar.ResumeLayout(false);
            this.buildToolbar.PerformLayout();
            this.scriptEditToolbar.ResumeLayout(false);
            this.scriptEditToolbar.PerformLayout();
            this.mainToolbar.ResumeLayout(false);
            this.mainToolbar.PerformLayout();
            this.menubar.ResumeLayout(false);
            this.menubar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ImageList imageList;
        private System.Windows.Forms.ContextMenuStrip cntxtMenu;
        private System.Windows.Forms.ToolStripMenuItem cutCntxtMenu;
        private System.Windows.Forms.ToolStripMenuItem copyCntxtMenu;
        private System.Windows.Forms.ToolStripMenuItem pasteCntxtMenu;
        private System.Windows.Forms.ToolStripMenuItem delCntxtMenu;
        private System.Windows.Forms.ToolStripSeparator cntxMenuSeparator2;
        private System.Windows.Forms.MenuStrip menubar;
        private System.Windows.Forms.ToolStrip mainToolbar;
        private System.Windows.Forms.ToolStrip scriptEditToolbar;
        private System.Windows.Forms.ToolStrip findToolbar;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem fileMenuOpenScript;
        private System.Windows.Forms.ToolStripSeparator menubarSeparator1;
        private System.Windows.Forms.ToolStripMenuItem fileMenuSaveAs;
        private System.Windows.Forms.ToolStripSeparator menubarSeparator2;
        private System.Windows.Forms.ToolStripMenuItem fileMenuPrint;
        private System.Windows.Forms.ToolStripMenuItem fileMenuPrintPreview;
        private System.Windows.Forms.ToolStripSeparator menubarSeparator3;
        private System.Windows.Forms.ToolStripMenuItem fileMenuExit;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        public System.Windows.Forms.ToolStripMenuItem editMenuUndo;
        public System.Windows.Forms.ToolStripMenuItem editMenuRedo;
        private System.Windows.Forms.ToolStripSeparator menubarSeparator4;
        public System.Windows.Forms.ToolStripMenuItem editMenuCut;
        public System.Windows.Forms.ToolStripMenuItem editMenuCopy;
        public System.Windows.Forms.ToolStripMenuItem editMenuPaste;
        public System.Windows.Forms.ToolStripMenuItem editMenuDelete;
        private System.Windows.Forms.ToolStripSeparator menubarSeparator5;
        private System.Windows.Forms.ToolStripMenuItem editMenuFind;
        private System.Windows.Forms.ToolStripMenuItem editMenuReplace;
        private System.Windows.Forms.ToolStripMenuItem editMenuGoTo;
        private System.Windows.Forms.ToolStripSeparator menubarSeparator6;
        public System.Windows.Forms.ToolStripMenuItem editMenuSelectAll;
        private System.Windows.Forms.ToolStripMenuItem buildMenu;
        private System.Windows.Forms.ToolStripMenuItem buildMenuCompile;
        private System.Windows.Forms.ToolStripMenuItem buildMenuDebug;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolStripMenuItem helpMenuContents;
        private System.Windows.Forms.ToolStripMenuItem helpMenuIndex;
        private System.Windows.Forms.ToolStripMenuItem helpMenuSearch;
        private System.Windows.Forms.ToolStripSeparator menubarSeparator10;
        private System.Windows.Forms.ToolStripMenuItem helpMenuAbout;
        private System.Windows.Forms.ToolStripButton openBtn;
        public System.Windows.Forms.ToolStripButton saveBtn;
        private System.Windows.Forms.ToolStripButton cutBtn;
        private System.Windows.Forms.ToolStripButton copyBtn;
        private System.Windows.Forms.ToolStripButton pasteBtn;
        public System.Windows.Forms.ToolStripButton saveAsBtn;
        internal System.Windows.Forms.ToolStripSeparator toolbarSeparator7;
        internal System.Windows.Forms.ToolStripButton undoBtn;
        internal System.Windows.Forms.ToolStripButton redoBtn;
        internal System.Windows.Forms.ToolStripButton selectAllBtn;
        internal System.Windows.Forms.ToolStripButton deleteBtn;
        internal System.Windows.Forms.ToolStripButton clearScriptBtn;
        internal System.Windows.Forms.ToolStripMenuItem undoCntxtMenu;
        internal System.Windows.Forms.ToolStripMenuItem redoCntxtMenu;
        internal System.Windows.Forms.ToolStripSeparator cntxMenuSeparator1;
        internal System.Windows.Forms.ToolStripMenuItem selectAllCntxtMenu;
        private System.Windows.Forms.ToolStripMenuItem fileMenuSaveAll;
        private System.Windows.Forms.ToolStripMenuItem toolsMenu;
        private System.Windows.Forms.ToolStripSeparator menubarSeparator9;
        private System.Windows.Forms.ToolStripMenuItem toolsMenuScriptProperties;
        private System.Windows.Forms.ToolStripMenuItem fileMenuPageSetup;
        private System.Windows.Forms.ToolStripMenuItem editMenuClear;
        private System.Windows.Forms.ToolStripMenuItem editMenuTimeDate;
        private System.Windows.Forms.ToolStripMenuItem editMenuFindNext;
        private System.Windows.Forms.ToolStripMenuItem clearCntxtMenu;
        private System.Windows.Forms.ToolStripButton findBtn;
        private System.Windows.Forms.ToolStripButton findNextBtn;
        private System.Windows.Forms.ToolStripButton replaceBtn;
        private System.Windows.Forms.ToolStripButton goToBtn;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem viewMenuStatusbar;
        private System.Windows.Forms.ToolStripMenuItem viewMenuLineNumbers;
        private System.Windows.Forms.StatusStrip statusbar;
        public System.Windows.Forms.ToolStripStatusLabel statusbarMainMessage;
        private System.Windows.Forms.ToolStripSeparator menubarSeparator8;
        private System.Windows.Forms.ToolStripMenuItem toolsMenuOptions;
        private System.Windows.Forms.ToolStripMenuItem formatMenu;
        private System.Windows.Forms.ToolStripMenuItem formatMenuWordWrap;
        private System.Windows.Forms.ToolStripMenuItem formatMenuFont;
        private System.Windows.Forms.ToolStripMenuItem viewMenuFoldingLines;
        private System.Windows.Forms.SplitContainer splitContainer;
        private System.Windows.Forms.RichTextBox outputBox;
        private System.Windows.Forms.ToolStripContainer toolbarContainer;
        private System.Windows.Forms.ToolStripMenuItem viewMenuToolbars;
        private System.Windows.Forms.ToolStripMenuItem viewMenuHideToolbars;
        private System.Windows.Forms.ToolStripMenuItem viewMenuMainToolbar;
        private System.Windows.Forms.ToolStripMenuItem viewMenuEditToolbar;
        public System.Windows.Forms.ToolStripMenuItem fileMenuSave;
        private System.Windows.Forms.HelpProvider helpProvider;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ToolStripMenuItem fileMenuNew;
        private System.Windows.Forms.ToolStripButton newBtn;
        private System.Windows.Forms.ToolStripMenuItem viewMenuInlineSpellChecker;
        private System.Windows.Forms.ToolStripMenuItem toolsMenuSpellCheck;
        private System.Windows.Forms.ToolStripMenuItem toolsMenuEditDictionary;
        private System.Windows.Forms.ToolStripMenuItem buildMenuCompileDebug;
        private System.Windows.Forms.ToolStripMenuItem buildMenuStopcompile;
        private System.Windows.Forms.TabControl tabPages;
        private System.Windows.Forms.TabPage propsTabPg;
        private System.Windows.Forms.TabPage scriptTabPg;
        private System.Windows.Forms.SplitContainer propsSplitCntnr;
        internal System.Windows.Forms.GroupBox projPubGrpBox;
        internal System.Windows.Forms.TextBox projPubTxtBox;
        internal System.Windows.Forms.GroupBox projNameGrpBox;
        internal System.Windows.Forms.TextBox projNameTxtBox;
        internal System.Windows.Forms.GroupBox projPathGrpBox;
        internal System.Windows.Forms.Button projPathBrowseBtn;
        internal System.Windows.Forms.TextBox projPathTxtBox;
        internal System.Windows.Forms.GroupBox projShortNameGrpBox;
        internal System.Windows.Forms.TextBox projShortNameTxtBox;
        private System.Windows.Forms.GroupBox projNotesGrpBox;
        private System.Windows.Forms.ToolStripMenuItem viewMenuFindToolbar;
        private System.Windows.Forms.CheckBox includeSRCChkBox;
        private System.Windows.Forms.GroupBox projMainEXEGrpBox;
        private System.Windows.Forms.TextBox projMainEXEInfoTxtBox;
        private System.Windows.Forms.TextBox projMainEXETxtBox;
        public System.Windows.Forms.TextBox projNotesTxtBox;
        private System.Windows.Forms.CheckBox aspectChkBox;
        private System.Windows.Forms.ToolStrip propsToolbar;
        internal System.Windows.Forms.ToolStripButton clearBtn;
        private System.Windows.Forms.ToolStripSeparator toolbarSeparator3;
        private System.Windows.Forms.ToolStripButton publishBtn;
        private System.Windows.Forms.ToolStripButton lockBtn;
        private System.Windows.Forms.ToolStripButton scriptPropsBtn;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripSeparator toolbarSeparator6;
        private System.Windows.Forms.ToolStripSeparator toolbarSeparator1;
        private System.Windows.Forms.ToolStripButton helpBtn;
        private System.Windows.Forms.ToolStripMenuItem fileMenuOpenConf;
        private System.Windows.Forms.TextBox projPathInfoTxtBox;
        private System.Windows.Forms.TextBox projPubInfoTxtBox;
        private System.Windows.Forms.TextBox projNameInfoTxtBox;
        private System.Windows.Forms.TextBox projShortNameInfoTxtBox;
        internal System.Windows.Forms.GroupBox projURLGrpBox;
        private System.Windows.Forms.TextBox projURLInfoTxtBox;
        internal System.Windows.Forms.TextBox projURLTxtBox;
        private System.Windows.Forms.ToolStripMenuItem viewMenuPropsToolbar;
        private System.Windows.Forms.ToolStripMenuItem viewMenuBuildToolbar;
        private System.Windows.Forms.ToolStripMenuItem helpMenuNSISWiki;
        private System.Windows.Forms.ToolStripMenuItem helpMenuNSISForum;
        private System.Windows.Forms.ToolStripMenuItem helpMenuSCICommunity;
        private System.Windows.Forms.ToolStripMenuItem helpMenuNSISManual;
        private System.Windows.Forms.ToolStripMenuItem toolsMenuByteConverter;
        private System.Windows.Forms.ToolStripMenuItem toolsMenuGetSEC1FolderSize;
        private System.Windows.Forms.ToolStripMenuItem toolsMenuClipboardTool;
        private System.Windows.Forms.ToolStrip buildToolbar;
        private System.Windows.Forms.ToolStripButton compileDebugBtn;
        private System.Windows.Forms.ToolStripButton stopcompileBtn;
        private System.Windows.Forms.ToolStripButton debugBtn;
        internal System.Windows.Forms.Label notifyLbl;
        private System.Windows.Forms.ToolStripSeparator toolbarSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolbarSeparator5;
        private System.Windows.Forms.Label infoLbl;
        private System.Windows.Forms.ContextMenuStrip outputBoxCntxtMenu;
        private System.Windows.Forms.ToolStripMenuItem selectAllOutputBoxCntxtMenu;
        private System.Windows.Forms.ToolStripMenuItem copyOutputBoxCntxtMenu;
        public FastColoredTextBoxNS.FastColoredTextBox scriptbox;
        private System.Windows.Forms.ToolStripButton readmeBtn;
        private System.Windows.Forms.ToolStripSeparator toolbarSeparator2;
        private System.Windows.Forms.ToolStripButton licenseBtn;
        public System.Windows.Forms.ToolStripComboBox projEngCmboBox;
        internal System.Windows.Forms.Label label1;
        public System.Windows.Forms.ToolStripButton compileBtn;
        private System.Windows.Forms.Panel splashPanel;
        private System.Windows.Forms.ToolStripButton openIncludeBtn;
        private System.Windows.Forms.ToolStripSeparator menubarSeparator7;
        private System.Windows.Forms.ToolStripMenuItem toolsMenuGetSEC2FolderSize;
        private System.Windows.Forms.CheckBox unicodeChkBox;
        private System.Windows.Forms.CheckBox loggingChkBox;
    }
}