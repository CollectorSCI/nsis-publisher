﻿/// To do list:
/// 
/// 

#region using directives ====================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using System.Windows.Forms;

#endregion using directives

namespace NSISPublisher
{
    /// <summary>
    /// Recursively retrieves file list if Include folder
    /// </summary>


    class GetFiles
    {
        //public static string unFiles;
        public static string FileList(string target)
        {
            string baseFiles = @"  
  ; Add Game Files --------------------------------------------
  SetOutPath ""$INSTDIR""
";
            //string unBaseFileName = null;

            if (Directory.Exists(target))
            {
                foreach (string obj in Directory.GetFiles(target, "*.*").OrderBy(f => new FileInfo(f).Extension))
                {
                    string file = Path.GetFileName(obj).ToUpper();
                    string ext = Path.GetExtension(obj).ToUpper();

                    if (file == "AGI" || file == "HGC_FONT" || file.EndsWith("DIR") || file == "OBJECT" || file.Contains("VOL.") || file == Props.projInterpEXE ||
                        ext == ".OVL" || file == "WORDS.TOK" || file == "VERSION" || file.StartsWith("RESOURCE.") || ext == ".DRV" || file == "INTERP.ERR" || 
                        file.StartsWith("INSTSTALL.") || file == "GMDRV_CHANGES" || file == "GMDRV_LICENSE" || file == "GMDRV_README.TXT" || file == "GUSDRV.COM" || 
                        file == "INTERP.TXT" || file.StartsWith("VIEW.") || file.StartsWith("PIC.") || file.StartsWith("SCRIPT.") || file.StartsWith("SOUND.") || 
                        file.StartsWith("VOCAB.") || file.StartsWith("FONT.") || file.StartsWith("CURSOR.") || file.StartsWith("PATCH.") || ext == ".V16" || 
                        ext == ".P16" || ext == ".V56" || ext == ".P56" || ext == ".SCR" || ext == ".TEX" || ext == ".SND" || ext == ".VOC" || ext == ".FON" ||
                        ext == ".CUR" || ext == ".PAT" || ext == ".PAL" || ext == ".AUD" || ext == ".SYN" || ext == ".MSG" || ext == ".MAP" || ext == ".HEP" || 
                        ext == ".WAV")
                    {
                        string item = obj.Replace(target + @"\", "");
                        item = @"  File """ + item + @"""" + Environment.NewLine;
                        baseFiles += item;
                    }
                }
            }

            return baseFiles;
        }
    }


    class GetIncludeFiles
    {
        public static string unFiles;
        public static string rmDir;
        public static string FileList(string target)
        {
            string includes = null;
            string unFileName = null;
            //Publish.includeSize

            long filesLength = 0;

            if (Directory.Exists(target))
            {
                string resourceFileList = null;
                foreach (string srcFile in Directory.GetFiles(target, "*.*").OrderBy(f => new FileInfo(f).Extension))
                {
                    long fileLength = new FileInfo(srcFile).Length;
                    filesLength += fileLength;

                    string item = srcFile.Replace(target, "Publish_Include");
                    item = @"  File """ + item + @"""" + Environment.NewLine;
                    resourceFileList += item;

                    unFileName = Path.GetFileName(srcFile);
                    if (unFileName != Props.projMainEXE)
                        unFiles += @"  Delete ""$INSTDIR\" + unFileName + "\"" + Environment.NewLine;
                }

                foreach (string obj in Directory.EnumerateDirectories(target, "*", SearchOption.AllDirectories).OrderBy(name => name.Replace(Path.DirectorySeparatorChar, '\0'), StringComparer.Ordinal))
                {
                    unFiles = Environment.NewLine;
                    FileAttributes attr = File.GetAttributes(obj);
                    if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                    {
                        string dirObj = obj.Replace(target, null);
                        string itm = Environment.NewLine;
                        itm = Environment.NewLine + @"  SetOutPath ""$INSTDIR" + dirObj + @"""" + Environment.NewLine;
                        resourceFileList += itm;

                        rmDir += "  RMDir /REBOOTOK \"$INSTDIR" + obj.Replace(target, null) + "\"" + Environment.NewLine;

                        foreach (string subFile in Directory.GetFiles(obj, "*.*").OrderBy(f => new FileInfo(f).Extension))
                        {
                            long fileLength = new FileInfo(subFile).Length;
                            filesLength += fileLength;

                            string fileItem = @"  File """ + subFile + @"""" + Environment.NewLine;
                            resourceFileList += fileItem;
                            string subItem = subFile.Replace(target, null);
                            unFileName = Path.GetFileName(subItem);
                            if (unFileName != Props.projMainEXE)
                                unFiles += @"  Delete ""$INSTDIR" + subItem + "\"" + Environment.NewLine;
                        }
                    }
                }
                
                includes += resourceFileList;
                includes = includes.Replace(target, "Publish_Include");
            }

            //Publish.includeSize = filesLength;
            return includes;
        }
    }
}
