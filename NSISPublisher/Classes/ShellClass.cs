/// To do list:
/// 
/// 

#region using directives ====================================================================================

using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Xml.Linq;

#endregion using directives


namespace NSISPublisher
{
    /// <summary>
    /// A collection of classes used for shell functions
    /// </summary>

    public class ShellClass
    {
        #region Declares, Imports. etc. =====================================================================

        MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];
        //ShellClass ShellClass = new ShellClass();

        [DllImport("user32.dll", SetLastError = true)]
        public static extern uint SetParent(IntPtr hWndChild, IntPtr hWndNewParent);
        public static void LoadProcessInControl(string _Process, Control _Control)
        {
            System.Diagnostics.Process p = System.Diagnostics.Process.Start(_Process);
            p.WaitForInputIdle();
            SetParent(p.MainWindowHandle, _Control.Handle);
        }


        [DllImport("shell32.dll", CharSet = CharSet.Auto)]
        static extern bool ShellExecuteEx(ref SHELLEXECUTEINFO lpExecInfo);

        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct SHELLEXECUTEINFO
        {
            public int cbSize;
            public uint fMask;
            public IntPtr hwnd;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpVerb;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpFile;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpParameters;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpDirectory;
            public int nShow;
            public IntPtr hInstApp;
            public IntPtr lpIDList;
            [MarshalAs(UnmanagedType.LPTStr)]
            public string lpClass;
            public IntPtr hkeyClass;
            public uint dwHotKey;
            public IntPtr hIcon;
            public IntPtr hProcess;
        }

        public const int SW_SHOW = 5;
        public const int SEE_MASK_INVOKEIDLIST = 0xc;

        public static bool ShowFileProperties(string Filename)
        {
            SHELLEXECUTEINFO info = new SHELLEXECUTEINFO();
            info.cbSize = Marshal.SizeOf(info);
            info.lpVerb = "properties";
            info.lpFile = Filename;
            info.nShow = SW_SHOW;
            info.fMask = SEE_MASK_INVOKEIDLIST;
            return ShellExecuteEx(ref info);
        }

        #endregion Declares, Imports. etc.


        #region Shell Functions =============================================================================

        // Call File Edit
        //public static bool CallFileEdit(string Filename) {
        public void CallFileEdit(string Filename)
        {
            SHELLEXECUTEINFO shInfo = new SHELLEXECUTEINFO();
            shInfo.cbSize = Marshal.SizeOf(shInfo);
            shInfo.lpVerb = "edit";
            shInfo.lpFile = Filename;
            shInfo.nShow = SW_SHOW;
            shInfo.fMask = SEE_MASK_INVOKEIDLIST;
            ShellExecuteEx(ref shInfo);
            //return ShellExecuteEx(ref shInfo);
        }


        /// <summary>
        /// Opens selected file in default application
        /// </summary>
        /// <param name="Filename">selected file</param>
        public void OpenFile(string Filename)
        {
            SHELLEXECUTEINFO shInfo = new SHELLEXECUTEINFO();
            shInfo.cbSize = Marshal.SizeOf(shInfo);
            shInfo.lpFile = Filename;
            shInfo.nShow = SW_SHOW;
            shInfo.fMask = SEE_MASK_INVOKEIDLIST;
            shInfo.lpVerb = "open";
            ShellExecuteEx(ref shInfo);
        }


        /// <summary>
        /// Opens selected file's properties dialog
        /// </summary>
        /// <param name="Filename">selected file</param>
        public void DisplayFileProperties(string Filename)
        {
            SHELLEXECUTEINFO shInfo = new SHELLEXECUTEINFO();
            shInfo.cbSize = Marshal.SizeOf(shInfo);
            shInfo.lpFile = Filename;
            shInfo.nShow = SW_SHOW;
            shInfo.fMask = SEE_MASK_INVOKEIDLIST;
            shInfo.lpVerb = "properties";
            ShellExecuteEx(ref shInfo);
        }


        /// <summary>
        /// Copys source file path
        /// </summary>
        /// <param name="Filename">selected file</param>
        public static void CopyFile(string Filename)
        {
            SHELLEXECUTEINFO shInfo = new SHELLEXECUTEINFO();
            shInfo.cbSize = Marshal.SizeOf(shInfo);
            shInfo.lpFile = Filename;
            shInfo.nShow = SW_SHOW;
            shInfo.fMask = SEE_MASK_INVOKEIDLIST;
            shInfo.lpVerb = "copy";
            ShellExecuteEx(ref shInfo);
        }


        /// <summary>
        /// Pastes File
        /// </summary>
        /// <param name="Pathname">target path</param>
        public static void PasteFile(string Pathname)
        {
            SHELLEXECUTEINFO shInfo = new SHELLEXECUTEINFO();
            shInfo.cbSize = Marshal.SizeOf(shInfo);
            shInfo.lpFile = Pathname;
            shInfo.nShow = SW_SHOW;
            shInfo.fMask = SEE_MASK_INVOKEIDLIST;
            shInfo.lpVerb = "paste";
            ShellExecuteEx(ref shInfo);
        }


        /// <summary>
        /// Get .lnk Target
        /// </summary>
        /// <param name="name">selected .lnk</param>
        /// <returns>target file</returns>
        public string ResolveShellLink(string name)
        {
            // Find target of short-cut
            if (string.Compare(Path.GetExtension(name), ".lnk", true) != 0)
                return name;
            Shell32.Shell shl = new Shell32.Shell();
            Shell32.Folder dir = shl.NameSpace(Path.GetDirectoryName(name));
            Shell32.FolderItem itm = dir.Items().Item(Path.GetFileName(name));
            Shell32.ShellLinkObject lnk = (Shell32.ShellLinkObject)itm.GetLink;
            return lnk.Path;
        }

        #endregion Shell Functions
    }


    #region Declares, Imports. etc. =====================================================================

    [StructLayout(LayoutKind.Sequential)]
    public struct SHFILEINFO
    {
        public IntPtr hIcon;
        public IntPtr iIcon;
        public uint dwAttributes;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
        public string szDisplayName;
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
        public string szTypeName;
    };

    #endregion Declares, Imports. etc.


    /// <summary>
    /// Class to gets Shell Icons for Drag&Drop shortcuts
    /// </summary>
    class Win32
    {
        public const uint SHGFI_ICON = 0x100;
        public const uint SHGFI_SMALLICON = 0x1; // 'Small icon
        public const uint SHGFI_LARGEICON = 0x0; // 'Large icon

        [DllImport("shell32.dll")]
        public static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbSizeFileInfo, uint uFlags);
    }
}
