﻿/// To do list:
/// Check if makensis.exe is Unicode or Logging build to add warning
/// 

#region using directives ====================================================================================

using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

#endregion using directives


namespace NSISPublisher
{
    /// <summary>
    /// Get NSIS Path and add missing macros/plugins
    /// </summary>

    class NSIS
    {
        public static void GetNSIS()
        {
            string nsisPubEXE = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
            string nsisPubPath = Path.GetDirectoryName(nsisPubEXE).Replace("%20", " ");

            string makeNSIS;
            string nsisPath = null;

            // Get NSIS Registry entry
            try
            {
                RegistryKey reg = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\NSIS");
                nsisPath = reg.GetValue(null).ToString();
            }
            catch (Exception ex)
            {
                string err = ("Error: " + ex.Message);
                MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];
                mainFrm.statusbarMainMessage.Text = (err);
                MessageBox.Show(err + Environment.NewLine + Environment.NewLine + "NSIS not found. You must download and install NSIS to use the Publish function." + Environment.NewLine + Environment.NewLine + "See http://nsis.sourceforge.net/ for details",
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
            }

            makeNSIS = nsisPath + @"\makensis.exe";

            // Check if makensis.exe exists
            if (System.IO.File.Exists(makeNSIS))
            {
                Properties.Settings.Default.nsisPath = nsisPath;
                Properties.Settings.Default.makeNSIS = makeNSIS;
                Properties.Settings.Default.Save();
            }
            else{
                MessageBox.Show(@"NSIS not found. NSIS is required to compile the setup file. Please downlaod and install NSIS from http://nsis.sourceforge.net/");
                return;
            }








            // Check for missing macros
            string nsisMacroPath = nsisPath + "\\Publish_Include\\un.MoveFileFolder.nsh";
            string nsisPubMacro = nsisPubPath + @"\NSIS\Publish_Include\un.MoveFileFolder.nsh";
            if (!File.Exists(nsisMacroPath))
                try
                {
                    File.Copy(nsisPubMacro, nsisMacroPath);
                }
                catch { }

            // Check for missing plugins
            string nsisPubANSIPlugins = nsisPubPath + "\\NSIS\\Plugins\\x86-ansi\\Aero.dll";
            string nsisPubUnicodePlugins = nsisPubPath + "\\NSIS\\Plugins\\x86-unicode\\Aero.dll";
            string nsisPluginsPath = nsisPath + "\\Plugins\\Aero.dll";
            string nsisANSIPluginsPath = nsisPath + "\\Plugins\\x86-ansi\\Aero.dll";
            string nsisUnicodePluginsPath = nsisPath + "\\Plugins\\x86-unicode\\Aero.dll";

            // NSIS 2.46
            if (!Directory.Exists(nsisPath + "\\Plugins\\x86-ansi"))
            {
                if (!File.Exists(nsisPluginsPath))
                    try
                    {
                        File.Copy(nsisPubANSIPlugins, nsisPluginsPath);
                    }
                    catch { }
            }
            // NSIS 3.x
            else if (Directory.Exists(nsisPath + "\\Plugins\\x86-ansi"))
            {
                if (!File.Exists(nsisANSIPluginsPath))
                    try
                    {
                        File.Copy(nsisPubANSIPlugins, nsisANSIPluginsPath);
                        File.Copy(nsisPubUnicodePlugins, nsisUnicodePluginsPath);
                    }
                    catch { }
            }
        }
    }
}
