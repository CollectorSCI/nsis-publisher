/// To Do
/// 
/// 

using Microsoft.VisualBasic;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Diagnostics;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Linq;
using System.Xml.Linq;
using i00SpellCheck;
using FastColoredTextBoxNS;


namespace NSISPublisher
{
    /// <summary>
    /// i00 .Net Spell Check
    /// ©i00 Productions All rights reserved
    /// Created by Kris Bennett
    /// ----------------------------------------------------------------------------------------------------
    /// All property in this file is and remains the property of i00 Productions, regardless of its usage,
    /// unless stated otherwise in writing from i00 Productions.
    /// 
    /// Anyone wishing to use this code in their projects may do so, however are required to leave a post on
    /// VBForums (under: http:/// www.vbforums.com/showthread.php?p=4075093) stating that they are doing so.
    /// A simple "I am using i00 Spell check in my project" will surffice.
    /// 
    /// i00 is not and shall not be held accountable for any damages directly or indirectly caused by the
    /// use or miss-use of this product.  This product is only a component and thus is intended to be used 
    /// as part of other software, it is not a complete software package, thus i00 Productions is not 
    /// responsible for any legal ramifications that software using this product breaches.
    /// </summary>

    public class SpellCheck : i00SpellCheck.SpellCheckControlBase, iTestHarness
    {
        MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];


        #region "Setup"

        //Called when the control is loaded
        public override void Load()
        {
            parentFastColoredTextBox = (FastColoredTextBox)this.Control;

            SpellErrorStyle.FastColoredTextBox = parentFastColoredTextBox;
            CaseErrorStyle.FastColoredTextBox = parentFastColoredTextBox;
            IgnoreErrorStyle.FastColoredTextBox = parentFastColoredTextBox;

            LoadSettingsToObjects();
        }

        //Lets the EnableSpellCheck() know what ControlTypes we can spellcheck
        public override IEnumerable<System.Type> ControlTypes
        {
            get { return new System.Type[] { typeof(FastColoredTextBox) }; }
        }

        private void LoadSettingsToObjects()
        {
            SpellErrorStyle.Color = Settings.MistakeColor;
            IgnoreErrorStyle.Color = Settings.IgnoreColor;
            CaseErrorStyle.Color = Settings.CaseMistakeColor;
        }

        //Repaint control when settings are changed
        private void SpellCheckFastColoredTextBox_SettingsChanged(object sender, System.EventArgs e)
        {
            LoadSettingsToObjects();
            RepaintControl();
        }


        string mc_SpellCheckMatch = "";
        [System.ComponentModel.Description("This regular expression is used to set what text gets spell checked in the FastColoredTextBox")]
        [System.ComponentModel.DisplayName("Spell Check Match")]
        public string SpellCheckMatch
        {
            get { return mc_SpellCheckMatch; }
            set
            {
                if (mc_SpellCheckMatch != value)
                {
                    mc_SpellCheckMatch = value;
                    RepaintControl();
                }
            }
        }


        #endregion  "Setup"


        #region "Underlying Control"

        //Make underlying control appear nicer in property grids
        [System.ComponentModel.Category("Control")]
        [System.ComponentModel.Description("The FastColoredTextBox associated with the SpellCheckFastColoredTextBox object")]
        [System.ComponentModel.DisplayName("FastColoredTextBox")]
        public override System.Windows.Forms.Control Control
        {
            get { return base.Control; }
        }

        //Quick access to underlying FastColoredTextBox object... with events...
        private FastColoredTextBox withEventsField_parentFastColoredTextBox;
        private FastColoredTextBox parentFastColoredTextBox
        {
            get { return withEventsField_parentFastColoredTextBox; }
            set
            {
                if (withEventsField_parentFastColoredTextBox != null)
                {
                    withEventsField_parentFastColoredTextBox.VisualMarkerClick -= FastColoredTextBox_VisualMarkerClick;
                    withEventsField_parentFastColoredTextBox.Disposed -= FastColoredTextBox_Disposed;
                    withEventsField_parentFastColoredTextBox.VisibleRangeChanged -= FastColoredTextBox_VisibleRangeChanged;
                    withEventsField_parentFastColoredTextBox.TextChanged -= FastColoredTextBox_TextChanged;
                    withEventsField_parentFastColoredTextBox.TextChanged -= TestFastColoredTextBox_TextChanged;
                }
                withEventsField_parentFastColoredTextBox = value;
                if (withEventsField_parentFastColoredTextBox != null)
                {
                    withEventsField_parentFastColoredTextBox.VisualMarkerClick += FastColoredTextBox_VisualMarkerClick;
                    withEventsField_parentFastColoredTextBox.Disposed += FastColoredTextBox_Disposed;
                    withEventsField_parentFastColoredTextBox.VisibleRangeChanged += FastColoredTextBox_VisibleRangeChanged;
                    withEventsField_parentFastColoredTextBox.TextChanged += FastColoredTextBox_TextChanged;
                    withEventsField_parentFastColoredTextBox.TextChanged += TestFastColoredTextBox_TextChanged;
                }
            }

        }

        #endregion "Underlying Control"

        
        #region "Click"

        private ContextMenuStrip withEventsField_ErrorMenu = new ContextMenuStrip();
        private ContextMenuStrip ErrorMenu
        {
            get { return withEventsField_ErrorMenu; }
            set
            {
                if (withEventsField_ErrorMenu != null)
                {
                    withEventsField_ErrorMenu.Closed -= ErrorMenu_Closed;
                }
                withEventsField_ErrorMenu = value;
                if (withEventsField_ErrorMenu != null)
                {
                    withEventsField_ErrorMenu.Closed += ErrorMenu_Closed;
                }
            }
        }


        //Menu
        private i00SpellCheck.Menu.AddSpellItemsToMenu withEventsField_SpellMenuItems = new i00SpellCheck.Menu.AddSpellItemsToMenu { }; //ContextMenuStrip = ErrorMenu };
        private i00SpellCheck.Menu.AddSpellItemsToMenu SpellMenuItems
        {
            get { return withEventsField_SpellMenuItems; }
            set
            {
                if (withEventsField_SpellMenuItems != null)
                {
                    withEventsField_SpellMenuItems.WordAdded -= SpellMenuItems_WordAdded;
                    withEventsField_SpellMenuItems.WordChanged -= SpellMenuItems_WordChanged;
                    withEventsField_SpellMenuItems.WordIgnored -= SpellMenuItems_WordIgnored;
                    withEventsField_SpellMenuItems.WordRemoved -= SpellMenuItems_WordRemoved;
                }
                withEventsField_SpellMenuItems = value;
                if (withEventsField_SpellMenuItems != null)
                {
                    withEventsField_SpellMenuItems.WordAdded += SpellMenuItems_WordAdded;
                    withEventsField_SpellMenuItems.WordChanged += SpellMenuItems_WordChanged;
                    withEventsField_SpellMenuItems.WordIgnored += SpellMenuItems_WordIgnored;
                    withEventsField_SpellMenuItems.WordRemoved += SpellMenuItems_WordRemoved;
                }
            }

        }


        //Error click
        private void FastColoredTextBox_VisualMarkerClick(object sender, FastColoredTextBoxNS.VisualMarkerEventArgs e)
        {
            if (parentFastColoredTextBox.ReadOnly)
                return;
            var ErrorStyleMarker = e.Marker as ErrorStyle.ErrorStyleMarker;
            if (ErrorStyleMarker != null)
            {
                parentFastColoredTextBox.Selection = ErrorStyleMarker.Range;

                SpellMenuItems.RemoveSpellMenuItems();
                SpellMenuItems.AddItems(ErrorStyleMarker.Range.Text, CurrentDictionary, CurrentDefinitions, CurrentSynonyms, Settings);
                ErrorMenu.Show(parentFastColoredTextBox, new Point(ErrorStyleMarker.rectangle.X, ErrorStyleMarker.rectangle.Bottom));
            }
        }


        private void ErrorMenu_Closed(object sender, System.Windows.Forms.ToolStripDropDownClosedEventArgs e)
        {
            SpellMenuItems.RemoveSpellMenuItems();
        }


        private void SpellMenuItems_WordAdded(object sender, i00SpellCheck.Menu.AddSpellItemsToMenu.SpellItemEventArgs e)
        {
            try
            {
                DictionaryAddWord(e.Word);
            }
            catch (Exception ex)
            {
                Interaction.MsgBox("The following error occured adding \"" + e.Word + "\" to the dictionary:" + Constants.vbCrLf + ex.Message, MsgBoxStyle.Critical);
            }
        }


        private void SpellMenuItems_WordChanged(object sender, i00SpellCheck.Menu.AddSpellItemsToMenu.SpellItemEventArgs e)
        {
            var Start = parentFastColoredTextBox.Selection.Start;
            parentFastColoredTextBox.InsertText(e.Word);
            parentFastColoredTextBox.Selection = new Range(parentFastColoredTextBox, Start, parentFastColoredTextBox.Selection.End);
        }


        private void SpellMenuItems_WordIgnored(object sender, i00SpellCheck.Menu.AddSpellItemsToMenu.SpellItemEventArgs e)
        {
            try
            {
                DictionaryIgnoreWord(e.Word);
            }
            catch (Exception ex)
            {
                Interaction.MsgBox("The following error ignoring \"" + e.Word + "\":" + Constants.vbCrLf + ex.Message, MsgBoxStyle.Critical);
            }
        }


        private void SpellMenuItems_WordRemoved(object sender, i00SpellCheck.Menu.AddSpellItemsToMenu.SpellItemEventArgs e)
        {
            try
            {
                DictionaryRemoveWord(e.Word);
            }
            catch (Exception ex)
            {
                Interaction.MsgBox("The following error occured removing \"" + e.Word + "\" from the dictionary:" + Constants.vbCrLf + ex.Message, MsgBoxStyle.Critical);
            }
        }

        #endregion  "Click"


        #region "Painting"

        #region "Error Styles"

        private ErrorStyle withEventsField_SpellErrorStyle = new ErrorStyle { WordState = Dictionary.SpellCheckWordError.SpellError };
        private ErrorStyle SpellErrorStyle
        {
            get { return withEventsField_SpellErrorStyle; }
            set
            {
                if (withEventsField_SpellErrorStyle != null)
                {
                    withEventsField_SpellErrorStyle.SpellCheckErrorPaint -= ErrorStyle_SpellCheckErrorPaint;
                }
                withEventsField_SpellErrorStyle = value;
                if (withEventsField_SpellErrorStyle != null)
                {
                    withEventsField_SpellErrorStyle.SpellCheckErrorPaint += ErrorStyle_SpellCheckErrorPaint;
                }
            }
        }


        private ErrorStyle withEventsField_IgnoreErrorStyle = new ErrorStyle { WordState = Dictionary.SpellCheckWordError.Ignore };
        private ErrorStyle IgnoreErrorStyle
        {
            get { return withEventsField_IgnoreErrorStyle; }
            set
            {
                if (withEventsField_IgnoreErrorStyle != null)
                {
                    withEventsField_IgnoreErrorStyle.SpellCheckErrorPaint -= ErrorStyle_SpellCheckErrorPaint;
                }
                withEventsField_IgnoreErrorStyle = value;
                if (withEventsField_IgnoreErrorStyle != null)
                {
                    withEventsField_IgnoreErrorStyle.SpellCheckErrorPaint += ErrorStyle_SpellCheckErrorPaint;
                }
            }
        }


        private ErrorStyle withEventsField_CaseErrorStyle = new ErrorStyle { WordState = Dictionary.SpellCheckWordError.CaseError };
        private ErrorStyle CaseErrorStyle
        {
            get { return withEventsField_CaseErrorStyle; }
            set
            {
                if (withEventsField_CaseErrorStyle != null)
                {
                    withEventsField_CaseErrorStyle.SpellCheckErrorPaint -= ErrorStyle_SpellCheckErrorPaint;
                }
                withEventsField_CaseErrorStyle = value;
                if (withEventsField_CaseErrorStyle != null)
                {
                    withEventsField_CaseErrorStyle.SpellCheckErrorPaint += ErrorStyle_SpellCheckErrorPaint;
                }
            }

        }


        //Owner Draw Errors
        private void ErrorStyle_SpellCheckErrorPaint(object sender, ref i00SpellCheck.SpellCheckControlBase.SpellCheckCustomPaintEventArgs e)
        {
            OnSpellCheckErrorPaint(ref e);
        }

        #region "Error Style"

        private class ErrorStyle : Style
        {

            #region "Marker"

            public class ErrorStyleMarker : FastColoredTextBoxNS.StyleVisualMarker
            {

                public override System.Windows.Forms.Cursor Cursor
                {
                    get { return Cursors.Default; }
                }


                public Range Range;
                public ErrorStyleMarker(Range Range, Rectangle rectangle, FastColoredTextBoxNS.Style style)
                    : base(rectangle, style)
                {
                    this.Range = Range;
                }
            }

            #endregion "Marker"

            public override void OnVisualMarkerClick(FastColoredTextBoxNS.FastColoredTextBox tb, FastColoredTextBoxNS.VisualMarkerEventArgs args)
            {
                base.OnVisualMarkerClick(tb, args);
            }


            public FastColoredTextBoxNS.FastColoredTextBox FastColoredTextBox;
            public event SpellCheckErrorPaintEventHandler SpellCheckErrorPaint;
            public delegate void SpellCheckErrorPaintEventHandler(object sender, ref SpellCheckCustomPaintEventArgs e);

            protected void OnSpellCheckErrorPaint(ref SpellCheckCustomPaintEventArgs e)
            {
                if (SpellCheckErrorPaint != null)
                {
                    //SpellCheckErrorPaint(this, e);
                }
            }


            public i00SpellCheck.Dictionary.SpellCheckWordError WordState;
            public override void Draw(Graphics gr, Point position, Range range)
            {
                Size size = Style.GetSizeOfRange(range);
                Rectangle rect = new Rectangle(position, size);

                var eArgs = new SpellCheckCustomPaintEventArgs
                {
                    Graphics = gr,
                    Word = range.Text,
                    Bounds = rect,
                    WordState = WordState
                };
                OnSpellCheckErrorPaint(ref eArgs);
                switch (WordState)
                {
                    case Dictionary.SpellCheckWordError.CaseError:
                    case Dictionary.SpellCheckWordError.SpellError:
                        if (eArgs.DrawDefault)
                        {
                            DrawingFunctions.DrawWave(gr, new Point(rect.Left, rect.Bottom), new Point(rect.Right, rect.Bottom), Color);
                        }
                        AddVisualMarker(FastColoredTextBox, new ErrorStyleMarker(range, rect, this));
                        break;
                    case Dictionary.SpellCheckWordError.Ignore:
                        if (eArgs.DrawDefault)
                        {
                            using (Pen p = new Pen(Color))
                            {
                                gr.DrawLine(p, new Point(rect.Left, rect.Bottom), new Point(rect.Right, rect.Bottom));
                            }
                        }
                        AddVisualMarker(FastColoredTextBox, new ErrorStyleMarker(range, rect, this));
                        break;
                }
            }

            public Color Color;
        }

        #endregion "Error Style"

        #endregion  "Error Styles"


        private Timer withEventsField_tmrRepaint = new Timer { Interval = 1, Enabled = false };


        private Timer tmrRepaint
        {
            get { return withEventsField_tmrRepaint; }
            set
            {
                if (withEventsField_tmrRepaint != null)
                {
                    withEventsField_tmrRepaint.Tick -= tmrRepaint_Tick;
                }
                withEventsField_tmrRepaint = value;
                if (withEventsField_tmrRepaint != null)
                {
                    withEventsField_tmrRepaint.Tick += tmrRepaint_Tick;
                }
            }
        }


        public override void RepaintControl()
        {
            // qwertyuiop - will probably look at a way to pass back new errors eventually so 
            // we can just paint those as this would be able to SetStyle on those new errors ...
            // this has not been an issue before... as most controls repaint as a whole...
            if (parentFastColoredTextBox != null)
            {
                UpdateErrors(parentFastColoredTextBox.VisibleRange);
            }
        }


        private void tmrRepaint_Tick(object sender, System.EventArgs e)
        {
            UpdateErrors(parentFastColoredTextBox.VisibleRange);
            tmrRepaint.Enabled = false;
        }


        private void FastColoredTextBox_Disposed(object sender, System.EventArgs e)
        {
            tmrRepaint.Dispose();
            SpellErrorStyle.Dispose();
            IgnoreErrorStyle.Dispose();
            CaseErrorStyle.Dispose();
        }


        private void FastColoredTextBox_VisibleRangeChanged(object sender, System.EventArgs e)
        {
            tmrRepaint.Stop();
            tmrRepaint.Start();
        }


        private void FastColoredTextBox_TextChanged(System.Object sender, FastColoredTextBoxNS.TextChangedEventArgs e)
        {
            //qwertyuiop - don't need this as the VisibleRangeChanged event gets called when text is changed :)
            //UpdateErrors(e.ChangedRange)
        }


        private void UpdateErrors(FastColoredTextBoxNS.Range Range)
        {
            Range.ClearStyle(IgnoreErrorStyle, CaseErrorStyle, SpellErrorStyle);
            if (OKToDraw == false)
                return;
            //clear errors...

            //var text = Range.Text;

            dynamic text = Range.Text;
            List<Range> RangesToSpellCheck = new List<Range>();
            RangesToSpellCheck.Add(Range);


            if (!string.IsNullOrEmpty(mc_SpellCheckMatch))
            {
                dynamic matches = Regex.Matches(text, mc_SpellCheckMatch, RegexOptions.Multiline);

                foreach (dynamic match in matches)
                    text += matches + " ";

                //text = Strings.Join((from xItem in matches.OfType<Match>()xItem.Value).ToArray());
                //text = Strings.Join((from xItem in matches.OfType<Match>(xItem.Value).ToArray());
                //text = Strings.Join((from xItem in matches.OfType<Match>(xItem.Value).ToArray, " ");

                RangesToSpellCheck = Range.GetRanges(mc_SpellCheckMatch, RegexOptions.Multiline).ToList();
            }



            RangesToSpellCheck.Add(Range);
            if (!string.IsNullOrEmpty(mc_SpellCheckMatch))
            {
                var matches = Regex.Matches(text, mc_SpellCheckMatch, RegexOptions.Multiline);

                //text = Strings.Join((from xItem in matches.OfType<Match>()xItem.Value).ToArray, " ");

                foreach (dynamic match in matches)
                    text += matches + " ";

                RangesToSpellCheck = Range.GetRanges(mc_SpellCheckMatch, RegexOptions.Multiline).ToList();
            }

            text = Dictionary.Formatting.RemoveWordBreaks(text);
            var Words = Strings.Split(text, " ");

            Dictionary<string, i00SpellCheck.Dictionary.SpellCheckWordError> NewWords = new Dictionary<string, i00SpellCheck.Dictionary.SpellCheckWordError>();
            foreach (string word in Words)
            {
                //word = word_loopVariable;
                if (!string.IsNullOrEmpty(word))
                {
                    i00SpellCheck.Dictionary.SpellCheckWordError WordState = Dictionary.SpellCheckWordError.SpellError;
                    if (dictCache.ContainsKey(word))
                    {
                        //load from cache
                        WordState = dictCache[word];
                    }
                    else
                    {
                        if (NewWords.ContainsKey(word) == false)
                        {
                            NewWords.Add(word, Dictionary.SpellCheckWordError.OK);
                        }

                        WordState = Dictionary.SpellCheckWordError.OK;
                    }
                    Style style = null;
                    switch (WordState)
                    {
                        case Dictionary.SpellCheckWordError.Ignore:
                            if (DrawIgnored())
                            {
                                style = IgnoreErrorStyle;
                            }
                            break;
                        case Dictionary.SpellCheckWordError.CaseError:
                            style = CaseErrorStyle;
                            break;
                        case Dictionary.SpellCheckWordError.SpellError:
                            style = SpellErrorStyle;
                            break;
                    }
                    if (style != null)
                    {
                        foreach (Range RangeToSpellCheck in RangesToSpellCheck)
                        {
                            //RangeToSpellCheck = RangeToSpellCheck_loopVariable;
                            RangeToSpellCheck.SetStyle(style, "\\b" + Regex.Escape(word) + "\\b");
                        }
                    }
                }
            }
            if (NewWords.Count > 0)
            {
                AddWordsToCache(NewWords);
            }
        }

        #endregion  "Painting"


        #region "Test Harness"

        #region "HTML Color Highlighting"

        private class HTMLColorMap : Dictionary<string, string>
        {
            static readonly Microsoft.VisualBasic.CompilerServices.StaticLocalInitFlag static_ColorMapList_mc_ColorMapList_Init = new Microsoft.VisualBasic.CompilerServices.StaticLocalInitFlag();
            static HTMLColorMap static_ColorMapList_mc_ColorMapList;

            public static HTMLColorMap ColorMapList
            {
                get
                {
                    lock (static_ColorMapList_mc_ColorMapList_Init)
                    {
                        try
                        {
                            if (InitStaticVariableHelper(static_ColorMapList_mc_ColorMapList_Init))
                            {
                                static_ColorMapList_mc_ColorMapList = new HTMLColorMap();
                            }
                        }
                        finally
                        {
                            static_ColorMapList_mc_ColorMapList_Init.State = 1;
                        }
                    }
                    return static_ColorMapList_mc_ColorMapList;
                }
            }


            public string GetDotNetColorName(string HTMLColorName)
            {
                string functionReturnValue = null;
                //functionReturnValue = (from xItem in this where Strings.LCase(xItem.Value) == Strings.LCase(HTMLColorName)xItem.Key).FirstOrDefault;
                //functionReturnValue = (from xItem in this where Strings.LCase(xItem.Value) == HTMLColorName.ToLower();

                if (string.IsNullOrEmpty(functionReturnValue))
                {
                    functionReturnValue = HTMLColorName;
                }
                return functionReturnValue;
            }


            public HTMLColorMap()
            {
                //color mapping
                base.Add("ActiveCaptionText", "CaptionText");
                base.Add("ControlText", "ButtonText");
                base.Add("Desktop", "Background");
                base.Add("ControlLight", "ThreeDLightShadow");
                base.Add("ControlLightLight", "ThreeDHighlight");
                base.Add("Info", "InfoBackground");

                //depriciated
                base.Add("GradientActiveCaption", "");
                base.Add("GradientInactiveCaption", "");
                base.Add("MenuBar", "");
                base.Add("MenuHighlight", "");
                base.Add("Control", "");
                base.Add("ControlDarkDark", "");
                base.Add("ControlDark", "");

            }


            static bool InitStaticVariableHelper(Microsoft.VisualBasic.CompilerServices.StaticLocalInitFlag flag)
            {
                if (flag.State == 0)
                {
                    flag.State = 2;
                    return true;
                }
                else if (flag.State == 2)
                {
                    throw new Microsoft.VisualBasic.CompilerServices.IncompleteInitialization();
                }
                else
                {
                    return false;
                }
            }
        }


        readonly Microsoft.VisualBasic.CompilerServices.StaticLocalInitFlag static_TestHarness_TextChanged_cs_Init = new Microsoft.VisualBasic.CompilerServices.StaticLocalInitFlag();
        ColorStyle static_TestHarness_TextChanged_cs;


        //// make it first...
        //dynamic Styles = (from xItem in parentFastColoredTextBox.Styleswhere ! object.ReferenceEquals(xItem, cs)).ToList;
        //Styles.Insert(0, cs);
        //for (i = Information.LBound(parentFastColoredTextBox.Styles); i <= Information.UBound(parentFastColoredTextBox.Styles); i++) {
        //    parentFastColoredTextBox.Styles(i) = Styles(i);
        //}


        string static_TestHarness_TextChanged_ColorMatch;
        private void TestHarness_TextChanged(System.Object sender, FastColoredTextBoxNS.TextChangedEventArgs e)
        {
            if (parentFastColoredTextBox.Language == Language.HTML)
            {
                lock (static_TestHarness_TextChanged_cs_Init)
                {
                    try
                    {
                        if (InitStaticVariableHelper(static_TestHarness_TextChanged_cs_Init))
                        {
                            static_TestHarness_TextChanged_cs = new ColorStyle(parentFastColoredTextBox);
                        }
                    }
                    finally
                    {
                        static_TestHarness_TextChanged_cs_Init.State = 1;
                    }
                }

                if (parentFastColoredTextBox.Styles[0] == null || !(parentFastColoredTextBox.Styles[0] is ColorStyle))
                {
                    parentFastColoredTextBox.ClearStylesBuffer();
                    parentFastColoredTextBox.AddStyle(static_TestHarness_TextChanged_cs);
                }

                if (string.IsNullOrEmpty(static_TestHarness_TextChanged_ColorMatch))
                {
                    List<string> lstColorMatch = new List<string>();
                    lstColorMatch.Add("#[a-f0-9]{6}\\b");
                    //match "#ff00ff"
                    lstColorMatch.Add("\\brgb\\(\\s*?\\d+?\\s*?\\,\\s*?\\d+?\\s*?\\,\\s*?\\d+?\\s*?\\)");
                    //match "rgb(255, 0, 255)"
                    //\s.*?

                    //match known colors (such as "red")
                    foreach (object item_loopVariable in Enum.GetValues(typeof(KnownColor)))
                    {
                        var item = item_loopVariable;
                        if (HTMLColorMap.ColorMapList.ContainsKey(item.ToString()))
                        {
                            if (string.IsNullOrEmpty(HTMLColorMap.ColorMapList[item.ToString()]))
                            {
                                //depriciated color
                            }
                            else
                            {
                                lstColorMatch.Add("\\b" + HTMLColorMap.ColorMapList[item.ToString()] + "\\b");
                            }
                        }
                        else
                        {
                            lstColorMatch.Add("\\b" + item.ToString() + "\\b");
                        }
                    }
                    static_TestHarness_TextChanged_ColorMatch = "(" + Strings.Join(lstColorMatch.ToArray(), "|") + ")";
                }

                e.ChangedRange.ClearStyle(static_TestHarness_TextChanged_cs);
                e.ChangedRange.SetStyle(static_TestHarness_TextChanged_cs, static_TestHarness_TextChanged_ColorMatch, RegexOptions.IgnoreCase);
            }
        }


        private class ColorStyle : TextStyle
        {
            FastColoredTextBox parent;
            public ColorStyle(FastColoredTextBox parent)
                : base(null, null, System.Drawing.FontStyle.Regular)
            {
                this.parent = parent;
            }

            public override void Draw(Graphics gr, Point position, Range range)
            {
                Rectangle rect = new Rectangle(position, Style.GetSizeOfRange(range));
                rect.Inflate(1, 1);
                using (System.Drawing.Drawing2D.GraphicsPath path = Style.GetRoundedRectangle(rect, 7))
                {
                    //try is here as otherwise if the word is "red" for eg and the "ed" part is off the visible area, the range.Text will be "r"... this does not translate to a html color
                    bool DrawBorder = false;

                    try
                    {
                        Color BGColor = default(Color);
                        try
                        {
                            BGColor = ColorTranslator.FromHtml(HTMLColorMap.ColorMapList.GetDotNetColorName(range.Text));
                        }
                        catch (Exception ex)
                        {
                            //try the css rgb()


                            //var TextColorSection = range.Text.Substring(range.Text.IndexOf("(") + 1);
                            dynamic TextColorSection = range.Text.Substring(range.Text.IndexOf("(") + 1);
                            TextColorSection = TextColorSection.Substring(0, TextColorSection.IndexOf(")"));
                            TextColorSection = Regex.Replace(TextColorSection, "\\s", "");

                            //dynamic arrColors = (from xItem in Strings.Split(TextColorSection, ",") + Convert.ToInt32(Math.Min(Conversion.Val(xItem), 255))).ToArray;
                            //var arrColors = (from xItem in Strings.Split(TextColorSection, ",") + Convert.ToInt32(Math.Min(Conversion.Val(xItem), 255))).ToArray;

                            //BGColor = Color.FromArgb(255, arrColors(0), arrColors(1), arrColors(2));



                            //object TextColorSection = range.Text.Substring((range.Text.IndexOf("(") + 1));
                            //TextColorSection = TextColorSection.Substring(0, TextColorSection.IndexOf(")"));
                            //TextColorSection = TextColorSection.Replace("\\s", "");
                            //object arrColors = From;
                            //xItem;
                            //InTextColorSection.Split(",");
                            //switch (int.Parse(Math.Min(double.Parse(xItem), 255))) {
                            //}
                            //.ToArray;
                            //BGColor = Color.FromArgb(255, arrColors(0), arrColors(1), arrColors(2));

                            string err = ("Error: " + ex.Message);
                            MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];
                            mainFrm.statusbarMainMessage.Text = (err);
                            //MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);

                        }
                        using (SolidBrush sb = new SolidBrush(BGColor))
                        {
                            //MyBase.BackgroundBrush = sb
                            gr.FillPath(sb, path);
                        }

                        if (Convert.ToInt32(BGColor.R) + Convert.ToInt32(BGColor.G) + Convert.ToInt32(BGColor.B) < 383)
                        {
                            //color is dark so make text light :)
                            base.ForeBrush = new SolidBrush(Color.White);
                        }
                        else
                        {
                            //color is light so make text dark :)
                            base.ForeBrush = new SolidBrush(Color.Black);
                        }

                        DrawBorder = (255 * 3) - (Convert.ToInt32(BGColor.R) + Convert.ToInt32(BGColor.G) + Convert.ToInt32(BGColor.B)) < 64;

                    }
                    catch (Exception ex)
                    {
                        base.ForeBrush = new SolidBrush(Color.Black);

                        string err = ("Error: " + ex.Message);
                        MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];
                        mainFrm.statusbarMainMessage.Text = (err);
                        //MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
                    }
                    base.Draw(gr, position, range);

                    if (DrawBorder)
                    {
                        //color is nearly invisible ... so add a border...
                        using (Pen p = new Pen(Color.LightGray))
                        {
                            gr.DrawPath(p, path);
                        }
                    }
                }

            }
        }

        #endregion "HTML Color Highlighting"


        public Control SetupControl(System.Windows.Forms.Control Control)
        {
            var FastColoredTextBox = Control as FastColoredTextBox;
            if (FastColoredTextBox != null)
            {
                Panel pnlHolder = new Panel();
                Panel pnlOptions = new Panel();
                pnlOptions.Dock = DockStyle.Bottom;
                pnlHolder.Controls.Add(pnlOptions);

                string[] CodeRadioButtonNames = {
					"VB.Net",
					"HTML"
				};
                int OptionLeftPos = 0;
                foreach (string item_loopVariable in CodeRadioButtonNames)
                {
                    var item = item_loopVariable;
                    RadioButton CodeRadioButton = new RadioButton();
                    CodeRadioButton.Text = item;
                    CodeRadioButton.Left = OptionLeftPos;
                    CodeRadioButton.AutoSize = true;
                    OptionLeftPos += CodeRadioButton.Width;
                    pnlOptions.Controls.Add(CodeRadioButton);
                    CodeRadioButton.CheckedChanged += CodeRadioButton_CheckedChanged;
                }
                pnlOptions.Height = pnlOptions.Controls[0].Height;
                pnlHolder.Dock = DockStyle.Fill;

                //AddHandler FastColoredTextBox.TextChanged, AddressOf TestHarness_TextChanged
                FastColoredTextBox.Dock = DockStyle.Fill;

                TestWebBrowser = new WebBrowser();
                TestWebBrowser.AllowWebBrowserDrop = false;
                TestWebBrowser.IsWebBrowserContextMenuEnabled = false;
                TestWebBrowser.Dock = DockStyle.Fill;
                TestWebBrowser.DocumentText = "<body></body>";
                TestWebBrowser.Navigating += TestWebBrowser_Navigating;

                TestSplitContainer = new SplitContainer();
                TestSplitContainer.BackColor = i00SpellCheck.DrawingFunctions.BlendColor(Color.FromKnownColor(KnownColor.Highlight), Color.FromKnownColor(KnownColor.Info), 63);
                TestSplitContainer.Orientation = Orientation.Horizontal;
                TestSplitContainer.Dock = DockStyle.Fill;
                TestSplitContainer.Panel1.Controls.Add(FastColoredTextBox);
                TestSplitContainer.Panel2.Controls.Add(TestWebBrowser);

                pnlHolder.Controls.Add(TestSplitContainer);
                TestSplitContainer.SplitterDistance = Convert.ToInt32((TestSplitContainer.Height - TestSplitContainer.SplitterWidth) * 0.75);
                TestSplitContainer.BringToFront();

                //select an option
                pnlOptions.Controls.OfType<RadioButton>().First().Checked = true;

                FastColoredTextBox.TextChanged += TestHarness_TextChanged;

                return pnlHolder;
            }
            else
            {
                return null;
            }
        }


        private void TestFastColoredTextBox_TextChanged(System.Object sender, FastColoredTextBoxNS.TextChangedEventArgs e)
        {
            if (TestWebBrowser != null && TestWebBrowser.Document != null && TestWebBrowser.Document.Body != null)
            {
                TestWebBrowser.Document.Body.InnerHtml = parentFastColoredTextBox.Text;
            }
        }


        WebBrowser TestWebBrowser;
        SplitContainer TestSplitContainer;
        private void TestWebBrowser_Navigating(object sender, System.Windows.Forms.WebBrowserNavigatingEventArgs e)
        {
            e.Cancel = true;
            try
            {
                System.Diagnostics.Process.Start(e.Url.AbsoluteUri);

            }
            catch { } 
            //catch (Exception ex) { }
        }


        private void CodeRadioButton_CheckedChanged(System.Object sender, System.EventArgs e)
        {
            var oSender = (RadioButton)sender;

            if (oSender.Checked == true)
            {
                switch (oSender.Text)
                {
                    case "VB.Net":
                        //parentFastColoredTextBox.LeftBracket = "("c;
                        //parentFastColoredTextBox.RightBracket = ")"c;

                        parentFastColoredTextBox.Language = Language.VB;

                        SpellCheckMatch = "('.*$|\".*?\")";

                        TestSplitContainer.Panel2Collapsed = true;

                        parentFastColoredTextBox.Text = "'Simple test to check spelling with 3rd party controls" + Constants.vbCrLf + "'This test is done on the FastColoredTextBox (open source control) that is hosted on CodeProject" + Constants.vbCrLf + "'The article can be found at: http://www.codeproject.com/Articles/161871/Fast-Colored-TextBox-for-syntax-highlighting" + Constants.vbCrLf + "" + Constants.vbCrLf + "'i00 Does not take credit for any work in the FastColoredTextBox.dll" + Constants.vbCrLf + "'i00 is however solely responsible for the spellcheck plugin to interface with FastColoredTextBox" + Constants.vbCrLf + "" + Constants.vbCrLf + "'As you can see only comments and string blocks are corrected" + Constants.vbCrLf + "'This is due to the SpellCheckMatch property being set" + Constants.vbCrLf + "" + Constants.vbCrLf + "'Click on a missspelled word to correct..." + Constants.vbCrLf + "" + Constants.vbCrLf + "Dim test = \"Test with some bad spellling!\"" + Constants.vbCrLf + "" + Constants.vbCrLf + "#Region \"Char\"" + Constants.vbCrLf + "   " + Constants.vbCrLf + "   ''' <summary>" + Constants.vbCrLf + "   ''' Char and style" + Constants.vbCrLf + "   ''' </summary>" + Constants.vbCrLf + "   Public Structure CharStyle" + Constants.vbCrLf + "       Public c As Char" + Constants.vbCrLf + "       Public style As StyleIndex" + Constants.vbCrLf + "   " + Constants.vbCrLf + "       Public Sub CharStyle(ByVal ch As Char)" + Constants.vbCrLf + "           c = ch" + Constants.vbCrLf + "           Style = StyleIndex.None" + Constants.vbCrLf + "       End Sub" + Constants.vbCrLf + "   " + Constants.vbCrLf + "   End Structure" + Constants.vbCrLf + "   " + Constants.vbCrLf + "#End Region";
                        break;
                    case "HTML":
                        //parentFastColoredTextBox.LeftBracket = "<"c;
                        //parentFastColoredTextBox.RightBracket = ">"c;

                        TestSplitContainer.Panel2Collapsed = false;


                        parentFastColoredTextBox.Language = Language.HTML;

                        SpellCheckMatch = "(?<!<[^>]*)[^<^>]*";

                        //LinkColor here is used to demo css rgb();
                        Color LinkColor = i00SpellCheck.DrawingFunctions.BlendColor(Color.FromKnownColor(KnownColor.HotTrack), Color.FromKnownColor(KnownColor.WindowText));
                        var LinkColorHTML = "rgb(" + LinkColor.R + ", " + LinkColor.G + ", " + LinkColor.B + ")";

                        var BGColorHTML = ColorTranslator.ToHtml(i00SpellCheck.DrawingFunctions.BlendColor(Color.FromKnownColor(KnownColor.Highlight), Color.FromKnownColor(KnownColor.Window), 63));

                        parentFastColoredTextBox.Text = "<table style=\"border:1px solid highlight;font-family:Arial;font-size:10 pt\" bgcolor=\"" + BGColorHTML + "\">" + Constants.vbCrLf + "    <tr>" + Constants.vbCrLf + "        <td valign=top>" + Constants.vbCrLf + "            <img src=\"http://i00productions.org/i00logo/?size=48\">" + Constants.vbCrLf + "        </td>" + Constants.vbCrLf + "        <td>" + Constants.vbCrLf + "            This is a test done on the <i>FastColoredTextBox</i> (open source control) that is hosted on CodeProject<br>" + Constants.vbCrLf + "            The article can be found <a style=\"color:" + LinkColorHTML + "\" href=\"http://www.codeproject.com/Articles/161871/Fast-Colored-TextBox-for-syntax-highlighting\">here</a><br>" + Constants.vbCrLf + "            <br>" + Constants.vbCrLf + "            i00 <b>does not</b> take credit for any work in the <i>FastColoredTextBox.dll</i> used in this project<br>" + Constants.vbCrLf + "            i00 is however solely responsible for the spellcheck plugin to interface with <i>FastColoredTextBox</i><br>" + Constants.vbCrLf + "            <br>" + Constants.vbCrLf + "            As you can see HTML tags are not corrected<br>" + Constants.vbCrLf + "            This is due to the <i>SpellCheckMatch</i> property being set<br>" + Constants.vbCrLf + "            <br>" + Constants.vbCrLf + "            Click on a missspelled word to correct...<br>" + Constants.vbCrLf + "            <br>" + Constants.vbCrLf + "            Test with some bad spellling!" + Constants.vbCrLf + "        </td>" + Constants.vbCrLf + "    </tr>" + Constants.vbCrLf + "</table>";
                        break;
                }

                parentFastColoredTextBox.SelectionStart = 0;
                parentFastColoredTextBox.SelectionLength = 0;
                parentFastColoredTextBox.DoCaretVisible();

            }
        }


        public SpellCheck()
        {
            SettingsChanged += SpellCheckFastColoredTextBox_SettingsChanged;
        }


        static bool InitStaticVariableHelper(Microsoft.VisualBasic.CompilerServices.StaticLocalInitFlag flag)
        {
            if (flag.State == 0)
            {
                flag.State = 2;
                return true;
            }
            else if (flag.State == 2)
            {
                throw new Microsoft.VisualBasic.CompilerServices.IncompleteInitialization();
            }
            else
            {
                return false;
            }
        }

        #endregion "Test Harness"
    }
}
