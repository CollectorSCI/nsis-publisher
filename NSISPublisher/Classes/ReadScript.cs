﻿/// To do list:
/// 
/// 

#region using directives ====================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

#endregion using directives


namespace NSISPublisher
{
    /// <summary>
    /// A class to parse an NSIS script for project variables
    /// </summary>

    class ReadScript
    {
        #region Declares, Imports. etc. =====================================================================

        public static string defaultPublisher = Properties.Settings.Default.defaultPublisher;
        public static string pubOutPath = Properties.Settings.Default.pubOutPath;
        public static string nsisPath = Properties.Settings.Default.nsisPath;
        public static bool runNSIS = Properties.Settings.Default.runNSIS;
        public static bool deleteNSISSrpt = Properties.Settings.Default.deleteNSISSrpt;
        public static bool missingSVGReadMeWarning = Properties.Settings.Default.missingSVGReadMeWarning;

        public static string projNotes = null;
        public static string projName = null;
        public static string projVer = null;
        public static string projPub = null;
        public static string projEngine = null;
        public static string projFileName = null;
        public static string projShortName = null;
        public static string projDOSName = null;
        public static string projPath = null;
        public static string projSaveName = null;

        public static string projMainEXE = null;

        public static bool includeSRC = true;

        #endregion Declares, Imports. etc.


        public static void ReadNSI(string scriptFile)
        {
            // Read the file and display it line by line.
            StreamReader fileReader = null;
            if (File.Exists(scriptFile))
            {
                fileReader = new StreamReader(scriptFile);
                //else
                //    return;

                string line;
                while ((line = fileReader.ReadLine()) != null)
                {
                    if (!String.IsNullOrEmpty(line))
                    {
                        if (line.Contains("; Installer Notes:"))
                        {
                            Props.projNotes = line.Replace("; Installer Notes: ", null);
                            Props.projNotes = line.Replace("; Installer Notes:", null);
                        }

                        if (line.Contains("Section /o \"Source Files"))
                        {
                            Props.includeSRC = true;
                        }

                        if (!line.Contains("!define PRODUCT_PUBLISHER"))
                            line = line.Replace("\"", null);

                        if (line.Contains("!define PRODUCT_NAME"))
                        {
                            Props.projName = line.Replace("!define PRODUCT_NAME ", null);
                            Props.projName = Props.projName.Replace(" ; Project Name", null);
                            Props.projName = Props.projName.Replace(" ; Installer Name", null);
                        }

                        if (line.Contains("!define PRODUCT_VERSION"))
                        {
                            Props.projVer = line.Replace("!define PRODUCT_VERSION ", null);
                            Props.projVer = Props.projVer.Replace(" ; Product version", null);
                        }

                        if (line.Contains("!define PRODUCT_PUBLISHER"))
                        {
                            Props.projPub = line.Replace("!define PRODUCT_PUBLISHER ", null);
                            Props.projPub = Props.projPub.Replace(" ; Game Developer", null);
                            Props.projPub = Props.projPub.Replace("; Game Developer", null);

                            // If Publisher is null, set project publisher to default publisher
                            if (line.Contains("\"\""))
                                Props.projPub = Properties.Settings.Default.defaultPublisher;

                            Props.projPub = Props.projPub.Replace("\"", null);
                        }

                        if (line.Contains("!define PRODUCT_ENGINE"))
                        {
                            Props.projEngine = line.Replace("!define PRODUCT_ENGINE ", null);
                            Props.projEngine = Props.projEngine.Replace(" ; Engine that game uses", null);
                        }

                        if (line.Contains("!define GAME_FILE_NAME"))
                        {
                            Props.projFileName = line.Replace("!define GAME_FILE_NAME ", null);
                            Props.projFileName = Props.projFileName.Replace(" ; PRODUCT_NAME without Illegal File Name Characters", null);
                        }

                        if (line.Contains("!define SHORT_NAME"))
                        {
                            Props.projShortName = line.Replace("!define SHORT_NAME ", null);
                            Props.projShortName = Props.projShortName.Replace(" ; Common abbreviated game name", null);
                        }

                        if (line.Contains("!define DOS_NAME"))
                        {
                            Props.projDOSName = line.Replace("!define DOS_NAME ", null);
                            Props.projDOSName = Props.projDOSName.Replace(" ; Name to Conform to 8.3 Naming Convention", null);

                            MainFrm.killMsg = true;
                        }

                        if (line.Contains("!define PROJECT_PATH"))
                        {
                            Props.projPath = line.Replace("!define PROJECT_PATH ", null);
                            Props.projPath = Props.projPath.Replace(" ; Name of Folder for Resource Files", null);
                        }

                        if (line.Contains("!define SAVE_NAME"))
                        {
                            Props.projSaveName = line.Replace("!define SAVE_NAME ", null);
                            Props.projSaveName = Props.projSaveName.Replace(" ; Name of Save Game Files", null);
                        }

                        if (line.Contains("!define MAIN_EXE"))
                        {
                            Props.projMainEXE = line.Replace("!define MAIN_EXE ", null);
                            Props.projMainEXE = Props.projMainEXE.Replace(" ; Windows EXE", null);
                            Props.projMainEXE = Props.projMainEXE.Replace(" ; Project Name", null);
                            Props.projMainEXE = Props.projMainEXE.Replace(" ; Project Name.exe", null);
                            Props.projMainEXE = Props.projMainEXE.Replace(" ; Name of Windows Launcher", null);
                        }

                        if (line.Contains("FileWrite $9 'aspect="))
                        {
                            if (line.Contains("true"))
                                Props.projAspect = true;
                            if (!line.Contains("true"))
                                Props.projAspect = false;
                        }
                    }
                }

                fileReader.Close();
            }
        }
    }
}
