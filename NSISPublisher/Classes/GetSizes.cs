﻿/// To do:
/// 
/// 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Reflection;

namespace NSISPublisher
{
    /// <summary>
    /// Class to determine the size of included files
    /// </summary>

    class GetSizes
    {
        #region Declares, Imports. etc. =====================================================================

        public static string mkNSIEXE = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
        public static string mkNSIPath = Path.GetDirectoryName(mkNSIEXE).Replace("%20", " ");

        public static bool accessErr = false;

        public static ErrorLogging errorLogging = new ErrorLogging();
        public static MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];

        #endregion Declares, Imports. etc.


        /// <summary>
        /// Get game base files
        /// </summary>
        /// <param name="target">Props.projPath</param>
        /// <returns>Size of game's base files</returns>
        public static long GetInstallSize(string target)
        {
            long filesLength = 0;

            if (Directory.Exists(Props.projPath))
            {
                foreach (string obj in Directory.GetFiles(Props.projPath, "*.*").OrderBy(f => new FileInfo(f).Extension))
                {
                    string file = Path.GetFileName(obj).ToUpper();
                    string ext = Path.GetExtension(obj).ToUpper();

                    if (file == "AGI" || file == "HGC_FONT" || file.EndsWith("DIR") || file == "OBJECT" || file.Contains("VOL.") || file == Props.projInterpEXE ||
                        ext == ".OVL" || file == "WORDS.TOK" || file == "VERSION" || file == "RESOURCE.*" || ext == ".DRV" || file == "INTERP.ERR" || 
                        file.StartsWith("INSTSTALL.") || file == "GMDRV_CHANGES" || file == "GMDRV_LICENSE" || file == "GMDRV_README.TXT" || file == "GUSDRV.COM" ||
                        file == "INTERP.TXT" || file.StartsWith("VIEW.") || file.StartsWith("PIC.") || file.StartsWith("SCRIPT.") || file.StartsWith("SOUND.") ||
                        file.StartsWith("VOCAB.") || file.StartsWith("FONT.") || file.StartsWith("CURSOR.") || file.StartsWith("PATCH.") || ext == ".V16" || 
                        ext == ".P16" || ext == ".V56" || ext == ".P56" || ext == ".SCR" || ext == ".TEX" || ext == ".SND" || ext == ".VOC" || ext == ".FON" || 
                        ext == ".CUR" || ext == ".PAT" || ext == ".PAL" || ext == ".AUD" || ext == ".SYN" || ext == ".MSG" || ext == ".MAP" || ext == ".HEP" || 
                        ext == ".WAV")
                    {
                        long fileLength = new FileInfo(obj).Length;
                        filesLength += fileLength;
                    }
                }
            }

            return filesLength;
        }


        /// <summary>
        /// Gets size of installer includes
        /// </summary>
        /// <param name="target">Props.projPath</param>
        /// <returns>Publish_Include size</returns>
        public static long GetIncludeSize(string target)
        {
            long filesLength = 0;

            if (Directory.Exists(target))
            {
                foreach (string srcFile in Directory.GetFiles(target, "*.*").OrderBy(f => new FileInfo(f).Extension))
                {
                    long fileLength = new FileInfo(srcFile).Length;
                    filesLength += fileLength;
                }

                foreach (string obj in Directory.EnumerateDirectories(target, "*", SearchOption.AllDirectories).OrderBy(name => name.Replace(Path.DirectorySeparatorChar, '\0'), StringComparer.Ordinal))
                {
                    FileAttributes attr = File.GetAttributes(obj);
                    if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                    {
                        foreach (string subFile in Directory.GetFiles(obj, "*.*").OrderBy(f => new FileInfo(f).Extension))
                        {
                            long fileLength = new FileInfo(subFile).Length;
                            filesLength += fileLength;
                        }
                    }
                }
            }

            return filesLength;
        }


        /// <summary>
        /// Gets folder size of SEC2 (SRC) files
        /// </summary>
        /// <param name="target">Props.projPath</param>
        /// <returns>Souce files size</returns>
        public static long GetSRCFolderSize(string target)
        {
            long filesLength = 0;

            // Select Source Folders
            string lipsyncPath = Props.projPath + "\\lipsync";
            string msgPath = Props.projPath + "\\msg";
            string polyPath = Props.projPath + "\\poly";
            string srcPath = Props.projPath + "\\src";
            
            long gameINILength = 0;
            if (File.Exists(Props.projPath + "\\game.ini"))
                gameINILength = new FileInfo(Props.projPath + "\\game.ini").Length;

            DirectoryInfo lipsyncDirInfo = new DirectoryInfo(lipsyncPath);
            long lipsyncSz = GetSizes.DirSize(lipsyncDirInfo);
            // Return if error
            if (GetSizes.accessErr == true) return 0;

            DirectoryInfo msgDirInfo = new DirectoryInfo(msgPath);
            long msgSz = GetSizes.DirSize(msgDirInfo);
            // Return if error
            if (GetSizes.accessErr == true) return 0;

            DirectoryInfo polyDirInfo = new DirectoryInfo(polyPath);
            long polySz = GetSizes.DirSize(polyDirInfo);
            // Return if error
            if (GetSizes.accessErr == true) return 0;

            DirectoryInfo srcDirInfo = new DirectoryInfo(srcPath);
            long srcSz = GetSizes.DirSize(srcDirInfo);
            // Return if error
            if (GetSizes.accessErr == true) return 0;

            filesLength = gameINILength + lipsyncSz + msgSz + polySz + srcSz;

            //decimal bytes = size;
            //decimal kb = Math.Round(bytes / 1024);
            //decimal mb = Math.Round(bytes / 1048576, 2);

            return filesLength;
        }


        /// <summary>
        /// Retrieves size of selected directory
        /// </summary>
        /// <param name="d">Selected directory DirectoryInfo</param>
        /// <returns>Size of selected directory</returns>
        public static long DirSize(DirectoryInfo d)
        {
            long Size = 0;

            if (!Directory.Exists(d.FullName))
            {
                return 0;
            }
            else
            {
                // Add file sizes.
                try
                {
                    FileInfo[] fis = d.GetFiles();
                    if (fis != null)
                    {
                        foreach (FileInfo fi in fis)
                        {
                            // Exit function if UnauthorizedAccessException error
                            if (accessErr == true)
                                break;

                            Size += fi.Length;
                        }

                        // Add subdirectory sizes.
                        DirectoryInfo[] dis = d.GetDirectories();
                        foreach (DirectoryInfo di in dis)
                        {
                            // Exit function if UnauthorizedAccessException error
                            if (accessErr == true)
                                break;

                            Size += DirSize(di);
                        }
                    }
                }
                catch (UnauthorizedAccessException)
                {
                    string logMsg = string.Format("Unable to access directory {0}", d.FullName);
                    MessageBox.Show(logMsg, MainFrm.appTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    accessErr = true;
                }

                return (Size);
            }
        }
    }
}
