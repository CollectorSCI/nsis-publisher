﻿/// To do list:
/// 
/// 

#region using directives ====================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using System.Windows.Forms;
using System.Globalization;
using Microsoft.Win32;

#endregion using directives


namespace NSISPublisher
{
    /// <summary>
    /// Class to get and read version file for version info
    /// </summary>

    class GetGame
    {
        /// <summary>
        /// Determines Engine and Interpreter EXE used by game
        /// </summary>
        public static void GetEngine()
        {
            // AGI
            if (File.Exists(Props.projPath + @"\AGI"))
            {
                Props.projEngine = "AGI";
                if (File.Exists(Props.projPath + @"\AGI.COM"))
                    Props.projInterpEXE = "AGI.COM";
                if (File.Exists(Props.projPath + @"\SIERRA.COM"))
                    Props.projInterpEXE = "SIERRA.COM";
                return;
            }

            // SCI0 Parser Interface / SCI0 Parser Interface with sciAudio
            // SCI0 Point and Click interface / SCI0 Point and Click interface with sciAudio
            if (File.Exists(Props.projPath + @"\SCIV.EXE"))
            {
                if (!File.Exists(Props.projPath + @"\SRC\PnCMenu.sc"))
                {
                    if (!File.Exists(Props.projPath + @"\sciAudio\sciAudio.exe"))
                        Props.projEngine = "SCI0 Parser Interface";
                    else
                        Props.projEngine = "SCI0 Parser Interface with sciAudio";
                }

                if (File.Exists(Props.projPath + @"\SRC\PnCMenu.sc"))
                {
                    if (!File.Exists(Props.projPath + @"\sciAudio\sciAudio.exe"))
                        Props.projEngine = "SCI0 Point and Click interface";
                    else
                        Props.projEngine = "SCI0 Point and Click interface with sciAudio";
                }

                Props.projInterpEXE = "SCIV.EXE";
            }

            // SCI1.0
            if (File.Exists(Props.projPath + @"\SCIDHUV.EXE"))
            {
                Props.projEngine = "SCI1";
                Props.projInterpEXE = "SCIDHUV.EXE";
            }

            // SCI1.1
            if (File.Exists(Props.projPath + @"\SIERRA.EXE"))
            {
                Props.projEngine = "SCI1.1";
                Props.projInterpEXE = "SIERRA.EXE";
            }
        }
        
        
        /// <summary>
        /// Gets game name
        /// </summary>
        public static void ProjName()
        {
            string line = null;

            // Try to get game name from game.ini
            string gameINI = Props.projPath + @"\game.ini";
            if (File.Exists(gameINI))
            {
                StreamReader reader = new StreamReader(gameINI);
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Contains("Name="))
                    {
                        line = line.Replace("Name=", null);
                        line = null;

                        if (!String.IsNullOrEmpty(line))
                            line = line.Trim();

                        Props.projName = line;
                    }
                }
                reader.Close();
            }

            // If projName is null, get game name from folder name
            if (String.IsNullOrEmpty(Props.projName))
                Props.projName = Path.GetFileName(Props.projPath);

            // If projName is still null, get game name from project's script/logic to populate Props.projName
            if (String.IsNullOrEmpty(Props.projName))
            {
                #region AGI

                if (File.Exists(Props.projPath + @"\AGI"))
                {
                    string definesAGISrc = Props.projPath + @"\SRC\Defines.agil";
                    if (File.Exists(definesAGISrc))
                    {
                        StreamReader reader = new StreamReader(definesAGISrc);
                        while ((line = reader.ReadLine()) != null)
                        {
                            bool contains = Regex.IsMatch(line, "#define game_about_message", RegexOptions.IgnoreCase);
                            if (contains == true)
                            {
                                if (line.Contains("(About message)"))
                                {
                                    line = line.Replace("\"", null);
                                    line = line.Replace("#define game_about_message ", null);

                                    if (!String.IsNullOrEmpty(line))
                                        line = line.Trim();
                                    if (String.IsNullOrEmpty(line))
                                        Props.projName = Path.GetFileName(Props.projPath);
                                }
                                else
                                {
                                    line = line.Trim();
                                    line = line.Split('(').First();
                                    line = line.Replace("\"", null);
                                    line = line.Replace("#define game_about_message ", null);
                                    Props.projName = line;
                                }
                            }
                        }
                        reader.Close();
                    }

                    definesAGISrc = Props.projPath + @"\SRC\Defines.txt";
                    if (File.Exists(definesAGISrc))
                    {
                        StreamReader reader = new StreamReader(definesAGISrc);
                        while ((line = reader.ReadLine()) != null)
                        {
                            bool contains = Regex.IsMatch(line, "#define game_about_message", RegexOptions.IgnoreCase);
                            if (contains == true)
                            {
                                if (line.Contains("(About message)"))
                                {
                                    line = line.Replace("\"", null);
                                    line = line.Replace("#define game_about_message ", null);

                                    if (!String.IsNullOrEmpty(line))
                                        line = line.Trim();
                                    if (String.IsNullOrEmpty(line))
                                        Props.projName = Path.GetFileName(Props.projPath);
                                }
                                else
                                {
                                    line = line.Trim();
                                    line = line.Split('(').First();
                                    line = line.Replace("\"", null);
                                    line = line.Replace("#define game_about_message ", null);
                                    Props.projName = line;
                                }
                            }
                        }
                        reader.Close();
                    }
                }

                #endregion AGI

                #region SCI0

                if (File.Exists(Props.projPath + @"\SCIV.EXE"))
                {
                    string mainSCISrc = Props.projPath + @"\SRC\Main.sc";
                    if (File.Exists(mainSCISrc))
                    {
                        StreamReader reader = new StreamReader(mainSCISrc);
                        while ((line = reader.ReadLine()) != null)
                        {
                            bool contains = Regex.IsMatch(line, "instance ", RegexOptions.IgnoreCase);
                            bool containsEnd = Regex.IsMatch(line, "of Game", RegexOptions.IgnoreCase);

                            if (contains == true && containsEnd == true)
                            {
                                line = line.Replace("(instance public ", null);
                                line = line.Replace("(instance ", null);
                                line = line.Replace(" of Game", null);
                                line = line.Trim();

                                Props.projName = line;
                            }
                        }
                        reader.Close();
                    }
                }

                #endregion SCI0

                #region SCI10

                if (File.Exists(Props.projPath + @"\SCIDHUV.EXE"))
                {
                    string mainSCISrc = Props.projPath + @"\SRC\Main.sc";
                    if (File.Exists(mainSCISrc))
                    {
                        StreamReader reader = new StreamReader(mainSCISrc);
                        while ((line = reader.ReadLine()) != null)
                        {
                            bool contains = Regex.IsMatch(line, "instance public", RegexOptions.IgnoreCase);
                            bool containsEnd = Regex.IsMatch(line, "of Game", RegexOptions.IgnoreCase);
                            if (contains == true)
                            {
                                line = line.Replace("(instance public ", null);
                                line = line.Replace(" of Game", null);
                                line = line.Trim();

                                Props.projName = line;
                            }
                        }
                        reader.Close();
                    }
                }

                #endregion SCI10

                #region SCI11

                if (File.Exists(Props.projPath + @"\SIERRA.EXE"))
                {
                    string mainSCISrc = Props.projPath + @"\SRC\Main.sc";
                    if (File.Exists(mainSCISrc))
                    {
                        StreamReader reader = new StreamReader(mainSCISrc);
                        while ((line = reader.ReadLine()) != null)
                        {
                            bool contains = Regex.IsMatch(line, "class", RegexOptions.IgnoreCase);
                            bool containsEnd = Regex.IsMatch(line, "of Game", RegexOptions.IgnoreCase);
                            if (contains == true)
                            {
                                line = line.Replace("(class public ", null);
                                line = line.Replace("(class ", null);
                                line = line.Replace(" of Game", null);
                                line = line.Trim();

                                Props.projName = line;
                            }
                        }
                        reader.Close();
                    }
                }

                #endregion SCI11
            }

            FileName();
        }


        /// <summary>
        /// Sets Props.projFileName from Props.projName without illegal File System characters
        /// </summary>
        public static void FileName()
        {
            if (!String.IsNullOrEmpty(Props.projName))
            {
                Props.projFileName = Props.projName.Replace(":", "-");
                Props.projFileName = Props.projName.Replace("\"", "'");
                Regex rgx = new Regex("[^a-zA-Z0-9-' ]");
                Props.projFileName = rgx.Replace(Props.projFileName, "");
                Props.projFileName = Props.projFileName.Trim();
            }

            AbbreviatedName();
        }


        /// <summary>
        /// Sets Props.projShortName from Props.projName without illegal File System characters
        /// </summary>
        public static void AbbreviatedName()
        {
            if (!String.IsNullOrEmpty(Props.projName))
            {
                Regex rgx = new Regex("[^a-zA-Z0-9 ]");
                Props.projShortName = rgx.Replace(Props.projName, "");
                Props.projShortName = Props.projShortName.Trim();
            }

            DOSName();
        }


        /// <summary>
        /// Sets Props.projDOSName string to conform to DOS 8.3 naming convention
        /// </summary>
        public static void DOSName()
        {
            // Set DOSName
            if (!String.IsNullOrEmpty(Props.projName))
            {
                Props.projDOSName = Props.projName.Trim();
                Regex rgx = new Regex("[^a-zA-Z0-9]");
                Props.projDOSName = rgx.Replace(Props.projName, "");
                Props.projDOSName = Props.projDOSName.Replace(" ", null);
                Props.projDOSName = Props.projDOSName.Replace("-", null);
                Props.projDOSName = Props.projDOSName.Replace("'", null);
                Props.projDOSName = Props.projDOSName.ToUpper();

                if (Props.projDOSName.Length > 8)
                    Props.projDOSName = Props.projDOSName.Substring(0, 8);
            }

            SaveName();
        }


        /// <summary>
        /// Sets Props.projSaveName string from first 6 of DOSName + "SG"
        /// </summary>
        public static void SaveName()
        {
            // Set SaveName
            if (!String.IsNullOrEmpty(Props.projDOSName))
            {
                if (Props.projDOSName.Length > 6)
                    Props.projSaveName = Props.projDOSName.Substring(0, 6);
                Props.projSaveName = Props.projSaveName + "SG";
            }
        }
        
        
        /// <summary>
        /// Gets game aspect ratio
        /// </summary>
        public static void ProjAspect()
        {
            // Set default value
            Props.projAspect = true;

            string line = null;
            string aspect = null;

            // Try to get game name from game.ini
            string gameINI = Props.projPath + @"\game.ini";
            if (File.Exists(gameINI))
            {
                StreamReader reader = new StreamReader(gameINI);

                while ((line = reader.ReadLine()) != null)
                {
                    if (line.Contains("UseSierraAspectRatio="))
                    {
                        line = line.Replace("UseSierraAspectRatio=", null);
                        aspect = line;
                        if (!String.IsNullOrEmpty(line))
                            line = line.Trim();

                        if (String.IsNullOrEmpty(line))
                            Props.projAspect = true;

                        if (line == "true")
                            Props.projAspect = true;

                        if (line == "false")
                            Props.projAspect = false;

                    }
                }
                reader.Close();
            }

            if (String.IsNullOrEmpty(aspect))
            {
                Props.projAspect = true;

                try
                {
                    string value = Registry.GetValue(@"HKEY_CURRENT_USER\Software\mtnPhilms\SCICompanion\SCICompanion", "OriginalAspectRatio", 0).ToString();

                    if (value == "1")
                        Props.projAspect = true;

                    if (value == "0")
                        Props.projAspect = false;
                }
                catch (Exception ex)
                {
                    string err = ("Error: " + ex.Message);
                    MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];
                    mainFrm.statusbarMainMessage.Text = (err);
                    MessageBox.Show(err + Environment.NewLine + Environment.NewLine + "" + Environment.NewLine + Environment.NewLine + "",
                        "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
                }
            }
        }


        /// <summary>
        /// Gets game version from project's script/logic to populate Props.projVer
        /// </summary>
        public static void Version()
        {
            string line = null;

            #region AGI

            if (File.Exists(Props.projPath + @"\AGi"))
            {
                string definesAGISrc = Props.projPath + @"\SRC\Defines.agil";
                if (File.Exists(definesAGISrc))
                {
                    StreamReader reader = new StreamReader(definesAGISrc);
                    while ((line = reader.ReadLine()) != null)
                    {
                        bool contains = Regex.IsMatch(line, "#define game_version_message", RegexOptions.IgnoreCase);
                        if (contains == true)
                        {
                            line = line.Replace("\"", null);
                            line = Regex.Split(line, "version").Last();
                            line = Regex.Split(line, @"\\n").Last();
                            line = line.Replace("v.", null);
                            line = line.Trim();

                            Props.projVer = line;
                        }
                    }
                    reader.Close();
                }

                definesAGISrc = Props.projPath + @"\SRC\Defines.txt";
                if (File.Exists(definesAGISrc))
                {
                    StreamReader reader = new StreamReader(definesAGISrc);
                    while ((line = reader.ReadLine()) != null)
                    {
                        bool contains = Regex.IsMatch(line, "#define game_version_message", RegexOptions.IgnoreCase);
                        if (contains == true)
                        {
                            line = line.Replace("\"", null);
                            line = Regex.Split(line, "version").Last();
                            line = Regex.Split(line, @"\\n").Last();
                            line = line.Replace("v.", null);
                            line = line.Trim();

                            Props.projVer = line;
                        }
                    }
                    reader.Close();
                }
            }

            #endregion AGI
            
            #region SCI0

            if (File.Exists(Props.projPath + @"\SCIV.EXE"))
            {
                string mainSCISrc = Props.projPath + @"\SRC\Main.sc";
                if (File.Exists(mainSCISrc))
                {
                    StreamReader reader = new StreamReader(mainSCISrc);
                    while ((line = reader.ReadLine()) != null)
                    {
                        bool contains = Regex.IsMatch(line, "= gVersion", RegexOptions.IgnoreCase);
                        if (contains == true)
                        {
                            line = line.Trim();
                            line = line.Replace("\"", null);
                            line = line.Replace("= gVersion ", null);
                            line = line.Replace("(", null);
                            line = line.Replace(")", null);
                            line = line.Replace("{", null);
                            line = line.Replace("}", null);

                            Props.projVer = line;
                        }
                    }
                    reader.Close();
                }
            }
            
            #endregion SCI0

            #region SCI10

            if (File.Exists(Props.projPath + @"\SCIDHUV.EXE"))
            {
                ReadVerFile();
            }

            #endregion SCI10

            #region SCI11

            if (File.Exists(Props.projPath + @"\SIERRA.EXE"))
            {
                ReadVerFile();
            }

            #endregion SCI11

            ProjName();
        }


        /// <summary>
        /// Reads QA/VERSION file
        /// </summary>
        public static void ReadVerFile()
        {
            string qaFile = null;
            string gameVERSIONFile = Props.projPath + @"\VERSION";
            string ver = null;
            string line = null;
            string versionFileContents = null;

            string[] files = Directory.GetFiles(Props.projPath, "*.*"); //, ".qa");

            foreach (string file in files)
            {
                string ext = Path.GetExtension(file).ToUpper();
                if (ext == ".QA")
                    qaFile = file;
            }

            if (File.Exists(qaFile))
            {
                StreamReader file = new StreamReader(qaFile);

                while ((line = file.ReadLine()) != null)
                {
                    //if (!String.IsNullOrEmpty(line))
                    //    return;
                    versionFileContents += line + Environment.NewLine;
                    line = line.ToUpper();
                    if (!line.Contains("/") || !line.Contains("INT"))
                    {
                        line = line.Replace("VERSION", "");

                        if (Regex.IsMatch(line, "[A-Z]", RegexOptions.IgnoreCase) == false)
                        {
                            string testLine = line.Replace(".", "");

                            if (Regex.IsMatch(testLine, "[0-9]+$") == true)
                            {
                                if (!String.IsNullOrEmpty(line))
                                    line = line.Trim();
                                ver = line;
                            }
                        }
                    }
                }
                file.Close();

                if (!String.IsNullOrEmpty(ver))
                    ver = ver.Trim();
                Props.projVer = ver;
            }

            else if (File.Exists(gameVERSIONFile))
            {
                files = null;
                if (File.Exists(gameVERSIONFile))
                {
                    versionFileContents = null;
                    StreamReader reader = new StreamReader(gameVERSIONFile);

                    while ((line = reader.ReadLine()) != null)
                    {
                        string tmp = line;
                        line.ToUpper();
                        if (!line.Contains("/") || !line.Contains("INT") || !line.Contains(""))
                        {
                            line = line.Replace("VERSION", "");

                            if (Regex.IsMatch(line, "[A-Z]", RegexOptions.IgnoreCase) == false)
                            {
                                string testLine = line.Replace(".", "");

                                if (Regex.IsMatch(testLine, "[0-9]+$") == true)
                                {
                                    if (!String.IsNullOrEmpty(line))
                                        line = line.Trim();
                                    ver = line;
                                }
                            }
                            //versionFileContents += tmp + Environment.NewLine;
                        }
                    }
                    reader.Close();
                    //Props.projVer = versionFileContents.Replace("", "");
                    Props.projVer = ver;
                }
            }
        }
    }
}
