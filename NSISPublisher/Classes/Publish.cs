﻿/// To do:
/// 
/// 

#region using directives ====================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.IO;
using System.Diagnostics;
using System.ComponentModel;
using System.Xml;
using System.Reflection;
using System.Configuration;
using System.Threading;
using System.Runtime.InteropServices;

#endregion using directives


namespace NSISPublisher
{
    /// <summary>
    /// Class to generate NSIS NSI script and compile installer for for selected AGI/SCI project
    /// </summary>

    class Publish
    {
        #region Declares, Imports. etc. =====================================================================

        public static string mkNSIEXE = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
        public static string mkNSIPath = Path.GetDirectoryName(mkNSIEXE).Replace("%20", " ");
        public static string dbSRC = mkNSIPath + @"\DOSBox";

        //private static long filesLength = 0;
        public static long includeSize = 0;
        private static string sec1Size = null;
        private static string srcSecSize = null;

        public static bool overwrite = false;

        public static ErrorLogging errorLogging = new ErrorLogging();
        public static MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];

        #endregion Declares, Imports. etc.


        /// <summary>
        /// Populate variables and build script string
        /// </summary>
        public static void PublishNSI()
        {
            string nl = Environment.NewLine;
            string projTemplate = mainFrm.projEngCmboBox.Text;

            string scriptFile = Props.projPath + @"\Setup Script.nsi";
            MainFrm.scriptFile = Props.projPath + @"\Setup Script.nsi";

            if (Directory.Exists(Props.projPath))
            {
                #region Check game ==============================================================

                if (!File.Exists(Props.projPath + "\\AGI") && !File.Exists(Props.projPath + "\\SCIV.EXE") && !File.Exists(Props.projPath + "\\SCIDHUV.EXE") &&
                    !File.Exists(Props.projPath + "\\SIERRA.EXE"))
                {
                    MessageBox.Show("No AGI, SCI0, SCI1.0 or SCI1.1 game was found in " + Props.projPath, "No Game Found", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                    return;
                }

                #endregion Check game

                #region Check Overwrite =========================================================

                if (File.Exists(scriptFile) && (Props.rePublish == false))
                {
                    if (Properties.Settings.Default.scriptOverwrite == "ask" || String.IsNullOrEmpty(Properties.Settings.Default.scriptOverwrite))
                    {
                        ScriptOverwriteMsgbox scriptOverwriteMsgbox = new NSISPublisher.ScriptOverwriteMsgbox();
                        scriptOverwriteMsgbox.ShowDialog();
                    }
                    else if (Properties.Settings.Default.scriptOverwrite == "overWrite")
                    {
                        overwrite = true;
                    }

                    if (overwrite == false || Properties.Settings.Default.scriptOverwrite == "noOverWrite")
                    {
                        Program.passedArgs = false;
                        mainFrm.OpenScript();
                        return;
                    }
                }

                Props.rePublish = false;

                #endregion Check Overwrite

                #region Get Publish Options and Populate NSIS Compile Time Variables ============

                // Processes variables for NSI script
                string PRODUCTNAME = Props.projName;
                string RESOURCENAME = Props.projPath;
                string publisher = Props.projPub;
                if (String.IsNullOrEmpty(publisher))
                    publisher = Properties.Settings.Default.defaultPublisher;

                #endregion Get Publish Options and Populate NSIS Compile Time Variables

                #region Set NSIS Compile Time Variables =========================================

                // ; Defines
                string notes = Props.projNotes;
                string projName = Props.projName.Replace("\"", @"$\""");
                string prod = "\"" + projName + "\" ; Project Name" + nl;
                string ver = "!define PRODUCT_VERSION \"" + Props.projVer + "\" ; Product version" + nl;
                string pub = "!define PRODUCT_PUBLISHER \"" + Props.projPub + "\" ; Game Developer" + nl;
                string ico = "!define PRODUCT_ICON \"$INSTDIR\\Game.ico\" ; Game Icon" + nl;
                string flnm = "!define GAME_FILE_NAME \"" + Props.projFileName + "\" ; PRODUCT_NAME without Illegal File Name Characters" + nl;
                string shrtnm = "!define SHORT_NAME \"" + Props.projShortName + "\" ; Common abbreviated game name" + nl;
                string dosnm = "!define DOS_NAME \"" + Props.projDOSName + "\" ; Name to Conform to 8.3 Naming Convention" + nl;
                string res = "!define PROJECT_PATH \"" + Props.projPath + "\" ; Name of Folder for Resource Files" + nl;
                string sav = "!define SAVE_NAME \"" + Props.projSaveName + "\" ; Name of Save Game Files" + nl;
                string setupFiles = mkNSIPath + @"\NSIS";
                string defines = prod + ver + pub + ico + flnm + shrtnm + dosnm + res + sav;
                
                // ; File Info
                string templatePath = "\"" + mkNSIPath + "\\Templates\"" + nl;
                string dbSRCPath = "!define DB_SRC_PATH \"" + dbSRC + "\"" + nl;
                string mainEXE = "!define MAIN_EXE \"" + Props.projMainEXE + "\" ; Name of Windows Launcher" + nl;
                string interp = "!define INTERP_EXE \"" + Props.projInterpEXE + "\" ; AGI.COM, SIERRA.COM, SCIV.EXE, SCIDHUV.EXE, SIERRA.EXE" + nl;

                decimal baseBytes = (GetSizes.GetInstallSize(Props.projPath) + GetSizes.GetIncludeSize(Props.projPath + "\\Publish_Include") + 4504560);
                decimal baseKB = Math.Round(baseBytes / 1024);
                decimal baseMB = Math.Round(baseBytes / 1048576, 2);

                sec1Size = ";!define SEC01_SIZE " + baseKB + " ; KB, " + baseMB + " MB (Size required for SEC01)";

                if (Props.includeSRC == true)
                {
                    decimal srcBytes = GetSizes.GetSRCFolderSize(Props.projPath);
                    decimal srcKB = Math.Round(srcBytes / 1024);
                    decimal srcMB = Math.Round(srcKB / 1048576, 2);
                    srcSecSize = nl + ";!define SEC02_SIZE " + srcKB + " ; KB, " + srcMB + " MB (Size required for SEC02)";
                    if (srcKB == 0)
                        srcSecSize = null;
                }
                else
                    srcSecSize = null;

                string fiInfo = templatePath + dbSRCPath + mainEXE + interp + sec1Size + srcSecSize;

                // ; Game Info
                string webSite = Props.projURL + "\"" + nl;

                string uni = null;
                if (Props.unicodeInst == true)
                    uni = "Unicode true";

                string log = null;
                if (Props.loggingInst == true)
                {
                    log = @"
  SetOutPath ""$INSTDIR""
  
  ; Begin Logging ---------------------------------------------
  LogSet on
  LogText ""$INSTDIR\install.log""
";
                }

                #endregion Set NSIS Compile Time Variables

                #region Add Template Include Files ==============================================

                Add.Items();

                string includePath = Props.projPath + @"\Publish_Include";

                string confAspect = null;
                string templateConfTool = null;
                if (Props.projAspect == false)
                    confAspect = "ConfNoAspect.exe";
                else
                    confAspect = "ConfAspect.exe";

                if (Props.projEngine == "AGI")
                    if (Props.projInterpEXE == "AGI.COM")
                        templateConfTool = "${TEMPLATE_PATH}\\Template AGI\\" + confAspect;
                    else if (Props.projInterpEXE == "SIERRA.COM")
                        templateConfTool = "${TEMPLATE_PATH}\\Template AGI\\" + confAspect;

                if (Props.projEngine == "SCI0 Parser Interface" || Props.projEngine == "SCI0 Point and Click interface")
                    templateConfTool = "${TEMPLATE_PATH}\\Template SCI0\\" + confAspect;
                if (Props.projEngine == "SCI0 Parser Interface with sciAudio" || Props.projEngine == "SCI0 Point and Click interface with sciAudio")
                    templateConfTool = "${TEMPLATE_PATH}\\Template SCI0\\" + confAspect;
                if (Props.projEngine == "SCI1")
                    templateConfTool = "${TEMPLATE_PATH}\\Template SCI10\\" + confAspect;
                if (Props.projEngine == "SCI1.1")
                    templateConfTool = "${TEMPLATE_PATH}\\Template SCI11\\" + confAspect;

                string confexe = "  File /oname=Conf.exe \"" + templateConfTool + "\"" + nl;

                #endregion Add Template Include Files

                #region Add Files ===============================================================

                #region variables

                string instFiles = null;

                string uninstFiles = null;
                string unFiles = null;

                #region DOSBox Files

                string dbUninst = @"
  Delete ""$INSTDIR\DOSBox\DOSBox 0.74 Options.bat""
  Delete ""$INSTDIR\DOSBox\Reset KeyMapper.bat""
  Delete ""$INSTDIR\DOSBox\Reset Options.bat""
  Delete ""$INSTDIR\DOSBox\Screenshots & Recordings.bat""
  Delete ""$INSTDIR\DOSBox\dosbox.conf""
  Delete ""$INSTDIR\DOSBox\SDL.dll""
  Delete ""$INSTDIR\DOSBox\SDL_net.dll""
  Delete ""$INSTDIR\DOSBox\DOSBox.exe""
  Delete ""$INSTDIR\DOSBox\DOSBox 0.74 Manual.txt""
  
  Delete ""$INSTDIR\DOSBox\Documentation\AUTHORS.TXT""
  Delete ""$INSTDIR\DOSBox\Documentation\COPYING.TXT""
  Delete ""$INSTDIR\DOSBox\Documentation\INSTALL.TXT""
  Delete ""$INSTDIR\DOSBox\Documentation\NEWS.TXT""
  Delete ""$INSTDIR\DOSBox\Documentation\README.TXT""
  Delete ""$INSTDIR\DOSBox\Documentation\THANKS.TXT""
  
  Delete ""$INSTDIR\DOSBox\Video Codec\zmbv.dll""
  Delete ""$INSTDIR\DOSBox\Video Codec\zmbv.inf""
  Delete ""$INSTDIR\DOSBox\Video Codec\Video Instructions.txt""
";

                #endregion DOSBox Files

                #endregion variables

                string includes = GetIncludeFiles.FileList(includePath);
                unFiles = GetIncludeFiles.unFiles;

                string svgReadMe = null;
                if (Properties.Settings.Default.missingSVGReadMeWarning == true)
                {
                    if (File.Exists(mkNSIPath + @"\Publish\Save Games Readme.txt"))
                        svgReadMe = @"File """ + mkNSIPath + @"\Publish\Save Games Readme.txt""" + nl;
                    else
                    {
                        MissingSVGMsgbox svgReadMeMsgbox = new MissingSVGMsgbox();
                        svgReadMeMsgbox.ShowDialog();

                        // Get missingSVGReadMeWarning setting
                        if (Properties.Settings.Default.missingSVGReadMeWarning == false)
                            svgReadMe = null;
                        else
                        {
                            svgReadMe = null;
                            return;
                        }
                    }
                }
                else
                    svgReadMe = null;

                #endregion Add Files

                #region Get sciAudio Files ======================================================

                string sciAudFi = null;
                string sciAud = null;
                string sciAudUnfi = null;
                string unsciAud = null;
                string sciAudioFiles = null;
                string sciAudUnfiles = null;
                string delsciAudioDirs = null;

                // Get sciAudio Files
                string sciAudioFolder = Props.projPath + @"\sciAudio";
                if (Directory.Exists(sciAudioFolder))
                {
                    List<string> sciAudlstFiles = new List<string>();
                    if (projTemplate == "SCI0 Parser Interface with sciAudio" || projTemplate == "SCI0 Point and Click interface with sciAudio")
                    {
                        foreach (string file in Directory.GetFiles(sciAudioFolder, "*.*").OrderBy(f => new FileInfo(f).Extension))
                        {
                            sciAudlstFiles.Add(file);
                            sciAudFi = file.Replace(Props.projPath, "");
                            sciAudUnfi = Path.GetFileName(file);
                            sciAud += @"  File """ + file + "\"" + nl;
                            sciAudUnfi = file.Replace(Props.projPath, "");
                            unsciAud += @"  Delete ""$INSTDIR" + sciAudUnfi + "\"" + nl;
                        }
                        sciAud = sciAud.Replace(Props.projPath + @"\", "");
                        sciAud = @"  SetOutPath ""$INSTDIR\sciAudio""" + nl + sciAud;

                        // Get sciAudio Effects Files
                        string sciAudEffectsFi = null;
                        string sciAudEffectsFiles = null;
                        string sciAudEffectsUnfi = null;
                        string sciAudEffectsUnfiles = null;
                        string sciAudioEffectsFolder = Props.projPath + @"\sciAudio\effects";
                        if (Directory.Exists(sciAudioEffectsFolder))
                        {
                            List<string> sciAudEffectslstFiles;
                            string[] AllEffectsFiles = Directory.GetFiles(sciAudioEffectsFolder);
                            sciAudEffectslstFiles = new List<string>();

                            foreach (string s in AllEffectsFiles)
                            {
                                string file = s.Replace(includePath, string.Empty);
                                sciAudEffectslstFiles.Add(s);
                                sciAudEffectsFi = s.Replace(Props.projPath, "");
                                sciAudEffectsUnfi = Path.GetFileName(s);
                                sciAudEffectsFiles += @"  File """ + s + "\"" + nl;
                                sciAudEffectsUnfi = s.Replace(Props.projPath, "");
                                sciAudEffectsUnfiles += @"  Delete ""$INSTDIR" + sciAudEffectsUnfi + "\"" + nl;
                            }
                            sciAudEffectsFiles = sciAudEffectsFiles.Replace(Props.projPath + @"\", "");
                            sciAudEffectsFiles = @"  SetOutPath ""$INSTDIR\sciAudio\effects""" + nl + sciAudEffectsFiles;
                        }

                        // Get sciAudio Music Files
                        string sciAudMusicFi = null;
                        string sciAudMusicFiles = null;
                        string sciAudMusicUnfi = null;
                        string sciAudMusicUnfiles = null;
                        string sciAudioMusicFolder = Props.projPath + @"\sciAudio\music";
                        if (Directory.Exists(sciAudioMusicFolder))
                        {
                            List<string> sciAudMusiclstFiles;
                            string[] allMusicFiles = Directory.GetFiles(sciAudioMusicFolder);
                            sciAudMusiclstFiles = new List<string>();

                            foreach (string s in allMusicFiles)
                            {
                                string file = s.Replace(includePath, string.Empty);
                                sciAudMusiclstFiles.Add(s);
                                sciAudMusicFi = s.Replace(Props.projPath, "");
                                sciAudMusicUnfi = Path.GetFileName(s);
                                sciAudMusicFiles += @"  File """ + s + "\"" + nl;
                                sciAudMusicUnfi = s.Replace(Props.projPath, "");
                                sciAudMusicUnfiles += @"  Delete ""$INSTDIR" + sciAudMusicUnfi + "\"" + nl;
                            }
                            sciAudMusicFiles = sciAudMusicFiles.Replace(Props.projPath + @"\", "");
                            sciAudMusicFiles = @"  SetOutPath ""$INSTDIR\sciAudio\music""" + nl + sciAudMusicFiles;
                        }

                        // Get sciAudio Speech Files
                        string sciAudSpeechFi = null;
                        string sciAudSpeechFiles = null;
                        string sciAudSpeechUnfi = null;
                        string sciAudSpeechUnfiles = null;
                        string sciAudioSpeechFolder = Props.projPath + @"\sciAudio\speech";
                        if (Directory.Exists(sciAudioSpeechFolder))
                        {
                            List<string> sciAudSpeechlstFiles;
                            string[] allSpeechFiles = Directory.GetFiles(sciAudioSpeechFolder);
                            sciAudSpeechlstFiles = new List<string>();

                            foreach (string s in allSpeechFiles)
                            {
                                string file = s.Replace(includePath, string.Empty);
                                sciAudSpeechlstFiles.Add(s);
                                sciAudSpeechFi = s.Replace(Props.projPath, "");
                                sciAudSpeechUnfi = Path.GetFileName(s);
                                sciAudSpeechFiles += @"  File """ + s + "\"" + nl;
                                sciAudSpeechUnfi = s.Replace(Props.projPath, "");
                                sciAudSpeechUnfiles += @"  Delete ""$INSTDIR" + sciAudSpeechUnfi + "\"" + nl;
                            }
                            sciAudSpeechFiles = sciAudSpeechFiles.Replace(Props.projPath + @"\", "");
                            sciAudSpeechFiles = @"  SetOutPath ""$INSTDIR\sciAudio\speech""" + nl + sciAudSpeechFiles;
                        }

                        sciAudioFiles = "  " + nl + sciAud + "  " + nl + sciAudEffectsFiles + "  " + nl + sciAudMusicFiles + "  " + nl + sciAudSpeechFiles;
                        sciAudUnfiles = "  " + nl + unsciAud + "  " + nl + sciAudEffectsUnfiles + "  " + nl + sciAudMusicUnfiles + "  " + nl + sciAudSpeechUnfiles;

                        delsciAudioDirs = @"  RMDir ""$INSTDIR\sciAudio\effects""
  RMDir ""$INSTDIR\sciAudio\music""
  RMDir ""$INSTDIR\sciAudio\speech""
  RMDir ""$INSTDIR\sciAudio""" + nl;
                    }
                    else
                    {
                        sciAudioFiles = null;
                        sciAudUnfiles = null;
                        delsciAudioDirs = null;
                    }
                }

                #endregion Get sciAudio Files

                #region Include Source ==========================================================

                string compntsPg = null;
                string srcFiles = null;
                string srcSec = null;
                string secDescript = null;

                string lipsyncFiles = null;
                string msgFiles = null;
                string polyFiles = null;
                string scrFiles = null;

                string unLipsyncfi = null;
                string unLipsyncfiles = null;
                string delLipsyncDir = null;

                string unMSGfi = null;
                string unMSGFiles = null;
                string delMSGDir = null;

                string unPolyfi = null;
                string unPolyFiles = null;
                string delPolyDir = null;

                string unSCRFi = null;
                string unSCRFiles = null;
                string delSCRDir = null;

                string SecHdr = @"Section ""${PRODUCT_NAME}"" SEC01 ; Main Section
  SectionIn RO ; Set Main Section as read only
  SetOverwrite on
  
  ; Begin Game Installation -----------------------------------
  SetOutPath ""$INSTDIR""
  
  ; Add Resource Files ----------------------------------------
";
                List<string> listSCRFiles = new List<string>();
                List<string> listLipsyncFiles = new List<string>();
                List<string> listMSGFiles = new List<string>();
                List<string> listPolyFiles = new List<string>();

                if (Props.includeSRC == true && Directory.Exists(Props.projPath + "\\SRC"))
                {
                    SecHdr = @"SectionGroup /e ""${PRODUCT_NAME}""
Section """" SEC01 ; Main Section
  SectionIn RO ; Set Main Section as read only
  SetOverwrite on
  
  ; Begin Game Installation -----------------------------------
  SetOutPath ""$INSTDIR""
  
  ; Add Resource Files ----------------------------------------
";
                    if (!nsiAGIStrings.instFunctions.Contains("${SEC02}"))
                        nsiAGIStrings.instFunctions = nsiAGIStrings.instFunctions.Replace(";SectionSetSize ${SEC01} ${SEC01_SIZE}", @";SectionSetSize ${SEC01} ${SEC01_SIZE}
  ;SectionSetSize ${SEC02} ${SEC02_SIZE}");
                    if (!nsiSCI0Strings.instFunctions.Contains("${SEC02}"))
                        nsiSCI0Strings.instFunctions = nsiSCI0Strings.instFunctions.Replace(";SectionSetSize ${SEC01} ${SEC01_SIZE}", @";SectionSetSize ${SEC01} ${SEC01_SIZE}
  ;SectionSetSize ${SEC02} ${SEC02_SIZE}");
                    if (!nsiSCI10Strings.instFunctions.Contains("${SEC02}"))
                        nsiSCI10Strings.instFunctions = nsiSCI10Strings.instFunctions.Replace(";SectionSetSize ${SEC01} ${SEC01_SIZE}", @";SectionSetSize ${SEC01} ${SEC01_SIZE}
  ;SectionSetSize ${SEC02} ${SEC02_SIZE}");
                    if (!nsiSCI11Strings.instFunctions.Contains("${SEC02}"))
                        nsiSCI11Strings.instFunctions = nsiSCI11Strings.instFunctions.Replace(";SectionSetSize ${SEC01} ${SEC01_SIZE}", @";SectionSetSize ${SEC01} ${SEC01_SIZE}
  ;SectionSetSize ${SEC02} ${SEC02_SIZE}");

                    string scrFolder = Props.projPath + @"\SRC";
                    if (Directory.Exists(scrFolder))
                    {
                        if (Props.projEngine == "AGI")
                            compntsPg = nsiAGIStrings.pages;

                        if (Props.projEngine == "SCI0 Parser Interface" || Props.projEngine == "SCI0 Parser Interface with sciAudio" ||
                            Props.projEngine == "SCI0 Point and Click interface" || Props.projEngine == "SCI0 Point and Click interface with sciAudio")
                            compntsPg = nsiSCI0Strings.pages;

                        if (Props.projEngine == "SCI1")
                            compntsPg = nsiSCI10Strings.pages;

                        if (Props.projEngine == "SCI1.1")
                            compntsPg = nsiSCI11Strings.pages;

                        srcSec = @"
Section /o ""Source Files"" SEC02
  SetOverwrite on
  
  SetOutPath ""$INSTDIR""
  File ""game.ini""
  
";
                        #region Get SRC Files

                        string[] allscrFiles = Directory.GetFiles(scrFolder); //, "*.*", SearchOption.AllDirectories);
                        foreach (string scrFile in allscrFiles)
                        {
                            if (!String.IsNullOrEmpty(scrFile))
                            {
                                string file = scrFile.Replace(scrFolder, string.Empty);
                                listSCRFiles.Add(scrFile);
                                scrFiles += @"  File """ + scrFile + "\"" + nl;
                                unSCRFi += @"  Delete ""$INSTDIR\" + scrFile + "\"" + nl;
                            }
                        }

                        if (!String.IsNullOrEmpty(scrFiles))
                        {
                            scrFiles = scrFiles.Replace(Props.projPath + @"\", "");
                            scrFiles = @"  SetOutPath ""$INSTDIR\SRC""
" + scrFiles + @"";
                        }
                        if (!String.IsNullOrEmpty(unSCRFi))
                            unSCRFi = unSCRFi.Replace(Props.projPath + @"\", "");

                        secDescript = nsiSCI0Strings.secDscrpt;

                        unSCRFiles = unSCRFi;
                        unSCRFiles = "  " + nl + unSCRFiles;

                        delSCRDir = @"  RMDir /REBOOTOK ""$INSTDIR\SRC""" + nl;

                        #endregion Get SRC Files
                    }

                    if (Props.projEngine == "SCI1.1")
                    {
                        #region Lipsync Folder

                        string lipsyncFolder = Props.projPath + @"\lipsync";
                        if (Directory.Exists(lipsyncFolder))
                        {
                            string[] allLipsyncFiles = Directory.GetFiles(lipsyncFolder); //, "*.*", SearchOption.AllDirectories);
                            foreach (string lipsyncFile in allLipsyncFiles)
                            {
                                if (!String.IsNullOrEmpty(lipsyncFile))
                                {
                                    string file = lipsyncFile.Replace(scrFolder, string.Empty);
                                    listSCRFiles.Add(lipsyncFile);
                                    lipsyncFiles += @"  File """ + lipsyncFile + "\"" + nl;
                                    unLipsyncfi += @"  Delete ""$INSTDIR\" + lipsyncFile + "\"" + nl;
                                }
                            }

                            if (!String.IsNullOrEmpty(lipsyncFiles))
                            {
                                lipsyncFiles = lipsyncFiles.Replace(Props.projPath + @"\", "");
                                lipsyncFiles = @"  SetOutPath ""$INSTDIR\lipsync""
" + lipsyncFiles + @"  
";
                            }

                            if (!String.IsNullOrEmpty(unLipsyncfi))
                                unLipsyncfi = unLipsyncfi.Replace(Props.projPath + @"\", "");

                            //secDescript = nsiSCI0Strings.secDscrpt;

                            unLipsyncfiles = unLipsyncfi;
                            unLipsyncfiles = "  " + nl + unLipsyncfiles;

                            delLipsyncDir = @"  RMDir /REBOOTOK ""$INSTDIR\lipsync""" + nl;
                        }

                        #endregion Lipsync Folder

                        #region MSG Folder

                        string msgFolder = Props.projPath + @"\msg";
                        if (Directory.Exists(msgFolder))
                        {
                            string[] allMSGFiles = Directory.GetFiles(msgFolder); //, "*.*", SearchOption.AllDirectories);
                            foreach (string msgFile in allMSGFiles)
                            {
                                if (!String.IsNullOrEmpty(msgFile))
                                {
                                    string file = msgFile.Replace(scrFolder, string.Empty);
                                    listMSGFiles.Add(msgFile);
                                    msgFiles += @"  File """ + msgFile + "\"" + nl;
                                    unMSGfi += @"  Delete ""$INSTDIR\" + msgFile + "\"" + nl;
                                }
                            }

                            if (!String.IsNullOrEmpty(msgFiles))
                            {
                                msgFiles = msgFiles.Replace(Props.projPath + @"\", "");
                                msgFiles = @"  SetOutPath ""$INSTDIR\msg""
" + msgFiles + @"  
";
                            }
                            if (!String.IsNullOrEmpty(unMSGfi))
                                unMSGfi = unMSGfi.Replace(Props.projPath + @"\", "");

                            //secDescript = nsiSCI0Strings.secDscrpt;

                            unMSGFiles = unMSGfi;
                            unMSGFiles = "  " + nl + unMSGFiles;

                            delMSGDir = @"  RMDir /REBOOTOK ""$INSTDIR\msg""" + nl;
                        }

                        #endregion MSG Folder

                        #region Poly Folder

                        string polyFolder = Props.projPath + @"\poly";
                        if (Directory.Exists(polyFolder))
                        {
                            string[] allPolyFiles = Directory.GetFiles(polyFolder); //, "*.*", SearchOption.AllDirectories);
                            foreach (string polyFile in allPolyFiles)
                            {
                                if (!String.IsNullOrEmpty(polyFile))
                                {
                                    string file = polyFile.Replace(scrFolder, string.Empty);
                                    listPolyFiles.Add(polyFile);
                                    polyFiles += @"  File """ + polyFile + "\"" + nl;
                                    unPolyfi += @"  Delete ""$INSTDIR\" + polyFile + "\"" + nl;
                                }
                            }

                            if (!String.IsNullOrEmpty(polyFiles))
                            {
                                polyFiles = polyFiles.Replace(Props.projPath + @"\", "");
                                polyFiles = @"  SetOutPath ""$INSTDIR\poly""
" + polyFiles + @"  
";
                            }
                            if (!String.IsNullOrEmpty(unPolyfi))
                                unPolyfi = unPolyfi.Replace(Props.projPath + @"\", "");

                            //secDescript = nsiSCI0Strings.secDscrpt;

                            unPolyFiles = unPolyfi;
                            unPolyFiles = "  " + nl + unPolyFiles;

                            delPolyDir = @"  RMDir /REBOOTOK ""$INSTDIR\poly""" + nl;
                        }
                        
                        #endregion Poly Folder
                    }

                    srcFiles = srcSec + lipsyncFiles + msgFiles + polyFiles + scrFiles + @"SectionEnd
SectionGroupEnd
";
                }
                else
                {
                    srcSec = null;
                    compntsPg = null;
                    srcFiles = null;
                    secDescript = null;
                    unSCRFiles = null;
                    delLipsyncDir = null;
                    delMSGDir = null;
                    delPolyDir = null;
                    delsciAudioDirs = null;
                    delSCRDir = null;
                }

                #endregion Include Source

                #region Join Install & Uninstall File List Strings ==============================

                // Join Install Files List
                string gameFiles = null;
                gameFiles = GetFiles.FileList(Props.projPath);

                instFiles = includes + gameFiles + sciAudioFiles;

                // Join Uninstall Files List
                uninstFiles = unFiles + dbUninst + unLipsyncfiles + unMSGFiles + unPolyFiles + sciAudUnfiles + unSCRFiles;

                // Join Delete Folders List
                string delDirs = GetIncludeFiles.rmDir + delLipsyncDir + delMSGDir + delPolyDir + delsciAudioDirs + delSCRDir;

                #endregion Join Install & Uninstall Files List Strings

                #region Build NSI Script String =================================================

                string outFile = Props.projOutPath + @"\${DOS_NAME}_Setup.exe";
                //SectionSetSize ${SEC01}  ${SEC01_SIZE} ;  MB nsiAGIStrings.instFunctions nsiSCI0Strings.instFunctions nsiSCI10Strings.instFunctions nsiSCI11Strings.instFunctions

                // Join populated variables together to to generate NSIS script
                string srptContent = null;
                if (Props.projEngine == "AGI")
                    srptContent = nsiAGIStrings.notes + notes + nsiAGIStrings.header + defines + nsiAGIStrings.fileInfo + fiInfo + nsiAGIStrings.gameInfo + webSite +
                        nsiAGIStrings.gameInfoPt2 + uni + nsiAGIStrings.compressor + compntsPg + nsiAGIStrings.pagesPt2 + outFile + nsiAGIStrings.postHeader + log +
                        nsiAGIStrings.sections + SecHdr + confexe + instFiles + nsiAGIStrings.conf + srcFiles + nsiAGIStrings.secPost + secDescript + nsiAGIStrings.instFunctions + 
                        svgReadMe + nsiAGIStrings.unMoveFolder + uninstFiles + nsiAGIStrings.delShortcuts + delDirs + nsiAGIStrings.scriptEnd;

                if (Props.projEngine == "SCI0 Parser Interface" || Props.projEngine == "SCI0 Parser Interface with sciAudio" ||
                            Props.projEngine == "SCI0 Point and Click interface" || Props.projEngine == "SCI0 Point and Click interface with sciAudio")
                    srptContent = nsiSCI0Strings.notes + notes + nsiSCI0Strings.header + defines + nsiSCI0Strings.fileInfo + fiInfo + nsiSCI0Strings.gameInfo + webSite +
                        nsiSCI0Strings.gameInfoPt2 + uni + nsiSCI0Strings.compressor + compntsPg + nsiSCI0Strings.pagesPt2 + outFile + nsiSCI0Strings.postHeader + log +
                        nsiSCI0Strings.sections +  SecHdr + confexe + instFiles + nsiSCI0Strings.conf + srcFiles + nsiSCI0Strings.secPost + secDescript + nsiSCI0Strings.instFunctions + 
                        svgReadMe + nsiSCI0Strings.unMoveFolder + uninstFiles + nsiSCI0Strings.delShortcuts + delDirs + nsiSCI0Strings.scriptEnd;

                if (Props.projEngine == "SCI1")
                    srptContent = nsiSCI10Strings.notes + notes + nsiSCI10Strings.header + defines + nsiSCI10Strings.fileInfo + fiInfo + nsiSCI10Strings.gameInfo + webSite +
                        nsiSCI10Strings.gameInfoPt2 + uni + nsiSCI10Strings.compressor + compntsPg + nsiSCI10Strings.pagesPt2 + outFile + nsiSCI10Strings.postHeader + log +
                        nsiSCI10Strings.sections + SecHdr + confexe + instFiles + nsiSCI10Strings.conf + srcFiles + nsiSCI10Strings.secPost + secDescript + 
                        nsiSCI10Strings.instFunctions + svgReadMe + nsiSCI10Strings.unMoveFolder + uninstFiles + nsiSCI10Strings.delShortcuts + delDirs + nsiSCI10Strings.scriptEnd;

                if (Props.projEngine == "SCI1.1")
                    srptContent = nsiSCI11Strings.notes + notes + nsiSCI11Strings.header + defines + nsiSCI11Strings.fileInfo + fiInfo + nsiSCI11Strings.gameInfo + webSite +
                        nsiSCI11Strings.gameInfoPt2 + uni + nsiSCI0Strings.compressor + compntsPg + nsiSCI11Strings.pagesPt2 + outFile + nsiSCI11Strings.postHeader + log +
                        nsiSCI11Strings.sections + SecHdr + confexe + instFiles + nsiSCI11Strings.conf + srcFiles + nsiSCI11Strings.secPost + secDescript + 
                        nsiSCI11Strings.instFunctions + svgReadMe + nsiSCI11Strings.unMoveFolder + uninstFiles + nsiSCI11Strings.delShortcuts + delDirs + nsiSCI11Strings.scriptEnd;

                if (!String.IsNullOrEmpty(srptContent))
                {
                    srptContent = srptContent.Replace("File \"\"", "");
                    srptContent = srptContent.Replace("Delete \"$INSTDIR\\\"" + nl, nl);
                }

                #endregion Build NSI Script String

                #region Write NSI Script ========================================================

                try
                {
                    if (Properties.Settings.Default.unicodeInst == true)
                    {
                        StreamWriter writer = new StreamWriter(scriptFile, false, Encoding.GetEncoding("UTF-8")); //Unicode"))
                        writer.Write(srptContent);
                        writer.Dispose();
                        writer.Close();
                    }
                    else if (Properties.Settings.Default.unicodeInst == false)
                    {
                        StreamWriter writer = new StreamWriter(scriptFile, false, Encoding.GetEncoding("ASCII"));
                        writer.Write(srptContent);
                        writer.Dispose();
                        writer.Close();
                    }
                }
                catch (Exception ex)
                {
                    string err = ("Error: " + ex.Message);
                    MessageBox.Show(err);
                    ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
                }

                #endregion Write NSI Script

                #region Load NSI Script in Editor ===============================================

                mainFrm.scriptbox.Text = srptContent;
                MainFrm.srptContent = srptContent;
                
                //mainFrm.Text = Props.projName + " - " + MainFrm.appTitle;
                mainFrm.Text = scriptFile + " - " + MainFrm.appTitle;
                
                //mainFrm.Text = mainFrm.Text.Replace("* ", "");
                MainFrm.altered = false;

                // Compile Installer if runNSIS is true
                if (Program.passedArgs == false)
                {
                    mainFrm.SetScriptTab();

                    if (Properties.Settings.Default.runNSIS == true)
                        mainFrm.Compile();
                }

                #endregion Load NSI Script in Editor
            }
            else
                MessageBox.Show("Please check your project's path and try again.", "Project Path Not Found!", 
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}