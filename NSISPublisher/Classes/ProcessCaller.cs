/// To do list:
/// 
/// 

#region using directives ====================================================================================

using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Collections;
using System.ComponentModel;

#endregion using directives


namespace NSISPublisher
{
    /// <summary>
    /// Used by the Script Editor to allow realtime compiler feedback
    /// 
    /// Mike Mayer, 1 Sep 2003
    ///  http://www.codeproject.com/Articles/4665/Launching-a-process-and-displaying-its-standard-ou
    /// </summary>


    // Delegate used by the events StdOutReceived and StdErrReceived
    public delegate void DataReceivedHandler(object sender, DataReceivedEventArgs e);


    /// <summary>
    /// Event Args for above delegate
    /// </summary>
    public class DataReceivedEventArgs : EventArgs
    {
        public string Text; // The text that was received
        public DataReceivedEventArgs(string text)
        {
            Text = text; // The text that was received for this event to be triggered
        }
    }


    /// <summary>
    /// This class can launch a process (like a bat file, perl script, etc) and return all of the StdOut and StdErr to GUI app for display in textboxes, etc.
    /// </summary>
    /// <remarks>
    /// This class (c) 2003 Michael Mayer. Use it as you like (public domain licensing). Please post any bugs / fixes to the page where you downloaded this code.
    /// </remarks>
    public class ProcessCaller : AsyncOperation
    {


        #region Declares, Imports. etc. =====================================================================

        public string FileName; // The command to run (should be made into a property)
        public string Arguments; // The Arguments for the cmd (should be made into a property)
        public string WorkingDirectory; // The WorkingDirectory (should be made into a property)
        public event DataReceivedHandler StdOutReceived; // Fired for every line of stdOut received
        public event DataReceivedHandler StdErrReceived; // Fired for every line of stdErr received
        public int SleepTime = 500; // Amount of time to sleep on threads while waiting for the process to finish
        private Process process; // The process used to run your task

        #endregion Declares, Imports. etc.


        /// <summary>
        /// Initializes a ProcessCaller with an association to the supplied ISynchronizeInvoke.  All events raised from this
        /// object will be delivered via this target. (This might be a Control object, so events would be delivered to that Control's UI thread.)
        /// </summary>
        /// <param name="isi">An object implementing the ISynchronizeInvoke interface.  All events will be delivered through this target, 
        /// ensuring that they are delivered to the correct thread.
        /// </param>
        public ProcessCaller(ISynchronizeInvoke isi) : base(isi)
        {

        }


        /// <summary>
        /// ProcessCaller Method
        /// Initializes a ProcessCaller without an association to an ISynchronizeInvoke. All events raised from 
        /// this object will be delievered via the worker thread.
        /// </summary>
        //public ProcessCaller() {
        //    This constructor only works with changes to AsyncOperation...
        //}


        /// <summary>
        /// Launch a process, but do not return until the process has exited. That way we can kill the process if a cancel is requested.
        /// </summary>
        protected override void DoWork()
        {
            StartProcess();

            // Wait for the process to end, or cancel it
            while (!process.HasExited)
            {
                Thread.Sleep(SleepTime); // sleep
                if (CancelRequested)
                {
                    // Not a very nice way to end a process,
                    // but effective.
                    process.Kill();
                    AcknowledgeCancel();
                }
            }
        }


        /// <summary>
        /// This method is generally called by DoWork() which is called by the base classs Start()
        /// </summary>
        protected virtual void StartProcess()
        {
            // Start a new process for the cmd
            process = new Process();
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.FileName = FileName;
            process.StartInfo.Arguments = Arguments;
            process.StartInfo.WorkingDirectory = WorkingDirectory;
            process.Start();


            // Invoke stdOut and stdErr readers - each
            // has its own thread to guarantee that they aren't
            // blocked by, or cause a block to, the actual
            // process running (or the gui).
            new MethodInvoker(ReadStdOut).BeginInvoke(null, null);
            new MethodInvoker(ReadStdErr).BeginInvoke(null, null);

        }


        /// <summary>
        /// Handles reading of stdout and firing an event for every line read
        /// </summary>
        protected virtual void ReadStdOut()
        {
            string str;
            while ((str = process.StandardOutput.ReadLine()) != null)
            {
                FireAsync(StdOutReceived, this, new DataReceivedEventArgs(str));
            }
        }


        /// <summary>
        /// Handles reading of stdErr
        /// </summary>
        protected virtual void ReadStdErr()
        {
            string str;
            while ((str = process.StandardError.ReadLine()) != null)
            {
                FireAsync(StdErrReceived, this, new DataReceivedEventArgs(str));
            }
        }
    }
}
