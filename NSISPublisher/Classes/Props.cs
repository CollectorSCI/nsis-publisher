﻿/// To do list:
/// 
/// 

#region using directives ====================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion using directives


namespace NSISPublisher
{
    /// <summary>
    /// Project variables
    /// </summary>
        
    public class Props
    {
        public static string scriptFilePath = null;

        public static string projPath = null;
        public static string projNotes = null;
        public static string projName = null;
        public static string projVer = null;
        public static string projPub = null;
        public static string projEngine = null;
        public static string projFileName = null;
        public static string projShortName = null;
        public static string projDOSName = null;
        public static string projSaveName = null;

        public static string projMainEXE = null;
        public static string projInterpEXE = null;
        public static string projURL = null;
        public static string projOutPath = null;

        public static bool includeSRC = true;
        public static bool projAspect = true;
        public static bool loggingInst = true;
        public static bool unicodeInst = true;
        public static bool rePublish = false;
    }
}
