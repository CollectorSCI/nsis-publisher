﻿/// To do list:
/// 
/// 

#region using directives ====================================================================================

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Reflection;
using System.Configuration;

#endregion using directives


namespace NSISPublisher
{
    /// <summary>
    /// Adds items to Include folder
    /// </summary>
        
    class Add
    {
        public static void Items()
        {
            string nsisPubEXE = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
            string nsisPubPath = Path.GetDirectoryName(nsisPubEXE).Replace("%20", " ");

            #region Check Include folder

            // Folder containing additional files to be included in installer
            string includePath = Props.projPath + @"\Publish_Include";

            if (!Directory.Exists(includePath))
            {
                if (Properties.Settings.Default.missingIncludeWarning == true)
                {
                    MissingIncludeMsgbox missingIncludeMsgbox = new NSISPublisher.MissingIncludeMsgbox();
                    missingIncludeMsgbox.ShowDialog();
                    MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];
                    if (mainFrm.cancel == true)
                        return;
                }
                else
                    try
                    {
                        Directory.CreateDirectory(includePath);
                    }
                    catch { }
            }

            #endregion Check Include folder

            #region Add Lanucher

            //Props.projAspect = Properties.Settings.Default.aspectDefault;

            string templateRun = null;
            string projRun = Props.projPath + @"\Publish_Include\" + Props.projMainEXE;
            if (Props.projEngine == "AGI")
                templateRun = nsisPubPath + @"\Templates\RunAGI.exe";

            if (Props.projEngine == "SCI0 Parser Interface" || Props.projEngine == "SCI0 Point and Click interface" || Props.projEngine == "SCI1" ||
                Props.projEngine == "SCI1.1")
                templateRun = nsisPubPath + @"\Templates\RunSCI.exe";

            if (Props.projEngine == "SCI0 Parser Interface with sciAudio" || Props.projEngine == "SCI0 Point and Click interface with sciAudio")
                templateRun = nsisPubPath + @"\Templates\RunSciAudio.exe";

            if (!File.Exists(projRun))
                try
                {
                    File.Copy(templateRun, projRun, false);
                }
                catch { }

            #endregion Add Lanucher

            #region Add Icon

            string templateICO = null;
            string projICO = Props.projPath + @"\Publish_Include\Game.ico"; // +Props.projIcon;
            if (Props.projEngine == "AGI")
                templateICO = nsisPubPath + @"\Templates\Template AGI\GameAGI.ico";

            if (Props.projEngine == "SCI0 Parser Interface" || Props.projEngine == "SCI0 Point and Click interface" || 
                Props.projEngine == "SCI0 Parser Interface with sciAudio" || Props.projEngine == "SCI0 Point and Click interface with sciAudio")
                templateICO = nsisPubPath + @"\Templates\Template SCI0\GameSCI0.ico";

            if (Props.projEngine == "SCI1")
                templateICO = nsisPubPath + @"\Templates\Template SCI10\GameSCI1.0.ico";
            
            if (Props.projEngine == "SCI1.1")
                templateICO = nsisPubPath + @"\Templates\Template SCI11\GameSCI1.1.ico";

            if (!File.Exists(projICO))
                try
                {
                    File.Copy(templateICO, projICO, false);
                }
                catch { }

            #endregion Add Icon

            #region Add License

            string templateLicense = nsisPubPath + @"\Include\" + Properties.Settings.Default.defaultLicense + ".txt";
            string projLicense = Props.projPath + @"\Publish_Include\LICENSE.TXT";

            if (!File.Exists(projLicense))
            {
                try
                {
                    File.Copy(templateLicense, projLicense);
                }
                catch { }
            }

            #endregion Add License

            #region Add README

            string templateReadme = nsisPubPath + @"\Include\README.TXT";
            string projReadme = Props.projPath + @"\Publish_Include\README.TXT";
            if (!File.Exists(projReadme))
                try
                {
                    File.Copy(templateReadme, projReadme);
                }
                catch { }

            if (!File.Exists(projReadme))
                try
                {
                    File.Copy(templateReadme, projReadme);
                }
                catch { }

            #endregion Add README
        }
    }
}
