﻿/// To do list:
/// 
/// 

using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace NSISPublisher
{
    /// <summary>
    /// An Overwrite Script dialog for NSIS Publisher
    /// </summary>

    public partial class ScriptOverwriteMsgbox : Form
    {
        public static string includePath = Props.projPath + @"\Publish_Include";
        public static string scriptOverwrite = Properties.Settings.Default.scriptOverwrite;

        MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];
        ErrorLogging errorLogging = new ErrorLogging();

        public ScriptOverwriteMsgbox()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Play System Exclamation Sound on Load
        /// </summary>
        private void Form_Load(object sender, EventArgs e)
        {
            System.Media.SystemSounds.Question.Play();
        }


        /// <summary>
        /// Set scriptOverwrite to overWrite
        /// </summary>
        private void overWriteRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            scriptOverwrite = "overWrite";
        }


        /// <summary>
        /// Set scriptOverwrite to noOverWrite
        /// </summary>
        private void noOverwriteRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            scriptOverwrite = "noOverWrite";
        }


        /// <summary>
        /// Set scriptOverwrite to ask
        /// </summary>
        private void askRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            scriptOverwrite = "ask";
        }


        /// <summary>
        /// Closes Messagebox
        /// </summary>
        private void yesBtn_Click(object sender, EventArgs e)
        {
            Publish.overwrite = true;
            Properties.Settings.Default.scriptOverwrite = scriptOverwrite;
            this.Close();
        }


        /// <summary>
        /// Closes Messagebox
        /// </summary>
        private void noBtn_Click(object sender, EventArgs e)
        {
            Publish.overwrite = false;
            Properties.Settings.Default.scriptOverwrite = scriptOverwrite;
            this.Close();
        }


        /// <summary>
        /// Closes Messagebox
        /// </summary>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            Publish.overwrite = false;
            mainFrm.cancel = true;
            this.Close();
        }
    }
}
