﻿namespace NSISPublisher
{
    partial class ByteConverterFrm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ByteConverterFrm));
            this.byteLabel = new System.Windows.Forms.Label();
            this.kilobyteLabel = new System.Windows.Forms.Label();
            this.megabyteLabel = new System.Windows.Forms.Label();
            this.gigabyteLabel = new System.Windows.Forms.Label();
            this.byteTextBox = new System.Windows.Forms.TextBox();
            this.kilobyteTextBox = new System.Windows.Forms.TextBox();
            this.megabyteTextBox = new System.Windows.Forms.TextBox();
            this.gigabyteTextBox = new System.Windows.Forms.TextBox();
            this.byteBtn = new System.Windows.Forms.Button();
            this.kilobyteBtn = new System.Windows.Forms.Button();
            this.megabyteBtn = new System.Windows.Forms.Button();
            this.gigabyteBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // byteLabel
            // 
            this.byteLabel.AutoSize = true;
            this.byteLabel.Location = new System.Drawing.Point(48, 9);
            this.byteLabel.Name = "byteLabel";
            this.byteLabel.Size = new System.Drawing.Size(33, 13);
            this.byteLabel.TabIndex = 10;
            this.byteLabel.Text = "Bytes";
            // 
            // kilobyteLabel
            // 
            this.kilobyteLabel.AutoSize = true;
            this.kilobyteLabel.Location = new System.Drawing.Point(144, 9);
            this.kilobyteLabel.Name = "kilobyteLabel";
            this.kilobyteLabel.Size = new System.Drawing.Size(49, 13);
            this.kilobyteLabel.TabIndex = 11;
            this.kilobyteLabel.Text = "Kilobytes";
            // 
            // megabyteLabel
            // 
            this.megabyteLabel.AutoSize = true;
            this.megabyteLabel.Location = new System.Drawing.Point(247, 9);
            this.megabyteLabel.Name = "megabyteLabel";
            this.megabyteLabel.Size = new System.Drawing.Size(59, 13);
            this.megabyteLabel.TabIndex = 12;
            this.megabyteLabel.Text = "Megabytes";
            // 
            // gigabyteLabel
            // 
            this.gigabyteLabel.AutoSize = true;
            this.gigabyteLabel.Location = new System.Drawing.Point(356, 9);
            this.gigabyteLabel.Name = "gigabyteLabel";
            this.gigabyteLabel.Size = new System.Drawing.Size(54, 13);
            this.gigabyteLabel.TabIndex = 13;
            this.gigabyteLabel.Text = "Gigabytes";
            // 
            // byteTextBox
            // 
            this.byteTextBox.Location = new System.Drawing.Point(12, 25);
            this.byteTextBox.Name = "byteTextBox";
            this.byteTextBox.Size = new System.Drawing.Size(100, 20);
            this.byteTextBox.TabIndex = 0;
            this.byteTextBox.TextChanged += new System.EventHandler(this.byteTextBox_TextChanged);
            this.byteTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.byteTextBox_KeyPress);
            // 
            // kilobyteTextBox
            // 
            this.kilobyteTextBox.Location = new System.Drawing.Point(118, 25);
            this.kilobyteTextBox.Name = "kilobyteTextBox";
            this.kilobyteTextBox.Size = new System.Drawing.Size(100, 20);
            this.kilobyteTextBox.TabIndex = 1;
            this.kilobyteTextBox.TextChanged += new System.EventHandler(this.kilobyteTextBox_TextChanged);
            this.kilobyteTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.kilobyteTextBox_KeyPress);
            // 
            // megabyteTextBox
            // 
            this.megabyteTextBox.Location = new System.Drawing.Point(224, 25);
            this.megabyteTextBox.Name = "megabyteTextBox";
            this.megabyteTextBox.Size = new System.Drawing.Size(100, 20);
            this.megabyteTextBox.TabIndex = 2;
            this.megabyteTextBox.TextChanged += new System.EventHandler(this.megabyteTextBox_TextChanged);
            this.megabyteTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.megabyteTextBox_KeyPress);
            // 
            // gigabyteTextBox
            // 
            this.gigabyteTextBox.Location = new System.Drawing.Point(330, 25);
            this.gigabyteTextBox.Name = "gigabyteTextBox";
            this.gigabyteTextBox.Size = new System.Drawing.Size(100, 20);
            this.gigabyteTextBox.TabIndex = 3;
            this.gigabyteTextBox.TextChanged += new System.EventHandler(this.gigabyteTextBox_TextChanged);
            this.gigabyteTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.gigabyteTextBox_KeyPress);
            // 
            // byteBtn
            // 
            this.byteBtn.Location = new System.Drawing.Point(25, 51);
            this.byteBtn.Name = "byteBtn";
            this.byteBtn.Size = new System.Drawing.Size(75, 23);
            this.byteBtn.TabIndex = 4;
            this.byteBtn.Text = ">";
            this.byteBtn.UseVisualStyleBackColor = true;
            this.byteBtn.Click += new System.EventHandler(this.byteBtn_Click);
            // 
            // kilobyteBtn
            // 
            this.kilobyteBtn.Location = new System.Drawing.Point(131, 51);
            this.kilobyteBtn.Name = "kilobyteBtn";
            this.kilobyteBtn.Size = new System.Drawing.Size(75, 23);
            this.kilobyteBtn.TabIndex = 5;
            this.kilobyteBtn.Text = "< >";
            this.kilobyteBtn.UseVisualStyleBackColor = true;
            this.kilobyteBtn.Click += new System.EventHandler(this.kilobyteBtn_Click);
            // 
            // megabyteBtn
            // 
            this.megabyteBtn.Location = new System.Drawing.Point(237, 51);
            this.megabyteBtn.Name = "megabyteBtn";
            this.megabyteBtn.Size = new System.Drawing.Size(75, 23);
            this.megabyteBtn.TabIndex = 6;
            this.megabyteBtn.Text = "< >";
            this.megabyteBtn.UseVisualStyleBackColor = true;
            this.megabyteBtn.Click += new System.EventHandler(this.megabyteBtn_Click);
            // 
            // gigabyteBtn
            // 
            this.gigabyteBtn.Location = new System.Drawing.Point(343, 51);
            this.gigabyteBtn.Name = "gigabyteBtn";
            this.gigabyteBtn.Size = new System.Drawing.Size(75, 23);
            this.gigabyteBtn.TabIndex = 7;
            this.gigabyteBtn.Text = "<";
            this.gigabyteBtn.UseVisualStyleBackColor = true;
            this.gigabyteBtn.Click += new System.EventHandler(this.gigabyteBtn_Click);
            // 
            // ByteConverterFrm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(442, 87);
            this.Controls.Add(this.gigabyteBtn);
            this.Controls.Add(this.megabyteBtn);
            this.Controls.Add(this.kilobyteBtn);
            this.Controls.Add(this.byteBtn);
            this.Controls.Add(this.gigabyteTextBox);
            this.Controls.Add(this.megabyteTextBox);
            this.Controls.Add(this.kilobyteTextBox);
            this.Controls.Add(this.byteTextBox);
            this.Controls.Add(this.gigabyteLabel);
            this.Controls.Add(this.megabyteLabel);
            this.Controls.Add(this.kilobyteLabel);
            this.Controls.Add(this.byteLabel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(458, 125);
            this.MinimumSize = new System.Drawing.Size(458, 125);
            this.Name = "ByteConverterFrm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Byte Converter Tool";
            this.Load += new System.EventHandler(this.Form_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label byteLabel;
        private System.Windows.Forms.Label kilobyteLabel;
        private System.Windows.Forms.Label megabyteLabel;
        private System.Windows.Forms.Label gigabyteLabel;
        private System.Windows.Forms.TextBox kilobyteTextBox;
        private System.Windows.Forms.TextBox megabyteTextBox;
        private System.Windows.Forms.TextBox gigabyteTextBox;
        private System.Windows.Forms.Button byteBtn;
        private System.Windows.Forms.Button kilobyteBtn;
        private System.Windows.Forms.Button megabyteBtn;
        private System.Windows.Forms.Button gigabyteBtn;
        public System.Windows.Forms.TextBox byteTextBox;
    }
}

