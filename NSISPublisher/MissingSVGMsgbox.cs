﻿/// To do list:
/// 
/// 

using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace NSISPublisher
{
    /// <summary>
    /// A Missing Save Game Readme dialog for NSIS Publisher
    /// </summary>

    public partial class MissingSVGMsgbox : Form
    {

        MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];
        ErrorLogging errorLogging = new ErrorLogging();


        public MissingSVGMsgbox()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Play System  Sound on Load
        /// </summary>
        private void MissingSVGMsgbox_Load(object sender, EventArgs e)
        {
            System.Media.SystemSounds.Exclamation.Play();
        }


        /// <summary>
        /// Turns warning on or off
        /// </summary>
        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox.Checked == true)
                Properties.Settings.Default.missingSVGReadMeWarning = true;
            if (checkBox.Checked == false)
                Properties.Settings.Default.missingSVGReadMeWarning = false;
        }


        /// <summary>
        /// Closes Messagebox
        /// </summary>
        private void CloseBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
