﻿namespace NSISPublisher
{
    partial class MissingSVGMsgbox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MissingSVGMsgbox));
            this.checkBox = new System.Windows.Forms.CheckBox();
            this.CloseBtn = new System.Windows.Forms.Button();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.SVGMsgTxtBox = new System.Windows.Forms.TextBox();
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.Title = new System.Windows.Forms.Label();
            this.Panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // checkBox
            // 
            this.checkBox.AutoSize = true;
            this.checkBox.Location = new System.Drawing.Point(8, 90);
            this.checkBox.Name = "checkBox";
            this.checkBox.Size = new System.Drawing.Size(179, 17);
            this.checkBox.TabIndex = 12;
            this.checkBox.Text = "Do not show this message again";
            this.checkBox.UseVisualStyleBackColor = true;
            this.checkBox.CheckedChanged += new System.EventHandler(this.checkBox_CheckedChanged);
            // 
            // CloseBtn
            // 
            this.CloseBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.CloseBtn.Location = new System.Drawing.Point(335, 117);
            this.CloseBtn.Name = "CloseBtn";
            this.CloseBtn.Size = new System.Drawing.Size(75, 23);
            this.CloseBtn.TabIndex = 10;
            this.CloseBtn.Text = "OK";
            this.CloseBtn.UseVisualStyleBackColor = true;
            this.CloseBtn.Click += new System.EventHandler(this.CloseBtn_Click);
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.Color.White;
            this.Panel1.Controls.Add(this.checkBox);
            this.Panel1.Controls.Add(this.SVGMsgTxtBox);
            this.Panel1.Controls.Add(this.PictureBox1);
            this.Panel1.Controls.Add(this.Title);
            this.Panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.Panel1.Location = new System.Drawing.Point(0, 0);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(422, 111);
            this.Panel1.TabIndex = 11;
            // 
            // SVGMsgTxtBox
            // 
            this.SVGMsgTxtBox.BackColor = System.Drawing.SystemColors.Window;
            this.SVGMsgTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SVGMsgTxtBox.Location = new System.Drawing.Point(8, 51);
            this.SVGMsgTxtBox.Multiline = true;
            this.SVGMsgTxtBox.Name = "SVGMsgTxtBox";
            this.SVGMsgTxtBox.ReadOnly = true;
            this.SVGMsgTxtBox.Size = new System.Drawing.Size(411, 33);
            this.SVGMsgTxtBox.TabIndex = 3;
            this.SVGMsgTxtBox.Text = "The \"Save Games Readme\" file was not found. You may restore this file by reinstal" +
    "lng NSIS Publisher.";
            // 
            // PictureBox1
            // 
            this.PictureBox1.Image = global::NSISPublisher.Properties.Resources.Warning;
            this.PictureBox1.Location = new System.Drawing.Point(16, 14);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(31, 28);
            this.PictureBox1.TabIndex = 2;
            this.PictureBox1.TabStop = false;
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(53, 18);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(94, 24);
            this.Title.TabIndex = 0;
            this.Title.Text = "Warning:";
            // 
            // MissingSVGMsgbox
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(422, 149);
            this.Controls.Add(this.CloseBtn);
            this.Controls.Add(this.Panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(438, 187);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(438, 187);
            this.Name = "MissingSVGMsgbox";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Missing File";
            this.Load += new System.EventHandler(this.MissingSVGMsgbox_Load);
            this.Panel1.ResumeLayout(false);
            this.Panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.CheckBox checkBox;
        internal System.Windows.Forms.Button CloseBtn;
        internal System.Windows.Forms.Panel Panel1;
        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.Label Title;
        public System.Windows.Forms.TextBox SVGMsgTxtBox;
    }
}