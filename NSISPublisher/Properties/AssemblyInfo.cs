﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("NSIS Publisher")]
[assembly: AssemblyDescription("NSIS Publisher is a plugin for SCI Companion that will generate an NSIS (Nullsoft Scriptable Install System) script to make an installer to distribute AGI, SCI0, SCI1.0 and SCI1.1 fan made games.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("The Sierra Help Pages")]
[assembly: AssemblyProduct("NSIS Publisher")]
[assembly: AssemblyCopyright("Copyright ©  2015")]
[assembly: AssemblyTrademark("SHP")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("5E710FC2-2AD2-4ab3-9FED-BC2AC3613A6B")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.1.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
