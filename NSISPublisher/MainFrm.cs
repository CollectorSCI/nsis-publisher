﻿/// To do list:
/// 
/// Auto rename MainEXE on projMainEXETxtBox_TextChanged Event?
/// Put outputbox in new thread?
/// 

#region using directives ====================================================================================

using NSISPublisher;
using i00SpellCheck;
using FastColoredTextBoxNS;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

#endregion using directives


namespace NSISPublisher
{
    /// <summary>
    /// Automated NSIS Script/Installer generator with NSI Script Editor using the FastColoredTextBox control
    /// By Andrew Branscom (Collector http://sierrahelp.com/ )
    /// 
    /// For use with Phil Fortier's SCI Companion ( http://scicompanion.com/ )
    /// NSIS Publisher is SCI Companion's first plugin
    /// </summary>

    public partial class MainFrm : Form
    {
        #region Declares, Imports. etc. =====================================================================

        public static string nsisPubEXE = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath;
        public static string nsisPubPath = Path.GetDirectoryName(nsisPubEXE).Replace("%20", " ");
        //public static string nsisPubPath = Path.GetDirectoryName(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
        public static string configPath = Path.GetDirectoryName(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath);
        public static string appTitle = "NSIS Publisher";

        public static string makeNSIS;
        public static string nsisPath;
        public static string scriptFile;
        public static string selectedLanguage;
        public static string srptContent = null;
        string openedContents;

        private static bool accessErr = false;

        public static bool altered = false;
        private static bool opened;
        public bool cancel = false;
        bool run = false;
        public static bool killMsg = true;

        // Error feedback variables
        string errLineNum;
        string line;
        int warnLine;
        public delegate void SetTextCallback(string key);

        // Setup spell check settings when extension is loaded for all controls - used by ControlExtensionAdding()
        static i00SpellCheck.SpellCheckSettings SpellCheckSettings = null;

        private ProcessCaller processCaller;

        #region Autocomplete

        AutocompleteMenu popupMenu;

        string[] keywords = { 
                                // NSIKeywordRegex
                                "AddBrandingImage", "AddSize", "AllowRootDirInstall", "AllowSkipFiles", "AutoCloseWindow", "BGFont", "BGGradient", "BrandingText", "Caption", 
                                "ChangeUI", "CheckBitmap", "CompletedText", "ComponentText", "CRCCheck", "DetailsButtonText", "DirShow", "DirText", "DirVar", "DirVerify", 
                                "FileBufSize", "FileErrorText", "Function", "FunctionEnd", "GetInstDirError", "Icon", "InstallButtonText", "InstallColors", "InstallDirRegKey", 
                                "InstallDir", "InstProgressFlags", "InstType", "LangStringUP", "LangString", "LicenseBkColor", "LicenseData", "LicenseForceSelection", 
                                "LicenseLangString", "LicenseText", "LoadLanguageFile", "Macro", "MacroEnd", "MiscButtonText", "Name", "OutFile", "PageCallbacks", "PageEx", 
                                "PageExEnd", "Page", "Section", "SectionEnd", "SectionGroup", "SectionGroupEnd", "SectionIn", "SetCompressionLevel", "SetCompressorDictSize", 
                                "SetCompressor", "SetCompress", "SetDatablockOptimize", "SetDateSave", "SetFont", "SetOverwrite", "SetPluginUnload", "ShowInstDetails", 
                                "ShowUnInstDetails", "SilentInstall", "SilentUnInstall", "SpaceTexts", "SubCaption", "SubSection", "SubSectionEnd", "Unicode", 
                                "UninstallButtonText", "UninstallCaption", "UninstallIcon", "UninstallSubCaption", "UninstallText", "UninstPage", "Var", "VIAddVersionKey", 
                                "VIProductVersion", "WindowIcon", "XPStyle", 
                                
                                // NSIFunctionRegex
                                "Abort", "BringToFront", "CallInstDLL", "Call", "ClearErrors", "CopyFiles", "CreateDirectory", "CreateFont", "CreateShortCut", "DeleteINISec", 
                                "DeleteINIStr", "DeleteRegKey", "DeleteRegValue", "Delete", "DetailPrint", "EnableWindow", "EnumRegKey", "EnumRegValue", "Exch", "ExecShell", 
                                "ExecWait", "Exec", "ExpandEnvStrings", "FileClose", "FileOpen", "FileReadByte", "FileRead", "FileSeek", "FileWriteByte", "FileWrite", "File", 
                                "FindClose", "FindFirst", "FindNext", "FindWindow", "FlushINI", "GetCurInstType", "GetCurrentAddress", "GetDlgItem", "GetDLLVersionLocal", 
                                "GetDLLVersion", "GetErrorLevel", "GetFileTimeLocal", "GetFileTime", "GetFullPathName", "GetFunctionAddress", "GetLabelAddress", 
                                "GetTempFileName", "GetWindowText", "Goto", "HideWindow", "IfAbort", "IfErrors", "IfFileExists", "IfRebootFlag", "IfSilent", "InitPluginsDir", 
                                "InstTypeGetText", "InstTypeSetText", "IntCmpU", "IntCmp", "IntFmt", "IntOp", "IsWindow", "LockWindow", "LogSet", "LogText", "MessageBox", 
                                "Nop", "Pop", "Push", "Quit", "ReadEnvStr", "ReadIniStr", "ReadRegDWORD", "ReadRegStr", "Reboot", "RegDLL", "Rename", "RequestExecutionLevel", 
                                "ReserveFile", "Return", "RMDir", "SearchPath", "SectionGetFlags", "SectionGetInstTypes", "SectionGetSize", "SectionGetText", "SectionSetFlags", 
                                "SectionSetInstTypes", "SectionSetSize", "SectionSetText", "SendMessage", "SetAutoClose", "SetBrandingImage", "SetCtlColors", "SetCurInstType", 
                                "SetDetailsPrint", "SetDetailsView", "SetErrorLevel", "SetErrors", "SetFileAttributes", "SetOutPath", "SetRebootFlag", "SetRegView", 
                                "SetShellVarContext", "SetSilent", "ShowWindow", "Sleep", "StrCmpS", "StrCmp", "StrCpy", "StrLen", "UnRegDLL", "var", "WriteINIStr", 
                                "WriteRegBin", "WriteRegDWORD", "WriteRegExpandStr", "WriteRegStr", "WriteUninstaller", 

                                // NSIDirectiveRegex
                                "!addIncludeDir", "!addplugindir", "!appendfile", "!cd", "!define", "!delfile", "!echo", "!else", "!endif", "!error", "!execute", "!ifdef", 
                                "!ifmacrodef", "!ifmacrondef", "!ifndef", "!if", "!include", "!insertmacro", "!macroend", "!macro", "!packhdr", "!searchparse", "!system", 
                                "!tempfile", "!undef", "!verbose", "!warning", 
                                
                                // NSIParameterRegex
                                "admin", "all", "alwaysoff", "ARCHIVE", "auto", "both", "bottom", "bzip", "checkbox", "colored", "components", "current", "custom", "directory", 
                                "false", "FILE_ATTRIBUTE_ARCHIVE", "FILE_ATTRIBUTE_HIDDEN", "FILE_ATTRIBUTE_NORMAL", "FILE_ATTRIBUTE_OFFLINE", "FILE_ATTRIBUTE_READONLY", 
                                "FILE_ATTRIBUTE_SYSTEM,TEMPORARY", "FILE_ATTRIBUTE_TEMPORARY", "force", "HIDDEN", "hide", "highest", "HKCC", "HKCR", "HKCU", "HKDD", 
                                "HKEY_CLASSES_ROOT", "HKEY_CURRENT_CONFIG", "HKEY_CURRENT_USER", "HKEY_DYN_DATA", "HKEY_LOCAL_MACHINE", "HKEY_PERFORMANCE_DATA", "HKEY_USERS", 
                                "HKLM", "HKPD", "HKU", "IDABORT", "IDCANCEL", "IDIGNORE", "IDNO", "IDOK", "IDRETRY", "IDYES", "ifdiff", "ifnewer", "instfiles", "lastused", 
                                "leave", "left", "license", "listonly", "lzma", "manual", "MB_ABORTRETRYIGNORE", "MB_DEFBUTTON2", "MB_DEFBUTTON", "MB_DEFBUTTON", "MB_DEFBUTTON", 
                                "MB_DEFBUTTON", "MB_ICONEXCLAMATION", "MB_ICONINFORMATION", "MB_ICONQUESTION", "MB_ICONSTOP", "MB_OKCANCEL", "MB_OK", "MB_RETRYCANCEL", 
                                "MB_RIGHT", "MB_RTLREADING", "MB_SETFOREGROUND", "MB_TOPMOST", "MB_USERICON", "MB_YESNOCANCEL", "MB_YESNO", "nevershow", "none", "normal", 
                                "OFFLINE", "off", "on", "radiobuttons", "READONLY", "right", "RO", "SHCTX", "show", "silentlog", "silent", "smooth", "SW_HIDE", 
                                "SW_SHOWMAXIMIZED", "SW_SHOWMINIMIZED", "SW_SHOWNORMAL", "SYSTEM", "textonly", "top", "true", "try", "uninstConfirm", "user", "zlib", 

                                // NSIParameterWSlash
                                "/a", "/BANNER", "/BRANDING", "/CAPTION", "/COMPONENTSONLYONCUSTOM", "/components", "/CUSTOMSTRING", "/ENABLECANCEL", "/e", "/FILESONLY", 
                                "/FINAL", "/GLOBAL", "/ifempty", "/IMGID", "/ITALIC", "/LANG", "/NOCUSTOM", "/nonfatal", "/NOUNLOAD", "/oname", "/o", "/REBOOTOK", 
                                "/RESIZETOFIT", "/r", "/SD", "/SHORT", "/silent", "/solid lzma", "/SOLID", "/STRIKE", "/TIMEOUT", "/TRIMCENTER", "/TRIMLEFT", "/TRIMRIGHT", 
                                "/UNDERLINE", "/windows", "/x", 

                                // NSICallbackRegex
                                ".onInit", ".onGUIInit", ".onGUIEnd", ".onInstFailed", ".onInstSuccess", ".onMouseOverSection", ".onRebootFailed", ".onSelChange", 
                                ".onUserAbort", ".onVerifyInstDir", "un.onGUIEnd", "un.onGUIInit", "un.onInit", "un.onRebootFailed", "un.onUninstFailed", "un.onUninstSuccess", 
                                "un.onUserAbort", 

                                // NSIVariable //"$0", "$1", "$2", "$3", "$4", "$5", "$6", "$7", "$8", "$9", "$R0", "$R1", "$R2", "$R3", "$R4", "$R5", "$R6", "$R7", "$R8", "$R9",
                                "$ADMINTOOLS", "$APPDATA", "$CDBURN_AREA", "$CMDLINE", "$COMMONFILES", "$COMMONFILES", "$COMMONFILES", "$COOKIES", "$DESKTOP", "$DOCUMENTS", 
                                "$EXEDIR", "$EXEFILE", "$EXEPATH", "$FAVORITES", "$FONTS", "$HISTORY", "$HWNDPARENT", "$INSTDIR", "$INTERNET_CACHE", "$LANGUAGE", 
                                "$LocalAppData", "$LOCALAPPDATA", "$MUSIC", "$NETHOOD", "$OUTDIR", "$PICTURES", "$PLUGINSDIR", "$PRINTHOOD", "$PROFILE", "$PROGRAMFILES", 
                                "$PROGRAMFILES", "$PROGRAMFILES", "$QUICKLAUNCH", "$RECENT", "$RESOURCES_LOCALIZED", "$RESOURCES", "$SENDTO", "$SMPROGRAMS", "$SMSTARTUP", 
                                "$STARTMENU", "$SYSDIR", "$TEMPLATES", "$TEMP", "$VIDEOS", "$WINDIR"
                            };

        string[] methods = { "" };
        string[] snippets = { "" };
        string[] declarationSnippets = { "" };

        //string[] keywords = { "and", "break", "default", "!define", "do", "else", "for", "if", "instance", "!include", "!insertmacro", "method", "not", "of", "or", "preload", "procedure", "properties", "rest", "return", "script", "send", "switch", "use", "var", "while", "asm", "case", "class", "local", "public", "self", "super", "objectName", "objectSpecies", "objectSuperclass", "objectFunctionArea", "objectInfo", "objectLocal", "objectSize", "objectTotalProperties", "objectType", "string" };
        //string[] methods = { "Equals()", "GetHashCode()", "GetType()", "ToString()" };
        //string[] snippets = { "if(^)\n{\n;\n}", "if(^)\n{\n;\n}\nelse\n{\n;\n}", "for(^;;)\n{\n;\n}", "while(^)\n{\n;\n}", "do${\n^;\n}while();", "switch(^)\n{\ncase : break;\n}" };
        //string[] declarationSnippets = { "public class ^\n{\n}", "private class ^\n{\n}", "internal class ^\n{\n}", "public struct ^\n{\n;\n}", "private struct ^\n{\n;\n}", 
        //                                   "internal struct ^\n{\n;\n}", "public void ^()\n{\n;\n}", "private void ^()\n{\n;\n}", "internal void ^()\n{\n;\n}", 
        //                                   "protected void ^()\n{\n;\n}", "public ^{ get; set; }", "private ^{ get; set; }", "internal ^{ get; set; }", "protected ^{ get; set; }" };
        
        #endregion Autocomplete

        #endregion Declares, Imports. etc.


        #region Constructor =================================================================================

        public MainFrm()
        {
            InitializeComponent();

            scriptbox.Language = Language.NSI;

            //create autocomplete popup menu
            popupMenu = new AutocompleteMenu(scriptbox);
            popupMenu.Items.ImageList = imageList;
            popupMenu.SearchPattern = @"[\w\.:=!<>]";
            popupMenu.AllowTabKey = true;
            BuildAutocompleteMenu();
        }

        #endregion Constructor


        #region Form Events =================================================================================

        /// <summary>
        /// Get initial settings
        /// </summary>
        private void Form_Load(object sender, EventArgs e)
        {
            // Open Options Form if no config file found
            string configPath = Path.GetDirectoryName(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath);
            OptionsFrm optionsFrm = new OptionsFrm();
            if (!Directory.Exists(configPath))
                optionsFrm.ShowDialog();

            #region Get Form Settings

            if (!Properties.Settings.Default.frmLocation.IsEmpty)
                this.Location = Properties.Settings.Default.frmLocation;
            else
                this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;

            if (!Properties.Settings.Default.frmSize.IsEmpty)
                this.Size = Properties.Settings.Default.frmSize;

            #endregion Get Form Settings

            #region Get NSIS

            NSIS.GetNSIS();
            nsisPath = Properties.Settings.Default.nsisPath;
            makeNSIS = Properties.Settings.Default.makeNSIS;

            #endregion Get NSIS

            #region Get Menu & Toolsbars States

            formatMenuWordWrap.Checked = Properties.Settings.Default.wordWrap;
            scriptbox.WordWrap = Properties.Settings.Default.wordWrap;

            if (Properties.Settings.Default.font != null)
                this.scriptbox.Font = Properties.Settings.Default.font;

            //Enable control extensions
            this.EnableControlExtensions();
            viewMenuInlineSpellChecker.Checked = Properties.Settings.Default.spellCheckVis;
            if (viewMenuInlineSpellChecker.Checked == true)
                scriptbox.EnableSpellCheck();
            if (viewMenuInlineSpellChecker.Checked == false)
                scriptbox.DisableSpellCheck();

            mainToolbar.Visible = Properties.Settings.Default.mainTBVis;
            propsToolbar.Visible = Properties.Settings.Default.propsTBVis;
            scriptEditToolbar.Visible = Properties.Settings.Default.editTBVis;
            buildToolbar.Visible = Properties.Settings.Default.buildTBVis;
            findToolbar.Visible = Properties.Settings.Default.findTBVis;
            statusbar.Visible = Properties.Settings.Default.statusbarVis;

            viewMenuLineNumbers.Checked = Properties.Settings.Default.foldingLinesVis;
            scriptbox.ShowLineNumbers = Properties.Settings.Default.foldingLinesVis;

            viewMenuFoldingLines.Checked = Properties.Settings.Default.foldingLinesVis;
            scriptbox.ShowFoldingLines = Properties.Settings.Default.foldingLinesVis;

            #endregion Get Get Menu & Toolsbars States

            #region i00SpellCheck

            ////load the extension
            //((iTestHarness)scriptbox.SpellCheck()).SetupControl(scriptbox);
            //ControlExtensions.LoadSingleControlExtension(scriptbox, new SpellCheck());

            ////Enable control extensions
            //scriptbox.EnableControlExtensions();
            //scriptbox.EnableSpellCheck();

            //To load a custom dictionary from a saved file:
            //i00SpellCheck.FlatFileDictionary Dictionary = new i00SpellCheck.FlatFileDictionary("dic.dic", true);
            //scriptbox.DisableSpellCheck();

            #endregion i00SpellCheck

            #region Settings

            // Default Project Path
            if (String.IsNullOrEmpty(Properties.Settings.Default.defaultProjPath))
                Properties.Settings.Default.defaultProjPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            // Publish Out Path
            if (Properties.Settings.Default.outToProjPath == false)
            {
                if (String.IsNullOrEmpty(Properties.Settings.Default.pubOutPath))
                    Properties.Settings.Default.pubOutPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }

            // Set includeSRCChkBox state
            includeSRCChkBox.Checked = Props.includeSRC = Properties.Settings.Default.includeSRCDefault;

            // Hide outputbox
            splitContainer.SplitterDistance = splitContainer.Height;

            // Set unicodeChkBox state
            unicodeChkBox.Checked = Props.unicodeInst = Properties.Settings.Default.unicodeInst;

            // Set loggingChkBox state
            loggingChkBox.Checked = Props.loggingInst = Properties.Settings.Default.loggingInst;


            // Set includeSRCChkBox state
            includeSRCChkBox.Checked = Props.includeSRC = Properties.Settings.Default.includeSRCDefault;

            // Set aspectChkBox state
            Props.projAspect = Properties.Settings.Default.aspectDefault;
            bool? tmpRatio = Props.projAspect;
            if (tmpRatio != null)
            {
                aspectChkBox.Checked = Props.projAspect = Properties.Settings.Default.aspectDefault;
            }
            else
            {
                Props.projAspect = true;

                try
                {
                    string value = Registry.GetValue(@"HKEY_CURRENT_USER\Software\mtnPhilms\SCICompanion\SCICompanion", "OriginalAspectRatio", 0).ToString();

                    if (value == "1")
                        Props.projAspect = true;

                    if (value == "0")
                        Props.projAspect = false;
                }
                catch (Exception ex)
                {
                    string err = ("Error: " + ex.Message);
                    MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];
                    mainFrm.statusbarMainMessage.Text = (err);
                    MessageBox.Show(err + Environment.NewLine + Environment.NewLine + "NSIS not found. You must download and install NSIS to use the Publish function." + Environment.NewLine + Environment.NewLine + "See http://nsis.sourceforge.net/ for details",
                        "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
                }
            }

            aspectChkBox.Checked = Props.projAspect;

            #endregion Settings

            // Publishes project if Props.projPath is passed from command line
            if (Directory.Exists(Props.projPath))
            {
                DirectoryInfo dirInfo = new DirectoryInfo(Props.projPath);
                Props.projPath = GetProperDirectoryCapitalization(dirInfo);

                LoadPath();
            }

            scriptbox.ClearUndo();
            editMenuUndo.Enabled = undoBtn.Enabled = undoCntxtMenu.Enabled = false;
            editMenuRedo.Enabled = redoBtn.Enabled = redoCntxtMenu.Enabled = false;

            opened = true;
            altered = false;
            killMsg = false;

            // Remove altered indicator asterisk from titlebar when opening newly created scripts
            this.Text = this.Text.Replace("* ", "");

            // Loads Script if passed from command line
            if (!String.IsNullOrEmpty(scriptFile))
            {
                OpenScript();
                Text = scriptFile + " - " + appTitle;
                Program.passedArgs = false;
            }

            timer.Start();
        }


        /// <summary>
        /// Starts compile after form shows if Props.projPath was passed from command line
        /// </summary>
        public void MainFrm_Shown(object sender, EventArgs e)
        {
            if (Program.passedArgs == true)
            {
                SetScriptTab();
                splashPanel.Show();
                statusbarMainMessage.Text = "Please wait...";


                Compile();
            }
        }


        /// <summary>
        /// Checks if script is altered to prompt user to save before closing Editor
        /// </summary>
        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            #region Promt save

            // Check if script is Unsaved New script
            string title = this.Text;
            bool contains = title.IndexOf("(new unsaved script) - ", StringComparison.OrdinalIgnoreCase) >= 0;

            if (altered == true || contains == true)
            {
                string msg;
                //if (File.Exists(scriptFile))
                //    scriptFile = Props.projPath + "\\Setup Script.nsi";
                string scriptName = Path.GetFileName(scriptFile);

                if (contains == true)
                    msg = "Do you wish to save " + scriptName + "?";
                else
                    msg = "Do you wish to save changes to " + scriptName + "?";

                DialogResult dialogResult = MessageBox.Show(msg, "Unsaved Changes", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                if (dialogResult == DialogResult.Yes)
                    Save();
                else if (dialogResult == DialogResult.Cancel)
                    e.Cancel = true;
            }

            #endregion Promt save

            // Save screen metrics
            Properties.Settings.Default.frmLocation = this.Location;
            Properties.Settings.Default.frmSize = this.Size;

            Properties.Settings.Default.Save();
        }

        #endregion Form Events


        #region Menubar =====================================================================================

        #region File Menu ===================================================================================

        /// <summary>
        /// Opens NewScript dialog 
        /// </summary>
        private void fileMenuNew_Click(object sender, EventArgs e)
        {
            New();
        }


        /// <summary>
        /// Opens a browse to file dialog to open existing script
        /// </summary>
        private void fileMenuOpen_Click(object sender, EventArgs e)
        {
            Open();
        }


        /// <summary>
        /// Saves current script
        /// </summary>
        private void fileMenuSave_Click(object sender, EventArgs e)
        {
            Save();
        }


        /// <summary>
        /// Opens a save file dialog to save current script with a new name
        /// </summary>
        private void fileMenuSaveAs_Click(object sender, EventArgs e)
        {
            SaveAs();
        }


        /// <summary>
        /// Opens config XML for debugging
        /// </summary>
        private void fileMenuOpenConf_Click(object sender, EventArgs e)
        {
            string configPath = Path.GetDirectoryName(ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath);

            try
            {
                Process.Start(configPath);
            }
            catch { }
        }


        /// <summary>
        /// Opens Page Setup dialog
        /// </summary>
        private void fileMenuPageSetup_Click(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Opens Print Preview dialog
        /// </summary>
        private void fileMenuPrintPreview_Click(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Opens print dialog
        /// </summary>
        private void fileMenuPrint_Click(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Closes Script Editor
        /// </summary>
        private void fileMenuExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion File Menu


        #region Edit Menu ===================================================================================

        /// <summary>
        /// Undoes last edit
        /// </summary>
        private void editMenuUndo_Click(object sender, EventArgs e)
        {
            Undo();
        }


        /// <summary>
        /// Undoes the last undo action
        /// </summary>
        private void editMenuRedo_Click(object sender, EventArgs e)
        {
            Redo();
        }


        /// <summary>
        /// Cuts selected text in current script
        /// </summary>
        private void editMenuCut_Click(object sender, EventArgs e)
        {
            Cut();
        }


        /// <summary>
        /// Copies selected text in current script to clipboard
        /// </summary>
        private void editMenuCopy_Click(object sender, EventArgs e)
        {
            Copy();
        }


        /// <summary>
        /// Pastes clipboard's contents into current script at current caret position
        /// </summary>
        private void editMenuPaste_Click(object sender, EventArgs e)
        {
            Paste();
        }


        /// <summary>
        /// Deletes selected text in current script
        /// </summary>
        private void editMenuDelete_Click(object sender, EventArgs e)
        {
            Delete();
        }


        /// <summary>
        /// Opens the FastColoredTextBox Find Dialog
        /// </summary>
        private void editMenuFind_Click(object sender, EventArgs e)
        {
            Find();
        }


        /// <summary>
        /// Invokes find next
        /// </summary>
        private void editMenuFindNext_Click(object sender, EventArgs e)
        {
            FindNext();
        }


        /// <summary>
        /// Opens the FastColoredTextBox Replace Dialog
        /// </summary>
        private void editMenuReplace_Click(object sender, EventArgs e)
        {
            Replace();
        }


        /// <summary>
        /// Opens the FastColoredTextBox GoTo Dialog
        /// </summary>
        private void editMenuGoTo_Click(object sender, EventArgs e)
        {
            GoTo();
        }


        /// <summary>
        /// Selects all text in current script
        /// </summary>
        private void editMenuSelectAll_Click(object sender, EventArgs e)
        {
            SelectAll();
        }


        /// <summary>
        /// Clears all text in scriptbox
        /// </summary>
        private void editMenuClear_Click(object sender, EventArgs e)
        {
            ClearScriptBox();
        }


        /// <summary>
        /// Pastes current Date and Time into open script
        /// </summary>
        private void editMenuTimeDate_Click(object sender, EventArgs e)
        {
            timeStamp();
        }

        #endregion Edit Menu


        #region Script Menu =================================================================================

        /// <summary>
        /// Compiles current script
        /// </summary>
        private void buildMenuCompile_Click(object sender, EventArgs e)
        {
            Compile();
        }


        /// <summary>
        /// Compiles current script and debugs compiled installer
        /// </summary>
        private void buildMenuCompileDebug_Click(object sender, EventArgs e)
        {
            Compile();

            run = true;
        }


        /// <summary>
        /// Stops Compiler
        /// </summary>
        private void buildMenuStopcompile_Click(object sender, EventArgs e)
        {
            Process[] processes = Process.GetProcessesByName("makensis");

            foreach (Process process in processes)
                process.Kill();
        }
            

        /// <summary>
        /// Debugs compiled installer
        /// </summary>
        private void scriptMenuDebug_Click(object sender, EventArgs e)
        {
            //Properties.Settings.Default.pubOutPath = Props.projPath;
            string installer = Props.projOutPath + @"\" + Props.projDOSName + @"_Setup.exe";
            this.statusbarMainMessage.Text = installer;
            try
            {
                Process.Start(installer);
            }
            catch (Exception ex)
            {
                string err = ("Error: " + ex.Message);
                this.statusbarMainMessage.Text = (err);
                //MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
            }

            run = false;
        }

        #endregion Script Menu


        #region Format Menu =================================================================================

        /// <summary>
        /// Turns word wrap on or off
        /// </summary>
        private void formatMenuWordWrap_Click(object sender, EventArgs e)
        {
            if (formatMenuWordWrap.Checked == true)
                scriptbox.WordWrap = true;

            if (formatMenuWordWrap.Checked == false)
                scriptbox.WordWrap = false;
        }


        /// <summary>
        /// Changes the Script Editor's display font
        /// </summary>
        private void formatMenuFont_Click(object sender, EventArgs e)
        {
            FontDialog fontDialog = new FontDialog();
            fontDialog.FixedPitchOnly = true;
            fontDialog.Font = scriptbox.Font;

            DialogResult result = fontDialog.ShowDialog();

            // Return if Cancel is clicked
            if (result == DialogResult.Cancel) return;

            Font font = fontDialog.Font;

            // Set editBox properties
            this.scriptbox.Font = font;

            // Remember selection
            Properties.Settings.Default.font = font;
        }

        #endregion Format Menu


        #region View Menu ===================================================================================

        /// <summary>
        /// Turns inline spell checker on and off
        /// </summary>
        private void viewMenuInlineSpellChecker_Click(object sender, EventArgs e)
        {
            //viewMenuInlineSpellChecker.Checked;

            if (scriptbox.IsSpellCheckEnabled())
                scriptbox.DisableSpellCheck();
            else
                scriptbox.EnableSpellCheck();

        }


        /// <summary>
        /// Tracks inline spell checker state
        /// </summary>
        private void viewMenuInlineSpellChecker_CheckStateChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.spellCheckVis = viewMenuInlineSpellChecker.Checked;

            if (viewMenuInlineSpellChecker.Checked == true)
                scriptbox.EnableSpellCheck();
            if (viewMenuInlineSpellChecker.Checked == false)
                scriptbox.DisableSpellCheck();
        }


        /// <summary>
        /// Show or hide all Toolbars
        /// </summary>
        private void viewMenuHideToolbars_CheckStateChanged(object sender, EventArgs e)
        {
            if (viewMenuHideToolbars.Checked == false)
            {
                viewMenuMainToolbar.Checked = viewMenuPropsToolbar.Checked = viewMenuEditToolbar.Checked =
                    viewMenuBuildToolbar.Checked = viewMenuFindToolbar.Checked = true;
            }
            else if (viewMenuHideToolbars.Checked == true)
            {
                viewMenuMainToolbar.Checked = viewMenuPropsToolbar.Checked = viewMenuEditToolbar.Checked =
                    viewMenuBuildToolbar.Checked = viewMenuFindToolbar.Checked = true;
            }
        }


        /// <summary>
        /// Show or hide Script Editor's Main Toolbar
        /// </summary>
        private void viewMenuMainToolbar_CheckStateChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.mainTBVis = mainToolbar.Visible = viewMenuMainToolbar.Checked;

            //mainToolbar.Visible = true;
            //if (viewMenuMainToolbar.Checked == true)
            //{
            //    mainToolbar.Visible = true;
            //    Properties.Settings.Default.mainTBVis = true;
            //}
            //if (viewMenuMainToolbar.Checked == false)
            //{
            //    mainToolbar.Visible = false;
            //    Properties.Settings.Default.mainTBVis = false;
            //}
        }


        /// <summary>
        /// Show or hide Project Properties Toolbar
        /// </summary>
        private void viewMenuPropsToolbar_CheckStateChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.propsTBVis = propsToolbar.Visible = viewMenuPropsToolbar.Checked;
        }


        /// <summary>
        /// Show or hide the Edit Toolbar
        /// </summary>
        private void viewMenuEditToolbar_CheckStateChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.editTBVis = scriptEditToolbar.Visible = viewMenuEditToolbar.Checked;
        }


        /// <summary>
        /// Show or hide the Compile Toolbar
        /// </summary>
        private void viewMenuBuildToolbar_CheckStateChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.buildTBVis = buildToolbar.Visible = viewMenuBuildToolbar.Checked;
        }


        /// <summary>
        /// Show or hide the Find Toolbar
        /// </summary>
        private void viewMenuFindToolbar_CheckStateChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.findTBVis = findToolbar.Visible = viewMenuFindToolbar.Checked;
        }


        /// <summary>
        /// Show or hide Script Editor's statusbar
        /// </summary>
        private void viewMenuStatusbar_CheckStateChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.statusbarVis = statusbar.Visible = viewMenuStatusbar.Checked;
        }


        /// <summary>
        /// Shows or hides line number display
        /// </summary>
        private void viewMenuLineNumbers_CheckStateChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.lineNumsVis = scriptbox.ShowLineNumbers = viewMenuLineNumbers.Checked;
        }


        /// <summary>
        /// Shows or hides folding lines
        /// </summary>
        private void viewMenuFoldingLines_CheckStateChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.foldingLinesVis = scriptbox.ShowFoldingLines = viewMenuFoldingLines.Checked;
        }

        #endregion View Menu


        #region Tools Menu ==================================================================================

        /// <summary>
        /// Gets folder size of SEC1 files
        /// </summary>
        private void toolsMenuGetFolderSize_Click(object sender, EventArgs e)
        {
            // Select Game Folder
            FolderBrowserDialog projPathBrowserDlg = new FolderBrowserDialog();
            projPathBrowserDlg.Description = "Select Installed Game Folder";
            projPathBrowserDlg.ShowNewFolderButton = true;
            projPathBrowserDlg.RootFolder = Environment.SpecialFolder.Desktop;
            projPathBrowserDlg.SelectedPath = "c:\\";

            // Check if Browse Files Dialog Cancel Button pressed and return if Cancel is pressed
            if (projPathBrowserDlg.ShowDialog() == DialogResult.Cancel)
            {
                this.statusbarMainMessage.Text = "Browse canceled"; return;
            }

            string selectedPath = projPathBrowserDlg.SelectedPath;

            // Open ConvertFrm to display folder size
            DirectoryInfo di = new DirectoryInfo(selectedPath);
            long z = GetSizes.DirSize(di);

            // Return if error
            if (accessErr == true) return;

            decimal bytes = z;
            decimal kb = Math.Round(bytes / 1024);
            decimal mb = Math.Round(bytes / 1048576, 2);

            string secSize = "!define SEC01_SIZE " + kb + " ; KB, " + mb + " MB (Size required for SEC01)";
            
            //scriptbox[] text = new TextBox[];
            int i = 0;
            if (scriptbox.Lines.Count >= 3)
            {
                foreach (string ln in scriptbox.Lines)
                {
                    string line = ln;
                    if (line.StartsWith(";!define SEC01_SIZE"))
                    {
                        // Go to SEC Size Line and Select it
                        scriptbox.Navigate(i);
                        scriptbox.SelectionLength = scriptbox.GetLineLength(i);

                        // Replace line
                        scriptbox.SelectedText = line.Replace(line, secSize);
                    }
                    i++;
                }
            }
        }


        /// <summary>
        /// Gets folder size of SEC2 (SRC) files
        /// </summary>
        private void toolsMenuGetSEC2FolderSize_Click(object sender, EventArgs e)
        {
            // Select Source Folders
            string lipsyncPath = Props.projPath + "\\lipsync";
            string msgPath = Props.projPath + "\\msg";
            string polyPath = Props.projPath + "\\poly";
            string srcPath = Props.projPath + "\\src";

            long gameINILength = new FileInfo(Props.projPath + "\\game.ini").Length;

            DirectoryInfo lipsyncDirInfo = new DirectoryInfo(lipsyncPath);
            long lipsyncSz = GetSizes.DirSize(lipsyncDirInfo);
            // Return if error
            if (accessErr == true) return;

            DirectoryInfo msgDirInfo = new DirectoryInfo(msgPath);
            long msgSz = GetSizes.DirSize(msgDirInfo);
            // Return if error
            if (accessErr == true) return;

            DirectoryInfo polyDirInfo = new DirectoryInfo(polyPath);
            long polySz = GetSizes.DirSize(polyDirInfo);
            // Return if error
            if (accessErr == true) return;

            DirectoryInfo srcDirInfo = new DirectoryInfo(srcPath);
            long srcSz = GetSizes.DirSize(srcDirInfo);
            // Return if error
            if (accessErr == true) return;

            long size = gameINILength + lipsyncSz + msgSz + polySz + srcSz;

            decimal bytes = size;
            decimal kb = Math.Round(bytes / 1024);
            decimal mb = Math.Round(bytes / 1048576, 2);

            string sec2Size = "!define FIX THIS!!!!!!!!!!!!!!!!! SEC02_SIZE " + kb + " ; KB, " + mb + " MB (Size required for SEC01)";

            //scriptbox[] text = new TextBox[];
            int i = 1;
            if (scriptbox.Lines.Count >= 3)
            {
                foreach (string ln in scriptbox.Lines)
                {
                    string line = ln;
                    if (line.StartsWith(";!define SEC01_SIZE"))
                    {
                        // Go to SEC Size Line and Select it
                        scriptbox.Navigate(i);
                        scriptbox.SelectionLength = scriptbox.GetLineLength(i);

                        // Add line
                        scriptbox.SelectedText = line + Environment.NewLine + sec2Size;
                    }
                    i++;
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        private void toolsMenuByteConverter_Click(object sender, EventArgs e)
        {
            ByteConverterFrm byteConverterFrm = new ByteConverterFrm();
            byteConverterFrm.Show();
        }


        /// <summary>
        /// 
        /// </summary>
        private void toolsMenuClipboardTool_Click(object sender, EventArgs e)
        {
            ClipBoardFrm clipBoardFrm = new ClipBoardFrm();
            clipBoardFrm.Show();
        }


        /// <summary>
        /// Opens Spell Checker dialog box
        ///     Does not currently work with FastColorBox control - Remove feature to prevent auto replace in script?
        /// </summary>
        private void toolsMenuSpellCheck_Click(object sender, EventArgs e)
        {
            //var iSpellCheckDialog = scriptbox.SpellCheck(true, null) as i00SpellCheck.SpellCheckControlBase.iSpellCheckDialog;
            var iSpellCheckDialog = scriptbox.SpellCheck(false, null) as i00SpellCheck.SpellCheckControlBase.iSpellCheckDialog;
            if (iSpellCheckDialog != null)
                iSpellCheckDialog.ShowDialog();

            if (iSpellCheckDialog != null)
                iSpellCheckDialog.ShowDialog();
        }


        /// <summary>
        /// Opens personal dictionary for editing
        /// </summary>
        private void toolsMenuEditDictionary_Click(object sender, EventArgs e)
        {
            #region
            //do {
            ////if (ctl.SpellCheck) {
            //    //IsNot;
            //    (null && ctl.SpellCheck.CurrentDictionary.Loading) = false;
            //    List controls = new List(Of, Control);
            //    object AllControlsWithSameDict = From;
            //    xItem;
            //    ctl.SpellCheck.AllControlsWithSameDict();
            //    switch (xItem.Control) {
            //    }
            //    ToArray;
            //    if ((AllControlsWithSameDict.Count > 1)) {
            //        Using;
            //        ((void)(MessageBoxManager));
            //        new MessageBoxManager();
            //        MessageBoxManager.Yes = "Just this";
            //        MessageBoxManager.No = "All controls";
            //        switch (MsgBox(("Do you want to modify the dictionary for just this control or all " 
            //                        + (AllControlsWithSameDict.Count + " controls that share this dictionary?")), (MsgBoxStyle.YesNoCancel | MsgBoxStyle.Question))) {
            //            case MsgBoxResult.Yes:
            //                // Just this
            //                controls.Add(ctl);
            //                break;
            //            case MsgBoxResult.No:
            //                // All controls
            //                controls.AddRange(AllControlsWithSameDict);
            //                break;
            //            case MsgBoxResult.Cancel:
            //                return;
            //                break;
            //        }
            //    }
            //}
            //else {
            //    // this is the only control using this dict... just do this one
            //    controls.Add(ctl);
            //}
            //object NewDict = ctl.SpellCheck.CurrentDictionary.ShowUIEditor();
            //foreach (item in controls) {
            //    item.SpellCheck.CurrentDictionary = NewDict;
            //}
            //return;
            #endregion
        }


        /// <summary>
        /// Opens options dialog queued to Script Editor's tab
        /// </summary>
        private void toolsMenuOptions_Click(object sender, EventArgs e)
        {
            OptionsFrm optionsFrm = new OptionsFrm();
            optionsFrm.ShowDialog();
        }


        /// <summary>
        /// Opens properties for script
        ///     Undecided feature, should this call file properties, script properties, both or be eliminated?
        /// </summary>
        private void toolsMenuScriptProperties_Click(object sender, EventArgs e)
        {
            ShellClass shellClass = new ShellClass();

            try
            {
                shellClass.DisplayFileProperties(scriptFile);
                this.statusbarMainMessage.Text = ("Displaying Properties for " + scriptFile);
            }
            catch (Exception ex)
            {
                this.statusbarMainMessage.Text = ("Error: " + ex.Message);
                string err = ("Error: " + ex.Message);
                MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
            }
        }

        #endregion Tools Menu


        #region Help Menu ===================================================================================

        /// <summary>
        /// Show Help File's contents list queued to Script Editor's entry
        /// </summary>
        private void helpMenuContents_Click(object sender, EventArgs e)
        {
            if (File.Exists("Documents\\NSIS Publisher Help.chm"))
                Help.ShowHelp(this, helpProvider.HelpNamespace, HelpNavigator.TableOfContents);
            else
                MessageBox.Show("NSIS Publisher Help File not found.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        /// <summary>
        /// Opens help file index
        /// </summary>
        private void helpMenuIndex_Click(object sender, EventArgs e)
        {
            if (File.Exists("Documents\\NSIS Publisher Help.chm"))
                Help.ShowHelp(this, helpProvider.HelpNamespace, HelpNavigator.Index);
            else
                MessageBox.Show("NSIS Publisher Help File not found.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        /// <summary>
        /// Opens help file search
        /// </summary>
        private void helpMenuSearch_Click(object sender, EventArgs e)
        {
            if (File.Exists("Documents\\NSIS Publisher Help.chm"))
                Help.ShowHelp(this, helpProvider.HelpNamespace, HelpNavigator.Find, "");
            else
                MessageBox.Show("NSIS Publisher Help File not found.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        /// <summary>
        /// Open NSIS Manual 
        /// </summary>
        private void helpMenuNSISManual_Click(object sender, EventArgs e)
        {
            if (File.Exists(nsisPath + "\\NSIS.chm"))
                Process.Start(nsisPath + "\\NSIS.chm");
            else
                MessageBox.Show("NSIS Help File not found. Is NSIS installed?", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        /// <summary>
        /// Go to the NSIS Wiki
        /// </summary>
        private void helpMenuNSISWiki_Click(object sender, EventArgs e)
        {
            Process.Start("http://nsis.sourceforge.net/Main_Page");
        }


        /// <summary>
        /// Go to NSIS Forum
        /// </summary>
        private void helpMenuNSISForum_Click(object sender, EventArgs e)
        {
            Process.Start("http://forums.winamp.com/forumdisplay.php?s=&forumid=65");
        }


        /// <summary>
        /// Go to SCI Community forum
        /// </summary>
        private void helpMenuSCICommunity_Click(object sender, EventArgs e)
        {
            Process.Start("http://sciprogramming.com/community/");
        }


        /// <summary>
        /// Open About form queued to Script Editor's tab
        /// </summary>
        private void helpMenuAbout_Click(object sender, EventArgs e)
        {
            AboutBox aboutBox = new AboutBox();
            //AboutBox.callingForm = "ScriptEdit";
            aboutBox.ShowDialog();
        }

        #endregion Help Menu

        #endregion Menubar


        #region Toolbars ====================================================================================

        #region Main Toolbar ================================================================================

        /// <summary>
        /// Opens the NewScript dialog
        /// </summary>
        private void newBtn_Click(object sender, EventArgs e)
        {
            New();
        }


        /// <summary>
        /// Opens a file browser to open an existing file in Script Editor
        /// </summary>
        private void openBtn_Click(object sender, EventArgs e)
        {
            Open();
        }


        /// <summary>
        /// Saves current script
        /// </summary>
        private void saveBtn_Click(object sender, EventArgs e)
        {
            Save();
        }


        /// <summary>
        /// Opens a save file dialog to save current script with a new name
        /// </summary>
        private void saveAsBtn_Click(object sender, EventArgs e)
        {
            SaveAs();
        }


        /// <summary>
        /// Opens project's license
        /// </summary>
        private void openLicenseBtn_Click(object sender, EventArgs e)
        {
            OpenLicense();
        }


        /// <summary>
        /// Opens project's Readme for editing
        /// </summary>
        private void editReadmeBtn_Click(object sender, EventArgs e)
        {
            EditReadme();
        }


        /// <summary>
        /// Opens help file contents queued to Script Editor entry
        /// </summary>
        private void helpBtn_Click(object sender, EventArgs e)
        {
            if (File.Exists("Documents\\NSIS Publisher Help.chm"))
                Help.ShowHelp(this, helpProvider.HelpNamespace, HelpNavigator.TableOfContents);
            else
                MessageBox.Show("NSIS Publisher Help File not found.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        #endregion Main Toolbar


        #region Project Properties Toolbar ==================================================================

        /// <summary>
        /// Clears all text in scriptbox
        /// </summary>
        private void clearBtn_Click(object sender, EventArgs e)
        {
            ClearField();
        }


        /// <summary>
        /// Opens project's Include Folder
        /// </summary>
        private void openIncludeBtn_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(Props.projPath))
            {
                DialogResult dialogResult = MessageBox.Show("No project folder has been selected. Would you like to select it now?", "No Folder Selected", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                    projPathBrowseBtn_Click(null, null);
                if (dialogResult == DialogResult.No)
                    return;
            }

            string includePath = Props.projPath + @"\Publish_Include";

            if (Directory.Exists(includePath))
                try
                {
                    Process.Start(includePath);
                }
                catch { }
            else
            {
                if (!Directory.Exists(includePath))
                {
                    if (Properties.Settings.Default.missingIncludeWarning == true)
                    {
                        MissingIncludeMsgbox missingIncludeMsgbox = new NSISPublisher.MissingIncludeMsgbox();
                        missingIncludeMsgbox.ShowDialog();
                        if (cancel == true)
                            return;
                    }
                }
            }
        }


        /// <summary>
        /// Generatges NSIS NSI script and compiles installer
        /// </summary>
        private void publishBtn_Click(object sender, EventArgs e)
        {
            Props.rePublish = true;
            PublishScript();
        }

        #endregion Project Properties Toolbar


        #region Script Toolbar ==============================================================================

        /// <summary>
        /// Locks/unlocks contents from editing
        /// </summary>
        private void lockBtn_CheckStateChanged(object sender, EventArgs e)
        {
            if (!lockBtn.Checked)
            {
                // Check if ScriptFile is ReadOnly
                FileAttributes attrib = FileAttributes.Normal;
                if (System.IO.File.Exists(Props.projPath))
                    attrib = System.IO.File.GetAttributes(Props.projPath);

                if ((attrib & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
                {
                    // Keep Lock button checked until DialogResult given
                    lockBtn.Checked = true;
                    //if (!File.Exists(scriptFile))
                    //    scriptFile = Props.projPath + "\\Setup Script.nsi";
                    string scriptName = Path.GetFileName(scriptFile);

                    DialogResult dialogResult = MessageBox.Show(@"Do you wish to make """ + scriptName + @""" writable?", "Script is Read Only!",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        System.IO.File.SetAttributes(Props.projPath, System.IO.File.GetAttributes(Props.projPath) & ~FileAttributes.ReadOnly);
                        lockBtn.Checked = false;
                    }
                    else
                    {
                        lockBtn.Checked = true;
                        return;
                    }
                }

                // Unlock scriptbox
                lockBtn.Image = Properties.Resources.UnlockScript;
                scriptbox.ReadOnly = false;
                scriptbox.BackColor = Color.White;
            }
            else
            {
                lockBtn.Image = Properties.Resources.LockScript;
                scriptbox.ReadOnly = true;
                scriptbox.BackColor = Color.FromArgb(255, 255, 225);
            }
        }


        /// <summary>
        /// Opens Script's File Properties Dialog
        /// </summary>
        private void scriptPropsBtn_Click(object sender, EventArgs e)
        {
            ShellClass shellClass = new ShellClass();

            try
            {
                shellClass.DisplayFileProperties(scriptFile);
                this.statusbarMainMessage.Text = ("Displaying Properties for " + scriptFile);
            }
            catch (Exception ex)
            {
                this.statusbarMainMessage.Text = ("Error: " + ex.Message);
                string err = ("Error: " + ex.Message);
                MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
            }
        }


        /// <summary>
        /// Cuts selected text in current script
        /// </summary>
        private void cutBtn_Click(object sender, EventArgs e)
        {
            Cut();
        }


        /// <summary>
        /// Copies selected text in current script to clipboard
        /// </summary>
        private void copyBtn_Click(object sender, EventArgs e)
        {
            Copy();
        }


        /// <summary>
        /// Pastes clipboard's contents into current script at current caret position
        /// </summary>
        private void pasteBtn_Click(object sender, EventArgs e)
        {
            Paste();
        }


        /// <summary>
        /// Undoes last edit
        /// </summary>
        private void undoBtnBtn_Click(object sender, EventArgs e)
        {
            Undo();
        }


        /// <summary>
        /// Undoes the last undo action
        /// </summary>
        private void redoBtn_Click(object sender, EventArgs e)
        {
            Redo();
        }


        /// <summary>
        /// Selects all text in current script
        /// </summary>
        private void selectAllBtn_Click(object sender, EventArgs e)
        {
            SelectAll();
        }


        /// <summary>
        /// Deletes selected text in current script
        /// </summary>
        private void deleteBtn_Click(object sender, EventArgs e)
        {
            Delete();
        }


        /// <summary>
        /// Clears all text in scriptbox
        /// </summary>
        private void clearScriptBtn_Click(object sender, EventArgs e)
        {
            ClearScriptBox();
        }

        #endregion Script Toolbar


        #region Compile Toolbar =============================================================================

        /// <summary>
        /// Place holder for rebuilding the game's resource archive/map
        /// </summary>
        public void compileBtn_Click(object sender, EventArgs e)
        {
            Compile();
        }


        /// <summary>
        /// Compile and Run script 
        /// </summary>
        private void compileDebugBtn_Click(object sender, EventArgs e)
        {
            Compile();

            run = true;
        }


        /// <summary>
        /// Stops Compiler
        /// </summary>
        private void CompileStopBtn_Click(object sender, EventArgs e)
        {
            processCaller.Cancel();

            Process[] processes = Process.GetProcessesByName("makensis");

            foreach (Process process in processes)
                try
                {
                    process.Kill();
                }
                catch { }
        }


        /// <summary>
        /// Debugs compiled installer
        /// </summary>
        private void debugBtn_Click(object sender, EventArgs e)
        {
            Debug();
        }

        #endregion Compile Toolbar


        #region Find & Replace Toolbar ======================================================================

        /// <summary>
        /// Opens the FastColoredTextBox Find Dialog
        /// </summary>
        private void findBtn_Click(object sender, EventArgs e)
        {
            Find();
        }


        /// <summary>
        /// Finds next occurrence of search term
        /// </summary>
        private void findNextBtn_Click(object sender, EventArgs e)
        {
            FindNext();
        }


        /// <summary>
        /// Opens the Replace form
        /// </summary>
        private void replaceBtn_Click(object sender, EventArgs e)
        {
            Replace();
        }


        /// <summary>
        /// Opens the FastColoredTextBox GoTo Dialog
        /// </summary>
        private void goToBtn_Click(object sender, EventArgs e)
        {
            GoTo();
        }

        #endregion Find & Replace Toolbar

        #endregion Toolbars


        #region TabPages ====================================================================================

        /// <summary>
        /// Tracks Which TabPage is Selected
        /// </summary>
        private void tabCntrl_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (tabPages.SelectedTab == propsTabPg)
            //    ManageBtns();
            //if (tabPages.SelectedTab == scriptTabPg)
            //    ManageBtns();

            //Properties.Settings.Default.currentTabPage = tabPages.SelectedIndex;
        }


        #region Properties Tab ==============================================================================

        /// <summary>
        /// Sets project's game engine
        /// </summary>
        private void projEngCmboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Props.projEngine = projEngCmboBox.Text;
        }


        #region Project Path ================================================================================

        /// <summary>
        /// If project path is changed, check if path exists
        /// </summary>
        private void projPathTxtBox_TextChanged(object sender, EventArgs e)
        {
            projPathTxtBox.Text = projPathTxtBox.Text.Replace("*", "");
            projPathTxtBox.Text = projPathTxtBox.Text.Replace("?", "");
            projPathTxtBox.Text = projPathTxtBox.Text.Replace("/", "");
            //projPathTxtBox.Text = projPathTxtBox.Text.Replace(@"\", "");
            projPathTxtBox.Text = projPathTxtBox.Text.Replace("|", "");
            projPathTxtBox.Text = projPathTxtBox.Text.Replace("\"", "'");
            projPathTxtBox.Text = projPathTxtBox.Text.Replace("<", "");
            projPathTxtBox.Text = projPathTxtBox.Text.Replace(">", "");

            if (Directory.Exists(projPathTxtBox.Text))
                Props.projPath = projPathTxtBox.Text;
        }


        /// <summary>
        /// Gets item being drug into projPathTxtBox
        /// </summary>
        private void projPathTxtBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
                e.Effect = DragDropEffects.All;
        }


        /// <summary>
        /// Open dropped folder
        /// </summary>
        private void projPathTxtBox_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] objs = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string obj in objs)
                {
                    FileAttributes attr = File.GetAttributes(obj);
                    if ((attr & FileAttributes.Directory) == FileAttributes.Directory)
                    {
                        ClearAll();
                        Props.projPath = obj;
                        LoadPath();
                    }
                }
            }
        }


        /// <summary>
        /// Browse to change project path, then check if path exists and check appropriate template
        /// </summary>
        private void projPathBrowseBtn_Click(object sender, EventArgs e)
        {
            // Set Game Folder
            FolderBrowserDialog projPathBrowserDlg = new FolderBrowserDialog();
            projPathBrowserDlg.Description = "Select Project Folder";
            projPathBrowserDlg.ShowNewFolderButton = true;
            projPathBrowserDlg.RootFolder = Environment.SpecialFolder.Desktop;


            if (Directory.Exists(Properties.Settings.Default.lastProjPath))
                projPathBrowserDlg.SelectedPath = Properties.Settings.Default.lastProjPath; // Props.projPath;
            else // Set Default Path
                projPathBrowserDlg.SelectedPath = Environment.SpecialFolder.MyDocuments + @"\" + Props.projName;

            // Check if Browse Files Dialog Cancel Button pressed and return if Cancel is pressed
            if (projPathBrowserDlg.ShowDialog() == DialogResult.Cancel)
            {
                this.statusbarMainMessage.Text = "Browse canceled"; return;
            }

            // Remember last path
            Properties.Settings.Default.lastProjPath = projPathBrowserDlg.SelectedPath; // Props.projPath;

            Props.projPath = projPathBrowserDlg.SelectedPath;
            LoadPath();
        }

        #endregion Project Path


        #region Project Names ===============================================================================


        /// <summary>
        /// If game name is changed, change other related fields to reflect change
        /// </summary>
        private void projNameTxtBox_TextChanged(object sender, EventArgs e)
        {
            Props.projName = projNameTxtBox.Text;
        }


        /// <summary>
        /// Repopulates SHORTNAME string variable with changed projShortNameTxtBox entry
        /// Removes any illegal characters from entered path
        ///     Note, this could also be done with Path.GetInvalidFileNameChars()
        /// </summary>
        private void projShortNameTxtBox_TextChanged(object sender, EventArgs e)
        {
            projShortNameTxtBox.Text = projShortNameTxtBox.Text.Replace("*", "");
            projShortNameTxtBox.Text = projShortNameTxtBox.Text.Replace("?", "");
            projShortNameTxtBox.Text = projShortNameTxtBox.Text.Replace("/", "");
            projShortNameTxtBox.Text = projShortNameTxtBox.Text.Replace(@"\", "");
            projShortNameTxtBox.Text = projShortNameTxtBox.Text.Replace("|", "");
            projShortNameTxtBox.Text = projShortNameTxtBox.Text.Replace("\"", "'");
            projShortNameTxtBox.Text = projShortNameTxtBox.Text.Replace("<", "");
            projShortNameTxtBox.Text = projShortNameTxtBox.Text.Replace(">", "");

            Props.projShortName = projShortNameTxtBox.Text;

            if (String.IsNullOrEmpty(projMainEXETxtBox.Text) && String.IsNullOrEmpty(Props.projMainEXE))
            {
                Props.projMainEXE = projShortNameTxtBox.Text;

                projMainEXETxtBox.Text = Props.projMainEXE;
            }

            projMainEXETxtBox.Text = Props.projMainEXE;
            projMainEXETxtBox_Leave(null, null);
        }

        #endregion Project Names


        #region Script Options ==============================================================================

        /// <summary>
        /// 
        /// </summary>
        private void loggingChkBox_CheckedChanged(object sender, EventArgs e)
        {
            //if (loggingChkBox.Checked == true)
            //    MessageBox.Show("To create a logging installer you must use a special logging build of NSIS", "Information", 
            //        MessageBoxButtons.OK, MessageBoxIcon.Information);

            // Logging Installer
            Props.loggingInst = loggingChkBox.Checked;
        }


        /// <summary>
        /// 
        /// </summary>
        private void unicodeChkBox_CheckedChanged(object sender, EventArgs e)
        {
            //if (unicodeChkBox.Checked == true)
            //    MessageBox.Show("To create a Unicode installer you must have at least NSIS 3.0b2 installed", "Information", 
            //        MessageBoxButtons.OK, MessageBoxIcon.Information);
            
            // Compile Unicode
            Props.unicodeInst = unicodeChkBox.Checked;
        }

        /// <summary>
        /// Include source in installer if checked
        /// </summary>
        private void includeSRCChkBox_CheckedChanged(object sender, EventArgs e)
        {
            Props.includeSRC = includeSRCChkBox.Checked;
        }


        /// <summary>
        /// Sets aspect ratio for game
        /// </summary>
        private void aspectChkBox_CheckedChanged(object sender, EventArgs e)
        {
            Props.projAspect = aspectChkBox.Checked;
        }

        #endregion Script Options


        #region Additional Fields ===========================================================================

        /// <summary>
        /// Sets PRODUCTPUBLISHER string variable to projPubTxtBox entry and reflects change in the About field
        /// </summary>
        private void projPubTxtBox_TextChanged(object sender, EventArgs e)
        {
            Props.projPub = projPubTxtBox.Text;
        }


        /// <summary>
        /// Sets name of Windows launcher EXE
        /// </summary>
        private void projMainEXETxtBox_TextChanged(object sender, EventArgs e)
        {
            Props.projMainEXE = projMainEXETxtBox.Text;
        }


        /// <summary>
        /// Sets Name of Main EXE
        /// </summary>
        private void projMainEXETxtBox_Leave(object sender, EventArgs e)
        {
            string tmp = projMainEXETxtBox.Text.ToLower();
            if (!tmp.EndsWith(".exe") && !String.IsNullOrEmpty(projMainEXETxtBox.Text))
                projMainEXETxtBox.Text = projMainEXETxtBox.Text + ".exe";

            tmp = Props.projMainEXE.ToLower();
            if (!tmp.EndsWith(".exe") && !String.IsNullOrEmpty(Props.projMainEXE))
                Props.projMainEXE = Props.projMainEXE + ".exe";
        }


        /// <summary>
        /// Enters any script notes
        /// </summary>
        private void projNotesTxtBox_TextChanged(object sender, EventArgs e)
        {
            Props.projNotes = projNotesTxtBox.Text;
        }


        /// <summary>
        /// Restore undo, redo, cut, copy, paste and select all keyboard shortcuts
        /// </summary>
        private void projNotesTxtBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Control | Keys.Z))
                projNotesTxtBox.Undo();
            //else if (e.KeyData == (Keys.Control | Keys.Y))
            //    projNotesTxtBox.Redo();
            else if (e.KeyData == (Keys.Control | Keys.X))
                projNotesTxtBox.Cut();
            else if (e.KeyData == (Keys.Control | Keys.C))
                projNotesTxtBox.Copy();
            else if (e.KeyData == (Keys.Control | Keys.V))
                projNotesTxtBox.Paste();
            else if (e.KeyData == (Keys.Control | Keys.A))
                projNotesTxtBox.SelectAll();
        }


        /// <summary>
        /// Sets Props.projURL
        /// </summary>
        private void projURLTxtBox_TextChanged(object sender, EventArgs e)
        {
            //Props.projURL = projURLTxtBox.Text;
        }


        /// <summary>
        /// Checks URL format on press of Enter
        /// </summary>
        private void projURLTxtBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Enter))
                CheckURL();
        }


        /// <summary>
        /// Checks URL format on loss of focus
        /// </summary>
        private void projURLTxtBox_Leave(object sender, EventArgs e)
        {
            CheckURL();
        }


        /// <summary>
        /// Checks for possible invalid address
        /// </summary>
        private void CheckURL()
        {
            // Allow null address
            if (String.IsNullOrEmpty(projURLTxtBox.Text)) return;

            // Check protocol
            if (projURLTxtBox.Text.StartsWith("http://") || projURLTxtBox.Text.StartsWith("https://"))
            {
                Props.projURL = projURLTxtBox.Text;
            }
            else
            {
                // Remove invalid protocol
                string[] address = projURLTxtBox.Text.Split(new string[] { "//" }, StringSplitOptions.None);
                var last = address.Last();

                // Add protocol
                Props.projURL = "http://" + last;
                projURLTxtBox.Text = Props.projURL;
            }

            // Warn if top level domain might be missing
            if (!projURLTxtBox.Text.Contains("."))
                MessageBox.Show("Please check if website address is valid. Is the top level domain missing?", "Check Address",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }

        #endregion Additional Fields

        #endregion Properties Tab


        #region Script Editor Tab ===========================================================================

        /// <summary>
        /// Place holder for any events based on loading data into scriptbox
        ///     Needed?
        /// </summary>
        private void scriptbox_Load(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Tracks any changes to script to enable save menu/button and on closing warnings
        /// </summary>
        private void scriptbox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (lockBtn.Checked) return;

            altered = true;

            if (opened == true)
            {
                
                if (Props.projPath != "")
                {
                    if (System.IO.File.Exists(Props.projPath))
                    {
                        string projFileName = Path.GetFileName(Props.projPath);
                        this.Text = projFileName + " - " + appTitle;
                    }
                }

                scriptbox.ClearUndo();
                undoBtn.Enabled = false;
                opened = false;
                return;
            }

            if (opened == false)
            {
                if (Props.projPath != null)
                {
                    if (System.IO.File.Exists(Props.projPath))
                    {
                        string FileName = Path.GetFileName(Props.projPath);
                        this.Text = "* " + FileName + " - " + appTitle;
                    }
                }

                if (Props.projPath == null)
                {
                    this.Text = appTitle;
                }

                //if (scriptbox.CanUndo)
                undoBtn.Enabled = true;
            }

            if (!this.Text.StartsWith("* "))
            {
                this.Text = "* " + this.Text;
                saveBtn.Enabled = true;
            }
        }


        /// <summary>
        /// Gets items being drug into scriptbox
        /// </summary>
        private void scriptbox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop, false) == true)
                e.Effect = DragDropEffects.All;
        }


        /// <summary>
        /// Open dropped script
        /// </summary>
        private void scriptbox_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                foreach (string file in files)
                {
                    //this.statusbarMainMessage.Text = "Opening script...";
                    string chkExt = Path.GetExtension(file).ToLower();
                    if (chkExt == ".nsi" || chkExt == ".nsh")
                    {
                        scriptFile = file;
                        OpenScript();
                    }
                }
            }
        }


        #region Auto Complete ===============================================================================

        /// <summary>
        /// BuildAutocompleteMenu Method
        /// </summary>
        private void BuildAutocompleteMenu()
        {
            List<AutocompleteItem> items = new List<AutocompleteItem>();

            foreach (var item in snippets)
                items.Add(new SnippetAutocompleteItem(item) { ImageIndex = 1 });
            foreach (var item in declarationSnippets)
                items.Add(new DeclarationSnippet(item) { ImageIndex = 0 });
            foreach (var item in methods)
                items.Add(new MethodAutocompleteItem(item) { ImageIndex = 2 });
            foreach (var item in keywords)
                items.Add(new AutocompleteItem(item));

            items.Add(new InsertSpaceSnippet());
            items.Add(new InsertSpaceSnippet(@"^(\w+)([=<>!:]+)(\w+)$"));
            items.Add(new InsertEnterSnippet());

            //set as autocomplete source
            popupMenu.Items.SetAutocompleteItems(items);
        }


        /// <summary>
        /// DeclarationSnippet Class
        /// </summary>
        class DeclarationSnippet : SnippetAutocompleteItem
        {
            // This item appears when any part of snippet text is typed
            public DeclarationSnippet(string snippet)
                : base(snippet)
            {
            }

            public override CompareResult Compare(string fragmentText)
            {
                var pattern = Regex.Escape(fragmentText);
                if (Regex.IsMatch(Text, "\\b" + pattern, RegexOptions.IgnoreCase))
                    return CompareResult.Visible;
                return CompareResult.Hidden;
            }
        }


        /// <summary>
        /// InsertSpaceSnippet Class
        /// </summary>
        class InsertSpaceSnippet : AutocompleteItem
        {
            // Divides numbers and words: "123AND456" -> "123 AND 456" or "i=2" -> "i = 2"
            string pattern;

            public InsertSpaceSnippet(string pattern)
                : base("")
            {
                this.pattern = pattern;
            }

            public InsertSpaceSnippet()
                : this(@"^(\d+)([a-zA-Z_]+)(\d*)$")
            {
            }

            public override CompareResult Compare(string fragmentText)
            {
                if (Regex.IsMatch(fragmentText, pattern))
                {
                    Text = insertSpaces(fragmentText);
                    if (Text != fragmentText)
                        return CompareResult.Visible;
                }
                return CompareResult.Hidden;
            }

            public string insertSpaces(string fragment)
            {
                // Automatically adds spaces in predtermined palces
                var m = Regex.Match(fragment, pattern);
                if (m == null)
                    return fragment;
                if (m.Groups[1].Value == "" && m.Groups[3].Value == "")
                    return fragment;
                return (m.Groups[1].Value + " " + m.Groups[2].Value + " " + m.Groups[3].Value).Trim();
            }

            public override string ToolTipTitle
            {
                // Function to manage tooltip contents
                get
                {
                    return Text;
                }
            }
        }


        /// <summary>
        /// Inserts line break after '}'
        /// </summary>
        class InsertEnterSnippet : AutocompleteItem
        {
            // Automatically adds line break after closing bracket
            Place enterPlace = Place.Empty;

            public InsertEnterSnippet()
                : base("[Line break]")
            {
            }

            public override CompareResult Compare(string fragmentText)
            {
                var r = Parent.Fragment.Clone();
                while (r.Start.iChar > 0)
                {
                    if (r.CharBeforeStart == '}')
                    {
                        enterPlace = r.Start;
                        return CompareResult.Visible;
                    }

                    r.GoLeftThroughFolded();
                }

                return CompareResult.Hidden;
            }

            public override string GetTextForReplace()
            {
                //extend range
                Range r = Parent.Fragment;
                Place end = r.End;
                r.Start = enterPlace;
                r.End = r.End;
                //insert line break
                return Environment.NewLine + r.Text;
            }

            public override void OnSelected(AutocompleteMenu popupMenu, SelectedEventArgs e)
            {
                // Maintains indent position of previous line
                base.OnSelected(popupMenu, e);
                if (Parent.Fragment.tb.AutoIndent)
                    Parent.Fragment.tb.DoAutoIndent();
            }

            public override string ToolTipTitle
            {
                // Inserts tooltip for the automatic line break after closing bracket
                get
                {
                    return "Insert line break after '}'";
                }
            }
        }

        #endregion Auto Complete


        #region Context Menu ================================================================================

        /// <summary>
        /// Catches the context menu opening event
        ///     Needed?
        /// </summary>
        private void ScriptContextMenu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {

        }


        /// <summary>
        /// Undoes last text action from context menu
        /// </summary>
        private void undoCntxtMenu_Click(object sender, EventArgs e)
        {
            Undo();
        }


        /// <summary>
        /// Redoes the last undo action from context menu
        /// </summary>
        private void redoCntxtMenu_Click(object sender, EventArgs e)
        {
            Redo();
        }


        /// <summary>
        /// Cuts selected text in current script from context menu
        /// </summary>
        private void cutCntxtMenu_Click(object sender, EventArgs e)
        {
            Cut();
        }


        /// <summary>
        /// Copies selected text in current script to clipboard from context menu
        /// </summary>
        private void copyCntxtMenu_Click(object sender, EventArgs e)
        {
            Copy();
        }


        /// <summary>
        /// Pastes clipboard's contents into current script at current caret position from context menu
        /// </summary>
        private void pasteCntxtMenu_Click(object sender, EventArgs e)
        {
            Paste();
        }


        /// <summary>
        /// Deletes selected text in current script from context menu
        /// </summary>
        private void delCntxtMenu_Click(object sender, EventArgs e)
        {
            Delete();
        }


        /// <summary>
        /// Selects all text in current script from context menu
        /// </summary>
        private void selectAllCntxtMenu_Click(object sender, EventArgs e)
        {
            SelectAll();
        }


        /// <summary>
        /// Clears all text from current script from context menu
        /// </summary>
        private void clearCntxtMenu_Click(object sender, EventArgs e)
        {
            ClearScriptBox();
        }

        #endregion Context Menu


        #region outputBox Events ============================================================================

        /// <summary>
        /// Gets line number from outputBox click
        /// </summary>
        private void outputBox_MouseClick(object sender, MouseEventArgs e)
        {
            int intLine = outputBox.GetLineFromCharIndex(outputBox.SelectionStart);

            //outputBox.Select(intLine, Sel);

            //MessageBox.Show(intLine.ToString());

            //outputBox.GetLineText(intLine);
            ////the number of the selected line
            //int i = (e.Location.Y) / 16;

            //// get the value of the number line.
            //MessageBox.Show(this.outputBox.Lines[i].ToString());
        }


        /// <summary>
        /// Selects all text in outputBox
        /// </summary>
        private void selectAllOutputBoxCntxtMenu_Click(object sender, EventArgs e)
        {
            outputBox.SelectAll();
        }


        /// <summary>
        /// Copies selected text in outputBox to clipboard
        /// </summary>
        private void copyOutputBoxCntxtMenu_Click(object sender, EventArgs e)
        {
            outputBox.Copy();
        }

        #endregion outputBox Events

        #endregion Script Editor Tab

        #endregion TabPages


        #region Extra Functions =============================================================================

        #region Form Methods ================================================================================

        /// <summary>
        /// Apply all settings
        /// </summary>
        public void Apply()
        {
            #region Null Warnings ================================================

            if (String.IsNullOrEmpty(projNameTxtBox.Text))
            {
                MessageBox.Show("No Project name has been specified. Please enter name.", "Unnamed Project", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            if (String.IsNullOrEmpty(projPathTxtBox.Text))
            {
                MessageBox.Show("No Project folder has been specified. Please enter path.", "No Project Folder", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            else
            {
                Props.projPath = projPathTxtBox.Text;
                object path = projPathTxtBox.Text;
                if (!Directory.Exists(Props.projPath))
                    Directory.CreateDirectory(Props.projPath);
            }
            if (String.IsNullOrEmpty(projShortNameTxtBox.Text))
            {
                MessageBox.Show("No Short Name has been specified. Please enter ", "No Short Name", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            #endregion Null Warnings

            #region Repopulate Variables =========================================

            Props.projName = projNameTxtBox.Text;
            Props.projPath = projPathTxtBox.Text;
            Props.projShortName = projShortNameTxtBox.Text;
            Props.projPub = projPubTxtBox.Text;

            #endregion Repopulate Variables
        }


        /// <summary>
        /// Autofills fields from path
        /// </summary>
        public void LoadPath()
        {
            string selectedPath = Props.projPath;

            if (!File.Exists(selectedPath + "\\AGI") && !File.Exists(selectedPath + "\\SCIV.EXE") && !File.Exists(selectedPath + "\\SCIDHUV.EXE") &&
                !File.Exists(selectedPath + "\\SIERRA.EXE"))
            {
                MessageBox.Show("No AGI, SCI0, SCI1.0 or SCI1.1 game was found in " + selectedPath, "No Game Found", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            killMsg = true;

            if (Directory.Exists(Props.projPath))
                projPathTxtBox.Text = Props.projPath;

            GetGame.ProjName();

            projPathTxtBox.Text = Props.projPath;
            if (!File.Exists(scriptFile))
                scriptFile = Props.projPath + "\\Setup Script.nsi";

            GetGame.GetEngine();
            projEngCmboBox.Text = Props.projEngine;

            //Get Project Name
            projNameTxtBox.Text = Props.projName;

            projShortNameTxtBox.Text = Props.projShortName;

            GetGame.Version();

            projShortNameTxtBox.Text = Props.projShortName;

            if (String.IsNullOrEmpty(projPubTxtBox.Text))
                projPubTxtBox.Text = Properties.Settings.Default.defaultPublisher;

            if (String.IsNullOrEmpty(projURLTxtBox.Text))
                projURLTxtBox.Text = Properties.Settings.Default.defaultSite;

            GetGame.ProjAspect();

            aspectChkBox.Checked = Props.projAspect;

            Add.Items();

            RefreshPropsTab();

            if (Program.passedArgs == true)
                PublishScript();
        }


        /// <summary>
        /// Gathers game info and invokes the Publish class
        /// </summary>
        public void PublishScript()
        {
            string includePath = Props.projPath + @"\Publish_Include";
            string projReadme = Props.projPath + "\\Publish_Include\\README.TXT";

            if (!Directory.Exists(projPathTxtBox.Text))
            {
                MessageBox.Show("Please select your working project folder", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
            }
            else
            {
                Props.projPath = projPathTxtBox.Text;
                scriptFile = Props.projPath + "\\Setup Script.nsi";
            }

            #region Null  Checks

            if (String.IsNullOrEmpty(projEngCmboBox.Text))
            {
                MessageBox.Show("Please select your project's engine", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
            }

            if (String.IsNullOrEmpty(projNameTxtBox.Text))
            {
                MessageBox.Show("Please enter the name of your project", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
            }

            if (String.IsNullOrEmpty(projShortNameTxtBox.Text))
            {
                MessageBox.Show("Please select an abbreviated for your project", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
            }

            if (String.IsNullOrEmpty(projPubTxtBox.Text))
            {
                MessageBox.Show("Please select name of the developer", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
            }

            if (String.IsNullOrEmpty(projMainEXETxtBox.Text))
            {
                MessageBox.Show("Please select a name for the game launcher", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
            }

            //if (String.IsNullOrEmpty(projURLTxtBox.Text))
            //{
            //    MessageBox.Show("Please select website address for your project", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
            //}

            //if (String.IsNullOrEmpty(projNotesTxtBox.Text))
            //{
            //    MessageBox.Show("Please enter any installer notes", "Warning!", MessageBoxButtons.OK, MessageBoxIcon.Warning); return;
            //}

            #endregion Null  Checks

            string readmeCnt = null;
            if (File.Exists(projReadme))
            {
                using (StreamReader reader = new StreamReader(projReadme))
                {
                    readmeCnt = reader.ReadToEnd();
                    reader.Close();
                }
                if (readmeCnt.Contains("README Place Holder"))
                {
                    DialogResult dialogResult = MessageBox.Show("You have not completed you game's Readme, would you like to edit it now?",
                        "Empty Readme", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        EditReadme();
                        Program.passedArgs = false;
                    }
                }
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Your game's Readme is missing, would you like to add it and edit it now?",
                        "Missing Readme", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    EditReadme();
                    Program.passedArgs = false;
                }
            }

            Props.projPath = projPathTxtBox.Text;

            // Publish Out Path
            if (Properties.Settings.Default.outToProjPath == true)
                Props.projOutPath = Props.projPath;
            if (Properties.Settings.Default.outToProjPath == false)
            {
                if (String.IsNullOrEmpty(Properties.Settings.Default.pubOutPath))
                    Properties.Settings.Default.pubOutPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                Props.projOutPath = Properties.Settings.Default.pubOutPath;
            }

            Props.projEngine = projEngCmboBox.Text;
            Props.projName = projNameTxtBox.Text;
            Props.projShortName = projShortNameTxtBox.Text;
            Props.projPub = projPubTxtBox.Text;
            Props.projMainEXE = projMainEXETxtBox.Text;
            Props.projURL = projURLTxtBox.Text;
            Props.projNotes = projNotesTxtBox.Text;

            if (includeSRCChkBox.Checked == true)
                Props.includeSRC = true;
            else
                Props.includeSRC = false;

            if (aspectChkBox.Checked == true)
                Props.projAspect = true;
            else
                Props.projAspect = false;

            Publish.PublishNSI();
            tabPages.SelectedTab = scriptTabPg;
        }


        /// <summary>
        /// Opens project's license
        /// </summary>
        public void OpenLicense()
        {
            string projLicense = Props.projPath + @"\Publish_Include\LICENSE.TXT";

            if (File.Exists(projLicense))
            {
                Process.Start(projLicense);
            }
            else
            {
                if (File.Exists(Props.projPath + @"\LICENSE.TXT"))
                    try
                    {
                        File.Move(Props.projPath + @"\LICENSE.TXT", projLicense);
                    }
                    catch { }

                try
                {
                    string templateLicense = nsisPubPath + @"\Publish_Include\LICENSE.TXT";
                    File.Copy(templateLicense, projLicense);
                }
                catch { }
            }
        }


        /// <summary>
        /// Opens README for editing
        /// </summary>
        public void EditReadme()
        {
            string projReadme = Props.projPath + @"\Publish_Include\README.TXT";

            if (File.Exists(projReadme))
            {
                try
                {
                    Process.Start(projReadme);
                }
                catch { }
            }
            else
            {
                if (File.Exists(Props.projPath + @"\README.TXT"))
                    try
                    {
                        File.Move(Props.projPath + @"\README.TXT", projReadme);
                    }
                    catch { }

                if (!File.Exists(projReadme))
                    try
                    {
                        string templateReadme = nsisPubPath + @"\Publish_Include\README.TXT";
                        File.Copy(templateReadme, projReadme);
                    }
                    catch { }
            }
        }


        /// <summary>
        /// Checks Project's Path
        /// </summary>
        public void CheckProjPath()
        {
            if (!Directory.Exists(Props.projPath))
            {
                DialogResult dialogResult = MessageBox.Show("No project folder has been selected. Would you like to select it now?", "No Folder Selected", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.Yes)
                    projPathBrowseBtn_Click(null, null);
                if (dialogResult == DialogResult.No)
                    return;
            }
        }


        /// <summary>
        /// Checks Project's Include Folder
        /// </summary>
        public void CheckIncludeFolder()
        {
            string includePath = Props.projPath + @"\Publish_Include";

            if (!Directory.Exists(includePath))
            {
                if (Properties.Settings.Default.missingIncludeWarning == true)
                {
                    MissingIncludeMsgbox missingIncludeMsgbox = new NSISPublisher.MissingIncludeMsgbox();
                    missingIncludeMsgbox.ShowDialog();
                    if (cancel == true)
                        return;
                }
                else
                    try
                    {
                        Directory.CreateDirectory(includePath);
                    }
                    catch { }
            }
        }


        /// <summary>
        /// Intercepts and overrides reserved key presses
        /// </summary>
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            bool handled = false;

            if (lockBtn.Checked)
            {
                handled = true;
                //return;
            }

            // Intercept F5 key
            if (keyData == Keys.F5)
            {
                timeStamp();

                handled = true;
            }

            // Intercept Tab key
            if (keyData == Keys.Tab)
            {
                scriptbox.SelectionLength = 0;
                scriptbox.SelectionLength = 0;
                //if (!lockBtn.Checked)
                scriptbox.SelectedText = scriptbox.SelectedText + "";

                handled = true;
            }

            // Return if pressed key is not F5 or Tab
            if (keyData != Keys.F5 || keyData != Keys.Tab)
            {
                //handled = true;
                return base.ProcessCmdKey(ref msg, keyData);
            }

            handled = true;

            return handled || base.ProcessCmdKey(ref msg, keyData);
        }


        /// <summary>
        /// Get current date and time to insert at current caret position as comment
        /// </summary>
        private void timeStamp()
        {
            string dateStamp = null;

            if (tabPages.SelectedTab == scriptTabPg)
            {
                MessageBox.Show("scriptTabPg");
                editMenuTimeDate.Enabled = true;
                scriptbox.SelectionLength = 0;

                if (scriptbox.Language == Language.CSharp || scriptbox.Language == Language.JS || scriptbox.Language == Language.PHP)
                    if (!lockBtn.Checked)
                        dateStamp = @"// " + (DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt")); // "// " added to comment out time stamp
                    else
                        dateStamp = null;
                else if (scriptbox.Language == Language.HTML || scriptbox.Language == Language.XML)
                    if (!lockBtn.Checked)
                        dateStamp = @"<!-- " + (DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt")) + @" -->"; // "<!-- --> " added to comment out time stamp
                    else
                        dateStamp = null;
                else if (scriptbox.Language == Language.NSI)
                    if (!lockBtn.Checked)
                        dateStamp = @" ; " + (DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt")); // "; " added to comment out time stamp
                    else
                        dateStamp = null;
                else if (scriptbox.Language == Language.SCI)
                    if (!lockBtn.Checked)
                        dateStamp = @"// " + (DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt")); // "// " added to comment out time stamp
                    else
                        dateStamp = null;
                else if (scriptbox.Language == Language.SQL)
                    if (!lockBtn.Checked)
                        dateStamp = @"-- " + (DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt")); // "-- " added to comment out time stamp
                    else
                        dateStamp = null;
                else if (scriptbox.Language == Language.VB)
                    if (!lockBtn.Checked)
                        dateStamp = @"' " + (DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt")); // "' " added to comment out time stamp
                    else
                        dateStamp = null;

                scriptbox.SelectedText = scriptbox.SelectedText + dateStamp;
            }

            if (tabPages.SelectedTab == propsTabPg)
            {
                editMenuTimeDate.Enabled = true;
                dateStamp = DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt");
                projNotesTxtBox.SelectedText = projNotesTxtBox.SelectedText + dateStamp;
            }
        }


        /// <summary>
        /// Hides TabPages
        /// </summary>
        public void HideTabPages()
        {
            //GenTabPg.Visable
            tabPages.Visible = false;
        }


        /// <summary>
        /// Shows TabPages
        /// </summary>
        public void UnhideTabPages()
        {
            //GenTabPg.Visable
            tabPages.Visible = true;
        }


        /// <summary>
        /// Sets TabControl to Properties TabPage
        /// </summary>
        public void SetPropsTab()
        {
            tabPages.SelectedTab = propsTabPg;
        }


        /// <summary>
        /// Sets TabControl to Script TabPage
        /// </summary>
        public void SetScriptTab()
        {
            tabPages.SelectedTab = scriptTabPg;
        }


        /// <summary>
        /// Refreshes Properties TabPage Fields
        /// </summary>
        private void RefreshPropsTab()
        {
            //if (!File.Exists(scriptFile))
            //    scriptFile = Props.projPath + "\\Setup Script.nsi";
            //ReadScript.ReadNSI(scriptFilePath);

            projEngCmboBox.Text = Props.projEngine;
            projNameTxtBox.Text = Props.projName;

            // Include SRC
            if (Props.includeSRC == false)
                includeSRCChkBox.Checked = false;
            else
                includeSRCChkBox.Checked = true;

            // Set Aspect Correction
            //aspectChkBox.Checked = Props.projAspect; // = Properties.Settings.Default.aspectDefault;

            projNotesTxtBox.Text = Props.projNotes;
            projPubTxtBox.Text = Props.projPub;
            projEngCmboBox.Text = Props.projEngine;
            projShortNameTxtBox.Text = Props.projShortName;
            projPathTxtBox.Text = Props.projPath;

            projMainEXETxtBox.Text = Props.projMainEXE;
        }


        /// <summary>
        /// Determine language of script for syntax highlighting
        /// </summary>
        private void GetLanguage()
        {
            //if (!File.Exists(scriptFile))
            //    scriptFile = Props.projPath + "\\Setup Script.nsi";
            //string scriptName = Path.GetFileName(scriptFilePath);

            if (File.Exists(scriptFile))
            {
                string scriptName = Path.GetFileName(scriptFile);
                string scriptEXT = Path.GetExtension(scriptFile).ToLowerInvariant(); //.ToLower();

                if (scriptEXT == ".cs")
                    scriptbox.Language = Language.CSharp;

                if (scriptEXT == ".htm" || scriptEXT == ".html")
                    scriptbox.Language = Language.HTML;

                if (scriptEXT == ".js")
                    scriptbox.Language = Language.JS;

                if (scriptEXT == ".nsi")
                    scriptbox.Language = Language.NSI;

                if (scriptEXT == ".php")
                    scriptbox.Language = Language.PHP;

                if (scriptEXT == ".agil")
                    scriptbox.Language = Language.AGI;

                if (scriptEXT == ".sc" || scriptEXT == ".sh")
                    scriptbox.Language = Language.SCI;

                if (scriptEXT == ".sql")
                    scriptbox.Language = Language.SQL;

                if (scriptEXT == ".vb")
                    scriptbox.Language = Language.VB;

                if (scriptEXT == ".xml")
                    scriptbox.Language = Language.XML;

                // Load no language if TXT or unknown
                if (scriptEXT == ".txt" || scriptEXT == null)
                    scriptbox.Language = Language.None;
            }
            else
                // Load Language.NSI if script file does not exist
                scriptbox.Language = Language.NSI;
        }


        /// <summary>
        /// Spell Checker
        /// </summary>
        private void ControlExtensionAdding(object sender, i00SpellCheck.MiscControlExtension.ControlExtensionAddingEventArgs e)
        {
            var SpellCheckControlBase = e.Extension as SpellCheckControlBase;
            if (SpellCheckControlBase != null)
            {
                //i00SpellCheck.SpellCheckSettings SpellCheckSettings = null;
                if (SpellCheckSettings == null)
                {
                    SpellCheckSettings = new i00SpellCheck.SpellCheckSettings();
                    SpellCheckSettings.AllowAdditions = true; //Specifies if you want to allow the user to add words to the dictionary
                    SpellCheckSettings.AllowIgnore = true; //Specifies if you want to allow the user ignore words
                    SpellCheckSettings.AllowRemovals = true; //Specifies if you want to allow users to delete words from the dictionary
                    SpellCheckSettings.AllowInMenuDefs = true; //Specifies if the in menu definitions should be shown for correctly spelled words
                    SpellCheckSettings.AllowChangeTo = true; //Specifies if "Change to..." (to change to a synonym) should be shown in the menu for correctly spelled words
                }

                SpellCheckControlBase.Settings = SpellCheckSettings;
            }
        }


        /// <summary>
        /// Set Language Method
        /// </summary>
        public void ReceiveValue(string value)
        {
            if (scriptbox.Language == Language.NSI)
                selectedLanguage = "NSI";
            //this.Activate();
            //this.Refresh();
        }


        #endregion Form Methods


        #region File Methods ================================================================================

        /// <summary>
        /// Gets original casing of directory
        /// </summary>
        /// <param name="dirInfo">DirectoryInfo of target directory</param>
        /// <returns>Original casing</returns>
        static string GetProperDirectoryCapitalization(DirectoryInfo dirInfo)
        {
            DirectoryInfo parentDirInfo = dirInfo.Parent;
            if (null == parentDirInfo)
                return dirInfo.Name;
            return Path.Combine(GetProperDirectoryCapitalization(parentDirInfo), parentDirInfo.GetDirectories(dirInfo.Name)[0].Name);
        }


        /// <summary>
        /// Gets original casing of file
        /// </summary>
        /// <param name="filename">Name if target file</param>
        /// <returns>Original casing</returns>
        static string GetProperFilePathCapitalization(string filename)
        {
            FileInfo fileInfo = new FileInfo(filename);
            DirectoryInfo dirInfo = fileInfo.Directory;
            return Path.Combine(GetProperDirectoryCapitalization(dirInfo), dirInfo.GetFiles(fileInfo.Name)[0].Name);
        }


        /// <summary>
        /// Opens the NewScript dialog
        /// </summary>
        public void New()
        {
            // Tell New Script Editor TextBox Contents is new
            //Props.projPath = "%NEW%";

            ClearAll();
            SetPropsTab();

            scriptbox.Language = Language.NSI;
            //this.Text = "(new unsaved script) - Script Editor";
            openedContents = scriptbox.Text;

            // Set Aspect Correction to chosen default
            try
                {
                    string value = Registry.GetValue(@"HKEY_CURRENT_USER\Software\mtnPhilms\SCICompanion\SCICompanion", "OriginalAspectRatio", 0).ToString();

                    if (value == "1")
                        Props.projAspect = true;

                    if (value == "0")
                        Props.projAspect = false;
                }
            catch (Exception ex)
            {
                string err = ("Error: " + ex.Message);
                MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];
                mainFrm.statusbarMainMessage.Text = (err);
                MessageBox.Show(err + Environment.NewLine + Environment.NewLine + "NSIS not found. You must download and install NSIS to use the Publish function." + Environment.NewLine + Environment.NewLine + "See http://nsis.sourceforge.net/ for details",
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
            }
            aspectChkBox.Checked = Properties.Settings.Default.aspectDefault = Props.projAspect;

            altered = false;
        }


        /// <summary>
        /// Opens a file browser to open an existing file in Script Editor
        /// </summary>
        public void Open()
        {
            OpenFileDialog openFileDlg = new OpenFileDialog();
            openFileDlg.InitialDirectory = Props.projPath; // Get files from Add Files Button dialog
            openFileDlg.AddExtension = true;
            openFileDlg.CheckFileExists = true;
            openFileDlg.CheckPathExists = true;
            openFileDlg.DefaultExt = "nsi";
            openFileDlg.DereferenceLinks = true;
            openFileDlg.Filter = "NSI Script files (*.nsi)|*.nsi| (*.nsi)|*.nsh|" + "All files|*.*"; // "NSI Script files (*.nsi)|*.nsi|(*.sc)|*.sh|All files (*.*)|*.*";
            openFileDlg.FileName = "Setup Script.nsi";
            openFileDlg.Multiselect = false;
            openFileDlg.RestoreDirectory = true;
            //openFileDlg.ShowHelp = true;
            openFileDlg.ShowReadOnly = false;
            openFileDlg.Title = "Select a file";
            openFileDlg.ValidateNames = true;

            // Check if Browse Files Dialog Cancel Button pressed and return if Cancel is pressed
            if (openFileDlg.ShowDialog() == DialogResult.Cancel)
            {
                this.statusbarMainMessage.Text = "Browse canceled"; return;
            }

            scriptFile = openFileDlg.FileName;
            OpenScript();
        }


        /// <summary>
        /// Opens script in Script Editor
        /// </summary>
        public void OpenScript()
        {
            altered = false;
            string scriptName = Path.GetFileName(scriptFile);
            string scriptExt = Path.GetExtension(scriptFile).ToLower();

            GetLanguage();

            if (scriptExt == ".nsi")
            {
                ReadScript.ReadNSI(scriptFile);

                if (String.IsNullOrEmpty(Props.projEngine))
                {
                    GetGame.GetEngine();
                    projEngCmboBox.Text = Props.projEngine;
                }
            }

            killMsg = true;

            if (Props.includeSRC == true)
                includeSRCChkBox.Checked = true;
            if (Props.includeSRC == false)
                includeSRCChkBox.Checked = false;

            RefreshPropsTab();

            string srptContent = null;
            if (File.Exists(scriptFile))
            {
                using (StreamReader reader = new StreamReader(scriptFile))
                {
                    srptContent = reader.ReadToEnd();
                    reader.Close();
                }
            }

            // Place caret at beginning of script
            if (scriptbox.Text.Length > 0)
            {
                scriptbox.Text = srptContent;
                scriptbox.SelectionStart = scriptbox.Text.Length;
                scriptbox.SelectionLength = 0;
            }

            scriptbox.Text = srptContent;
            scriptbox.SelectionStart = 0;
            scriptbox.SelectionLength = 0;

            // Remove altered indicator asterisk from titlebar when opening scripts
            this.Text = scriptFile + " - " + appTitle;
            //this.Text = this.Text.Replace("* ", "");
            altered = false;
            openedContents = scriptbox.Text;
            tabPages.SelectedTab = scriptTabPg;
        }


        /// <summary>
        /// Saves current script
        /// </summary>
        public void Save()
        {
            string title = this.Text;
            if (!File.Exists(scriptFile))
                scriptFile = "\"" + Props.projPath + "\\Setup Script.nsi\"";

            string scriptEXT = null;
            string scriptName = null;

            if (File.Exists(scriptFile))
            {
                scriptName = Path.GetFileName(scriptFile);
                scriptEXT = Path.GetExtension(scriptFile).ToLowerInvariant();
            }

            srptContent = scriptbox.Text;

            // Check if script is Unsaved New script
            if (title.Contains("(new unsaved script) - "))
            {
                scriptName = Path.GetFileName(scriptFile);

                if (System.IO.File.Exists(scriptFile))
                {
                    DialogResult dialogResult = MessageBox.Show("The Project folder already contains a file named \"" + scriptName + @""" already exists, do you wish to overwrite?",
                        "Overwrite Warning!", MessageBoxButtons.YesNo);
                    if (dialogResult == DialogResult.No) return;
                }
            }

            try
            {
                if (Properties.Settings.Default.unicodeInst == true)
                {
                    //System.IO.File.WriteAllBytes(scriptFile, Encoding.UTF8.GetBytes(srptContent));
                    StreamWriter writer = new StreamWriter(scriptFile, false, Encoding.GetEncoding("UTF-8")); //Unicode"))
                    writer.Write(srptContent);
                    writer.Dispose();
                    writer.Close();
                }
                else if (Properties.Settings.Default.unicodeInst == false)
                {
                    //System.IO.File.WriteAllBytes(scriptFile, Encoding.ASCII.GetBytes(srptContent));
                    StreamWriter writer = new StreamWriter(scriptFile, false, Encoding.GetEncoding("ASCII"));
                    writer.Write(srptContent);
                    writer.Dispose();
                    writer.Close();
                }

                this.Text = this.Text.Replace("* ", null);
            }
            catch (Exception ex)
            {
                this.statusbarMainMessage.Text = ("Error: " + ex.Message);
                string err = ("Error: " + ex.Message);
                MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
            }

            RefreshPropsTab();

            fileMenuSave.Enabled = false;
            saveBtn.Enabled = false;

            title = scriptFile + " - " + appTitle;
            this.Text = this.Text.Replace("* ", null);
            openedContents = scriptbox.Text;
            altered = false;
        }


        /// <summary>
        /// Saves script to a new file
        /// </summary>
        public void SaveAs()
        {
            string title = this.Text;

            SaveFileDialog saveFileDlg = new SaveFileDialog();
            saveFileDlg.InitialDirectory = Props.projPath;
            saveFileDlg.Title = "Save Resource Files";
            saveFileDlg.DefaultExt = "nsi";
            saveFileDlg.Filter = "NSIS Script files (*.nsi)|*.nsi|All files (*.*)|*.*";
            saveFileDlg.FilterIndex = 1;
            saveFileDlg.RestoreDirectory = true;
            saveFileDlg.FileName = Props.projPath + @"\Setup Script.nsi";

            if (saveFileDlg.ShowDialog() == DialogResult.Cancel) return;

            string scriptName = null;
            try
            {
                if (Properties.Settings.Default.unicodeInst == true)
                {
                    //System.IO.File.WriteAllBytes(saveFileDlg.FileName, Encoding.UTF8.GetBytes(scriptbox.Text));
                    //scriptFile = saveFileDlg.FileName;
                    //scriptName = Path.GetFileName(scriptFile);

                    //System.IO.File.WriteAllBytes(scriptFile, Encoding.UTF8.GetBytes(srptContent));
                    StreamWriter writer = new StreamWriter(scriptFile, false, Encoding.GetEncoding("UTF-8")); //Unicode"))
                    writer.Write(srptContent);
                    writer.Dispose();
                    writer.Close();
                }
                else if (Properties.Settings.Default.unicodeInst == false)
                {
                    //System.IO.File.WriteAllBytes(scriptFile, Encoding.ASCII.GetBytes(srptContent));
                    StreamWriter writer = new StreamWriter(scriptFile, false, Encoding.GetEncoding("ASCII"));
                    writer.Write(srptContent);
                    writer.Dispose();
                    writer.Close();
                }

            }
            catch (Exception ex)
            {
                this.statusbarMainMessage.Text = ("Error: " + ex.Message);
                string err = ("Error: " + ex.Message);
                MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
            }

            RefreshPropsTab();
            GetLanguage();

            // Set ScriptEditor title
            title = scriptName + " - Script Editor";
            this.Text = title;

            fileMenuSave.Enabled = false;
            saveBtn.Enabled = false;

            openedContents = scriptbox.Text;
            altered = false;
        }

        #endregion File Methods


        #region Edit Methods ================================================================================

        /// <summary>
        /// Undoes last edit in current control
        /// </summary>
        public void Undo()
        {
            Control control = this.propsSplitCntnr.ActiveControl;
            if (control is TextBox)
            {
                ((TextBox)control).Undo();

                if (!String.IsNullOrEmpty(((TextBox)control).Text))
                {
                    fileMenuPageSetup.Enabled = true;
                    fileMenuPrintPreview.Enabled = true;
                    fileMenuPrint.Enabled = true;
                }
                else
                {
                    fileMenuPageSetup.Enabled = false;
                    fileMenuPrintPreview.Enabled = false;
                    fileMenuPrint.Enabled = false;
                }
            }

            if (tabPages.SelectedTab == scriptTabPg)
            {
                if (scriptbox.UndoEnabled)
                    scriptbox.Undo();
            }
        }


        /// <summary>
        /// Undoes last Undo in current control
        /// </summary>
        public void Redo()
        {
            Control control = this.propsSplitCntnr.ActiveControl;
            if (Clipboard.ContainsText())
            {
                if (control is TextBox)
                {
                    ((TextBox)control).Focus();
                    try
                    {
                        SendKeys.Send("^+z");
                    }
                    catch (Exception ex)
                    {
                        string error = ("Error: " + ex.Message);
                        this.statusbarMainMessage.Text = ("Error: " + ex.Message);
                        ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
                        MessageBox.Show(error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }

            if (tabPages.SelectedTab == scriptTabPg)
            {
                if (scriptbox.RedoEnabled)
                    scriptbox.Redo();
            }
        }


        /// <summary>
        /// Cuts selected in current control and places it on clipboard
        /// </summary>
        public void Cut()
        {
            Control control = this.propsSplitCntnr.ActiveControl;
            if (Clipboard.ContainsText())
            {
                if (control is TextBox)
                {
                    ((TextBox)control).Focus();
                    try
                    {
                        ((TextBox)control).Cut();
                    }
                    catch (Exception ex)
                    {
                        string error = ("Error: " + ex.Message);
                        this.statusbarMainMessage.Text = ("Error: " + ex.Message);
                        ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
                        MessageBox.Show(error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            
            if (Clipboard.ContainsText() && tabPages.SelectedTab == scriptTabPg && scriptbox.SelectionLength > 0)
            {
                scriptbox.Cut();
            }
        }


        /// <summary>
        /// Copies selected in current control
        /// </summary>
        public void Copy()
        {
            Control control = this.propsSplitCntnr.ActiveControl;
            if (Clipboard.ContainsText())
            {
                if (control is TextBox)
                {
                    ((TextBox)control).Focus();
                    try
                    {
                        ((TextBox)control).Copy();
                    }
                    catch (Exception ex)
                    {
                        string error = ("Error: " + ex.Message);
                        this.statusbarMainMessage.Text = ("Error: " + ex.Message);
                        ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
                        MessageBox.Show(error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }

            if (tabPages.SelectedTab == scriptTabPg)
            {
                if (scriptbox.SelectionLength > 0)
                {
                    scriptbox.Copy();
                }
            }
        }


        /// <summary>
        /// Pastes clipboard contents in current control
        /// </summary>
        public void Paste()
        {
            Control control = this.propsSplitCntnr.ActiveControl;
            if (Clipboard.ContainsText())
            {
                if (control is TextBox)
                {
                    ((TextBox)control).Focus();
                    try
                    {
                        ((TextBox)control).Paste();
                    }
                    catch (Exception ex)
                    {
                        string error = ("Error: " + ex.Message);
                        this.statusbarMainMessage.Text = ("Error: " + ex.Message);
                        ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + ex.Message);
                        MessageBox.Show(error, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }

            if (Clipboard.ContainsText() && tabPages.SelectedTab == scriptTabPg)
            {
                scriptbox.Paste();
            }
        }


        /// <summary>
        /// Deletes selected in current control
        /// </summary>
        public void Delete()
        {
            Control control = this.propsSplitCntnr.ActiveControl;
            if (((TextBox)control).SelectionLength > 0)
            {
                if (control is TextBox)
                {
                    ((TextBox)control).Focus();
                    int location = ((TextBox)control).SelectionStart - 1;
                    if (location < 1)
                        location = 0;
                    ((TextBox)control).Text = ((TextBox)control).Text.Remove(location, ((TextBox)control).SelectedText.Length);
                    ((TextBox)control).SelectionStart = location + 1;
                }
            }

            if (tabPages.SelectedTab == scriptTabPg && scriptbox.SelectionLength > 0)
            {
                string selected = scriptbox.SelectedText;
                scriptbox.Text = scriptbox.Text.Replace(selected, "");
            }
        }


        /// <summary>
        /// Selects all in current control
        /// </summary>
        public void SelectAll()
        {
            Control control = this.propsSplitCntnr.ActiveControl;
            if (control is TextBox)
            {
                ((TextBox)control).SelectAll();
            }

            if (tabPages.SelectedTab == scriptTabPg)
            {
                scriptbox.SelectAll();
            }
        }


        /// <summary>
        /// Clears all in current control
        /// </summary>
        public void ClearField()
        {
            if (tabPages.SelectedTab == propsTabPg)
            {
                Control control = this.propsSplitCntnr.ActiveControl;
                if (control is TextBox)
                {
                    ((TextBox)control).Clear();
                }
            }
        }


        /// <summary>
        /// Clears all in current script
        /// </summary>
        public void ClearScriptBox()
        {
            scriptbox.Text = ""; // Note: scriptbox.Clear(); also clears the Undo/Redo stack
        }


        /// <summary>
        /// Clears all fields
        /// </summary>
        public void ClearAll()
        {
            projEngCmboBox.Text = "Game Template *";
            projNameTxtBox.Text = null;
            projPathTxtBox.Text = null;
            projShortNameTxtBox.Text = null;
            projMainEXETxtBox.Text = null;
            projPubTxtBox.Text = null;
            projURLTxtBox.Text = null;
            projNotesTxtBox.Text = null;
            outputBox.Text = null;
            includeSRCChkBox.Checked = Props.includeSRC = Properties.Settings.Default.includeSRCDefault;
            aspectChkBox.Checked = Properties.Settings.Default.aspectDefault;

            Props.projNotes = null;
            Props.projName = null;
            Props.projVer = null;
            Props.projPub = null;
            Props.projEngine = null;
            Props.projFileName = null;
            Props.projShortName = null;
            Props.projDOSName = null;
            Props.projPath = null;
            Props.projSaveName = null;
            Props.projMainEXE = null;
            Props.projInterpEXE = null;
            Props.projURL = null;
            Props.projAspect = Properties.Settings.Default.aspectDefault;

            // Hide StdOut outputBox
            splitContainer.SplitterDistance = splitContainer.Height;
            scriptbox.Clear();

            opened = true;
        }

        #endregion Edit Methods


        #region Find and Replace Methods ====================================================================

        /// <summary>
        /// Opens the FastColoredTextBox Find Dialog
        /// </summary>
        public void Find()
        {
            scriptbox.ShowFindDialog();
        }


        /// <summary>
        /// Finds next occurrence of search term
        /// </summary>
        public void FindNext()
        {
            SendKeys.Send("F3");
            //Find Find = new Find();
            //FindForm FindForm = new FindForm;
            //scriptbox.btFindNext_Click(FindForm, new EventArgs());
        }


        /// <summary>
        /// Opens the Replace dialog
        /// </summary>
        public void Replace()
        {
            scriptbox.ShowReplaceDialog();
        }


        /// <summary>
        /// Goes to specified line number in current file
        /// </summary>
        public void GoTo()
        {
            scriptbox.ShowGoToDialog();
        }


        /// <summary>
        /// Searches for user specified term across multiple files
        /// </summary>
        public void FindInFiles()
        {

        }

        #endregion Find and Replace Methods


        #region Compile Methods =============================================================================

        /// <summary>
        /// Invokes NSIS compiler into another thread and enables realtime compiler feedback
        /// </summary>
        public void Compile()
        {
            // Get NSIS Path
            makeNSIS = Properties.Settings.Default.makeNSIS;
            if (!File.Exists(makeNSIS))
                NSIS.GetNSIS();

            // Reset output pane height to prevent it from increasing in size
            splitContainer.SplitterDistance = splitContainer.Height;

            // Save script
            if (Program.passedArgs == false && Properties.Settings.Default.saveScript == true)
                Save();

            // Move outPutBox splitter
            splitContainer.SplitterDistance = splitContainer.SplitterDistance - 100;
            scriptFile = @"""" + scriptFile + @"""";

            // Compile Installer
            if (System.IO.File.Exists(makeNSIS))
            {
                try
                {
                    this.Cursor = Cursors.AppStarting;

                    processCaller = new NSISPublisher.ProcessCaller(this);
                    processCaller.FileName = makeNSIS;
                    //processCaller.WorkingDirectory = "";
                    processCaller.Arguments = scriptFile;
                    processCaller.StdErrReceived += new DataReceivedHandler(writeStreamInfo);
                    processCaller.StdOutReceived += new DataReceivedHandler(writeStreamInfo);
                    processCaller.Completed += new EventHandler(processCompletedOrCanceled);
                    processCaller.Cancelled += new EventHandler(processCompletedOrCanceled);

                    this.outputBox.Text = "Compiling Script..." + Environment.NewLine;

                    // Start process in new thread
                    processCaller.Start();
                }
                catch (Win32Exception ex)
                {
                    string err = ("Error: " + ex.Message);
                    this.statusbarMainMessage.Text = (err);
                    //MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    outputBox.Text = ("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
                    ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
                }
            }
            else
            {
                MessageBox.Show(@"NSIS not found. NSIS is required to compile the setup file. Please downlaod and install NSIS from http://nsis.sourceforge.net/");
            }
        }


        /// <summary>
        /// Debugs compiled installer
        /// </summary>
        private void Debug()
        {
            string installer = Props.projOutPath + @"\" + Props.projDOSName + @"_Setup.exe";
            this.statusbarMainMessage.Text = installer;
            try
            {
                Process.Start(installer);
            }
            catch (Exception ex)
            {
                string err = ("Error: " + ex.Message);
                this.statusbarMainMessage.Text = (err);
                //MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
            }

            run = false;
        }

        #endregion Compile Methods


        #region Process RedirectStandard Output & Error Handling Methods ====================================

        /// <summary>
        /// Writes compiler output to and scrolls outputBox 
        /// </summary>
        private void writeStreamInfo(object sender, NSISPublisher.DataReceivedEventArgs e)
        {
            this.outputBox.AppendText(e.Text + Environment.NewLine);
            this.outputBox.ScrollToCaret(); //Now scroll it automatically
        }


        /// <summary>
        /// Handles processCompleted, processCanceled & Processes compiler outputs
        /// </summary>
        private void processCompletedOrCanceled(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
            //notifyLbl.Text = null;
            splashPanel.Hide();
            ManageCompileBtns();

            if (scriptbox.Text.Length < 1)
                return;

            #region Get Errors

            // Check for error
            foreach (string ln in outputBox.Lines)
            {
                line = ln;

                // If error, highlight error and line
                if (line == null) return;

                if (line.Contains("aborting creation process"))
                {
                    tabPages.Visible = true;
                    SetScriptTab();

                    // Highlight Output's Error Message
                    string txt = outputBox.Text;
                    outputBox.Select(txt.IndexOf(ln), ln.Length);
                    outputBox.SelectionColor = Color.Red;

                    // Get Output Error Message's Location and Length
                    int SelLngth = line.Length;
                    warnLine = outputBox.GetFirstCharIndexOfCurrentLine() - 2;

                    // Parse Script's Error Line Number
                    errLineNum = line; // Error in script "E:\Games\My SCI0 Game\Setup Script.nsi" on line 128 -- aborting creation process"
                    errLineNum = errLineNum.Replace("Error in script \"" + scriptFile + "\" on line ", "");
                    errLineNum = errLineNum.Replace(" -- aborting creation process", "");

                    // Cast Error Line string to int
                    int ErrLine = 0;
                    int i = -1;
                    if (Int32.TryParse(errLineNum, out i))
                    {
                        ErrLine = i;
                    }
                    ErrLine = i;

                    // Go to Error Line and Select it
                    scriptbox.Navigate(ErrLine - 1);
                    string ErrorLine = null;
                    if (ErrLine >= 1)
                    {
                        ErrorLine = scriptbox.GetLineText(ErrLine - 1);
                        int ErrorLength = ErrorLine.Length;
                        int tmp = scriptbox.GetLineLength(ErrLine - 1);
                        scriptbox.SelectionLength = ErrorLength;
                    }

                    run = false;
                    //return;
                }
            }

            #endregion Get Errors

            bool openFolder = Properties.Settings.Default.openFolder;

            if (Program.passedArgs == true)
            {
                this.statusbarMainMessage.Text = "Ready";
                Program.passedArgs = false;
                openFolder = true;
            }
            //else
            //{
            //    openFolder = Properties.Settings.Default.openFolder;
            //}

            // Open folder containing installer
            if (openFolder == true)
                try
                {
                    Process.Start(Props.projOutPath);
                }
                catch (Exception ex)
                {
                    string err = ("Error: " + ex.Message);
                    this.statusbarMainMessage.Text = (err);
                    //MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
                }

            //string installFile = Props.projOutPath + @"\" + Props.projDOSName + "_Setup.exe";
            //if (File.Exists(installFile))
            //{
            //    buildMenuDebug.Enabled = true;
            //    debugBtn.Enabled = true;
            //}
            //else
            //{
            //    buildMenuDebug.Enabled = false;
            //    debugBtn.Enabled = false;
            //}

            if (run == true)
            {
                string installFile = Props.projOutPath + @"\" + Props.projDOSName + @"_Setup.exe";
                this.statusbarMainMessage.Text = installFile;
                try
                {
                    Process.Start(installFile);
                }
                catch (Exception ex)
                {
                    string err = ("Error: " + ex.Message);
                    this.statusbarMainMessage.Text = (err);
                    //MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
                }
            }

            Program.passedArgs = false;
            tabPages.Visible = true;
            run = false;
        }


        /// <summary>
        /// Receives output information 
        /// </summary>
        private void GotData(object sendingProcess, System.Diagnostics.DataReceivedEventArgs outLine)
        {
            if (!String.IsNullOrEmpty(outLine.Data))
            {
                SetText(outLine.Data);
            }
        }


        /// <summary>
        /// Redirects standard output to outputBox
        /// </summary>
        private void SetText(string key)
        {
            if (this.outputBox.InvokeRequired)
            {
                //SetTextCallback d = new SetTextCallback(new System.EventHandler(this.SetText));
                //this.Invoke(d, new object[] {
                //        key}); //value});
            }
            else
            {
                this.outputBox.Text = (this.outputBox.Text + (key + Environment.NewLine));
            }
        }

        #endregion Process RedirectStandard Output & Error Handling Methods


        #region Button management  ==========================================================================

        /// <summary>
        /// Manages the enabling/disabling of items after changes to current script, clipboard status, selection, etc.
        /// </summary>
        private void ManageBtns()
        {
            #region Edit Buttons

            if (lockBtn.Checked == false)
            {
                // Determine if text box is empty
                if (String.IsNullOrEmpty(scriptbox.Text))
                {
                    #region null values

                    editMenuCut.Enabled = false;
                    editMenuCopy.Enabled = false;
                    editMenuDelete.Enabled = false;
                    editMenuFind.Enabled = false;
                    editMenuFindNext.Enabled = false;
                    editMenuReplace.Enabled = false;
                    editMenuGoTo.Enabled = false;
                    editMenuSelectAll.Enabled = false;
                    editMenuClear.Enabled = false;

                    saveBtn.Enabled = false;
                    //printBtn.Enabled = false;
                    cutBtn.Enabled = false;
                    copyBtn.Enabled = false;
                    selectAllBtn.Enabled = false;
                    deleteBtn.Enabled = false;
                    clearScriptBtn.Enabled = false;

                    findBtn.Enabled = false;
                    findNextBtn.Enabled = false;
                    replaceBtn.Enabled = false;
                    goToBtn.Enabled = false;

                    cutCntxtMenu.Enabled = false;
                    copyCntxtMenu.Enabled = false;
                    delCntxtMenu.Enabled = false;
                    selectAllCntxtMenu.Enabled = false;
                    clearCntxtMenu.Enabled = false;

                    #endregion null values
                }
                else
                {
                    #region not null values

                    editMenuFind.Enabled = true;
                    editMenuFindNext.Enabled = true;
                    editMenuReplace.Enabled = true;
                    editMenuGoTo.Enabled = true;
                    editMenuSelectAll.Enabled = true;
                    editMenuClear.Enabled = true;

                    saveBtn.Enabled = true;
                    //printBtn.Enabled = true;
                    selectAllBtn.Enabled = true;
                    clearScriptBtn.Enabled = true;

                    findBtn.Enabled = true;
                    findNextBtn.Enabled = true;
                    replaceBtn.Enabled = true;
                    goToBtn.Enabled = true;

                    selectAllCntxtMenu.Enabled = true;
                    clearCntxtMenu.Enabled = true;

                    #endregion not null values

                    // Determine if any text is selected in the text box
                    if (scriptbox.SelectionLength > 0)
                    {
                        #region Selected Text

                        editMenuCut.Enabled = true;
                        editMenuCopy.Enabled = true;
                        editMenuDelete.Enabled = true;

                        cutBtn.Enabled = true;
                        copyBtn.Enabled = true;
                        deleteBtn.Enabled = true;

                        cutCntxtMenu.Enabled = true;
                        copyCntxtMenu.Enabled = true;
                        delCntxtMenu.Enabled = true;

                        #endregion Selected Text
                    }
                    else
                    {
                        #region No Selected Text

                        editMenuCut.Enabled = false;
                        editMenuCopy.Enabled = false;
                        editMenuDelete.Enabled = false;

                        cutBtn.Enabled = false;
                        copyBtn.Enabled = false;
                        deleteBtn.Enabled = false;

                        cutCntxtMenu.Enabled = false;
                        copyCntxtMenu.Enabled = false;
                        delCntxtMenu.Enabled = false;

                        #endregion No Selected Text
                    }

                    // Manages Undo/Redo enabling/disabling after Undo/Redo stack change
                    editMenuUndo.Enabled = undoBtn.Enabled = undoCntxtMenu.Enabled = scriptbox.UndoEnabled;
                    editMenuRedo.Enabled = redoBtn.Enabled = redoCntxtMenu.Enabled = scriptbox.RedoEnabled;

                    if (Clipboard.ContainsText() && tabPages.SelectedTab == scriptTabPg)
                    {
                        editMenuPaste.Enabled = true;
                        pasteBtn.Enabled = true;
                        pasteCntxtMenu.Enabled = true;
                    }
                    else
                    {
                        editMenuPaste.Enabled = false;
                        pasteBtn.Enabled = false;
                        pasteCntxtMenu.Enabled = false;
                    }
                }
            }

            #endregion Edit Buttons

            #region lockBtn

            if (lockBtn.Checked == true)
            {
                editMenuUndo.Enabled = false;
                editMenuRedo.Enabled = false;
                editMenuCut.Enabled = false;
                editMenuCopy.Enabled = false;
                editMenuPaste.Enabled = false;
                editMenuDelete.Enabled = false;
                editMenuFind.Enabled = false;
                editMenuFindNext.Enabled = false;
                editMenuReplace.Enabled = false;
                editMenuGoTo.Enabled = false;
                editMenuSelectAll.Enabled = false;

                cutBtn.Enabled = false;
                copyBtn.Enabled = false;
                pasteBtn.Enabled = false;
                undoBtn.Enabled = false;
                redoBtn.Enabled = false;
                selectAllBtn.Enabled = false;
                deleteBtn.Enabled = false;
                clearBtn.Enabled = false;

                undoCntxtMenu.Enabled = false;
                redoCntxtMenu.Enabled = false;
                cutCntxtMenu.Enabled = false;
                copyCntxtMenu.Enabled = false;
                pasteCntxtMenu.Enabled = false;
                delCntxtMenu.Enabled = false;
                selectAllCntxtMenu.Enabled = false;
                clearCntxtMenu.Enabled = false;
            }

            #endregion lockBtn

            #region Properties menu item & button

            if (File.Exists(scriptFile))
            {
                toolsMenuScriptProperties.Enabled = true;
                scriptPropsBtn.Enabled = true;
            }
            else if (!File.Exists(scriptFile))
            {
                toolsMenuScriptProperties.Enabled = false;
                scriptPropsBtn.Enabled = false;
            }

            #endregion

            ManageCompileBtns();
            ManagePubBtn();
        }


        /// <summary>
        /// Manage Publish Button
        /// </summary>
        private void ManagePubBtn()
        {
            if (projEngCmboBox.Text == "Game Template *" ||
                String.IsNullOrEmpty(projEngCmboBox.Text) ||
                !Directory.Exists(projPathTxtBox.Text) ||
                String.IsNullOrEmpty(projNameTxtBox.Text) ||
                String.IsNullOrEmpty(projShortNameTxtBox.Text) ||
                String.IsNullOrEmpty(projMainEXETxtBox.Text))
            {
                publishBtn.Enabled = false;
            }
            else
                publishBtn.Enabled = true;
        }


        /// <summary>
        /// Manage buttons for compile functions & refreshes Script Editor
        /// </summary>
        public void ManageCompileBtns()
        {
            if (!File.Exists(scriptFile))
                scriptFile = Props.projPath + @"\Setup Script.nsi";

            if (!Directory.Exists(Props.projPath))
            {
                //buildMenuCompile.Enabled = false;
                buildMenuCompileDebug.Enabled = false;
                buildMenuStopcompile.Enabled = false;

                //compileBtn.Enabled = false;
                compileDebugBtn.Enabled = false;
                stopcompileBtn.Enabled = false;
                debugBtn.Enabled = false;
                return;
            }
            else
            {
                if (!File.Exists(scriptFile) || scriptbox.Text.Length == 0)
                {
                    buildMenuCompile.Enabled = false;
                    buildMenuCompileDebug.Enabled = false;
                    buildMenuStopcompile.Enabled = false;

                    compileBtn.Enabled = false;
                    compileDebugBtn.Enabled = false;
                    stopcompileBtn.Enabled = false;
                    return;
                }

                if (File.Exists(scriptFile) && scriptFile.Length > 0)
                {
                    if (scriptbox.Language == Language.NSI)
                    {
                        buildMenuCompile.Enabled = true;
                        buildMenuCompileDebug.Enabled = true;
                        buildMenuStopcompile.Enabled = true;

                        compileBtn.Enabled = true;
                        compileDebugBtn.Enabled = true;
                        stopcompileBtn.Enabled = true;
                    }
                    else
                    {
                        buildMenuCompile.Enabled = false;
                        buildMenuCompileDebug.Enabled = false;
                        buildMenuStopcompile.Enabled = false;

                        compileBtn.Enabled = false;
                        compileDebugBtn.Enabled = false;
                        stopcompileBtn.Enabled = false;
                    }
                }
                else
                {
                    buildMenuCompile.Enabled = false;
                    buildMenuCompileDebug.Enabled = false;
                    buildMenuStopcompile.Enabled = false;

                    compileBtn.Enabled = false;
                    compileDebugBtn.Enabled = false;
                    stopcompileBtn.Enabled = false;
                }

                string installFile = Props.projOutPath + @"\" + Props.projDOSName + "_Setup.exe";
                if (File.Exists(installFile))
                {
                    buildMenuDebug.Enabled = true;
                    debugBtn.Enabled = true;
                }
                else
                {
                    // Read the file and display it line by line.
                    if (File.Exists(scriptFile))
                    {
                        StreamReader fileReader = new StreamReader(scriptFile);
                        string line;
                        while ((line = fileReader.ReadLine()) != null)
                        {
                            if (!String.IsNullOrEmpty(line))
                            {
                                if (line.Contains("OutFile "))
                                {
                                    string outFile = null;
                                    outFile = line.Replace("\"", null);
                                    outFile = outFile.Replace("OutFile ", null);
                                    outFile = outFile.Replace("${DOS_NAME}", Props.projDOSName);
                                    outFile = Props.projPath + @"\" + outFile;

                                    if (File.Exists(outFile))
                                    {
                                        buildMenuDebug.Enabled = true;
                                        debugBtn.Enabled = true;
                                    }
                                    else
                                    {
                                        buildMenuDebug.Enabled = false;
                                        debugBtn.Enabled = false;
                                    }
                                }
                            }
                        }

                        fileReader.Close();
                    }
                }
            }
        }

        #endregion Button management


        #region Timer Methods ===============================================================================

        /// <summary>
        /// Refresh Script Editor on timer tick
        /// </summary>
        private void timer_Tick(object sender, EventArgs e)
        {
            // If scriptbox contents are the same as last saved version, unmark as changed
            if (scriptbox.Text == openedContents)
            {
                altered = false;
                this.Text = this.Text.Replace("* ", "");
                undoBtn.Enabled = false;
            }

            ManageBtns();
        }

        #endregion Timer Methods

        #endregion Extra Functions
    }
}
