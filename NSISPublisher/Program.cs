﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace NSISPublisher
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static bool passedArgs = false;
        static string scriptFile;
        [STAThread]
        static void Main()
        {
            string[] args = Environment.GetCommandLineArgs();

            int i = 0;
            foreach (string arg in args)
            {
                string argument = arg.Replace("*", null);
                i = i + 1;
                if (i == 2)
                {
                    // Open script if argument is script path
                    if (argument.ToLower().Contains(".nsi") || argument.ToLower().Contains(".nsh"))
                    {
                        scriptFile = Path.GetFullPath(argument);
                        MainFrm.scriptFile = scriptFile;
                        return;
                    }
                    // If argument is game path
                    else
                    {
                        if (Directory.Exists(argument) && arg.Contains("*"))
                        {
                            Props.projPath = argument;
                            passedArgs = true;
                            NSIS.GetNSIS();
                        }
                        // Open NSIS Publisher without publishing or opening script if not opened by Publish.exe
                        else
                        {
                            Props.projPath = null;
                            passedArgs = false;
                        }
                    }
                }
            }

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainFrm());
        }
    }
}
