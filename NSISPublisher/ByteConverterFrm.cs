﻿/// To do list:
/// Round off to whole KB or two decimals for MB and GB
/// 
/// 

#region using directives ====================================================================================

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

#endregion using directives


namespace NSISPublisher
{
    /// <summary>
    /// A class to convert between Bytes, Kilobytes, Megabytes and Gigabytes
    /// Use to calculate installed folder size for installer
    /// Reference: http://stackoverflow.com/questions/14488796/does-net-provide-an-easy-way-convert-bytes-to-kb-mb-gb-etc 
    /// </summary>
    
    public partial class ByteConverterFrm : Form
    {

        //decimal bits;
        decimal bytes;
        decimal kb;
        decimal mb;
        decimal gb;


        public ByteConverterFrm()
        {
            InitializeComponent();
        }


        private void Form_Load(object sender, EventArgs e)
        {
            byteBtn_Click(null, null);
        }


        #region TextBox Events

        private void bitTextBox_TextChanged(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Disallow non-numeric entry
        /// </summary>
        private void bitTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }


        private void byteTextBox_TextChanged(object sender, EventArgs e)
        {
            //char[] inputText = byteTextBox.Text.ToCharArray();
            //foreach (char c in inputText) {
            //    if (!(Char.IsNumber(c))) {
            //        byteTextBox.Text = byteTextBox.Text.Remove(byteTextBox.Text.IndexOf(c));
            //        //lblError.Visible = true;
            //    } else {
            //        //lblError.Visible = false;
            //    }
            //}

            //byteTextBox.Select(byteTextBox.Text.Length, 0);
        }


        private void byteTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }


        private void kilobyteTextBox_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(kilobyteTextBox.Text))
                kilobyteTextBox.Text = kilobyteTextBox.Text.Replace(",", "");
        }


        /// <summary>
        /// Disallow non-numeric entry
        /// </summary>
        private void kilobyteTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }


        private void megabyteTextBox_TextChanged(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Disallow non-numeric entry
        /// </summary>
        private void megabyteTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }


        private void gigabyteTextBox_TextChanged(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Disallow non-numeric entry
        /// </summary>
        private void gigabyteTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        #endregion TextBox Events


        #region Buttons Events

        //private void bitBtn_Click(object sender, EventArgs e)
        //{
        //    if (!String.IsNullOrEmpty(bitTextBox.Text))
        //    {
        //        bitTextBox.Text = bitTextBox.Text.Replace(",", "");

        //        decimal.TryParse(bitTextBox.Text, out bits);

        //        //bits = int.TryParse(bitTextBox.Text);
        //        bits = bits * 1;
        //        bytes = Math.Round(bits / 8);
        //        kb = Math.Round(bits / 8192);
        //        mb = Math.Round(bits / 8388608, 2);
        //        gb = Math.Round(bits / 1073741824, 2);

        //        //bitTextBox.Text = bit.ToString();
        //        byteTextBox.Text = bytes.ToString();
        //        kilobyteTextBox.Text = kb.ToString();
        //        megabyteTextBox.Text = mb.ToString();
        //        gigabyteTextBox.Text = gb.ToString();
        //    }
        //}


        private void byteBtn_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(byteTextBox.Text))
            {
                byteTextBox.Text = byteTextBox.Text.Replace(",", "");

                decimal.TryParse(byteTextBox.Text, out bytes);

                //bits = bytes * 8;
                bytes = bytes * 1;
                kb = Math.Round(bytes / 1024);
                mb = Math.Round(bytes / 1048576, 2);
                gb = Math.Round(bytes / 1073741824, 2);

                //bitTextBox.Text = bits.ToString();
                //byteTextBox.Text = b.ToString();
                kilobyteTextBox.Text = kb.ToString();
                megabyteTextBox.Text = mb.ToString();
                gigabyteTextBox.Text = gb.ToString();
            }
        }


        private void kilobyteBtn_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(kilobyteTextBox.Text))
            {
                kilobyteTextBox.Text = kilobyteTextBox.Text.Replace(",", "");

                decimal.TryParse(kilobyteTextBox.Text, out kb);

                //bits = Math.Round(kb * 8192);
                bytes = Math.Round(kb * 1024);
                kb = kb * 1;
                mb = Math.Round(kb / 1024, 2);
                gb = Math.Round(kb / 1048576, 2);

                //bitTextBox.Text = bits.ToString();
                byteTextBox.Text = bytes.ToString();
                //kilobyteTextBox.Text = kb.ToString();
                megabyteTextBox.Text = mb.ToString();
                gigabyteTextBox.Text = gb.ToString();
            }
        }


        private void megabyteBtn_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(megabyteTextBox.Text))
            {
                megabyteTextBox.Text = megabyteTextBox.Text.Replace(",", "");

                decimal.TryParse(megabyteTextBox.Text, out mb);

                //bits = Math.Round(mb * 8388608);
                bytes = Math.Round(mb * 1048576);
                kb = Math.Round(mb * 1024);
                mb = mb * 1;
                gb = Math.Round(mb / 1024, 2);

                //bitTextBox.Text = bits.ToString();
                byteTextBox.Text = bytes.ToString();
                kilobyteTextBox.Text = kb.ToString();
                //megabyteTextBox.Text = mb.ToString();
                gigabyteTextBox.Text = gb.ToString();
            }
        }


        private void gigabyteBtn_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(gigabyteTextBox.Text))
            {
                gigabyteTextBox.Text = gigabyteTextBox.Text.Replace(",", "");

                decimal.TryParse(gigabyteTextBox.Text, out gb);

                //bits = Math.Round(gb * 8589934592);
                bytes = Math.Round(gb * 1073741824);
                kb = Math.Round(gb * 1048576);
                mb = Math.Round(gb * 1024, 2);
                gb = gb * 1;

                //bitTextBox.Text = bits.ToString();
                byteTextBox.Text = bytes.ToString();
                kilobyteTextBox.Text = kb.ToString();
                megabyteTextBox.Text = mb.ToString();
                //gigabyteTextBox.Text = gb.ToString();
            }
        }

        #endregion Buttons Events
    }


    //public static class Ext
    //{
    //    private const long oneKb = 1024;
    //    private const long oneMb = oneKb * 1024;
    //    private const long oneGb = oneMb * 1024;
    //    private const long oneTb = oneGb * 1024;

    //    public static string ToPrettySize(this int value, int decimalPlaces = 0)
    //    {
    //        return ((long)value).ToPrettySize(decimalPlaces);
    //    }


    //    public static string ToPrettySize(this long value, int decimalPlaces = 0)
    //    {
    //        var asTb = Math.Round((double)value / oneTb, decimalPlaces);
    //        var asGb = Math.Round((double)value / oneGb, decimalPlaces);
    //        var asMb = Math.Round((double)value / oneMb, decimalPlaces);
    //        var asKb = Math.Round((double)value / oneKb, decimalPlaces);
    //        string chosenValue = asTb > 1 ? string.Format("{0}Tb", asTb)
    //            : asGb > 1 ? string.Format("{0}Gb", asGb)
    //            : asMb > 1 ? string.Format("{0}Mb", asMb)
    //            : asKb > 1 ? string.Format("{0}Kb", asKb)
    //            : string.Format("{0}B", Math.Round((double)value, decimalPlaces));
    //        return chosenValue;
    //    }
    //}
}
