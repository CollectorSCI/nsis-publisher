﻿/// To do list:
/// 
/// 

using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace NSISPublisher
{
    /// <summary>
    /// A Missing Include folder dialog for NSIS Publisher
    /// </summary>

    public partial class MissingIncludeMsgbox : Form
    {
        public static string includePath = Props.projPath + @"\Publish_Include";

        MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];
        ErrorLogging errorLogging = new ErrorLogging();


        public MissingIncludeMsgbox()
        {
            InitializeComponent();

            if (Properties.Settings.Default.missingIncludeWarning == false)
            {
                CreateDir();
                this.Close();
            }
        }


        /// <summary>
        /// Play System Exclamation Sound on Load
        /// </summary>
        private void Form_Load(object sender, EventArgs e)
        {
            System.Media.SystemSounds.Exclamation.Play();
        }


        /// <summary>
        /// Turns warning on or off
        /// </summary>
        private void checkBox_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox.Checked == true)
                Properties.Settings.Default.missingIncludeWarning = false;
            if (checkBox.Checked == false)
                Properties.Settings.Default.missingIncludeWarning = true;
        }


        /// <summary>
        /// Closes Messagebox
        /// </summary>
        private void yesBtn_Click(object sender, EventArgs e)
        {
            CreateDir();
            this.Close();
        }


        /// <summary>
        /// Closes Messagebox
        /// </summary>
        private void noBtn_Click(object sender, EventArgs e)
        {
            mainFrm.cancel = false;
            this.Close();
        }


        /// <summary>
        /// Closes Messagebox
        /// </summary>
        private void cancelBtn_Click(object sender, EventArgs e)
        {
            mainFrm.cancel = true;
            this.Close();
        }


        /// <summary>
        /// Creates the project's Publish_Include folder
        /// </summary>
        private void CreateDir()
        {
            try
            {
                Directory.CreateDirectory(includePath);
            }
            catch { }
            mainFrm.cancel = false;
        }
    }
}
