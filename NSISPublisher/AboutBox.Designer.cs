﻿namespace NSISPublisher
{
    partial class AboutBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutBox));
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.logoPictureBox = new System.Windows.Forms.PictureBox();
            this.labelProductName = new System.Windows.Forms.Label();
            this.labelVersion = new System.Windows.Forms.Label();
            this.labelCopyright = new System.Windows.Forms.Label();
            this.labelCompanyName = new System.Windows.Forms.Label();
            this.textBoxDescription = new System.Windows.Forms.TextBox();
            this.okButton = new System.Windows.Forms.Button();
            this.tabCtrl = new System.Windows.Forms.TabControl();
            this.pubTabPg = new System.Windows.Forms.TabPage();
            this.FastColoredTextBoxTabPg = new System.Windows.Forms.TabPage();
            this.fctbTxtBox = new System.Windows.Forms.TextBox();
            this.fctbLbl = new System.Windows.Forms.Label();
            this.codeProjLinkLbl = new System.Windows.Forms.LinkLabel();
            this.fctbGitHubLinkLbl = new System.Windows.Forms.LinkLabel();
            this.tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).BeginInit();
            this.tabCtrl.SuspendLayout();
            this.pubTabPg.SuspendLayout();
            this.FastColoredTextBoxTabPg.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.ColumnCount = 2;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67F));
            this.tableLayoutPanel.Controls.Add(this.logoPictureBox, 0, 0);
            this.tableLayoutPanel.Controls.Add(this.labelProductName, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.labelVersion, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.labelCopyright, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.labelCompanyName, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.textBoxDescription, 1, 4);
            this.tableLayoutPanel.Controls.Add(this.okButton, 1, 5);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 6;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(442, 265);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // logoPictureBox
            // 
            this.logoPictureBox.BackColor = System.Drawing.SystemColors.Window;
            this.logoPictureBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.logoPictureBox.Image = global::NSISPublisher.Properties.Resources.NSIS_Pub;
            this.logoPictureBox.Location = new System.Drawing.Point(3, 3);
            this.logoPictureBox.Name = "logoPictureBox";
            this.tableLayoutPanel.SetRowSpan(this.logoPictureBox, 6);
            this.logoPictureBox.Size = new System.Drawing.Size(139, 259);
            this.logoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.logoPictureBox.TabIndex = 12;
            this.logoPictureBox.TabStop = false;
            // 
            // labelProductName
            // 
            this.labelProductName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelProductName.Location = new System.Drawing.Point(151, 0);
            this.labelProductName.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.labelProductName.MaximumSize = new System.Drawing.Size(0, 17);
            this.labelProductName.Name = "labelProductName";
            this.labelProductName.Size = new System.Drawing.Size(288, 17);
            this.labelProductName.TabIndex = 19;
            this.labelProductName.Text = "Product Name";
            this.labelProductName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelVersion
            // 
            this.labelVersion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelVersion.Location = new System.Drawing.Point(151, 26);
            this.labelVersion.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.labelVersion.MaximumSize = new System.Drawing.Size(0, 17);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(288, 17);
            this.labelVersion.TabIndex = 0;
            this.labelVersion.Text = "Version";
            this.labelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelCopyright
            // 
            this.labelCopyright.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCopyright.Location = new System.Drawing.Point(151, 52);
            this.labelCopyright.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.labelCopyright.MaximumSize = new System.Drawing.Size(0, 17);
            this.labelCopyright.Name = "labelCopyright";
            this.labelCopyright.Size = new System.Drawing.Size(288, 17);
            this.labelCopyright.TabIndex = 21;
            this.labelCopyright.Text = "Copyright";
            this.labelCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelCompanyName
            // 
            this.labelCompanyName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelCompanyName.Location = new System.Drawing.Point(151, 78);
            this.labelCompanyName.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.labelCompanyName.MaximumSize = new System.Drawing.Size(0, 17);
            this.labelCompanyName.Name = "labelCompanyName";
            this.labelCompanyName.Size = new System.Drawing.Size(288, 17);
            this.labelCompanyName.TabIndex = 22;
            this.labelCompanyName.Text = "Company Name";
            this.labelCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // textBoxDescription
            // 
            this.textBoxDescription.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxDescription.Location = new System.Drawing.Point(151, 107);
            this.textBoxDescription.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.textBoxDescription.Multiline = true;
            this.textBoxDescription.Name = "textBoxDescription";
            this.textBoxDescription.ReadOnly = true;
            this.textBoxDescription.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxDescription.Size = new System.Drawing.Size(288, 126);
            this.textBoxDescription.TabIndex = 23;
            this.textBoxDescription.TabStop = false;
            this.textBoxDescription.Text = "Description";
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.okButton.Location = new System.Drawing.Point(364, 239);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 24;
            this.okButton.Text = "&OK";
            // 
            // tabCtrl
            // 
            this.tabCtrl.Controls.Add(this.pubTabPg);
            this.tabCtrl.Controls.Add(this.FastColoredTextBoxTabPg);
            this.tabCtrl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabCtrl.Location = new System.Drawing.Point(9, 9);
            this.tabCtrl.Name = "tabCtrl";
            this.tabCtrl.SelectedIndex = 0;
            this.tabCtrl.Size = new System.Drawing.Size(456, 297);
            this.tabCtrl.TabIndex = 25;
            // 
            // pubTabPg
            // 
            this.pubTabPg.Controls.Add(this.tableLayoutPanel);
            this.pubTabPg.Location = new System.Drawing.Point(4, 22);
            this.pubTabPg.Name = "pubTabPg";
            this.pubTabPg.Padding = new System.Windows.Forms.Padding(3);
            this.pubTabPg.Size = new System.Drawing.Size(448, 271);
            this.pubTabPg.TabIndex = 0;
            this.pubTabPg.Text = "NSIS Publisher";
            this.pubTabPg.UseVisualStyleBackColor = true;
            // 
            // FastColoredTextBoxTabPg
            // 
            this.FastColoredTextBoxTabPg.Controls.Add(this.fctbGitHubLinkLbl);
            this.FastColoredTextBoxTabPg.Controls.Add(this.codeProjLinkLbl);
            this.FastColoredTextBoxTabPg.Controls.Add(this.fctbLbl);
            this.FastColoredTextBoxTabPg.Controls.Add(this.fctbTxtBox);
            this.FastColoredTextBoxTabPg.Location = new System.Drawing.Point(4, 22);
            this.FastColoredTextBoxTabPg.Name = "FastColoredTextBoxTabPg";
            this.FastColoredTextBoxTabPg.Padding = new System.Windows.Forms.Padding(3);
            this.FastColoredTextBoxTabPg.Size = new System.Drawing.Size(448, 271);
            this.FastColoredTextBoxTabPg.TabIndex = 1;
            this.FastColoredTextBoxTabPg.Text = "FastColoredTextBox";
            this.FastColoredTextBoxTabPg.UseVisualStyleBackColor = true;
            // 
            // fctbTxtBox
            // 
            this.fctbTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.fctbTxtBox.Location = new System.Drawing.Point(9, 27);
            this.fctbTxtBox.Multiline = true;
            this.fctbTxtBox.Name = "fctbTxtBox";
            this.fctbTxtBox.Size = new System.Drawing.Size(431, 190);
            this.fctbTxtBox.TabIndex = 0;
            this.fctbTxtBox.Text = resources.GetString("fctbTxtBox.Text");
            // 
            // fctbLbl
            // 
            this.fctbLbl.AutoSize = true;
            this.fctbLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fctbLbl.Location = new System.Drawing.Point(0, 0);
            this.fctbLbl.Name = "fctbLbl";
            this.fctbLbl.Size = new System.Drawing.Size(416, 24);
            this.fctbLbl.TabIndex = 1;
            this.fctbLbl.Text = "FastColoredTextBox for Syntax Highlighting";
            // 
            // codeProjLinkLbl
            // 
            this.codeProjLinkLbl.AutoSize = true;
            this.codeProjLinkLbl.Location = new System.Drawing.Point(6, 224);
            this.codeProjLinkLbl.Name = "codeProjLinkLbl";
            this.codeProjLinkLbl.Size = new System.Drawing.Size(434, 13);
            this.codeProjLinkLbl.TabIndex = 2;
            this.codeProjLinkLbl.TabStop = true;
            this.codeProjLinkLbl.Text = "http://www.codeproject.com/Articles/161871/Fast-Colored-TextBox-for-syntax-highli" +
    "ghting";
            this.codeProjLinkLbl.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.codeProjLinkLbl_LinkClicked);
            // 
            // fctbGitHubLinkLbl
            // 
            this.fctbGitHubLinkLbl.AutoSize = true;
            this.fctbGitHubLinkLbl.Location = new System.Drawing.Point(6, 244);
            this.fctbGitHubLinkLbl.Name = "fctbGitHubLinkLbl";
            this.fctbGitHubLinkLbl.Size = new System.Drawing.Size(278, 13);
            this.fctbGitHubLinkLbl.TabIndex = 3;
            this.fctbGitHubLinkLbl.TabStop = true;
            this.fctbGitHubLinkLbl.Text = "https://github.com/PavelTorgashov/FastColoredTextBox";
            this.fctbGitHubLinkLbl.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.fctbGitHubLinkLbl_LinkClicked);
            // 
            // AboutBox
            // 
            this.AcceptButton = this.okButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(474, 315);
            this.Controls.Add(this.tabCtrl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutBox";
            this.Padding = new System.Windows.Forms.Padding(9);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "About NSIS Publisher";
            this.tableLayoutPanel.ResumeLayout(false);
            this.tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).EndInit();
            this.tabCtrl.ResumeLayout(false);
            this.pubTabPg.ResumeLayout(false);
            this.FastColoredTextBoxTabPg.ResumeLayout(false);
            this.FastColoredTextBoxTabPg.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.PictureBox logoPictureBox;
        private System.Windows.Forms.Label labelProductName;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Label labelCopyright;
        private System.Windows.Forms.Label labelCompanyName;
        private System.Windows.Forms.TextBox textBoxDescription;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.TabControl tabCtrl;
        private System.Windows.Forms.TabPage pubTabPg;
        private System.Windows.Forms.TabPage FastColoredTextBoxTabPg;
        private System.Windows.Forms.LinkLabel fctbGitHubLinkLbl;
        private System.Windows.Forms.LinkLabel codeProjLinkLbl;
        private System.Windows.Forms.Label fctbLbl;
        private System.Windows.Forms.TextBox fctbTxtBox;
    }
}
