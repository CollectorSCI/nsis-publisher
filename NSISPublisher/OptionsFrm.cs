﻿/// To do list:
/// 
/// Add DOSBox language to DOSBox Folder and add to installers
///     Check if languageFile exists on OK/Apply
///     languageFile overwrite check
/// 

#region using directives ====================================================================================

using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Reflection;

#endregion using directives


namespace NSISPublisher
{
    /// <summary>
    /// Form to set preferences and program options
    /// </summary>

    public partial class OptionsFrm : Form
    {

        #region Declares, Imports. etc. =====================================================================

        public static string nsisPubEXE = new System.Uri(Assembly.GetExecutingAssembly().CodeBase).AbsolutePath.Replace("%20", " ");
        public static string nsisPubPath = Path.GetDirectoryName(nsisPubEXE);
        public static string scriptOverwrite = Properties.Settings.Default.scriptOverwrite;

        public int mruMaxEntries;

        private bool isValid = false;
        private bool nullCheckCancel = false;

        MainFrm mainFrm = (MainFrm)Application.OpenForms["MainFrm"];

        #endregion Declares, Imports. etc.


        #region Constructor =================================================================================

        public OptionsFrm()
        {
            InitializeComponent();
        }

        #endregion Constructor


        #region Form Events =================================================================================

        /// <summary>
        /// Retrieves saved application and current projects settings to populate fields
        /// </summary>
        private void Form_Load(object sender, EventArgs e)
        {
            #region Get Screen Metrics =======================================

            if (!Properties.Settings.Default.optionsFrmLocation.IsEmpty)
                this.Location = Properties.Settings.Default.optionsFrmLocation;
            else
                this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;

            #endregion Get Screen Metrics

            #region Load TextBoxes ===========================================

            // Publisher
            defaultPubNameTxtBox.Text = Properties.Settings.Default.defaultPublisher;

            // Projects Folder
            defaultProjPathTxtBox.Text = Properties.Settings.Default.defaultProjPath;
            if (!Directory.Exists(Properties.Settings.Default.defaultProjPath))
                defaultProjPathTxtBox.Text = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            // Default Website
            pubURLTxtBox.Text = Properties.Settings.Default.defaultSite;

            // Publish Out Path
            pubOutChkBox.Checked = Properties.Settings.Default.outToProjPath;
            if (pubOutChkBox.Checked == true)
            {
                pubOutPathTxtBox.Enabled = false;
                browsePubOutPathBtn.Enabled = false;
                pubOutPathTxtBox.Text = null;
                //Props.projPath = 
            }
            if (pubOutChkBox.Checked == false)
            {
                pubOutPathTxtBox.Enabled = true;
                browsePubOutPathBtn.Enabled = true;
                pubOutPathTxtBox.Text = Properties.Settings.Default.pubOutPath;
                if (!Directory.Exists(Properties.Settings.Default.pubOutPath))
                    pubOutPathTxtBox.Text = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\NSIS Publisher\PublishOutPath";
            }

            #endregion Load TextBoxes

            #region Set Checkbox States ======================================

            #region Application Settings

            // Set scrOverwrite RadioButtons states
            if (Properties.Settings.Default.scriptOverwrite == "overWrite")
                overWriteRadioBtn.Checked = true;
            if (Properties.Settings.Default.scriptOverwrite == "noOverWrite")
                noOverwriteRadioBtn.Checked = true;
            if (Properties.Settings.Default.scriptOverwrite == "ask")
                askRadioBtn.Checked = true;

            // Set missngSGReadmeChkBox state
            missngSGReadmeChkBox.Checked = Properties.Settings.Default.missingSVGReadMeWarning;

            // Set noIncludeWarning state
            noIncludeWarning.Checked = Properties.Settings.Default.missingIncludeWarning;

            // Set pubOutChkBox state
            pubOutChkBox.Checked = Properties.Settings.Default.outToProjPath;

            #endregion Application Settings

            #region NSIS Options

            // Set deleteNSISSrptCheckBox state
            deleteNSISSrptChkBox.Checked = Properties.Settings.Default.deleteNSISSrpt;

            // SetRunNSISCheckBox state
            runNSISChkBox.Checked = Properties.Settings.Default.runNSIS;

            // Set unicodeChkBox state
            unicodeChkBox.Checked = Properties.Settings.Default.unicodeInst;

            // Set loggingChkBox state
            loggingChkBox.Checked = Properties.Settings.Default.loggingInst;

            #endregion NSIS Options

            #region DOSBox Options

            // Set aspectChkBox state
            aspectChkBox.Checked = Properties.Settings.Default.aspectDefault;

            // Set useLangFileChkBox state
            useLangFileChkBox.Checked = Properties.Settings.Default.langFile;

            #endregion DOSBox Options

            #region Editor Options

            // Set seInlineSpellCheckerChkBox state
            seInlineSpellCheckerChkBox.Checked = Properties.Settings.Default.spellCheck;

            // saveScriptChkBox state
            saveScriptChkBox.Checked = Properties.Settings.Default.saveScript;

            // Set includeSRCChkBox state
            includeSRCChkBox.Checked = Properties.Settings.Default.includeSRCDefault;

            // Set openInstFolderChkBox state
            openInstFolderChkBox.Checked = Properties.Settings.Default.openFolder;

            #endregion Editor Options

            #endregion Set Checkbox States

            licenseCmboBox.Text = Properties.Settings.Default.defaultLicense;
        }


        /// <summary>
        /// Saves settings and prompts if closing without saving changes
        /// </summary>
        private void Form_Closing(object sender, FormClosingEventArgs e)
        {
            // Save screen metrics
            Properties.Settings.Default.optionsFrmLocation = this.Location;

            Properties.Settings.Default.Save();
        }

        #endregion Form Events


        #region Info Links ==================================================================================

        /// <summary>
        /// Opens help file to Options settings
        /// </summary>
        private void helpLinkLbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (File.Exists("Documents\\NSIS Publisher Help.chm"))
                Help.ShowHelp(this, helpProvider.HelpNamespace, "OptionsDialog.htm");
            else
                MessageBox.Show("NSIS Publisher Help File not found.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }


        /// <summary>
        /// 
        /// </summary>
        private void nsisDocsLinkLbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            NSIS.GetNSIS();
            string nsisPath = Properties.Settings.Default.nsisPath;
            if (File.Exists(nsisPath + "\\NSIS.chm"))
                Process.Start(nsisPath + "\\NSIS.chm");
            else
                MessageBox.Show("NSIS Help File not found. Is NSIS installed?", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        #endregion Info Links


        #region General Options =============================================================================

        /// <summary>
        /// Set scriptOverwrite to overWrite
        /// </summary>
        private void overWriteRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            scriptOverwrite = "overWrite";
        }


        /// <summary>
        /// Set scriptOverwrite to noOverWrite
        /// </summary>
        private void noOverwriteRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            scriptOverwrite = "noOverWrite";
        }


        /// <summary>
        /// Set scriptOverwrite to ask
        /// </summary>
        private void askRadioBtn_CheckedChanged(object sender, EventArgs e)
        {
            scriptOverwrite = "ask";
        }


        /// <summary>
        /// 
        /// </summary>
        private void regDevBtn_Click(object sender, EventArgs e)
        {
            AddReg();
        }


        /// <summary>
        /// Sets which license to use
        /// </summary>
        private void licenseCmboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.defaultLicense = licenseCmboBox.Text;
        }


        /// <summary>
        /// Opens default browser to License help page
        /// </summary>
        private void chooseLicenseHelpLnk_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://choosealicense.com/");
        }


        /// <summary>
        /// Opens custom license for editing
        /// </summary>
        private void editLicenseBtn_Click(object sender, EventArgs e)
        {
            string customLicense = nsisPubPath + @"\Include\LICENSE.TXT";

            if (File.Exists(customLicense))
                Process.Start(customLicense);
        }


        /// <summary>
        /// Opens default Readme for editing
        /// </summary>
        private void editReadmeBtn_Click(object sender, EventArgs e)
        {
            string defaultReadme = nsisPubPath + @"\Include\README.TXT";

            if (File.Exists(defaultReadme))
                Process.Start(defaultReadme);
        }

        #endregion General Options


        #region All Projects Options ========================================================================

        /// <summary>
        /// Sets default developer/publisher for your projects
        /// </summary>
        private void defaultPubNameTxtBox_TextChanged(object sender, EventArgs e)
        {
            Properties.Settings.Default.defaultPublisher = defaultPubNameTxtBox.Text;
        }


        /// <summary>
        /// Sets Properties.Settings.Default.defaultSite
        /// </summary>
        private void pubURLTxtBox_TextChanged(object sender, EventArgs e)
        {
            //Properties.Settings.Default.defaultSite = projURLTxtBox.Text;
        }


        /// <summary>
        /// Checks URL format on press of Enter
        /// </summary>
        private void projURLTxtBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == (Keys.Enter))
                CheckURL();
        }


        /// <summary>
        /// Checks URL format on loss of focus
        /// </summary>
        private void projURLTxtBox_Leave(object sender, EventArgs e)
        {
            CheckURL();
        }

        #endregion All Projects Options


        #region Paths =======================================================================================

        /// <summary>
        /// Manages the TextChanged event for the Set Default All Projects Location Textbox
        /// </summary>
        private void defaultProjPathTxtBox_TextChanged(object sender, EventArgs e)
        {
            //Properties.Settings.Default.defaultProjPath = defaultProjPathTxtBox.Text;
        }


        /// <summary>
        /// Opens folder browse dialog to select default location for All Projects Folder
        /// </summary>
        private void browseDefaultProjPathBtn_Click(object sender, EventArgs e)
        {
            // Set Projects Folder
            FolderBrowserDialog progFolderBrowserDlg = new FolderBrowserDialog();
            progFolderBrowserDlg.RootFolder = Environment.SpecialFolder.Desktop;
            if (Directory.Exists(Props.projPath))
                progFolderBrowserDlg.SelectedPath = Props.projPath;
            else
                progFolderBrowserDlg.SelectedPath = (System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\NSIS Publisher\Projects");
            progFolderBrowserDlg.Description = "Select Projects Folder";

            // Return if Cancel is pressed
            if (progFolderBrowserDlg.ShowDialog() == DialogResult.Cancel)
            {
                mainFrm.statusbarMainMessage.Text = "Browse canceled";
                return;
            }

            //progFolderBrowserDlg.ShowDialog();
            Properties.Settings.Default.defaultProjPath = (progFolderBrowserDlg.SelectedPath);
            defaultProjPathTxtBox.Text = Properties.Settings.Default.defaultProjPath;
        }


        /// <summary>
        /// Sets Publish Out Path or the default location for installer to be compiled
        /// </summary>
        private void pubOutPathTextBox_TextChanged(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Opens folder browse dialog to select Publish out Path
        /// </summary>
        private void browsePubOutPathBtn_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog publishFolderBrowserDlg = new FolderBrowserDialog();
            publishFolderBrowserDlg.Description = "Select Publish Folder";
            publishFolderBrowserDlg.RootFolder = Environment.SpecialFolder.Desktop;

            if (Directory.Exists(Properties.Settings.Default.pubOutPath))
                publishFolderBrowserDlg.SelectedPath = Properties.Settings.Default.pubOutPath;
            else
                publishFolderBrowserDlg.SelectedPath = (System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\NSIS Publisher\PublishOutPath");

            if (publishFolderBrowserDlg.ShowDialog() == DialogResult.Cancel)
            {
                // Return if Cancel is pressed
                mainFrm.statusbarMainMessage.Text = "Browse canceled";
                return;
            }

            //publishFolderBrowserDlg.ShowDialog();
            Properties.Settings.Default.pubOutPath = (publishFolderBrowserDlg.SelectedPath);
            pubOutPathTxtBox.Text = Properties.Settings.Default.pubOutPath;
        }


        /// <summary>
        /// 
        /// </summary>
        private void pubOutChkBox_CheckedChanged(object sender, EventArgs e)
        {
            if (pubOutChkBox.Checked == true)
            {
                pubOutPathTxtBox.Enabled = false;
                browsePubOutPathBtn.Enabled = false;
                pubOutPathTxtBox.Text = null;
                //Props.projPath = 
            }
            if (pubOutChkBox.Checked == false)
            {
                pubOutPathTxtBox.Enabled = true;
                browsePubOutPathBtn.Enabled = true;
                pubOutPathTxtBox.Text = Properties.Settings.Default.pubOutPath;
                if (!Directory.Exists(Properties.Settings.Default.pubOutPath))
                    pubOutPathTxtBox.Text = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\NSIS Publisher\PublishOutPath";
            }
        }

        #endregion Paths


        #region Script Editor Options =======================================================================

        /// <summary>
        /// Sets Inline Spell Checker Status
        /// </summary>
        private void seInlineSpellCheckerChkBox_CheckedChanged(object sender, EventArgs e)
        {

        }

        #endregion Script Editor Options


        #region NSIS Options ================================================================================

        /// <summary>
        /// Deletes NSIS script after compile if checked
        /// </summary>
        private void DeleteNSISSrptCheckBox_Click(object sender, EventArgs e)
        {
            if (deleteNSISSrptChkBox.Checked == true)
            {
                // run must be enabled
                runNSISChkBox.Enabled = false;
                runNSISChkBox.Checked = true;
            }
            if (deleteNSISSrptChkBox.Checked == false)
                runNSISChkBox.Enabled = true;
        }


        /// <summary>
        /// Deletes NSIS script after compile if checked
        /// </summary>
        private void DeleteNSISSrptCheckBox_CheckedChanged(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Run NSIS Script After Publish CheckBox Clicked Event
        /// </summary>
        private void RunNSISCheckBox_Click(object sender, EventArgs e)
        {
            if (runNSISChkBox.Checked == true)
            {
                // run must be enabled
                deleteNSISSrptChkBox.Enabled = true;
                //deleteNSISSrptChkBox.Checked = true;
            }
            if (runNSISChkBox.Checked == false)
            {
                deleteNSISSrptChkBox.Enabled = false;
                deleteNSISSrptChkBox.Checked = false;
            }
        }


        /// <summary>
        /// Run NSIS Script After Publish CheckBox CheckedChanged Event
        /// </summary>
        private void RunNSISCheckBox_CheckedChanged(object sender, EventArgs e)
        {

        }


        /// <summary>
        /// Warns user must have NSIS 3 or greater on selection
        /// </summary>
        private void unicodeChkBox_CheckedChanged(object sender, EventArgs e)
        {
            //if (unicodeChkBox.Checked == true)
            //    MessageBox.Show("To create a Unicode installer you must have at least NSIS 3 or greater installed", "Information", 
            //        MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        /// <summary>
        /// Warns user must have a logging build of NSIS on selection
        /// </summary>
        private void loggingChkBox_CheckedChanged(object sender, EventArgs e)
        {
            //if (loggingChkBox.Checked == true)
            //    MessageBox.Show("To create a logging installer you must use a special logging build of NSIS", "Information", 
            //        MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        /// <summary>
        /// Opens default browser to NSIS download page
        /// </summary>
        private void getNSISLink_LnkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://nsis.sourceforge.net/Download");
        }


        /// <summary>
        /// Opens default browser to Logging Build of NSIS download page
        /// </summary>
        private void getLogNSISLnk_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://nsis.sourceforge.net/Special_Builds");
        }

        #endregion NSIS Options


        #region DOSBox Options ==============================================================================

        /// <summary>
        /// Sets Aspect correction default
        /// </summary>
        private void aspectChkBox_CheckedChanged(object sender, EventArgs e)
        {
            //if (aspectChkBox.Checked == true)
            //    Properties.Settings.Default.aspectDefault = true;
            //if (aspectChkBox.Checked == false)
            //    Properties.Settings.Default.aspectDefault = false;
        }


        /// <summary>
        /// Go to DOSBox Download Page
        /// </summary>
        private void getLanguageFileLnk_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("http://www.dosbox.com/download.php?main=1");
        }

        #endregion DOSBox Options


        #region Button Events ===============================================================================

        /// <summary>
        /// Apply all settings and closes form
        /// </summary>
        private void BtnOptionsOK_Click(object sender, EventArgs e)
        {
            Apply();

            // Close only if no null fields
            if (nullCheckCancel == false)
            {
                nullCheckCancel = false;
                this.Close();
            }
            else
                nullCheckCancel = false;
        }


        /// <summary>
        /// Closes form without applying changes
        /// </summary>
        private void BtnOptionsCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        /// <summary>
        /// Apply all settings and closes form
        /// </summary>
        private void BtnOptionsApply_Click(object sender, EventArgs e)
        {
            nullCheckCancel = false;
            Apply();
        }

        #endregion Button Events


        #region Extra Functions =============================================================================

        /// <summary>
        /// Applies all selected options
        /// </summary>
        private void Apply()
        {
            #region Null Checks ================================================

            Properties.Settings.Default.defaultPublisher = defaultPubNameTxtBox.Text;

            // Publisher 
            if (String.IsNullOrEmpty(defaultPubNameTxtBox.Text))
            {
                MessageBox.Show("No Developer has been specified. Please enter name.", "No Developer", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                nullCheckCancel = true;
                return;
            }
            else
                Properties.Settings.Default.defaultPublisher = defaultPubNameTxtBox.Text;

            // Default Projects Folder 
            if (String.IsNullOrEmpty(defaultProjPathTxtBox.Text)) //if (!Directory.Exists(defaultProjPathTxtBox.Text))
            {
                MessageBox.Show("No Default Projects Folder has been specified. Please enter path.", "No Default Projects Folder", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                nullCheckCancel = true;
                return;
            }
            else
            {
                if (Directory.Exists(Properties.Settings.Default.defaultProjPath))
                {
                    Properties.Settings.Default.defaultProjPath = defaultProjPathTxtBox.Text;
                }
                else
                {
                    try
                    {
                        Directory.CreateDirectory(Properties.Settings.Default.defaultProjPath);
                        Properties.Settings.Default.defaultProjPath = defaultProjPathTxtBox.Text;
                    }
                    catch (Exception ex)
                    {
                        nullCheckCancel = true;
                        Properties.Settings.Default.defaultProjPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                        mainFrm.statusbarMainMessage.Text = ("Error: " + ex.Message);
                        string err = ("Error: " + ex.Message);
                        MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
                    }
                }
            }

            // Default Website
            if (String.IsNullOrEmpty(pubURLTxtBox.Text))
            {
                MessageBox.Show("No Default Developer Website has been specified. Please enter URL.", "No Default Website", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                nullCheckCancel = true;
                return;
            }
            else
            {
                CheckURL();
                if (isValid == true)
                    Properties.Settings.Default.defaultSite = pubURLTxtBox.Text;
                else
                {
                    nullCheckCancel = true;
                    return;
                }
            }

            // Publish Out Path 
            if (pubOutChkBox.Checked == false)
                Props.projOutPath = Props.projPath;
            if (pubOutChkBox.Checked == false)
            {
                if (String.IsNullOrEmpty(pubOutPathTxtBox.Text)) //if (!Directory.Exists(pubOutPathTxtBox.Text))
                {
                    MessageBox.Show("No Publish Out Folder has been specified. Please enter path.", "No Projects Folder", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    nullCheckCancel = true;
                    return;
                }
                else
                {
                    if (Directory.Exists(Properties.Settings.Default.pubOutPath))
                    {
                        Properties.Settings.Default.pubOutPath = pubOutPathTxtBox.Text;
                    }
                    else
                    {
                        try
                        {
                            Directory.CreateDirectory(Properties.Settings.Default.pubOutPath);
                            Properties.Settings.Default.pubOutPath = pubOutPathTxtBox.Text;
                        }
                        catch (Exception ex)
                        {
                            nullCheckCancel = true;
                            Properties.Settings.Default.pubOutPath = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                            mainFrm.statusbarMainMessage.Text = ("Error: " + ex.Message);
                            string err = ("Error: " + ex.Message);
                            MessageBox.Show(err, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            ErrorLogging.WriteLog("Error: " + DateTime.Now.ToString("MM/dd/yyyy, h:mm:ss tt") + " " + err);
                        }
                    }
                }
                Props.projOutPath = Properties.Settings.Default.pubOutPath;
            }

            #endregion Null Checks

            #region Set CheckBox values ========================================

            #region Application Settings

            // Script Overwrite Warning
            Properties.Settings.Default.scriptOverwrite = scriptOverwrite; // scrOverwriteChkBox.Checked;

            // Missng Save Games Readme Warning
            Properties.Settings.Default.missingSVGReadMeWarning = missngSGReadmeChkBox.Checked;

            // noIncludeWarning
            Properties.Settings.Default.missingIncludeWarning = noIncludeWarning.Checked;

            // Out Path
            Properties.Settings.Default.outToProjPath = pubOutChkBox.Checked;

            #endregion Application Settings

            #region NSIS Options

            // Delete NSIS Script
            Properties.Settings.Default.deleteNSISSrpt = deleteNSISSrptChkBox.Checked;

            // Run NSIS
            Properties.Settings.Default.runNSIS = runNSISChkBox.Checked;

            // Compile Unicode
            Properties.Settings.Default.unicodeInst = Props.unicodeInst = unicodeChkBox.Checked;

            // Logging Installer
            Properties.Settings.Default.loggingInst = Props.loggingInst = loggingChkBox.Checked;

            #endregion NSIS Options

            #region DOSBox Options

            // Aspect Correction
            Properties.Settings.Default.aspectDefault = Props.projAspect = aspectChkBox.Checked;

            // Language File
            Properties.Settings.Default.langFile = useLangFileChkBox.Checked;

            #endregion DOSBox Options

            #region Script Editor Options

            // Inline Spell Checker
            Properties.Settings.Default.spellCheck = seInlineSpellCheckerChkBox.Checked;

            // Save Script Before Compile
            Properties.Settings.Default.saveScript = saveScriptChkBox.Checked;

            // Set Default for Include SRC
            Properties.Settings.Default.includeSRCDefault = Props.includeSRC = includeSRCChkBox.Checked;

            // Open Folder After Compile
            Properties.Settings.Default.openFolder = openInstFolderChkBox.Checked;

            #endregion Script Editor Options

            #endregion Set CheckBox values

            Properties.Settings.Default.defaultLicense = licenseCmboBox.Text;

            Properties.Settings.Default.Save();
        }


        /// <summary>
        /// 
        /// </summary>
        private void AddReg()
        {
            string value = "\"" + nsisPubEXE.Replace("/", "\\") + "\" \"%1\"";

            Registry.ClassesRoot.CreateSubKey("NSIS.Header\\shell\\edit\\command");
            Registry.SetValue("HKEY_CLASSES_ROOT\\NSIS.Header\\shell\\edit\\command", "", value);

            Registry.ClassesRoot.CreateSubKey("NSIS.Header\\shell\\open\\command");
            Registry.SetValue("HKEY_CLASSES_ROOT\\NSIS.Header\\shell\\open\\command", "", value);

            Registry.ClassesRoot.CreateSubKey("NSIS.Script\\shell\\edit\\command");
            Registry.SetValue("HKEY_CLASSES_ROOT\\NSIS.Script\\shell\\edit\\command", "", value);

            Registry.ClassesRoot.CreateSubKey("NSIS.Script\\shell\\open\\command");
            Registry.SetValue("HKEY_CLASSES_ROOT\\NSIS.Script\\shell\\open\\command", "", value);
        }


        /// <summary>
        /// Checks for valid URL
        /// </summary>
        private void CheckURL()
        {
            // Allow null address
            if (String.IsNullOrEmpty(pubURLTxtBox.Text)) return;

            // Check protocol
            if (pubURLTxtBox.Text.StartsWith("http://") || pubURLTxtBox.Text.StartsWith("https://"))
            {
                Properties.Settings.Default.defaultSite = pubURLTxtBox.Text;
            }
            else
            {
                // Remove invalid protocol
                string[] address = pubURLTxtBox.Text.Split(new string[] { "//" }, StringSplitOptions.None);
                var last = address.Last();

                // Add protocol
                Properties.Settings.Default.defaultSite = "http://" + last;
                pubURLTxtBox.Text = Properties.Settings.Default.defaultSite;
            }

            // Warn if top level domain might be missing
            if (!pubURLTxtBox.Text.Contains("."))
            {
                isValid = false;
                MessageBox.Show("Please check if website address is valid. Is the top level domain missing?", "Check Address",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
                isValid = true;
        }

        #endregion Extra Functions
    }
}
