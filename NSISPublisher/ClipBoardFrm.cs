﻿/// To do list:
/// 
/// 

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace NSISPublisher
{
    /// <summary>
    /// A utility to repeatedly copy text to the Windows Clipboard
    /// </summary>

    public partial class ClipBoardFrm : Form
    {

        public ClipBoardFrm()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Copies clipBox.Text to the Windows Clipboard
        /// </summary>
        private void CopyBtn_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(clipBox.Text))
                Clipboard.SetText(clipBox.Text);
        }
    }
}
